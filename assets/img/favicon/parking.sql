-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 09, 2018 at 02:01 PM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `parking`
--

-- --------------------------------------------------------

--
-- Table structure for table `availablity`
--

CREATE TABLE `availablity` (
  `id` int(5) UNSIGNED NOT NULL,
  `parkingspot_id` int(6) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `isAvailable` varchar(1) NOT NULL DEFAULT '1',
  `isAlwaysOpen` varchar(1) NOT NULL DEFAULT '1',
  `open_time` time NOT NULL DEFAULT '09:00:00',
  `close_time` time NOT NULL DEFAULT '05:00:00',
  `date_for` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `availablity`
--

INSERT INTO `availablity` (`id`, `parkingspot_id`, `date`, `isAvailable`, `isAlwaysOpen`, `open_time`, `close_time`, `date_for`) VALUES
(17, 17, '0000-00-00', '1', '1', '00:00:00', '00:00:00', '2018-07-01 08:15:23'),
(18, 18, '0000-00-00', '1', '1', '00:00:00', '00:00:00', '2018-07-01 08:21:24'),
(19, 19, '0000-00-00', '1', '1', '00:00:00', '00:00:00', '2018-07-01 08:21:52'),
(20, 20, '0000-00-00', '1', '1', '00:00:00', '00:00:00', '2018-07-01 08:23:18'),
(21, 21, '0000-00-00', '1', '1', '00:00:00', '00:00:00', '2018-07-01 08:23:49'),
(22, 22, '0000-00-00', '1', '1', '00:00:00', '00:00:00', '2018-07-01 08:24:32'),
(23, 23, '0000-00-00', '1', '1', '00:00:00', '00:00:00', '2018-07-01 08:24:56'),
(24, 24, '0000-00-00', '1', '1', '00:00:00', '00:00:00', '2018-07-01 08:26:09'),
(25, 25, '0000-00-00', '1', '1', '00:00:00', '00:00:00', '2018-07-01 08:26:50'),
(26, 26, '0000-00-00', '1', '1', '00:00:00', '00:00:00', '2018-07-01 08:27:20'),
(27, 27, '0000-00-00', '1', '1', '00:00:00', '00:00:00', '2018-07-01 08:27:50'),
(28, 28, '0000-00-00', '1', '1', '00:00:00', '00:00:00', '2018-07-01 08:28:15'),
(29, 29, '0000-00-00', '1', '1', '00:00:00', '00:00:00', '2018-07-01 08:28:51'),
(30, 30, '0000-00-00', '1', '1', '00:00:00', '00:00:00', '2018-07-01 08:29:37'),
(31, 31, '0000-00-00', '1', '1', '00:00:00', '00:00:00', '2018-07-01 08:30:12'),
(32, 32, '0000-00-00', '1', '1', '00:00:00', '00:00:00', '2018-07-01 08:30:49'),
(33, 33, '0000-00-00', '1', '1', '00:00:00', '00:00:00', '2018-07-01 08:31:39'),
(34, 34, '0000-00-00', '1', '1', '00:00:00', '00:00:00', '2018-07-01 08:32:08'),
(35, 35, '0000-00-00', '1', '1', '00:00:00', '00:00:00', '2018-07-01 08:32:41'),
(36, 36, '0000-00-00', '1', '1', '00:00:00', '00:00:00', '2018-07-01 08:33:14'),
(37, 37, '0000-00-00', '1', '1', '00:00:00', '00:00:00', '2018-07-01 08:34:03'),
(38, 38, '0000-00-00', '1', '1', '00:00:00', '00:00:00', '2018-07-01 08:34:48'),
(39, 39, '0000-00-00', '1', '1', '00:00:00', '00:00:00', '2018-07-01 08:35:23');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(5) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `postal_code` varchar(100) NOT NULL,
  `state` varchar(100) NOT NULL,
  `lat` double NOT NULL,
  `long` double NOT NULL,
  `timezone` varchar(100) NOT NULL,
  `country_id` int(6) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `name`, `postal_code`, `state`, `lat`, `long`, `timezone`, `country_id`) VALUES
(1, 'Boston', '', 'MA', 0, 0, 'America/New_York', 1),
(3, 'Florida', '', 'Florida', 0, 0, 'America/New_York', 1),
(4, 'Miami', '', 'Miami', 0, 0, 'America/New_York', 1);

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `id` int(5) UNSIGNED NOT NULL,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `email` varchar(100) NOT NULL,
  `pass` varchar(100) NOT NULL,
  `img` varchar(100) NOT NULL,
  `status` varchar(1) NOT NULL DEFAULT '1',
  `type` varchar(20) NOT NULL DEFAULT 'guest',
  `uppdated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`id`, `fname`, `lname`, `mobile`, `email`, `pass`, `img`, `status`, `type`, `uppdated_at`) VALUES
(1, 'anup', 'sarkar', '01676667712', 'anup@gmail.com', '0e7517141fb53f21ee439b355b5a1d0a', '', '1', 'registered', '2018-06-09 12:25:46');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(5) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `country_code` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `name`, `country_code`) VALUES
(1, 'USA', '1');

-- --------------------------------------------------------

--
-- Table structure for table `default_flat_rate`
--

CREATE TABLE `default_flat_rate` (
  `id` int(5) UNSIGNED NOT NULL,
  `parkingspot_id` int(6) UNSIGNED NOT NULL,
  `rate` int(11) NOT NULL DEFAULT '20',
  `isAlwaysOpen` varchar(1) NOT NULL DEFAULT '1',
  `open_time` time NOT NULL,
  `close_time` time NOT NULL,
  `status` varchar(1) NOT NULL DEFAULT '1',
  `date_for` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `default_flat_rate`
--

INSERT INTO `default_flat_rate` (`id`, `parkingspot_id`, `rate`, `isAlwaysOpen`, `open_time`, `close_time`, `status`, `date_for`) VALUES
(17, 17, 20, '1', '00:00:00', '00:00:00', '1', '2018-07-01 08:15:23'),
(18, 18, 20, '1', '00:00:00', '00:00:00', '1', '2018-07-01 08:21:24'),
(19, 19, 20, '1', '00:00:00', '00:00:00', '1', '2018-07-01 08:21:52'),
(20, 20, 20, '1', '00:00:00', '00:00:00', '1', '2018-07-01 08:23:18'),
(21, 21, 20, '1', '00:00:00', '00:00:00', '1', '2018-07-01 08:23:49'),
(22, 22, 20, '1', '00:00:00', '00:00:00', '1', '2018-07-01 08:24:32'),
(23, 23, 20, '1', '00:00:00', '00:00:00', '1', '2018-07-01 08:24:56'),
(24, 24, 20, '1', '00:00:00', '00:00:00', '1', '2018-07-01 08:26:09'),
(25, 25, 20, '1', '00:00:00', '00:00:00', '1', '2018-07-01 08:26:50'),
(26, 26, 20, '1', '00:00:00', '00:00:00', '1', '2018-07-01 08:27:20'),
(27, 27, 20, '1', '00:00:00', '00:00:00', '1', '2018-07-01 08:27:50'),
(28, 28, 20, '1', '00:00:00', '00:00:00', '1', '2018-07-01 08:28:15'),
(29, 29, 20, '1', '00:00:00', '00:00:00', '1', '2018-07-01 08:28:51'),
(30, 30, 20, '1', '00:00:00', '00:00:00', '1', '2018-07-01 08:29:37'),
(31, 31, 20, '1', '00:00:00', '00:00:00', '1', '2018-07-01 08:30:12'),
(32, 32, 20, '1', '00:00:00', '00:00:00', '1', '2018-07-01 08:30:49'),
(33, 33, 20, '1', '00:00:00', '00:00:00', '1', '2018-07-01 08:31:39'),
(34, 34, 20, '1', '00:00:00', '00:00:00', '1', '2018-07-01 08:32:08'),
(35, 35, 20, '1', '00:00:00', '00:00:00', '1', '2018-07-01 08:32:41'),
(36, 36, 20, '1', '00:00:00', '00:00:00', '1', '2018-07-01 08:33:14'),
(37, 37, 20, '1', '00:00:00', '00:00:00', '1', '2018-07-01 08:34:03'),
(38, 38, 20, '1', '00:00:00', '00:00:00', '1', '2018-07-01 08:34:48'),
(39, 39, 20, '1', '00:00:00', '00:00:00', '1', '2018-07-01 08:35:23');

-- --------------------------------------------------------

--
-- Table structure for table `default_hourly_rate`
--

CREATE TABLE `default_hourly_rate` (
  `id` int(5) UNSIGNED NOT NULL,
  `parkingspot_id` int(6) UNSIGNED NOT NULL,
  `rate` int(11) NOT NULL DEFAULT '1',
  `status` varchar(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `default_hourly_rate`
--

INSERT INTO `default_hourly_rate` (`id`, `parkingspot_id`, `rate`, `status`) VALUES
(17, 17, 20, '0'),
(18, 18, 20, '0'),
(19, 19, 20, '0'),
(20, 20, 20, '0'),
(21, 21, 20, '0'),
(22, 22, 20, '0'),
(23, 23, 20, '0'),
(24, 24, 20, '0'),
(25, 25, 20, '0'),
(26, 26, 20, '0'),
(27, 27, 20, '0'),
(28, 28, 20, '0'),
(29, 29, 20, '0'),
(30, 30, 20, '0'),
(31, 31, 20, '0'),
(32, 32, 20, '0'),
(33, 33, 20, '0'),
(34, 34, 20, '0'),
(35, 35, 20, '0'),
(36, 36, 20, '0'),
(37, 37, 20, '0'),
(38, 38, 20, '0'),
(39, 39, 20, '0');

-- --------------------------------------------------------

--
-- Table structure for table `default_openclosetime`
--

CREATE TABLE `default_openclosetime` (
  `id` int(5) UNSIGNED NOT NULL,
  `parkingspot_id` int(6) UNSIGNED NOT NULL,
  `isAvailable` varchar(1) NOT NULL DEFAULT '1',
  `isAlwaysOpen` varchar(1) NOT NULL DEFAULT '1',
  `open_time` time NOT NULL DEFAULT '09:00:00',
  `close_time` time NOT NULL DEFAULT '05:00:00',
  `total_available` int(6) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `default_openclosetime`
--

INSERT INTO `default_openclosetime` (`id`, `parkingspot_id`, `isAvailable`, `isAlwaysOpen`, `open_time`, `close_time`, `total_available`) VALUES
(17, 17, '1', '1', '00:00:00', '00:00:00', 25),
(18, 18, '1', '1', '00:00:00', '00:00:00', 25),
(19, 19, '1', '1', '00:00:00', '00:00:00', 25),
(20, 20, '1', '1', '00:00:00', '00:00:00', 25),
(21, 21, '1', '1', '00:00:00', '00:00:00', 25),
(22, 22, '1', '1', '00:00:00', '00:00:00', 25),
(23, 23, '1', '1', '00:00:00', '00:00:00', 25),
(24, 24, '1', '1', '00:00:00', '00:00:00', 25),
(25, 25, '1', '1', '00:00:00', '00:00:00', 25),
(26, 26, '1', '1', '00:00:00', '00:00:00', 25),
(27, 27, '1', '1', '00:00:00', '00:00:00', 25),
(28, 28, '1', '1', '00:00:00', '00:00:00', 25),
(29, 29, '1', '1', '00:00:00', '00:00:00', 25),
(30, 30, '1', '1', '00:00:00', '00:00:00', 25),
(31, 31, '1', '1', '00:00:00', '00:00:00', 25),
(32, 32, '1', '1', '00:00:00', '00:00:00', 25),
(33, 33, '1', '1', '00:00:00', '00:00:00', 25),
(34, 34, '1', '1', '00:00:00', '00:00:00', 25),
(35, 35, '1', '1', '00:00:00', '00:00:00', 25),
(36, 36, '1', '1', '00:00:00', '00:00:00', 25),
(37, 37, '1', '1', '00:00:00', '00:00:00', 25),
(38, 38, '1', '1', '00:00:00', '00:00:00', 25),
(39, 39, '1', '1', '00:00:00', '00:00:00', 25);

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(5) UNSIGNED NOT NULL,
  `event_name` varchar(200) NOT NULL,
  `event_details` varchar(500) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `status` varchar(1) NOT NULL DEFAULT '1',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `event_name`, `event_details`, `start_date`, `end_date`, `status`, `date`) VALUES
(1, 'Eid Day', 'No details', '2018-06-07 00:00:00', '2018-06-09 00:00:00', '1', '2018-06-06 09:59:26'),
(2, 'Macy''s Thanksgiving Day Parade.', 'no details', '2018-06-11 00:00:00', '2018-06-14 00:00:00', '1', '2018-06-06 10:00:02'),
(3, 'New Year''s Eve in Times Square.', 'n/a', '2018-06-12 00:00:00', '2018-06-04 00:00:00', '1', '2018-06-06 10:02:41');

-- --------------------------------------------------------

--
-- Table structure for table `event_rate`
--

CREATE TABLE `event_rate` (
  `id` int(5) UNSIGNED NOT NULL,
  `parkingspot_id` int(6) UNSIGNED NOT NULL,
  `event_id` int(6) UNSIGNED NOT NULL,
  `rate` int(11) NOT NULL DEFAULT '0',
  `open_time` datetime NOT NULL,
  `close_time` datetime NOT NULL,
  `status` varchar(1) NOT NULL DEFAULT '1',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `flat_rate`
--

CREATE TABLE `flat_rate` (
  `id` int(5) UNSIGNED NOT NULL,
  `parkingspot_id` int(6) UNSIGNED NOT NULL,
  `rate` int(11) NOT NULL DEFAULT '0',
  `isAlwaysOpen` varchar(1) NOT NULL DEFAULT '1',
  `open_time` time NOT NULL,
  `close_time` time NOT NULL,
  `date` date NOT NULL,
  `status` varchar(1) NOT NULL DEFAULT '1',
  `date_for` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `flat_rate`
--

INSERT INTO `flat_rate` (`id`, `parkingspot_id`, `rate`, `isAlwaysOpen`, `open_time`, `close_time`, `date`, `status`, `date_for`) VALUES
(39, 17, 0, '1', '00:00:00', '00:00:00', '0000-00-00', '1', '2018-07-01 08:15:23'),
(40, 18, 0, '1', '00:00:00', '00:00:00', '0000-00-00', '1', '2018-07-01 08:21:24'),
(41, 19, 0, '1', '00:00:00', '00:00:00', '0000-00-00', '1', '2018-07-01 08:21:52'),
(42, 20, 0, '1', '00:00:00', '00:00:00', '0000-00-00', '1', '2018-07-01 08:23:18'),
(43, 21, 0, '1', '00:00:00', '00:00:00', '0000-00-00', '1', '2018-07-01 08:23:49'),
(44, 22, 0, '1', '00:00:00', '00:00:00', '0000-00-00', '1', '2018-07-01 08:24:32'),
(45, 23, 0, '1', '00:00:00', '00:00:00', '0000-00-00', '1', '2018-07-01 08:24:56'),
(46, 24, 0, '1', '00:00:00', '00:00:00', '0000-00-00', '1', '2018-07-01 08:26:09'),
(47, 25, 0, '1', '00:00:00', '00:00:00', '0000-00-00', '1', '2018-07-01 08:26:50'),
(48, 26, 0, '1', '00:00:00', '00:00:00', '0000-00-00', '1', '2018-07-01 08:27:20'),
(49, 27, 0, '1', '00:00:00', '00:00:00', '0000-00-00', '1', '2018-07-01 08:27:50'),
(50, 28, 0, '1', '00:00:00', '00:00:00', '0000-00-00', '1', '2018-07-01 08:28:15'),
(51, 29, 0, '1', '00:00:00', '00:00:00', '0000-00-00', '1', '2018-07-01 08:28:51'),
(52, 30, 0, '1', '00:00:00', '00:00:00', '0000-00-00', '1', '2018-07-01 08:29:37'),
(53, 31, 0, '1', '00:00:00', '00:00:00', '0000-00-00', '1', '2018-07-01 08:30:12'),
(54, 32, 0, '1', '00:00:00', '00:00:00', '0000-00-00', '1', '2018-07-01 08:30:49'),
(55, 33, 0, '1', '00:00:00', '00:00:00', '0000-00-00', '1', '2018-07-01 08:31:39'),
(56, 34, 0, '1', '00:00:00', '00:00:00', '0000-00-00', '1', '2018-07-01 08:32:08'),
(57, 35, 0, '1', '00:00:00', '00:00:00', '0000-00-00', '1', '2018-07-01 08:32:41'),
(58, 36, 0, '1', '00:00:00', '00:00:00', '0000-00-00', '1', '2018-07-01 08:33:14'),
(59, 37, 0, '1', '00:00:00', '00:00:00', '0000-00-00', '1', '2018-07-01 08:34:03'),
(60, 38, 0, '1', '00:00:00', '00:00:00', '0000-00-00', '1', '2018-07-01 08:34:48'),
(61, 39, 0, '1', '00:00:00', '00:00:00', '0000-00-00', '1', '2018-07-01 08:35:23'),
(62, 24, 25, '0', '00:00:00', '00:00:00', '2018-07-06', '0', '2018-07-06 11:52:47'),
(63, 32, 0, '0', '00:00:00', '00:00:00', '2018-07-07', '0', '2018-07-07 16:57:12'),
(64, 32, 15, '0', '00:00:00', '00:00:00', '2018-07-08', '0', '2018-07-07 18:12:34');

-- --------------------------------------------------------

--
-- Table structure for table `hourly_rate`
--

CREATE TABLE `hourly_rate` (
  `id` int(5) UNSIGNED NOT NULL,
  `parkingspot_id` int(6) UNSIGNED NOT NULL,
  `rate` int(11) NOT NULL DEFAULT '0',
  `status` varchar(1) NOT NULL DEFAULT '1',
  `date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hourly_rate`
--

INSERT INTO `hourly_rate` (`id`, `parkingspot_id`, `rate`, `status`, `date`) VALUES
(24, 17, 0, '0', NULL),
(25, 18, 0, '0', NULL),
(26, 19, 0, '0', NULL),
(27, 20, 0, '0', NULL),
(28, 21, 0, '0', NULL),
(29, 22, 0, '0', NULL),
(30, 23, 0, '0', NULL),
(31, 24, 0, '0', NULL),
(32, 25, 0, '0', NULL),
(33, 26, 0, '0', NULL),
(34, 27, 0, '0', NULL),
(35, 28, 0, '0', NULL),
(36, 29, 0, '0', NULL),
(37, 30, 0, '0', NULL),
(38, 31, 0, '0', NULL),
(39, 32, 0, '0', NULL),
(40, 33, 0, '0', NULL),
(41, 34, 0, '0', NULL),
(42, 35, 0, '0', NULL),
(43, 36, 0, '0', NULL),
(44, 37, 0, '0', NULL),
(45, 38, 0, '0', NULL),
(46, 39, 0, '0', NULL),
(47, 24, 2, '1', '2018-07-06'),
(48, 32, 4, '1', '2018-07-07'),
(49, 32, 2, '1', '2018-07-09'),
(50, 32, 2, '1', '2018-07-08');

-- --------------------------------------------------------

--
-- Table structure for table `map_spot_facilites`
--

CREATE TABLE `map_spot_facilites` (
  `id` int(5) UNSIGNED NOT NULL,
  `parkingspot_id` int(6) UNSIGNED NOT NULL,
  `facility_id` int(6) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `map_spot_images`
--

CREATE TABLE `map_spot_images` (
  `id` int(5) UNSIGNED NOT NULL,
  `parkingspot_id` int(6) UNSIGNED NOT NULL,
  `images_id` int(6) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `map_spot_options`
--

CREATE TABLE `map_spot_options` (
  `id` int(5) UNSIGNED NOT NULL,
  `parkingspot_id` int(6) UNSIGNED NOT NULL,
  `options_id` int(6) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`version`) VALUES
(27);

-- --------------------------------------------------------

--
-- Table structure for table `monthly_rate`
--

CREATE TABLE `monthly_rate` (
  `id` int(5) UNSIGNED NOT NULL,
  `parkingspot_id` int(6) UNSIGNED NOT NULL,
  `rate` int(11) NOT NULL DEFAULT '0',
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `status` varchar(1) NOT NULL DEFAULT '1',
  `date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `openclosetime`
--

CREATE TABLE `openclosetime` (
  `id` int(5) UNSIGNED NOT NULL,
  `isAvailable` int(11) NOT NULL DEFAULT '1',
  `parkingspot_id` int(6) UNSIGNED NOT NULL,
  `isAlwaysOpen` varchar(1) NOT NULL DEFAULT '1',
  `open_time` time NOT NULL DEFAULT '09:00:00',
  `close_time` time NOT NULL DEFAULT '05:00:00',
  `total_available` int(6) UNSIGNED NOT NULL,
  `date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `openclosetime`
--

INSERT INTO `openclosetime` (`id`, `isAvailable`, `parkingspot_id`, `isAlwaysOpen`, `open_time`, `close_time`, `total_available`, `date`) VALUES
(44, 1, 17, '1', '00:00:00', '00:00:00', 25, NULL),
(45, 1, 18, '1', '00:00:00', '00:00:00', 25, NULL),
(46, 1, 19, '1', '00:00:00', '00:00:00', 25, NULL),
(47, 1, 20, '1', '00:00:00', '00:00:00', 25, NULL),
(48, 1, 21, '1', '00:00:00', '00:00:00', 25, NULL),
(49, 1, 22, '1', '00:00:00', '00:00:00', 25, NULL),
(50, 1, 23, '1', '00:00:00', '00:00:00', 25, NULL),
(51, 1, 24, '1', '00:00:00', '00:00:00', 25, NULL),
(52, 1, 25, '1', '00:00:00', '00:00:00', 25, NULL),
(53, 1, 26, '1', '00:00:00', '00:00:00', 25, NULL),
(54, 1, 27, '1', '00:00:00', '00:00:00', 25, NULL),
(55, 1, 28, '1', '00:00:00', '00:00:00', 25, NULL),
(56, 1, 29, '1', '00:00:00', '00:00:00', 25, NULL),
(57, 1, 30, '1', '00:00:00', '00:00:00', 25, NULL),
(58, 1, 31, '1', '00:00:00', '00:00:00', 25, NULL),
(59, 1, 32, '1', '00:00:00', '00:00:00', 25, NULL),
(60, 1, 33, '1', '00:00:00', '00:00:00', 25, NULL),
(61, 1, 34, '1', '00:00:00', '00:00:00', 25, NULL),
(62, 1, 35, '1', '00:00:00', '00:00:00', 25, NULL),
(63, 1, 36, '1', '00:00:00', '00:00:00', 25, NULL),
(64, 1, 37, '1', '00:00:00', '00:00:00', 25, NULL),
(65, 1, 38, '1', '00:00:00', '00:00:00', 25, NULL),
(66, 1, 39, '1', '00:00:00', '00:00:00', 25, NULL),
(67, 1, 24, '0', '00:00:00', '00:00:00', 25, '2018-07-05 18:00:00'),
(68, 1, 32, '0', '00:00:00', '00:00:00', 25, '2018-07-06 18:00:00'),
(69, 1, 32, '0', '00:00:00', '00:00:00', 25, '2018-07-07 18:00:00'),
(70, 1, 32, '0', '00:00:00', '00:00:00', 25, '2018-07-08 18:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `parkingspot`
--

CREATE TABLE `parkingspot` (
  `id` int(11) UNSIGNED NOT NULL,
  `public_name` varchar(100) NOT NULL,
  `short_name` varchar(100) NOT NULL,
  `description` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `lat` double NOT NULL,
  `long` double NOT NULL,
  `owner_id` int(11) UNSIGNED NOT NULL,
  `city_id` int(11) UNSIGNED NOT NULL,
  `status` varchar(1) NOT NULL DEFAULT '1',
  `uppdated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `parkingspot`
--

INSERT INTO `parkingspot` (`id`, `public_name`, `short_name`, `description`, `address`, `lat`, `long`, `owner_id`, `city_id`, `status`, `uppdated_at`) VALUES
(17, 'The Verb', 'The Verb', '', '1271 Boylston St, Boston, MA 02215', 42.3453385, -71.09693219999997, 7, 1, '1', '2018-07-01 08:15:23'),
(18, 'Liberty Waterfront', '', '', '250 Northern Ave, Boston, MA 02210, USA', 42.3486079, -71.03832690000002, 8, 1, '1', '2018-07-01 08:21:24'),
(19, 'Congress 1', '', '', '390 Congress St Boston, MA 02210', 42.3506286, -71.04638590000002, 9, 1, '1', '2018-07-01 08:21:52'),
(20, 'Congress 2', '', '', '391 Congress St Boston, MA 02210', 42.3492832, -71.04631749999999, 10, 1, '1', '2018-07-01 08:23:18'),
(21, 'Envoy Hotel', '', '', '70 Sleeper St, Boston, MA 02210', 42.35375090000001, -71.0482098, 11, 1, '1', '2018-07-01 08:23:49'),
(22, 'Park Row', '', '', '15 Park Row W, Providence, RI 02903', 41.8282865, -71.41373879999998, 12, 1, '1', '2018-07-01 08:24:32'),
(23, 'Francis', '', '', '69 Francis St, Providence, RI 02903', 41.8265643, -71.4150075, 13, 1, '1', '2018-07-01 08:24:56'),
(24, '71 SW 10th St, Miami, FL 33130', '', '', '71 SW 10th St, Miami, FL 33130', 25.7644587, -80.1946322, 14, 4, '1', '2018-07-01 08:26:09'),
(25, 'Back Bay 1', '', '', '79-99 Exeter St, Boston, MA 02116', 42.348685, -71.07919720000001, 15, 1, '1', '2018-07-01 08:26:50'),
(26, 'Back Bay 2', '', '', '58 Trinity Pl, Boston, MA 02116', 42.34822339999999, -71.075197, 16, 1, '1', '2018-07-01 08:27:20'),
(27, 'Cambridge 1', '', '', '290 Binney St, Cambridge, MA 02142', 42.36540519999999, -71.08778489999997, 17, 1, '1', '2018-07-01 08:27:50'),
(28, 'Cambridge 2', '', '', '290 Binney St, Cambridge, MA 02142', 42.36540519999999, -71.08778489999997, 18, 1, '1', '2018-07-01 08:28:15'),
(29, 'Back Bay 3', '', '', '222 Berkeley St, Boston, MA 02116', 42.3506611, -71.0733123, 19, 1, '1', '2018-07-01 08:28:51'),
(30, 'Downtown Xing 1', '', '', '6 Avery St, Boston, MA 02111', 42.35318059999999, -71.06275870000002, 20, 1, '1', '2018-07-01 08:29:37'),
(31, 'Cambdrige 4', '', '', '75 Ames St, Cambridge, MA 02142', 42.3635904, -71.08878179999999, 21, 1, '1', '2018-07-01 08:30:12'),
(32, 'Cambridge 5', '', '', '90 Broadway, Cambridge, MA 02142', 42.36320449999999, -71.08627160000003, 22, 1, '1', '2018-07-01 08:30:49'),
(33, '19 Lincoln St, Boston, MA 02111', '', '', '19 Lincoln St, Boston, MA 02111', 42.35290800000001, -71.05836959999999, 23, 1, '1', '2018-07-01 08:31:39'),
(34, '125 High St, Boston, MA 02110', '', '', '125 High St, Boston, MA 02110', 42.3555144, -71.0530268, 24, 1, '1', '2018-07-01 08:32:08'),
(35, 'Seaport 1', '', '', '11 Stillings St, Boston, MA 02210', 42.3502762, -71.04712359999996, 25, 1, '1', '2018-07-01 08:32:41'),
(36, 'Seaport 2', '', '', '10 Necco St, Boston, MA 02210', 42.3488481, -71.0507379, 26, 1, '1', '2018-07-01 08:33:14'),
(37, 'Seaport 3', '', '', '116 W First St, Boston, MA 02127', 42.3434819, -71.05214590000003, 27, 1, '1', '2018-07-01 08:34:03'),
(38, 'Seaport 4', '', '', '101 Seaport Blvd, Boston, MA 02210', 42.3514207, -71.0452507, 28, 1, '1', '2018-07-01 08:34:48'),
(39, 'Back Bay 4', '', '', '501 Boylston St, Boston, MA 02116', 42.351028, -71.07450499999999, 29, 1, '1', '2018-07-01 08:35:23');

-- --------------------------------------------------------

--
-- Table structure for table `parkingspotinfo`
--

CREATE TABLE `parkingspotinfo` (
  `id` int(11) UNSIGNED NOT NULL,
  `parking_spot_id` int(11) UNSIGNED NOT NULL,
  `default_space` int(11) NOT NULL DEFAULT '100',
  `total_space` int(11) NOT NULL DEFAULT '50',
  `total_facility_space` int(11) NOT NULL DEFAULT '25',
  `vehicle_height_restriction` varchar(100) NOT NULL DEFAULT 'Undefined !',
  `timezone` varchar(5000) NOT NULL DEFAULT 'USA ',
  `descriptions` varchar(5000) NOT NULL DEFAULT 'No information provided !',
  `how_to_find` varchar(5000) NOT NULL DEFAULT 'No information provided !',
  `how_to_redem` varchar(5000) NOT NULL DEFAULT 'No information provided !',
  `term_and_condition` varchar(100) NOT NULL DEFAULT 'No information provided !',
  `customer_review` varchar(1) NOT NULL DEFAULT '0',
  `status` varchar(1) NOT NULL DEFAULT '1',
  `uppdated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `parkingspotinfo`
--

INSERT INTO `parkingspotinfo` (`id`, `parking_spot_id`, `default_space`, `total_space`, `total_facility_space`, `vehicle_height_restriction`, `timezone`, `descriptions`, `how_to_find`, `how_to_redem`, `term_and_condition`, `customer_review`, `status`, `uppdated_at`) VALUES
(17, 17, 100, 50, 25, 'Undefined !', 'USA ', 'No information provided !', 'No information provided !', 'No information provided !', 'No information provided !', '0', '1', '2018-07-01 08:15:23'),
(18, 18, 100, 50, 25, 'Undefined !', 'USA ', 'No information provided !', 'No information provided !', 'No information provided !', 'No information provided !', '0', '1', '2018-07-01 08:21:24'),
(19, 19, 100, 50, 25, 'Undefined !', 'USA ', 'No information provided !', 'No information provided !', 'No information provided !', 'No information provided !', '0', '1', '2018-07-01 08:21:52'),
(20, 20, 100, 50, 25, 'Undefined !', 'USA ', 'No information provided !', 'No information provided !', 'No information provided !', 'No information provided !', '0', '1', '2018-07-01 08:23:18'),
(21, 21, 100, 50, 25, 'Undefined !', 'USA ', 'No information provided !', 'No information provided !', 'No information provided !', 'No information provided !', '0', '1', '2018-07-01 08:23:49'),
(22, 22, 100, 50, 25, 'Undefined !', 'USA ', 'No information provided !', 'No information provided !', 'No information provided !', 'No information provided !', '0', '1', '2018-07-01 08:24:32'),
(23, 23, 100, 50, 25, 'Undefined !', 'USA ', 'No information provided !', 'No information provided !', 'No information provided !', 'No information provided !', '0', '1', '2018-07-01 08:24:56'),
(24, 24, 100, 50, 25, 'Undefined !', 'USA ', 'No information provided !', 'No information provided !', 'No information provided !', 'No information provided !', '0', '1', '2018-07-01 08:26:09'),
(25, 25, 100, 50, 25, 'Undefined !', 'USA ', 'No information provided !', 'No information provided !', 'No information provided !', 'No information provided !', '0', '1', '2018-07-01 08:26:50'),
(26, 26, 100, 50, 25, 'Undefined !', 'USA ', 'No information provided !', 'No information provided !', 'No information provided !', 'No information provided !', '0', '1', '2018-07-01 08:27:20'),
(27, 27, 100, 50, 25, 'Undefined !', 'USA ', 'No information provided !', 'No information provided !', 'No information provided !', 'No information provided !', '0', '1', '2018-07-01 08:27:50'),
(28, 28, 100, 50, 25, 'Undefined !', 'USA ', 'No information provided !', 'No information provided !', 'No information provided !', 'No information provided !', '0', '1', '2018-07-01 08:28:15'),
(29, 29, 100, 50, 25, 'Undefined !', 'USA ', 'No information provided !', 'No information provided !', 'No information provided !', 'No information provided !', '0', '1', '2018-07-01 08:28:51'),
(30, 30, 100, 50, 25, 'Undefined !', 'USA ', 'No information provided !', 'No information provided !', 'No information provided !', 'No information provided !', '0', '1', '2018-07-01 08:29:37'),
(31, 31, 100, 50, 25, 'Undefined !', 'USA ', 'No information provided !', 'No information provided !', 'No information provided !', 'No information provided !', '0', '1', '2018-07-01 08:30:12'),
(32, 32, 100, 50, 25, 'Undefined !', 'USA ', 'No information provided !', 'No information provided !', 'No information provided !', 'No information provided !', '0', '1', '2018-07-01 08:30:49'),
(33, 33, 100, 50, 25, 'Undefined !', 'USA ', 'No information provided !', 'No information provided !', 'No information provided !', 'No information provided !', '0', '1', '2018-07-01 08:31:39'),
(34, 34, 100, 50, 25, 'Undefined !', 'USA ', 'No information provided !', 'No information provided !', 'No information provided !', 'No information provided !', '0', '1', '2018-07-01 08:32:08'),
(35, 35, 100, 50, 25, 'Undefined !', 'USA ', 'No information provided !', 'No information provided !', 'No information provided !', 'No information provided !', '0', '1', '2018-07-01 08:32:41'),
(36, 36, 100, 50, 25, 'Undefined !', 'USA ', 'No information provided !', 'No information provided !', 'No information provided !', 'No information provided !', '0', '1', '2018-07-01 08:33:14'),
(37, 37, 100, 50, 25, 'Undefined !', 'USA ', 'No information provided !', 'No information provided !', 'No information provided !', 'No information provided !', '0', '1', '2018-07-01 08:34:03'),
(38, 38, 100, 50, 25, 'Undefined !', 'USA ', 'No information provided !', 'No information provided !', 'No information provided !', 'No information provided !', '0', '1', '2018-07-01 08:34:48'),
(39, 39, 100, 50, 25, 'Undefined !', 'USA ', 'No information provided !', 'No information provided !', 'No information provided !', 'No information provided !', '0', '1', '2018-07-01 08:35:23');

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `id` int(5) UNSIGNED NOT NULL,
  `payer_email` varchar(100) NOT NULL,
  `payer_id` varchar(100) NOT NULL,
  `payer_status` varchar(100) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `txn_id` varchar(1) NOT NULL,
  `mc_currency` varchar(10) NOT NULL,
  `mc_gross` decimal(10,2) NOT NULL,
  `mc_fee` decimal(10,2) NOT NULL,
  `payment_status` varchar(50) NOT NULL,
  `item_name` varchar(30) NOT NULL,
  `item_number` varchar(50) NOT NULL,
  `serial` varchar(50) NOT NULL,
  `receiver_id` varchar(30) NOT NULL,
  `verify_sign` varchar(60) NOT NULL,
  `payment_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `providers`
--

CREATE TABLE `providers` (
  `id` int(5) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `designation` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `email` varchar(100) NOT NULL,
  `pass` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `status` varchar(1) NOT NULL DEFAULT '1',
  `has_spot` int(1) NOT NULL DEFAULT '0',
  `uppdated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `providers`
--

INSERT INTO `providers` (`id`, `name`, `designation`, `address`, `mobile`, `email`, `pass`, `type`, `status`, `has_spot`, `uppdated_at`) VALUES
(7, 'The Verb', 'Manager', '1271 Boylston St, Boston, MA 02215', '9292780649', 'verb@hellohiparking.com', '0e7517141fb53f21ee439b355b5a1d0a', 'Provider', '1', 1, '2018-06-20 01:49:36'),
(8, 'liberty waterfront', 'manager ', '250 Northern Ave, Boston, MA 02210', '9292780649', 'liberty@hellohiparking.com', 'e6722920bab2326f8217e4bf6b1b58ac', 'Provider', '1', 1, '2018-06-20 01:56:04'),
(9, 'congress1', 'manager ', '390 Congress St Boston, MA 02210', '9292780649', 'congress1@hellohiparking.com', '8ef3a435bd65dc7d35330a83e73b1b22', 'Provider', '1', 1, '2018-06-20 02:05:13'),
(10, 'Congress2', 'manager ', '391 Congress St Boston, MA 02210', '9292780649', 'Congress2@hellohiparking.com', '56d7b55ccedc10b551016e65cc4406f3', 'Provider', '1', 1, '2018-06-20 02:22:43'),
(11, 'envoy hotel', 'manager', '70 Sleeper St, Boston, MA 02210', '9292780649', 'envoy@hellohiparking.com', 'ed4b161b8ec9249f49a32a7755b99e0a', 'Provider', '1', 1, '2018-06-21 03:04:35'),
(12, 'ParkRow', 'manager', '15 Park Row W, Providence, RI 02903', '9292780649', 'parkrow@hellohiparking.com', '2850c7525398116ed47f2d6f747c07a9', 'Provider', '1', 1, '2018-06-23 00:55:51'),
(13, 'francis', 'Manager ', '69 Francis St, Providence, RI 02903', '9292780649', 'francis@hellohiparking.com', '8d709b4b6461aef614529a83d883c64b', 'Provider', '1', 1, '2018-06-23 00:58:31'),
(14, 'Miami', 'manager ', '71 SW 10th St, Miami, FL 33130', '9292278069', 'miami@hellohiparking.com', '0e7517141fb53f21ee439b355b5a1d0a', 'Provider', '1', 1, '2018-06-23 01:01:36'),
(15, 'Back Bay1', 'manager', '79-99 Exeter St, Boston, MA 02116', '9292780649', 'backbay1@hellohiparking.com', '7a32b7f0a1c0921912475034dfed721e', 'Provider', '1', 1, '2018-06-29 01:21:22'),
(16, 'Back Bay2', 'manager', '58 Trinity Pl, Boston, MA 02116', '9292780649', 'backbay2@hellohiparking.com', '509b85f119c35e96beb7bdadf0213506', 'Provider', '1', 1, '2018-06-29 01:36:00'),
(17, 'Cambridge1', 'manager', '290 Binney St, Cambridge, MA 02142', '9292780649', 'cambridge1@hellohiparking.com', '6cffda86cac19aee2fd828d1cd17bf09', 'Provider', '1', 1, '2018-06-29 01:42:25'),
(18, 'Cambridge2', 'manager', '290 Binney St, Cambridge, MA 02142', '9292780649', 'cambridge2@hellohiparking.com', '288e8b2b39774f5deb236e72b0000f6c', 'Provider', '1', 1, '2018-06-29 01:46:15'),
(19, 'backbay3', 'manager', '222 Berkeley St, Boston, MA 02116', '9292780649', 'backbay3@hellohiparking.com', '556be2ca0f685417758e8b31dd93bad0', 'Provider', '1', 1, '2018-06-29 01:50:17'),
(20, 'Downtown Xing1', 'manager', '6 Avery St, Boston, MA 02111', '9292780649', 'downtownxing1@hellohiparking.com', '829597571cf91f3196842a812154eea3', 'Provider', '1', 1, '2018-06-29 21:53:29'),
(21, 'Cambridge4', 'manager', '75 Ames St, Cambridge, MA 02142', '9292780649', 'cambridge4@hellohiparking.com', '472c6895f0962a8daf770eb7e983f453', 'Provider', '1', 1, '2018-06-29 21:56:48'),
(22, 'Cambridge5', 'manager', '90 Broadway, Cambridge, MA 02142', '9292780649', 'cambridge5@hellohiparking.com', '0e7517141fb53f21ee439b355b5a1d0a', 'Provider', '1', 1, '2018-06-29 21:59:14'),
(23, 'Financial1', 'manager', '19 Lincoln St, Boston, MA 02111', '9292780649', 'financial1@hellohiparking.com', '1db6d84389a88106339ba6d575fff835', 'Provider', '1', 1, '2018-06-29 22:02:42'),
(24, 'Financial2', 'manager', '125 High St, Boston, MA 02110', '9292780649', 'financial2@hellohiparking.com', '1134ccd4d9682ebd5d9b91968bb31028', 'Provider', '1', 1, '2018-06-29 22:05:33'),
(25, 'Seaport1', 'manager', '11 Stillings St, Boston, MA 02210', '9292780649', 'seaport1@hellohiparking.com', 'a9fb1c1143399683a44e0999714a925a', 'Provider', '1', 1, '2018-06-29 22:09:06'),
(26, 'Seaport2', 'manager', '10 Necco St, Boston, MA 02210', '9292780649', 'seaport2@hellohiparking.com', '7937b5926cc56bbeb26a8b097b474d9d', 'Provider', '1', 1, '2018-06-29 22:12:35'),
(27, 'Seaport3', 'manager', '116 W First St, Boston, MA 02127', '9292780649', 'seaport3@hellohiparking.com', 'bc00c4374748e5c709c722ba4f71934f', 'Provider', '1', 1, '2018-06-29 22:15:45'),
(28, 'Seaport4', 'manager', '101 Seaport Blvd, Boston, MA 02210', '9292780649', 'seaport4@hellohiparking.com', 'da7b9edc9104d6f6c004b38bdd8b2395', 'Provider', '1', 1, '2018-06-29 22:18:32'),
(29, 'Back Bay4', 'manager', '501 Boylston St, Boston, MA 02116', '9292780649', 'backbay4@hellohiparking.com', '7fdea3418492efee94d7427eeabd81a1', 'Provider', '1', 1, '2018-06-29 22:28:20'),
(30, 'Financial3', 'manager', '280 Congress St, Boston, MA 02210', '9292780649', 'financial3@hellohiparking.com', '86bdf1044a8181d3890be1f0e4ac744e', 'Provider', '1', 0, '2018-06-29 22:31:25'),
(31, 'Financial4', 'manager', '184 South St, Boston, MA 02111', '9292780649', 'financial4@hellohiparking.com', '1f6ab03dc0b9fe5fdac2b93de2f31fc8', 'Provider', '1', 0, '2018-06-29 22:34:09'),
(32, 'Financial5', 'manager', '225 Franklin St, Boston, MA 02110', '9292780649', 'financial5@hellohiparking.com', '33c37fad51f20ae5723dcd25477896a6', 'Provider', '1', 0, '2018-06-29 22:36:55'),
(33, 'Financial6', 'manager', '22 Merchants Row, Boston, MA 02109', '9292780649', 'financial6@hellohiparking.com', 'f0f51a5f63f6744154e196f8fcf0b1d2', 'Provider', '1', 0, '2018-06-29 22:39:15'),
(34, 'Financial7', 'manager', '75-101 Federal St, Boston, MA 02110', '9292780649', 'financial7@hellohiparking.com', '3d4366ad4714e17f3d8f864657721384', 'Provider', '1', 0, '2018-06-29 22:41:41'),
(35, 'Back Bay5', 'manager', '10 St. James Avenue, Boston, MA 02210', '9292780649', 'backbay5@hellohiparking.com', '82f3d556c9702f8494d825dbb9641e61', 'Provider', '1', 0, '2018-06-29 22:44:13'),
(36, 'Financial8', 'manager', '1 Federal St, Boston, MA 02110', '9292780649', 'financial8@hellohiparking.com', 'cd04dc7909236945c62f2247c911d2a8', 'Provider', '1', 0, '2018-06-29 22:46:32'),
(37, 'Financial9', 'manager', '99 High St, Boston, MA 02110', '9292780649', 'financial9@hellohiparking.com', '768654fcad919ad0c9685fe3c2b80f32', 'Provider', '1', 0, '2018-06-29 22:50:06'),
(38, 'Financial10', 'manager', '28 Lincoln St, Boston, MA 02110', '9292780649', 'financial10@hellohiparking.com', '9648a4c6e2a6ca63b6c79715bdeba511', 'Provider', '1', 0, '2018-06-29 22:52:49'),
(39, 'Financial11', 'manager', '214 Essex St, Boston, MA 02110', '9292780649', 'financial11@hellohiparking.com', '2e99aec2aa753f4e52a7b638c324ca8d', 'Provider', '1', 0, '2018-06-29 22:55:38'),
(40, 'Seaport5', 'manager', '17-19 Farnsworth St, Boston, MA 02210', '9292780649', 'seaport5@hellohiparking.com', '0e7517141fb53f21ee439b355b5a1d0a', 'Provider', '1', 0, '2018-06-29 22:59:30'),
(41, 'Cambridge6', 'manager', '45 University Rd, Cambridge, MA 02138', '9292780649', 'cambridge6@hellohiparking.com', '32407929a9dd91053a625fe29dfc7e59', 'Provider', '1', 0, '2018-06-29 23:02:25'),
(42, 'Cambridge7', 'manager', '1 Memorial Dr, Cambridge, MA 02142', '9292780649', 'cambridge7@hellohiparking.com', '9e5de9dd31c04e46e7903979a098dca1', 'Provider', '1', 0, '2018-06-29 23:05:24'),
(43, 'SouthEnd', 'manager', '801 Massachusetts Ave, Boston, MA 02118', '9292780649', 'southend@hellohiparking.com', '3becc6e5bcc5b4b69e0b957dfbb9d97d', 'Provider', '1', 0, '2018-06-29 23:07:54'),
(44, 'Fenway1', 'manager', '1341 Boylston St, Boston, MA 02215', '9292780649', 'fenway1@hellohiparking.com', 'df367f783845ea6942e912b26474e2ac', 'Provider', '1', 0, '2018-06-29 23:10:03'),
(45, 'Financial12', 'manager', '50 Post Office Square, Boston, MA 02110', '9292780649', 'financial12@hellohiparking.com', '5368af0b79d6a251e2fe28693387ef36', 'Provider', '1', 0, '2018-06-29 23:12:01'),
(46, 'Fenway2', 'manager', '300 Fenway, Boston, MA 02115', '9292780649', 'fenway2@hellohiparking.com', 'e51ce0f5e7d35a41380ae71024094f31', 'Provider', '1', 0, '2018-06-29 23:14:28'),
(47, 'Financial13', 'manager', '200 State St, Boston, MA 02109', '9292780649', 'financial13@hellohiparking.com', '643b79a537639a28a25b48a8ebd0565e', 'Provider', '1', 0, '2018-06-29 23:16:54'),
(48, 'Back Bay6', 'manager', '234 Berkeley St, Boston, MA 02116', '9292780649', 'backbay6@hellohiparking.com', '63bb47eaa6668ea0f692ef7553e05ea2', 'Provider', '1', 0, '2018-06-29 23:18:53'),
(49, 'Financial14', 'manager', '296 State St, Boston, MA 02109', '9292780649', 'financial14@hellohiparking.com', '99f1bf1f2e8f48b7c3a9d31cd66b592f', 'Provider', '1', 0, '2018-06-29 23:21:16'),
(50, 'Downtown Xing2', 'manager', '75 Somerset St, Boston, MA 02108', '9292780649', 'downtownxing2@hellohiparking.com', '90844835d02237efa04349aafd6cb1d2', 'Provider', '1', 0, '2018-06-29 23:24:55'),
(51, 'Chinatown', 'manager', '40 Beach St, Boston, MA 02111', '9292780649', 'chinatown@hellohiparking.com', 'd3b9f7411a2296f1637ae5dae55056fa', 'Provider', '1', 0, '2018-06-29 23:31:31'),
(52, 'Fenway3', 'manager', '645 Beacon St, Boston, MA 02215', '9292780649', 'fenway3@hellohiparking.com', '09c78903b46c7a521cfb9f2e3aa1d3ef', 'Provider', '1', 0, '2018-06-29 23:34:16'),
(53, 'Seaport6', 'manager', '130 Northern Ave, Boston, MA 02210', '9292780649', 'seaport6@hellohiparking.com', 'eb644aa752da771f5168066f0402e14b', 'Provider', '1', 0, '2018-06-29 23:36:14'),
(54, 'seaport7', 'manager', '121 Seaport Boulevard, Boston, MA 02210', '9292780649', 'seaport7@hellohiparking.com', '3268ea33ea39d8c47331ed598786cd10', 'Provider', '1', 0, '2018-06-29 23:41:33'),
(55, 'Downtown Xing3', 'manager', '99 Summer Street, Boston, MA 02210', '9292780649', 'downtownxing3@hellohiparking.com', '055a35d212ee3880accaf485f930f21a', 'Provider', '1', 0, '2018-06-29 23:43:49'),
(56, 'Fenway4', 'manager', '1330 Boylston St, Boston, MA 02215', '9292780649', 'fenway4@hellohiparking.com', '977a629e6e683f34aaedb04c5149e9e0', 'Provider', '1', 0, '2018-06-29 23:45:30'),
(57, 'Fenway5', 'manager', '180 Brookline Ave, Boston, MA 02215', '9292780649', 'fenway5@hellohiparking.com', 'cdbd8829c20331299e516221f4b10cd9', 'Provider', '1', 0, '2018-06-29 23:47:16'),
(58, 'Fenway6', 'manager', '199 North Harvard Street, Allston, MA 02134', '9292780649', 'fenway6@hellohiparking.com', '5f79bec51955dee39d7bb8293c18b2ab', 'Provider', '1', 0, '2018-06-29 23:49:05'),
(59, 'Fenway7', 'manager', '401 Park Dr #11, Boston, MA 02215', '9292780649', 'fenway7@hellohiparking.com', 'd4ec288e646e83a9af6780e50e18afaf', 'Provider', '1', 0, '2018-06-29 23:50:42'),
(60, 'Downtown Xing4', 'manager', '', '9292780649', 'downtownxing4@hellohiparking.com', '5ec7393b1f590b38ee10bf5fce118dc1', 'Provider', '1', 0, '2018-06-29 23:55:45'),
(61, 'Downtown Xing5', 'manager', '', '9292780649', 'downtownxing5@hellohiparking.com', '950c579b92ab03e7deb99f8c5ebf0378', 'Provider', '1', 0, '2018-06-29 23:57:35');

-- --------------------------------------------------------

--
-- Table structure for table `rates`
--

CREATE TABLE `rates` (
  `id` int(5) UNSIGNED NOT NULL,
  `parkingspot_id` int(6) UNSIGNED DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `available` varchar(1) NOT NULL DEFAULT '1',
  `flat_rate_available` varchar(1) NOT NULL DEFAULT '1',
  `flat_rate_id` int(6) UNSIGNED DEFAULT NULL,
  `hourly_rate_available` varchar(1) NOT NULL DEFAULT '0',
  `hourly_rate_id` int(6) UNSIGNED DEFAULT NULL,
  `monthly_available` varchar(1) NOT NULL DEFAULT '0',
  `monthly_rate_id` int(6) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `spot_facilities`
--

CREATE TABLE `spot_facilities` (
  `id` int(5) UNSIGNED NOT NULL,
  `con_name` varchar(200) NOT NULL,
  `con_details` varchar(500) NOT NULL,
  `icon_name` varchar(200) NOT NULL,
  `status` varchar(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `spot_facilities`
--

INSERT INTO `spot_facilities` (`id`, `con_name`, `con_details`, `icon_name`, `status`) VALUES
(1, 'Wifi', 'n/a', '/assets/img/icons/facilities/Wifi_1528279466.png', '1'),
(2, 'CC Camera Security', 'n/a', '/assets/img/icons/facilities/CC_Camera_Security_1528279492.png', '1'),
(3, 'On Site Security Guarad', 'n/a', '/assets/img/icons/facilities/On_Site_Security_Guarad_1528279514.png', '1');

-- --------------------------------------------------------

--
-- Table structure for table `spot_images`
--

CREATE TABLE `spot_images` (
  `id` int(5) UNSIGNED NOT NULL,
  `img_name` varchar(200) NOT NULL,
  `img_details` varchar(500) NOT NULL,
  `img_address` varchar(200) NOT NULL,
  `parkingspot_id` int(6) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `spot_options`
--

CREATE TABLE `spot_options` (
  `id` int(5) UNSIGNED NOT NULL,
  `option_name` varchar(200) NOT NULL,
  `option_details` varchar(500) NOT NULL,
  `icon_name` varchar(200) NOT NULL,
  `status` varchar(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `spot_options`
--

INSERT INTO `spot_options` (`id`, `option_name`, `option_details`, `icon_name`, `status`) VALUES
(1, 'Electric Charge', 'n/a', '/assets/img/icons/options/Electric_Charge_1528279257.jpg', '1'),
(2, 'Car Wash', 'n/a', '/assets/img/icons/options/Option_2_1528279279.png', '1');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` int(5) UNSIGNED NOT NULL,
  `payment_id` int(11) NOT NULL,
  `serial` varchar(30) NOT NULL,
  `prior_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `paid_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `base_amount` decimal(10,2) NOT NULL,
  `remit_amount` decimal(10,2) NOT NULL,
  `profit_amount` decimal(10,2) NOT NULL,
  `spot_type` varchar(20) NOT NULL,
  `date_transaction` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_completion` timestamp NULL DEFAULT NULL,
  `status` varchar(1) NOT NULL DEFAULT '0',
  `user_name` varchar(100) NOT NULL,
  `user_mobile` varchar(15) NOT NULL,
  `user_email` varchar(100) NOT NULL,
  `v_type` varchar(50) NOT NULL,
  `v_make` varchar(40) NOT NULL,
  `v_model` varchar(50) NOT NULL,
  `v_year` int(11) NOT NULL,
  `v_color` varchar(30) NOT NULL,
  `v_state` varchar(60) NOT NULL,
  `v_plate` varchar(50) NOT NULL,
  `v_name` varchar(50) NOT NULL,
  `spot` varchar(300) NOT NULL,
  `pid` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `duration_day` int(11) NOT NULL,
  `duration_hour` int(11) NOT NULL,
  `arrive` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `depart` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `payment_id`, `serial`, `prior_amount`, `paid_amount`, `base_amount`, `remit_amount`, `profit_amount`, `spot_type`, `date_transaction`, `date_completion`, `status`, `user_name`, `user_mobile`, `user_email`, `v_type`, `v_make`, `v_model`, `v_year`, `v_color`, `v_state`, `v_plate`, `v_name`, `spot`, `pid`, `uid`, `duration_day`, `duration_hour`, `arrive`, `depart`) VALUES
(15, 0, 'P32U0X1531028408', '9.00', '0.00', '8.00', '0.00', '0.00', 'Hourly', '2018-07-09 09:26:47', '2018-07-09 09:26:47', '0', 'asdsdsd', '1111111111', 'sdsds@gmail.com', '', '', '', 2018, '', '', '', '', '  Cambridge 5  ', 32, 0, 0, 4, '2018-07-09 09:25:37', '2018-07-09 13:25:37');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(5) UNSIGNED NOT NULL,
  `fname` varchar(100) NOT NULL,
  `uname` varchar(100) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `email` varchar(100) NOT NULL,
  `pass` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `status` varchar(1) NOT NULL DEFAULT '1',
  `uppdated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fname`, `uname`, `mobile`, `email`, `pass`, `type`, `status`, `uppdated_at`) VALUES
(1, 'Anup Sarkar', 'Anup', '01676667712', 'test@admin.com', '0e7517141fb53f21ee439b355b5a1d0a', '', '1', '2018-06-04 04:45:12'),
(2, 'assfsf', 'sdsds', '0167666772', 'me@admin.com', 'Admin@123', 'admin', '1', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `vehicle`
--

CREATE TABLE `vehicle` (
  `id` int(5) UNSIGNED NOT NULL,
  `type` varchar(100) NOT NULL,
  `make` varchar(100) NOT NULL,
  `model` varchar(15) NOT NULL,
  `year` varchar(100) NOT NULL,
  `color` varchar(100) NOT NULL,
  `licence_state` varchar(100) NOT NULL,
  `licence_plate` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `img` varchar(100) NOT NULL,
  `client_id` int(6) UNSIGNED NOT NULL,
  `status` varchar(1) NOT NULL DEFAULT '1',
  `uppdated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `availablity`
--
ALTER TABLE `availablity`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parkingspot_id` (`parkingspot_id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `country_id` (`country_id`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `default_flat_rate`
--
ALTER TABLE `default_flat_rate`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parkingspot_id` (`parkingspot_id`);

--
-- Indexes for table `default_hourly_rate`
--
ALTER TABLE `default_hourly_rate`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parkingspot_id` (`parkingspot_id`);

--
-- Indexes for table `default_openclosetime`
--
ALTER TABLE `default_openclosetime`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parkingspot_id` (`parkingspot_id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event_rate`
--
ALTER TABLE `event_rate`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parkingspot_id` (`parkingspot_id`),
  ADD KEY `event_id` (`event_id`);

--
-- Indexes for table `flat_rate`
--
ALTER TABLE `flat_rate`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parkingspot_id` (`parkingspot_id`);

--
-- Indexes for table `hourly_rate`
--
ALTER TABLE `hourly_rate`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parkingspot_id` (`parkingspot_id`);

--
-- Indexes for table `map_spot_facilites`
--
ALTER TABLE `map_spot_facilites`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parkingspot_id` (`parkingspot_id`),
  ADD KEY `facility_id` (`facility_id`);

--
-- Indexes for table `map_spot_images`
--
ALTER TABLE `map_spot_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parkingspot_id` (`parkingspot_id`),
  ADD KEY `images_id` (`images_id`);

--
-- Indexes for table `map_spot_options`
--
ALTER TABLE `map_spot_options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parkingspot_id` (`parkingspot_id`),
  ADD KEY `options_id` (`options_id`);

--
-- Indexes for table `monthly_rate`
--
ALTER TABLE `monthly_rate`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parkingspot_id` (`parkingspot_id`);

--
-- Indexes for table `openclosetime`
--
ALTER TABLE `openclosetime`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parkingspot_id` (`parkingspot_id`);

--
-- Indexes for table `parkingspot`
--
ALTER TABLE `parkingspot`
  ADD PRIMARY KEY (`id`),
  ADD KEY `owner_id` (`owner_id`),
  ADD KEY `city_id` (`city_id`);

--
-- Indexes for table `parkingspotinfo`
--
ALTER TABLE `parkingspotinfo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parking_spot_id` (`parking_spot_id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `providers`
--
ALTER TABLE `providers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rates`
--
ALTER TABLE `rates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parkingspot_id` (`parkingspot_id`),
  ADD KEY `flat_rate_id` (`flat_rate_id`),
  ADD KEY `hourly_rate_id` (`hourly_rate_id`),
  ADD KEY `monthly_rate_id` (`monthly_rate_id`);

--
-- Indexes for table `spot_facilities`
--
ALTER TABLE `spot_facilities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `spot_images`
--
ALTER TABLE `spot_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parkingspot_id` (`parkingspot_id`);

--
-- Indexes for table `spot_options`
--
ALTER TABLE `spot_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicle`
--
ALTER TABLE `vehicle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `client_id` (`client_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `availablity`
--
ALTER TABLE `availablity`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `default_flat_rate`
--
ALTER TABLE `default_flat_rate`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `default_hourly_rate`
--
ALTER TABLE `default_hourly_rate`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `default_openclosetime`
--
ALTER TABLE `default_openclosetime`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `event_rate`
--
ALTER TABLE `event_rate`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `flat_rate`
--
ALTER TABLE `flat_rate`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT for table `hourly_rate`
--
ALTER TABLE `hourly_rate`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `map_spot_facilites`
--
ALTER TABLE `map_spot_facilites`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `map_spot_images`
--
ALTER TABLE `map_spot_images`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `map_spot_options`
--
ALTER TABLE `map_spot_options`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `monthly_rate`
--
ALTER TABLE `monthly_rate`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `openclosetime`
--
ALTER TABLE `openclosetime`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;
--
-- AUTO_INCREMENT for table `parkingspot`
--
ALTER TABLE `parkingspot`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `parkingspotinfo`
--
ALTER TABLE `parkingspotinfo`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `providers`
--
ALTER TABLE `providers`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT for table `rates`
--
ALTER TABLE `rates`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `spot_facilities`
--
ALTER TABLE `spot_facilities`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `spot_images`
--
ALTER TABLE `spot_images`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `spot_options`
--
ALTER TABLE `spot_options`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `vehicle`
--
ALTER TABLE `vehicle`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `availablity`
--
ALTER TABLE `availablity`
  ADD CONSTRAINT `availablity_ibfk_1` FOREIGN KEY (`parkingspot_id`) REFERENCES `parkingspot` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cities`
--
ALTER TABLE `cities`
  ADD CONSTRAINT `cities_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `default_flat_rate`
--
ALTER TABLE `default_flat_rate`
  ADD CONSTRAINT `default_flat_rate_ibfk_1` FOREIGN KEY (`parkingspot_id`) REFERENCES `parkingspot` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `default_hourly_rate`
--
ALTER TABLE `default_hourly_rate`
  ADD CONSTRAINT `default_hourly_rate_ibfk_1` FOREIGN KEY (`parkingspot_id`) REFERENCES `parkingspot` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `default_openclosetime`
--
ALTER TABLE `default_openclosetime`
  ADD CONSTRAINT `default_openclosetime_ibfk_1` FOREIGN KEY (`parkingspot_id`) REFERENCES `parkingspot` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `event_rate`
--
ALTER TABLE `event_rate`
  ADD CONSTRAINT `event_rate_ibfk_1` FOREIGN KEY (`parkingspot_id`) REFERENCES `parkingspot` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `event_rate_ibfk_2` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `flat_rate`
--
ALTER TABLE `flat_rate`
  ADD CONSTRAINT `flat_rate_ibfk_1` FOREIGN KEY (`parkingspot_id`) REFERENCES `parkingspot` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `hourly_rate`
--
ALTER TABLE `hourly_rate`
  ADD CONSTRAINT `hourly_rate_ibfk_1` FOREIGN KEY (`parkingspot_id`) REFERENCES `parkingspot` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `map_spot_facilites`
--
ALTER TABLE `map_spot_facilites`
  ADD CONSTRAINT `map_spot_facilites_ibfk_1` FOREIGN KEY (`parkingspot_id`) REFERENCES `parkingspot` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `map_spot_facilites_ibfk_2` FOREIGN KEY (`facility_id`) REFERENCES `spot_facilities` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `map_spot_images`
--
ALTER TABLE `map_spot_images`
  ADD CONSTRAINT `map_spot_images_ibfk_1` FOREIGN KEY (`parkingspot_id`) REFERENCES `parkingspot` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `map_spot_images_ibfk_2` FOREIGN KEY (`images_id`) REFERENCES `spot_images` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `map_spot_options`
--
ALTER TABLE `map_spot_options`
  ADD CONSTRAINT `map_spot_options_ibfk_1` FOREIGN KEY (`parkingspot_id`) REFERENCES `parkingspot` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `map_spot_options_ibfk_2` FOREIGN KEY (`options_id`) REFERENCES `spot_options` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `monthly_rate`
--
ALTER TABLE `monthly_rate`
  ADD CONSTRAINT `monthly_rate_ibfk_1` FOREIGN KEY (`parkingspot_id`) REFERENCES `parkingspot` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `openclosetime`
--
ALTER TABLE `openclosetime`
  ADD CONSTRAINT `openclosetime_ibfk_1` FOREIGN KEY (`parkingspot_id`) REFERENCES `parkingspot` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `parkingspot`
--
ALTER TABLE `parkingspot`
  ADD CONSTRAINT `parkingspot_ibfk_1` FOREIGN KEY (`owner_id`) REFERENCES `providers` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `parkingspot_ibfk_2` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `parkingspotinfo`
--
ALTER TABLE `parkingspotinfo`
  ADD CONSTRAINT `parkingspotinfo_ibfk_1` FOREIGN KEY (`parking_spot_id`) REFERENCES `parkingspot` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `rates`
--
ALTER TABLE `rates`
  ADD CONSTRAINT `rates_ibfk_1` FOREIGN KEY (`parkingspot_id`) REFERENCES `parkingspot` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `rates_ibfk_2` FOREIGN KEY (`flat_rate_id`) REFERENCES `flat_rate` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `rates_ibfk_3` FOREIGN KEY (`hourly_rate_id`) REFERENCES `hourly_rate` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `rates_ibfk_4` FOREIGN KEY (`monthly_rate_id`) REFERENCES `monthly_rate` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `spot_images`
--
ALTER TABLE `spot_images`
  ADD CONSTRAINT `spot_images_ibfk_1` FOREIGN KEY (`parkingspot_id`) REFERENCES `parkingspot` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `vehicle`
--
ALTER TABLE `vehicle`
  ADD CONSTRAINT `vehicle_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
