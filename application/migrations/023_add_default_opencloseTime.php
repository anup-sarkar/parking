<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_default_opencloseTime extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 5,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        )
                         ,
                        'parkingspot_id' => array(
                                'type' => 'INT',
                                'constraint' => '6',
                                'unsigned' => TRUE,
                        ),
                         'isAvailable' => array(
                              'type' => 'VARCHAR',
                                'constraint' => '1',
                                'default' => '1'
                        ),
                        'isAlwaysOpen' => array(
                               'type' => 'VARCHAR',
                                'constraint' => '1',
                                'default' => '1'
                        )
                        ,
                        'open_time' => array(
                                'type' => 'TIME',
                                'default' => '09:00:00',
                        ),
                        'close_time' => array(
                                'type' => 'TIME',
                                'default' => '05:00:00',
                        
                        ), 
                         'total_available' => array(
                                'type' => 'INT',
                                'constraint' => '6',
                                'unsigned' => TRUE,
                        )
                     
                       
                     
                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->add_field('CONSTRAINT FOREIGN KEY (parkingspot_id) REFERENCES parkingspot(id) ON DELETE CASCADE');

               
                $this->dbforge->create_table('default_opencloseTime');
        }

        public function down()
        {
                $this->dbforge->drop_table('default_opencloseTime');
        }
}