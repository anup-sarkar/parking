<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_events extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 5,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        )
                         ,
                        'event_name' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '200',
                            
                        ),
                         'event_details' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '500',
                            
                        )
                       
                        ,
                        'start_date' => array(
                                'type' => 'DATETIME',
                                
                        ),
                        'end_date' => array(
                                'type' => 'DATETIME',
                               
                        
                        )  ,
                        'status' => array(
                              'type' => 'VARCHAR',
                                'constraint' => '1',
                                'default' => '1'
                        ),
                        'date TIMESTAMP DEFAULT CURRENT_TIMESTAMP',
                      
                   
                ));
                $this->dbforge->add_key('id', TRUE);
                

               
                $this->dbforge->create_table('events');
        }

        public function down()
        {
                $this->dbforge->drop_table('events');
        }
}