<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_vehicle extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 5,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'type' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ),
                        'make' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ),

                        'model' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '15',
                        ),
                        'year' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ),
                         'color' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ),
                         'licence_state' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ),
                         'licence_plate' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ),
                         'name' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ),
                         'img' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ) ,
                        'client_id' => array(
                                'type' => 'INT',
                                'constraint' => '6',
                                'unsigned' => TRUE,
                        )
                      ,'status' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '1',
                                'default' => '1'
                        )
                        , 'uppdated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP',
                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->add_field('CONSTRAINT FOREIGN KEY (client_id) REFERENCES client(id) ON DELETE CASCADE');
                $this->dbforge->create_table('vehicle');
        }

        public function down()
        {
                $this->dbforge->drop_table('vehicle');
        }
}