<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_spot_images extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 5,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        )  ,      
                        'img_name' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '200',
                            
                        ),
                         'img_details' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '500',
                            
                        ),
                         'img_address' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '200',
                            
                        )
                            ,
                        'parkingspot_id' => array(
                                'type' => 'INT',
                                'constraint' => '6',
                                'unsigned' => TRUE,
                        )
                        
                        , 
                ));
    
                 

                    $this->dbforge->add_key('id', TRUE);
                    $this->dbforge->add_field('CONSTRAINT FOREIGN KEY (parkingspot_id) REFERENCES parkingspot(id) ON DELETE CASCADE');

                $this->dbforge->create_table('spot_images');
        }

        public function down()
        {
                $this->dbforge->drop_table('spot_images');
        }
}