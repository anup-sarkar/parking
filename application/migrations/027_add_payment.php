<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_payment extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 5,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        )
                         , 
                        'payer_email' => array(
                              'type' => 'VARCHAR',
                                'constraint' => '100',
                               
                        ) ,
 
                        'payer_id' => array(
                              'type' => 'VARCHAR',
                              'constraint' => '100'
                              
                        ) ,
 
                        'payer_status' => array(
                               'type' => 'VARCHAR',
                              'constraint' => '100'
                              
                        ) 
                         ,
 
                        'first_name' => array(
                               'type' => 'VARCHAR',
                              'constraint' => '100'
                              
                        )  ,
 
                        'last_name' => array(
                               'type' => 'VARCHAR',
                              'constraint' => '100'
                              
                        ) 




                         
                        
                        , 
                        'txn_id' => array(
                              'type' => 'VARCHAR',
                                'constraint' => '1',
                                
                        ) , 
                        'mc_currency' => array(
                              'type' => 'VARCHAR',
                                'constraint' => '10',
                               
                        ) 
                        , 
                        'mc_gross' => array(
                              'type' => 'DECIMAL',
                             'constraint' => '10,2',
                               
                        ) , 
                        'mc_fee' => array(
                               'type' => 'DECIMAL',
                               'constraint' => '10,2',
                               
                        ) , 
                        'payment_status' => array(
                              'type' => 'VARCHAR',
                                'constraint' => '50',
                               
                        ) ,'item_name' => array(
                              'type' => 'VARCHAR',
                                'constraint' => '30',
                               
                        ) ,  'item_number' => array(
                              'type' => 'VARCHAR',
                                'constraint' => '50',
                               
                        ) ,  'serial' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '50',
                                 
                               
                        ) ,  'receiver_id' => array(
                              'type' => 'VARCHAR',
                                'constraint' => '30',
                               
                        ) , 'verify_sign' => array(
                              'type' => 'VARCHAR',
                                'constraint' => '60',
                               
                        ), 
                         'payment_date TIMESTAMP '      
                          , 
                         'updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP'
                      
                ));
                $this->dbforge->add_key('id', TRUE);
                
               
                $this->dbforge->create_table('payment');
        }

        public function down()
        {
                $this->dbforge->drop_table('payment');
        }
}