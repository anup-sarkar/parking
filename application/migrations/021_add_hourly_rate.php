<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_hourly_rate extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 5,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        )
                         ,
                        'parkingspot_id' => array(
                                'type' => 'INT',
                                'constraint' => '6',
                                'unsigned' => TRUE,
                        )
                     
                         ,

                        'rate' => array(
                              'type' => 'INT',
                              'default'=> 0,
                              
                        )      
                        
                        , 
                        'status' => array(
                              'type' => 'VARCHAR',
                                'constraint' => '1',
                                'default' => '1'
                        )
                        , 
                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->add_field('CONSTRAINT FOREIGN KEY (parkingspot_id) REFERENCES parkingspot(id) ON DELETE CASCADE');
                 

               
                $this->dbforge->create_table('hourly_rate');
        }

        public function down()
        {
                $this->dbforge->drop_table('hourly_rate');
        }
}