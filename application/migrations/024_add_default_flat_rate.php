<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_default_flat_rate extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 5,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        )
                         ,
                        'parkingspot_id' => array(
                                'type' => 'INT',
                                'constraint' => '6',
                                'unsigned' => TRUE,
                        )
                     
                         ,

                        'rate' => array(
                              'type' => 'INT',
                              'default'=> 0,
                              
                        ),

                        'isAlwaysOpen' => array(
                              'type' => 'VARCHAR',
                                'constraint' => '1',
                                'default' => '1'
                        )
                        ,
                        'open_time' => array(
                                'type' => 'TIME',
                             
                        ),
                        'close_time' => array(
                                'type' => 'TIME',
                               
                        
                        )
                          
                        , 
                        'status' => array(
                              'type' => 'VARCHAR',
                                'constraint' => '1',
                                'default' => '1'
                        )
                        ,'date_for TIMESTAMP DEFAULT CURRENT_TIMESTAMP',
                      
                       
                   
                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->add_field('CONSTRAINT FOREIGN KEY (parkingspot_id) REFERENCES parkingspot(id) ON DELETE CASCADE');
                 

               
                $this->dbforge->create_table('default_flat_rate');
        }

        public function down()
        {
                $this->dbforge->drop_table('default_flat_rate');
        }
}