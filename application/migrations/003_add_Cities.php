<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_Cities extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 5,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'name' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ),
                        'postal_code' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        )
                        , 
                        'state' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ),
                         'lat' => array(
                                'type' => 'DOUBLE',
                              
                        ),
                         'long' => array(
                                'type' => 'DOUBLE',
                               
                        )
                           , 
                        'timezone' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                              
                        )
                        ,
                        'country_id' => array(
                                'type' => 'INT',
                                'constraint' => '6',
                                'unsigned' => TRUE,
                        )
                        , 
                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->add_field('CONSTRAINT FOREIGN KEY (country_id) REFERENCES Countries(id) ON DELETE CASCADE');

               
                $this->dbforge->create_table('Cities');
        }

        public function down()
        {
                $this->dbforge->drop_table('Cities');
        }
}