<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_Providers extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 5,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'name' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ),
                        'designation' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ),

                        'address' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ),

                        'mobile' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '15',
                        ),
                        'email' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ),
                         'pass' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ),'type' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ),'status' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '1',
                                'default' => '1'
                        )
                        ,'has_spot' => array(
                                'type' => 'INT',
                                'constraint' => '1',
                                'default' => '0'
                        )
                        , 'uppdated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP',
                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table('Providers');
        }

        public function down()
        {
                $this->dbforge->drop_table('Providers');
        }
}