<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_spot_facilities extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 5,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        )  ,      
                        'con_name' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '200',
                            
                        ),
                         'con_details' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '500',
                            
                        ),
                         'icon_name' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '200',
                            
                        )
                       , 
                        'status' => array(
                              'type' => 'VARCHAR',
                                'constraint' => '1',
                                'default' => '1'
                        )
                        , 
                ));
    
                 

                    $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table('spot_facilities');
        }

        public function down()
        {
                $this->dbforge->drop_table('spot_facilities');
        }
}