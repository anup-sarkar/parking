<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_event_rate extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 5,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        )
                         ,
                        'parkingspot_id' => array(
                                'type' => 'INT',
                                'constraint' => '6',
                                'unsigned' => TRUE,
                        )
                        , 
                        'event_id' => array(
                                'type' => 'INT',
                                'constraint' => '6',
                                'unsigned' => TRUE,
                        )
                         ,

                        'rate' => array(
                              'type' => 'INT',
                              'default'=> 0,
                              
                        )
                        ,
                        'open_time' => array(
                                'type' => 'DATETIME',
                             
                        ),
                        'close_time' => array(
                                'type' => 'DATETIME',
                               
                        
                        ), 
                      'status' => array(
                              'type' => 'VARCHAR',
                                'constraint' => '1',
                                'default' => '1'
                        )
                       ,
                       'date TIMESTAMP DEFAULT CURRENT_TIMESTAMP',
                  
                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->add_field('CONSTRAINT FOREIGN KEY (parkingspot_id) REFERENCES parkingspot(id) ON DELETE CASCADE');
                 $this->dbforge->add_field('CONSTRAINT FOREIGN KEY (event_id) REFERENCES events(id) ON DELETE CASCADE');

               
                $this->dbforge->create_table('event_rate');
        }

        public function down()
        {
                $this->dbforge->drop_table('event_rate');
        }
}