<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_rates extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 5,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ) ,
                        'parkingspot_id' => array(
                                'type' => 'INT',
                                'constraint' => '6',
                                'unsigned' => TRUE,
                                'null' => TRUE,
                        )
                         ,
                         'date TIMESTAMP',

                         'available' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '1',
                                'default' => '1'
                        ) ,
                         'flat_rate_available' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '1',
                                'default' => '1'
                        ),

                        'flat_rate_id' => array(
                                'type' => 'INT',
                                'constraint' => '6',
                                'unsigned' => TRUE,
                                'null' => TRUE,

                        ),
                        'hourly_rate_available' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '1',
                                'default' => '0'
                        ),

                        'hourly_rate_id' => array(
                                'type' => 'INT',
                                'constraint' => '6',
                                'unsigned' => TRUE,
                                'null' => TRUE,
                        ),

                        'monthly_available' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '1',
                                'default' => '0'
                        ),

                        'monthly_rate_id' => array(
                                'type' => 'INT',
                                'constraint' => '6',
                                'unsigned' => TRUE,
                                'null' => TRUE,
                        )
                        
                   , 
                ));
                $this->dbforge->add_key('id', TRUE);
                
                $this->dbforge->add_field('CONSTRAINT FOREIGN KEY (parkingspot_id) REFERENCES parkingspot(id) ON DELETE CASCADE');

                $this->dbforge->add_field('CONSTRAINT FOREIGN KEY (flat_rate_id) REFERENCES flat_rate(id) ON DELETE CASCADE');

                $this->dbforge->add_field('CONSTRAINT FOREIGN KEY (hourly_rate_id) REFERENCES hourly_rate(id) ON DELETE CASCADE');

                $this->dbforge->add_field('CONSTRAINT FOREIGN KEY (monthly_rate_id) REFERENCES monthly_rate(id) ON DELETE CASCADE');
              
                $this->dbforge->create_table('rates');
        }

        public function down()
        {
                $this->dbforge->drop_table('rates');
        }
}