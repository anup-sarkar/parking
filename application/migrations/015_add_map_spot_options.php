<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_map_spot_options extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 5,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        )
                         ,
                        'parkingspot_id' => array(
                                'type' => 'INT',
                                'constraint' => '6',
                                'unsigned' => TRUE,
                        )
                         ,
                        'options_id' => array(
                                'type' => 'INT',
                                'constraint' => '6',
                                'unsigned' => TRUE,
                        )
                     
                         
                        , 
                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->add_field('CONSTRAINT FOREIGN KEY (parkingspot_id) REFERENCES parkingspot(id) ON DELETE CASCADE');

                $this->dbforge->add_field('CONSTRAINT FOREIGN KEY (options_id) REFERENCES spot_options(id) ON DELETE CASCADE');
                 

               
                $this->dbforge->create_table('map_spot_options');
        }

        public function down()
        {
                $this->dbforge->drop_table('map_spot_options');
        }
}