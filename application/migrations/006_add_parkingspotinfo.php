<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_ParkingSpotInfo extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                         'parking_spot_id' => array(
                                'type' => 'INT',
                                'constraint' => '11',
                                'unsigned' => TRUE
                        ),
                         'default_space' => array(
                                'type' => 'INT',
                                'constraint' => '11',
                                'default' => '100'
                        ), 
                        'total_space' => array(
                                'type' => 'INT',
                                'constraint' => '11',
                                'default' => '50'
                        ),
                          'total_facility_space' => array(
                                'type' => 'INT',
                                'constraint' => '11',
                                'default' => '25'
                        )
                           ,
                        'vehicle_height_restriction' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                                'default' => 'Undefined !'
                        ) ,
                        'timezone' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '5000',
                                 'default' => 'USA '
                        )
                        ,
                        'descriptions' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '5000',
                                 'default' => 'No information provided !'
                        )
                         ,
                        'how_to_find' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '5000',
                                'default' => 'No information provided !'
                        ),
                         'how_to_redem' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '5000',
                                'default' => 'No information provided !'
                              
                        ),
                         'term_and_condition' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                                'default' => 'No information provided !'                               
                        )
                         ,'customer_review' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '1',
                                'default' => '0'
                        )
                        ,'status' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '1',
                                'default' => '1'
                        )
                        , 'uppdated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP',
                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->add_field('CONSTRAINT FOREIGN KEY (parking_spot_id) REFERENCES parkingspot(id) ON DELETE CASCADE');
                
                $this->dbforge->create_table('parkingspotinfo');


                
        }

        public function down()
        {
                $this->dbforge->drop_table('parkingspotinfo');
        }
}