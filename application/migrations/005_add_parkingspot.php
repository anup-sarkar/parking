<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_ParkingSpot extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'public_name' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ) , 
                        'short_name' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        )
                        ,
                        'description' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        )
                         ,
                        'address' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '100',
                        ),
                         'lat' => array(
                                'type' => 'DOUBLE',
                              
                        ),
                         'long' => array(
                                'type' => 'DOUBLE',
                               
                        )
                        ,
                        'owner_id' => array(
                                'type' => 'INT',
                                'constraint' => '11',
                                'unsigned' => TRUE,
                        )
                       
                        ,
                        'city_id' => array(
                                'type' => 'INT',
                                'constraint' => '11',
                                'unsigned' => TRUE,
                        )
                        ,'status' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '1',
                                'default' => '1'
                        )
                        , 'uppdated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP',
                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->add_field('CONSTRAINT FOREIGN KEY (owner_id) REFERENCES providers(id) ON DELETE CASCADE');
                $this->dbforge->add_field('CONSTRAINT FOREIGN KEY (city_id) REFERENCES cities(id) ON DELETE CASCADE');
                $this->dbforge->create_table('parkingspot');


               
        }

        public function down()
        {
                $this->dbforge->drop_table('parkingspot');
        }
}