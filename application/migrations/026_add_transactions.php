<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_transactions extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 5,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
 
                        'payment_id' => array(
                              'type' => 'INT',
                             
                              
                        )
                         , 
                        'serial' => array(
                              'type' => 'VARCHAR',
                                'constraint' => '30',
                               
                        ) ,
 
                        'prior_amount' => array(
                             'type' => 'DECIMAL',
                             'constraint' => '10,2',
                              
                        ) ,
 
                        'paid_amount' => array(
                             'type' => 'DECIMAL',
                             'constraint' => '10,2',
                              
                        ) 

                         , 
                         'date_transaction TIMESTAMP DEFAULT CURRENT_TIMESTAMP'
                         , 
                         'date_completion TIMESTAMP DEFAULT CURRENT_TIMESTAMP'
                        , 
                        'status' => array(
                              'type' => 'VARCHAR',
                                'constraint' => '1',
                                'default' => '0'
                        ) , 
                        'user_name' => array(
                              'type' => 'VARCHAR',
                                'constraint' => '100',
                               
                        ) 
                        , 
                        'user_mobile' => array(
                              'type' => 'VARCHAR',
                                'constraint' => '15',
                               
                        ) , 
                        'user_email' => array(
                              'type' => 'VARCHAR',
                                'constraint' => '100',
                               
                        ) , 
                        'v_type' => array(
                              'type' => 'VARCHAR',
                                'constraint' => '50',
                               
                        ) ,'v_make' => array(
                              'type' => 'VARCHAR',
                                'constraint' => '40',
                               
                        ) ,  'v_model' => array(
                              'type' => 'VARCHAR',
                                'constraint' => '50',
                               
                        ) ,  'v_year' => array(
                               'type' => 'INT',
                                 
                               
                        ) ,  'v_color' => array(
                              'type' => 'VARCHAR',
                                'constraint' => '30',
                               
                        ) , 'v_state' => array(
                              'type' => 'VARCHAR',
                                'constraint' => '60',
                               
                        ) , 'v_plate' => array(
                              'type' => 'VARCHAR',
                                'constraint' => '50',
                               
                        ) , 'v_name' => array(
                              'type' => 'VARCHAR',
                                'constraint' => '50',
                               
                        ) , 'pid' => array(
                              'type' => 'INT',
                                
                               
                        ) , 'spot' => array(
                              'type' => 'VARCHAR',
                                'constraint' => '300',
                               
                        ) , 
                        'uid' => array(
                               'type' => 'INT',
                                
                               
                        ) , 'duration_day' => array(
                              'type' => 'INT',
                              
                               
                        ) , 
                        'duration_hour' => array(
                               'type' => 'INT',
                                
                               
                        ), 
                        'arrive TIMESTAMP '
                         , 
                         'depart TIMESTAMP ', 
                ));
                $this->dbforge->add_key('id', TRUE);
                
               
                $this->dbforge->create_table('transactions');
        }

        public function down()
        {
                $this->dbforge->drop_table('transactions');
        }
}