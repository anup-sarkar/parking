<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
class Home extends CI_Controller {

  
  public function index()
  {
  
   $data['Cities'] = $this->Admin_model->get_cities();
   $data['address'] = $this->Admin_model->get_address();


  // var_dump($this->Admin_model->get_address());
    $this->load->view('home/home2',$data);
  }



  public function login()
  {
  
    
    $this->load->view('home/signin' );
  }


  public function signup()
  {
  
    
    $this->load->view('home/signup' );
  }


  public function provider_listing()
  {
  
    
    $this->load->view('home/list_parking' );
  }



  public function profile()
  {
   $cid = $_SESSION['cid'];

   $data['client'] = $this->Client_model->get_client_by_id($cid);
    $data['vehicles'] = $this->Client_model->get_vehicle($cid);
 
 
   $this->load->view('home/profile',$data );
  }



  public function date()
  {
    $date=date_create("2013-03-15");
      // echo date("d M Y-h:m p");
       date_default_timezone_set("Asia/Dhaka");
echo "The time is " . date("d M Y - h:i a");
  }

  public function logout()
  {
   
    $this->session->sess_destroy();
    redirect("home");
  }

  public function register()
    {
		//set validation rules
		$this->form_validation->set_rules('fname', 'First Name', 'trim|required|min_length[3]|max_length[100] ');
		$this->form_validation->set_rules('lname', 'last Name Name', 'trim|required|alpha|min_length[3]|max_length[100] ');
		$this->form_validation->set_rules('email', 'Email ID', 'trim|required|valid_email');

		$this->form_validation->set_rules('mobile', 'Mobile Number', 'trim|required|min_length[9]|max_length[14] ');
	
		$this->form_validation->set_rules('pass', 'Password', 'trim|required|md5|min_length[4]|max_length[40]');
		//$this->form_validation->set_rules('type', 'Account Type', 'required|min_length[1]');
		

		//validate form input
		if ($this->form_validation->run() == FALSE)
        {
			// fails
		 
			$this->session->set_flashdata('msg','<div class="alert alert-danger text-center">Errors : Please Recheck Following Problems ! </div>');
				$this->load->view('home/signup');
			//$this->load->view('account/register');
        }
		else
		{
			//insert the user registration details into database
			$data = array(
				'fname' => $this->input->post('fname'),
				'lname' => $this->input->post('lname'),
				'email' => $this->input->post('email'),
				'mobile' => $this->input->post('mobile'),			
				'pass' => $this->input->post('pass'),
        'type' => "registered",
			 
			);
			
			// insert form data into database
			if ($this->Client_model->insert_client($data))
			{
				$this->session->set_flashdata('msg','<div class="alert alert-success text-center">You are Successfully Registered! Login Now !!!</div>');
					redirect('home/login');
				
			}
			else
			{
				//echo "Failed";
				// error
				$this->session->set_flashdata('msg','<div class="alert alert-danger text-center">Oops! Error.  Please try again later!!!</div>');
				redirect('home/signup');
			}
		}
	}


public function login_check()
     {
          //get the posted values
          $email = $this->input->post("email");
          $password = $this->input->post("pass");

          //set validations
          $this->form_validation->set_rules("email", "Email", "trim|required|valid_email");
          $this->form_validation->set_rules("pass", "Password", "trim|required");

          if ($this->form_validation->run() == FALSE)
          {
               //validation fails
              // $this->load->view('login');
                $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">validation Failed !</div>');
                
				$this->load->view('home/login');

          }
          else
          {
               //validation succeeds
             
                    //check if username and password is correct
                    $usr_result = $this->Client_model->user_validate($email, $password);

                    if ($usr_result > 0) //active user record is present
                    {
                         //set the session variables
                    	 $client = $this->Client_model->get_client($email, $password);

                    	 $fname=$client[0]['fname'];
                    	 $lname=$client[0]['lname'];
                       $email=$client[0]['email'];
                       $cid=$client[0]['id'];
                       $mobile=$client[0]['mobile'];
                     
                         $sessiondata = array(
                         	    'client_name'=>$fname .' '.$lname,
                              'client_fname'=>$fname,
                              'client_lname'=>$lname,
                              'client_email' => $email,
                              'cid' => $cid,
                              'client_mobile' => $mobile,
                              'type' => 'client' 
                          
                         );
                         $this->session->set_userdata($sessiondata);
                         
                    $this->session->set_flashdata('msg','<div class="alert alert-success text-center">successfully logged in your account!</div>');

                   

		redirect('home');
 }
                    else
                    {
                         $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Invalid username and password! </div>');
                          
                         redirect('home/login');
                         
                    }
               }
              
          
     }



  public function update_user()
    {
    //set validation rules
       $cid = $_SESSION['cid'];
    $this->form_validation->set_rules('fname', 'First Name', 'trim|required|min_length[3]|max_length[100] ');
    $this->form_validation->set_rules('lname', 'last Name Name', 'trim|required|alpha|min_length[3]|max_length[100] ');
    $this->form_validation->set_rules('email', 'Email ID', 'trim|valid_email');

    $this->form_validation->set_rules('mobile', 'Mobile Number', 'trim|required|min_length[9]|max_length[14] ');
  
    $this->form_validation->set_rules('pass', 'Password', 'trim|md5|min_length[4]|max_length[40]');
    //$this->form_validation->set_rules('type', 'Account Type', 'required|min_length[1]');
    

    //validate form input
    if ($this->form_validation->run() == FALSE)
        {
      // fails
     
      $this->session->set_flashdata('msg','<div class="alert alert-danger text-center">Errors : Please Recheck Following Problems ! </div>');
        $this->load->view('home/profile');
      //$this->load->view('account/register');
        }
    else
    {
      //insert the user registration details into database
$pass=$this->input->post('pass');
      if($pass!=null)
      {
          $data = array(
        'fname' => $this->input->post('fname'),
        'lname' => $this->input->post('lname'),
        'email' => $this->input->post('email'),
        'mobile' => $this->input->post('mobile'),  
          'type' => "registered",   
        'pass' => $pass,
       
      );
      }
      else
      {
          $data = array(
        'fname' => $this->input->post('fname'),
        'lname' => $this->input->post('lname'),
        'email' => $this->input->post('email'),
        'mobile' => $this->input->post('mobile'), 
          'type' => "registered",    
        
      );
      }
    
      
       
      if ($this->Client_model->update_client($data,$cid))
      {
        $this->session->set_flashdata('msg','<div class="alert alert-success text-center">You have updated your profile info ! </div>');
          redirect('home/profile');
        
      }
      else
      {
        
        // error
        $this->session->set_flashdata('msg','<div class="alert alert-danger text-center">Oops! Error.  Please try again later!!!</div>');
        redirect('home/profile');
      }
    }
  }


 public function add_vehicle()
    {
    //set validation rules
       $cid = $_SESSION['cid'];

    $this->form_validation->set_rules('type', 'Vehicle Type', 'trim|required|min_length[2]|max_length[100] ');
    $this->form_validation->set_rules('make', 'Vehicle Make', 'trim|required|min_length[2]|max_length[100] ');

    $this->form_validation->set_rules('model', 'Vehicle Model', 'trim|required|min_length[2]|max_length[100] ');

    $this->form_validation->set_rules('year', 'Vehicle Yearl', 'trim|required|numeric|min_length[4]|max_length[4] ');

    $this->form_validation->set_rules('color', 'Vehicle Color', 'trim|required|min_length[2]|max_length[40] ');

    $this->form_validation->set_rules('state', 'Vehicle Licence state', 'trim|required|min_length[2]|max_length[50] ');

    $this->form_validation->set_rules('plate', 'Vehicle Licence Plate', 'trim|required|min_length[2]|max_length[50] ');

    $this->form_validation->set_rules('name', 'Vehicle Name', 'trim|required|min_length[2]|max_length[50] ');
    
    
    //$this->form_validation->set_rules('type', 'Account Type', 'required|min_length[1]');
    

    //validate form input
    if ($this->form_validation->run() == FALSE)
        {
      // fails
     
   $data['client'] = $this->Client_model->get_client_by_id($cid);
      $this->session->set_flashdata('msg3','<div class="alert alert-danger text-center">Errors : Please Recheck Following Problems ! </div>');
        $this->load->view('home/profile',$data);
      //$this->load->view('account/register');
        }
    else
    {
      //insert the user registration details into database
 
          $data = array(
        'type' => $this->input->post('type'),
        'make' => $this->input->post('make'),
        'model' => $this->input->post('model'),
        'year' => $this->input->post('year'),     
        'color' => $this->input->post('color'),
          'licence_state' => $this->input->post('state'),  
           'licence_plate' => $this->input->post('plate'),
            'name' => $this->input->post('name'),
            'client_id'=>$cid,);
       
   
    
      
       
      if ($this->Client_model->insert_vehicle($data))
      {
        
          $this->session->set_flashdata('msg3','<div class="alert alert-success text-center">You have added a vehicle your account !</div>');
          

          redirect('home/profile');
        
      }
      else
      {
        
        // error
        $this->session->set_flashdata('msg3','<div class="alert alert-danger text-center">Oops! Error.  Please try again later!!!</div>');
        redirect('home/profile');
      }
    }
  }



 public function listing()
  {

    // get the posted values


    $this->form_validation->set_rules("provider_name", "Provider name", "trim|required|min_length[3]|max_length[300]");

     $this->form_validation->set_rules("txt_provider_desig", "Provider Designation", "trim|min_length[3]|max_length[100]");

     $this->form_validation->set_rules("txt_provider_address", "Provider Address", "trim|min_length[3]|max_length[200]");

    $this->form_validation->set_rules('txt_provider_pass', 'Password', 'trim|required|md5|min_length[4]|max_length[40]');
    $this->form_validation->set_rules('txt_provider_email', 'Email ', 'trim|required|valid_email|is_unique[providers.email]');

    $this->form_validation->set_rules('txt_provider_mobile', 'Mobile Number', 'trim|min_length[9]|max_length[14] ');

    $name = $this->input->post("provider_name");
    $desig = $this->input->post("txt_provider_desig");
    $address = $this->input->post("txt_provider_Address");
    $mob = $this->input->post("txt_provider_mobile");
    $email = $this->input->post("txt_provider_email");
    $pass = $this->input->post("txt_provider_pass");
    $type = "request";

    if ($this->form_validation->run() == FALSE)
    {

      // $_POST['txt_provider_name']=$name;
      // $_POST['txt_provider_desig']=$desig;
      // $_POST['txt_provider_Address']=$address;
      // $_POST['txt_provider_mobile']=$mob;
      // $_POST['txt_provider_email']=$email;
      // $_POST['txt_provider_pass']=$pass;
      // $_POST['txt_provider_type']=$type;

      // validation fails
        $data['current']='providers';
      $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">
       '.validation_errors().'</div>');

     

   
      $this->load->view('home/list_parking',$data);
    }
    else
    {

      // validation succeeds

      
        $data = array(
          'name' => $name,
          'designation' => $desig,
           'address' => $address,
            'mobile' => $mob,
             'email' => $email,
              'pass' => md5($pass),
               'type' => $type,
        );

        // check if username and password is correct

        $result = $this->Providers_model->insert($data);
        if ($result) //active user record is present
        {
          
          $this->load->view('home/list_success');
        }
        else
        {
          $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">insert Failed . Unknown Error ! </div>');
            $data['current']='providers';
     
 
   
       $this->load->view('home/list_parking',$data);
        }
      
    }
  }


}
