<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends CI_Controller {

  
  public function index()
  {
  	$q = $this->input->get("q");
    $date = $this->input->get("date");
    $to = $this->input->get("to");
    $addr = $this->input->get("addr");
    $stype = $this->input->get("stype");
    $days = $this->input->get("days");
       $place = $this->input->get("place");

    $data["spots"]=null;

   $lat = $this->input->post("lat");
    $long = $this->input->post("long");
 

   $data['Cities'] = $this->Admin_model->get_cities();
   $data['address'] = $this->Admin_model->get_address();


    $data['q']=$q;

    $data['addr']=$addr;

    $data['date']=$date;
    
    $data['to']=$to;

    $data['stype']=$stype;

      $data['lat']=$lat;
    $data['long']=$long;
    
       $data['place']=$place;
    $data['address']=$addr;
    //echo  $this->Client_model->search($q); 

    //echo "<pre>";
    //print_r($this->Client_model->search($q)); // or var_dump($data);
    //echo "</pre>";
    $this->load->view('search/searchpage',$data);
  }

 public function city($q)
  {
  	
  	 
  	$data["spots"]=$this->Client_model->search($q);
  	var_dump($data);
    //$this->load->view('search/searchpage');
  }


   public function locations()
  {
    
     
    $q = $this->input->post("place");
    $q2 = $this->input->post("q2");
 
    $lat = $this->input->post("lat");
    $long = $this->input->post("long");
    $addr = $this->input->post("addr");

    $from = $this->input->post("from");
    $to = $this->input->post("to");

    $days = $this->input->post("days");
     $hours = $this->input->post("hours");

    if($lat==null || $long==null)
    {
      redirect("search");
    }

    $distance=$this->distance($lat,$long,42.3600825,-71.05888010000001);
    $data['q']=$q;


    $data['lat']=$lat;
    $data['long']=$long;

    $data['place']=$q;
    $data['addr']=$addr;
    $data['days']=$days;
    $data['hours']=$hours;


    $cords=$this->getMaxCordinates(1,$lat,$long);

 
    $date= date(" Y-m-d",time());

    $data["spots"]=null;
 
    if($lat!=null && $long!=null)
    {
  
    $data["spots"]=$this->Location_model->search($date,$cords,$addr);
     }
 

    //Search Multiple day
    if($days !=null && $hours != null )
    {
      $data["spots"]=$this->Location_model->search_multiple($date,$cords,$days,$hours);
      
    }


    $data['addr']=$addr;

    $data['date']=$date;

 
   $data['Cities'] = $this->Admin_model->get_cities();
   $data['address'] = $this->Admin_model->get_address();
   $this->load->view('search/searchpage',$data);

  }




private function getMaxCordinates($d,$lat,$lng)
{
  
$distance = 1;

// earth's radius in km = ~6371
$radius = 6371;

// latitude boundaries
$maxlat = $lat + rad2deg($distance / $radius);
$minlat = $lat - rad2deg($distance / $radius);

// longitude boundaries (longitude gets smaller when latitude increases)
$maxlng = $lng + rad2deg($distance / $radius / cos(deg2rad($lat)));
$minlng = $lng - rad2deg($distance / $radius / cos(deg2rad($lat)));

$cord = array('maxlat' => $maxlat,
              'minlat'=> $minlat,
              'maxlng' => $maxlng,
              'minlng'=> $minlng );

return $cord;

}

private function distance($lat1, $lng1, $lat2, $lng2) {
    // convert latitude/longitude degrees for both coordinates
    // to radians: radian = degree * π / 180
    $lat1 = deg2rad($lat1);
    $lng1 = deg2rad($lng1);
    $lat2 = deg2rad($lat2);
    $lng2 = deg2rad($lng2);

    // calculate great-circle distance
    $distance = acos(sin($lat1) * sin($lat2) + cos($lat1) * cos($lat2) * cos($lng1 - $lng2));

    // distance in human-readable format:
    // earth's radius in km = ~6371
    return 6371 * $distance;
}




 public function images($pid)
    {
     
    echo json_encode($this->Panel_model->get_images($pid));

    }


 public function mail()
    {
      $email="me.anup.sarkar@gmail.com";
     
     // $this->Account_model->send_mail($email);
   
      $d=$this->load->view("search/mail",'',true);
      echo $d;
    }

 


 public function book()
  {    
    $val=$this->input->post(); 
    if($val==null)
    {
      redirect("home");
    }

      $rate = $this->input->post("rate");
      $flat_rate = $this->input->post("flat_rate");
      $hourly_rate = $this->input->post("hourly_rate");

      $pid = $this->input->post("pid");

      $from=$this->input->post("from");
      $to=$this->input->post("to");
      $day=$this->input->post("day");
      $hour=$this->input->post("hour");
      $total_available=$this->input->post("total_available");

      $m_date1=$this->input->post("m_date1");
      $m_date2=$this->input->post("m_date2");


      $type=$this->input->post("type");

      $data["spot"]=$this->Client_model->get_spot_info($pid);
      $data["images"]=$this->Panel_model->get_images($pid);

      $data["rate"]=$rate;
      $data["pid"]=$pid;
      $data["from"]=$from;
      $data["to"]=$to;
      $data["day"]=$day;
      $data["hour"]=$hour;
      $data["flat_rate"]=$flat_rate;
       $data["hourly_rate"]=$hourly_rate;
         $data["total_available"]=$total_available;

         $data["m_date1"]=$m_date1;
      $data["m_date2"]=$m_date2;

       $data["type"]=$type;


      //echo $rate."-".$pid."-".$from."-".$to."-".$day."-".$hour;

    $this->load->view('search/book',$data);
  }



}
