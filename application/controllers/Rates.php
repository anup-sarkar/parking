<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rates extends CI_Controller {
  
   function __construct() 
  {
    parent::__construct();

 if (!isset($_SESSION['panel_user'])) {redirect('Panel/login');}
   
  }



public function index()
  {
      

    $this->load->view('panel/dashboard');
  }

  

public function set()
  {
   $spot_id=$_SESSION['spot_id'];
  

   $data['timings'] = $this->Rates_model->get_default($spot_id);

   
   $data['event_rate'] = $this->Admin_model->get_event_rate();
   $data['flatrate'] = $this->Rates_model->get_flatrate_default($spot_id);
   $data['hourly_rate'] = $this->Rates_model->get_hour_default($spot_id);
   $data['monthlyrate'] = $this->Rates_model->get_monthlyrate($spot_id);
   $data['events'] = $this->Admin_model->get_events();
  
 
  
   $this->load->view('panel/rates',$data );
  }



public function get_timings()
  {
   $spot_id=$_SESSION['spot_id'];
   $date = $this->input->post("date");

   $data = $this->Rates_model->get_timing_by_date($spot_id,$date);

   if($data==false)
   {
  
            $out = array('status' => 0 );
 
            echo json_encode($out);

   }
   else
   {
      $out = array('status' => 1 );
      $out=array_merge($out,$data);
 
      echo json_encode($out);
   }
 

  }

  public function get_flat_timings()
  {
   $spot_id=$_SESSION['spot_id'];
   $date = $this->input->post("date");

   $data = $this->Rates_model->get_flat_timing_by_date($spot_id,$date);

   if($data==false)
   {
  
            $out = array('status' => 0 );
 
            echo json_encode($out);

   }
   else
   {
      $out = array('status' => 1 );
      $out=array_merge($out,$data);
 
      echo json_encode($out);
   }
 

  }


    public function get_hourly_timings()
  {
   $spot_id=$_SESSION['spot_id'];
   $date = $this->input->post("date");

   $data = $this->Rates_model->get_hourly_timing_by_date($spot_id,$date);

   if($data==false)
   {
  
            $out = array('status' => 0 );
 
            echo json_encode($out);

   }
   else
   {
      $out = array('status' => 1 );
      $out=array_merge($out,$data);
 
      echo json_encode($out);
   }
 

  }
 
 
 
 public  function  update_eventrate()
  {
      $status = $this->input->post("status");
      $rate = $this->input->post("rate");
      $open = $this->input->post("start");
      $close = $this->input->post("end");
      $id = $this->input->post("id");


      $this->form_validation->set_rules("rate", "Rate", "required");
      $this->form_validation->set_rules("start", "Start time", "required");
      $this->form_validation->set_rules("end", "Close Time", "required");

            // validation succeeds

 $spot_id=$_SESSION['spot_id'];




    if ($this->form_validation->run() == FALSE)
    {
      echo "Error:\n".validation_errors();
    }
    else
    {
 
     $data = array(
            'parkingspot_id'=>$spot_id,
           'status' => $status,
           'event_id' =>$id,
           'open_time'=>$open.':00',
           'close_time'=>$close.':00',
           'rate'=>$rate,
       
      );

     
        if($this->Admin_model->insert_eventrate($data,$spot_id,$id))
        {
          echo "Success: Event Setting updated!";
        }
        else
        {
          echo "Error: Database Error Failed!";
        }
      }
     
  }



  public function create_open_close()
  {

    // get the posted values
    $isAvailable = $this->input->post("isAvailable");
    $open = $this->input->post("txt_openclose_open");
    $close = $this->input->post("txt_openclose_close");
    $total = $this->input->post("txt_openclose_total");

    $isalwaysopen = $this->input->post("txt_openclose_isalwaysopen");
    $date = $this->input->post("date");

     $this->form_validation->set_rules("txt_openclose_total", "Total", "required|numeric");

    

    $spot_id=$_SESSION['spot_id'];
    $id=$_SESSION['id'];


     $data=null;
    if($isalwaysopen ==0)
    {
      $data = array(
        'isAvailable'=>$isAvailable,
          'parkingspot_id' => $spot_id,
          'isalwaysopen' => 0,
           'open_time' => $open.':00',
            'close_time' => $close.':00',
            'total_available' =>$total,
            'date'=>$date,
          
        );
    }
    else
    {
      $data = array(
          'isAvailable'=>$isAvailable,
          'parkingspot_id' => $spot_id,
          'isalwaysopen' => 1,
          'open_time' => "00:00:00",
          'close_time' => "00:00:00",
          'total_available' =>$total,
          'date'=>$date,
          
        );
    }
   

 

    if ($this->form_validation->run() == FALSE)
    {
 
         
      $msg=$this->message("<b>Error : </b>  Input value is not valid ! Please Check again . ","danger");
      $data = array('status' => 0,
                        'msg' =>$msg,
                        'errors' =>validation_errors());
      echo json_encode($data);
    }
    else
    {

 

        $result = $this->Rates_model->insert($data,$spot_id,$date);
        if ($result) //active user record is present
        {
          //$this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Parkingspot Open Close Timing has updated !</div>');
         // redirect('rates/set');

            $msg=$this->message("<b>Info : </b>  Parkingspot Open Close Timing has updated !!","success");
 

            $timings = $this->Rates_model->get_array($spot_id);
            $data = array('status' => 1,
                        'msg' =>$msg
                   );
  
            $out=array_merge($data,$timings);
 
            echo json_encode($out);
  

        }
        else
        {
            $msg=$this->message("<b>Error : </b>  Update Failed !!","danger");
   
            $timings = $this->Rates_model->get_array($spot_id);
            $data = array('status' => 0,
                        'msg' =>$msg,

                   );
 
            $out=array_merge($data,$timings);
 

            echo json_encode($out);

 
        }
      
    }
  }


 public function create_flat_rate()
  {

    // get the posted values

    $open = $this->input->post("txt_flatrate_open");
    $close = $this->input->post("txt_flatrate_close");
    $rate = $this->input->post("txt_flatrate_rate");
    $date = $this->input->post("date");

        $status = $this->input->post("status");
    $isalwaysopen = $this->input->post("txt_flatrate_isalwaysopen");


    $this->form_validation->set_rules("txt_flatrate_rate", "Rate", "required|numeric");

    

    $spot_id=$_SESSION['spot_id'];
    $id=$_SESSION['id'];

    $data=null;

    if($isalwaysopen ==0)
    {
      $data = array(
          'parkingspot_id' => $spot_id,
          'isalwaysopen' => 0,
           'open_time' => $open.':00',
            'close_time' => $close.':00',
            'rate' =>$rate,
            'date' =>$date,
            'status'=>$status,
          
        );
    }
    else
    {
      $data = array(
          'parkingspot_id' => $spot_id,
          'isalwaysopen' => 1,
          'open_time' => "00:00:00",
          'close_time' => "00:00:00",
          'rate' =>$rate,
          'date' =>$date,
          'status'=>$status,
          
        );
    }
   



    if ($this->form_validation->run() == FALSE)
    {

      // validation fails
      $msg=$this->message("<b>Error : </b>  Input value is not valid ! Please Check again . ","danger");
      $data = array('status' => 0,
                        'msg' =>$msg,
                        'errors' =>validation_errors());
      echo json_encode($data);
    }
    else
    {
 // check if username and password is correct

        $result = $this->Rates_model->insert_flatrate($data,$spot_id,$date);
        if ($result) //active user record is present
        {
           $msg=$this->message("<b>Info : </b>  Parkingspot Flat Rate Settings has updated !!","success");
 

            $flatrate = $this->Rates_model->get_flatrate_array($spot_id);

            $data = array('status' => 1,
                        'msg' =>$msg
                   );
  
            $out=array_merge($data,$flatrate);
 
            echo json_encode($out);



        }
        else
        {
            $msg=$this->message("<b>Error : </b>  Update Failed !!","danger");
   
           
            $data = array('status' => 0,
                        'msg' =>$msg,

                   );

            echo json_encode($data);

        }
      
    }
  }

 


 public function create_monthly_rate()
  {

    // get the posted values

    $rate = $this->input->post("txt_monthly_rate");
   

    $status = $this->input->post("txt_monthly_status");


    $this->form_validation->set_rules("txt_monthly_rate", "Rate", "numeric");

    

    $spot_id=$_SESSION['spot_id'];
    $id=$_SESSION['id'];

    $data=null;
    if($status ==null)
    {
      $data = array(
          'parkingspot_id' => $spot_id,
          'status' => 0,
         
            'rate' =>$rate,
          
        );
    }
    else
    {
      $data = array(
          'parkingspot_id' => $spot_id,
          'status' => 1,
       
          'rate' =>$rate,
          
        );
    }
   



    if ($this->form_validation->run() == FALSE)
    {

      // validation fails
      $this->session->set_flashdata('msg3', '<div class="alert alert-danger text-center">
         '.validation_errors(). '</div>');

      redirect('rates/set');
    }
    else
    {
 // check if username and password is correct

        $result = $this->Rates_model->insert_monthlyrate($data,$spot_id);
        if ($result) //active user record is present
        {
          $this->session->set_flashdata('msg3', '<div class="alert alert-success text-center">Flat Rate  has updated !</div>');
          redirect('rates/set');
        }
        else
        {
          $this->session->set_flashdata('msg3', '<div class="alert alert-danger text-center">insert Failed . Unknown Error ! </div>');
          $this->load->view('rates/set');
        }
      
    }
  }






 public function create_hourly_rate()
  {

    // get the posted values

    $rate = $this->input->post("txt_hourly_rate");
   

    $status = $this->input->post("txt_hourly_status");
     $date = $this->input->post("date");


    $this->form_validation->set_rules("txt_hourly_rate", "Rate", "numeric|required");

    

    $spot_id=$_SESSION['spot_id'];
    $id=$_SESSION['id'];

    $data=null;

 


    if($status == 0)
    {
      $data = array(
          'parkingspot_id' => $spot_id,
          'status' => 0,
          'date' =>$date,
          'rate' =>$rate,
 
        );
    }
    else
    {
      $data = array(
          'parkingspot_id' => $spot_id,
          'status' => 1,
           'date' =>$date,
       
          'rate' =>$rate,
          
        );
    }
   



    if ($this->form_validation->run() == FALSE)
    {

      // validation fails
            $msg=$this->message("<b>Error : </b>  Rate Field Cannot Be Empty !!","danger");
 
            $data = array('status' => 0,
                        'msg' =>$msg
                   );
  
             
 
            echo json_encode($data);
    }
    else
    {
 // check if username and password is correct

        $result = $this->Rates_model->insert_hourly_rate($data,$spot_id,$date);
        if ($result) //active user record is present
        {
              $msg=$this->message("<b>Success : </b>  Hourly Rate Successfully Updated !!","success");
 
            $data = array('status' => 1,
                        'msg' =>$msg
                   );
  
            
            echo json_encode($data);
        }
        else
        {
           $msg=$this->message("<b>Error : </b> Unknown error occured !!","danger");
 
            $data = array('status' => 0,
                        'msg' =>$msg
                   );
  
          
 
            echo json_encode($data);
        }
      
    }
  }



private function message($msg,$type)
{
       $result="<script type='text/javascript'>
        $(document).ready(function(){
          demo.initChartist();
            $.notify({
                icon: 'ti-settings',
                message: '".$msg."'

            },{
                type: '".$type."',
                timer: 4000
            });

        });
    </script>";
    return $result;

}



  public function create_open_close_default()
  {

    // get the posted values

    $open = $this->input->post("txt_openclose_open");
    $close = $this->input->post("txt_openclose_close");
    $total = $this->input->post("txt_openclose_total");

    $isalwaysopen = $this->input->post("txt_openclose_isalwaysopen");


     $this->form_validation->set_rules("txt_openclose_total", "Total", "required|numeric");

    

    $spot_id=$_SESSION['spot_id'];
    $id=$_SESSION['id'];

     $data=null;
    if($isalwaysopen ==null)
    {
      $data = array(
          'parkingspot_id' => $spot_id,
          'isalwaysopen' => 0,
           'open_time' => $open.':00',
            'close_time' => $close.':00',
            'total_available' =>$total,
          
        );
    }
    else
    {
      $data = array(
          'parkingspot_id' => $spot_id,
          'isalwaysopen' => 1,
          'open_time' => "00:00:00",
          'close_time' => "00:00:00",
          'total_available' =>$total,
          
        );
    }
   



    if ($this->form_validation->run() == FALSE)
    {

      // validation fails
      $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">
         '.validation_errors(). '</div>');

      $this->load->view('parkingspot/add');
    }
    else
    {

      // validation succeeds

      
 

        // check if username and password is correct

        $result = $this->Rates_model->insert_default($data,$spot_id);
        if ($result) //active user record is present
        {
          $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Parkingspot Open Close Timing has updated !</div>');
          redirect('rates/set');
        }
        else
        {
          $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">insert Failed . Unknown Error ! </div>');
          $this->load->view('rates/set');
        }
      
    }
  }






 public function create_flat_rate_default()
  {

    // get the posted values

    $open = $this->input->post("txt_flatrate_open");
    $close = $this->input->post("txt_flatrate_close");
    $rate = $this->input->post("txt_flatrate_rate");

    $isalwaysopen = $this->input->post("txt_flatrate_isalwaysopen");


    $this->form_validation->set_rules("txt_flatrate_rate", "Rate", "required|numeric");

    

    $spot_id=$_SESSION['spot_id'];
    $id=$_SESSION['id'];

    $data=null;
    if($isalwaysopen ==null)
    {
      $data = array(
          'parkingspot_id' => $spot_id,
          'isalwaysopen' => 0,
           'open_time' => $open.':00',
            'close_time' => $close.':00',
            'rate' =>$rate,
          
        );
    }
    else
    {
      $data = array(
          'parkingspot_id' => $spot_id,
          'isalwaysopen' => 1,
          'open_time' => "00:00:00",
          'close_time' => "00:00:00",
          'rate' =>$rate,
          
        );
    }
   



    if ($this->form_validation->run() == FALSE)
    {

      // validation fails
      $this->session->set_flashdata('msg2', '<div class="alert alert-danger text-center">
         '.validation_errors(). '</div>');

      redirect('rates/set');
    }
    else
    {
 // check if username and password is correct

        $result = $this->Rates_model->insert_flatrate_default($data,$spot_id);
        if ($result) //active user record is present
        {
          $this->session->set_flashdata('msg2', '<div class="alert alert-success text-center">Flatrate  has updated !</div>');
          redirect('rates/set');
        }
        else
        {
          $this->session->set_flashdata('msg2', '<div class="alert alert-danger text-center">insert Failed . Unknown Error ! </div>');
          $this->load->view('rates/set');
        }
      
    }
  }



 public function create_hourly_rate_dafault()
  {

    // get the posted values

    $rate = $this->input->post("txt_hourly_rate");
   

    $status = $this->input->post("txt_hourly_status");


    $this->form_validation->set_rules("txt_monthly_rate", "Rate", "numeric");

    

    $spot_id=$_SESSION['spot_id'];
    $id=$_SESSION['id'];

    $data=null;
    if($status ==null)
    {
      $data = array(
          'parkingspot_id' => $spot_id,
          'status' => 0,
         
            'rate' =>$rate,
          
        );
    }
    else
    {
      $data = array(
          'parkingspot_id' => $spot_id,
          'status' => 1,
       
          'rate' =>$rate,
          
        );
    }
   



    if ($this->form_validation->run() == FALSE)
    {

      // validation fails
      $this->session->set_flashdata('msg3', '<div class="alert alert-danger text-center">
         '.validation_errors(). '</div>');

      redirect('rates/set');
    }
    else
    {
 // check if username and password is correct

        $result = $this->Rates_model->insert_hour_default($data,$spot_id);
        if ($result) //active user record is present
        {
          $this->session->set_flashdata('msg3', '<div class="alert alert-success text-center">Flat Rate  has updated !</div>');
          redirect('rates/set');
        }
        else
        {
          $this->session->set_flashdata('msg3', '<div class="alert alert-danger text-center">insert Failed . Unknown Error ! </div>');
          $this->load->view('rates/set');
        }
      
    }
  }




 


}

