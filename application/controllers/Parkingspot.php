<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Parkingspot extends CI_Controller {
  
   function __construct() 
  {
    parent::__construct();

    if (!isset($_SESSION['admin_email'])) {
       redirect('account');
    }
   
  }
  
  public function index()
  {
      $data['current']='parkingspot';
    $data['parkingspots'] = $this->Parkingspot_model->get();
    $this->load->view('parkingspots/get',$data);
  }

 

  public function add()
  {

    $data['current']='parkingspot';
    $data['Cities'] = $this->Admin_model->get_cities();
    $data['Providers'] = $this->Providers_model->get_without_spot();
    $this->load->view('parkingspots/add',$data);
  }



  public function create()
  {

    // get the posted values

    $pname = $this->input->post("txt_parkingspot_pname");
    $sname = $this->input->post("txt_parkingspot_sname");
    $details = $this->input->post("txt_parkingspot_desc");
    $address = $this->input->post("txt_parkingspot_addr");
    $long = $this->input->post("txt_parkingspot_long");
    $lat = $this->input->post("txt_parkingspot_lat");
    $owner = $this->input->post("txt_parkingspot_owner");
    $city = $this->input->post("txt_parkingspot_city");
    
    $this->form_validation->set_rules("txt_parkingspot_pname", "Parkingspot Public Name", "trim|required|min_length[3]|max_length[300]");

    $this->form_validation->set_rules("txt_parkingspot_sname", "Parkingspot Short Name", "trim|min_length[3]|max_length[100]");

     $this->form_validation->set_rules("txt_parkingspot_addr", "Parkingspot Address", "trim|required|min_length[3]|max_length[200]");

    $this->form_validation->set_rules('txt_parkingspot_long', 'Longitude', 'trim|required|min_length[2]|max_length[60]');

    $this->form_validation->set_rules('txt_parkingspot_lat', 'Latitude', 'trim|required|min_length[2]|max_length[60]');


    


    if ($this->form_validation->run() == FALSE)
    {

 /*     // validation fails
      $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">
         '.validation_errors(). '</div>');
*/
    $data['current']='parkingspot';
    $data['Cities'] = $this->Admin_model->get_cities();
    $data['Providers'] = $this->Providers_model->get_without_spot();

      $this->load->view('parkingspots/add',$data);
    }
    else
    {

      // validation succeeds

      
        $data = array(
          'public_name' => $pname,
          'short_name' => $sname,
           'description' => $details,
            'address' => $address,
             'lat' => $lat,
              'long' => $long,
               'owner_id' => $owner,
                'city_id' => $city,
        );

        // check if username and password is correct

        $result = $this->Parkingspot_model->insert($data,$owner);
        if ($result) //active user record is present
        {
          $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">A New Parking Spot has added !</div>');
          redirect('parkingspot');
        }
        else
        {
          $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">insert Failed . Unknown Error ! </div>');
          $this->load->view('parkingspots/add');
        }
      
    }
  }


  public function delete($id)
  {
    if ($this->Parkingspot_model->delete($id))
    {
      

      $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">parkingspot has been Deleted !</div>');
      redirect('parkingspot');
    }
    else
    {
      $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">parkingspot Deletion Failed!</div>');
      $this->load->view('parkingspot');
    }

    // echo var_dump($data);

  }

  

  public function details($id)
  {
   

    $data['current']='parkingspot';
    $data['parkingspot'] = $this->Parkingspot_model->getById($id);

    $data['Cities'] = $this->Admin_model->get_cities();
    $data['Providers'] = $this->Providers_model->get();


    $this->load->view('parkingspots/details', $data);

    // echo var_dump($data);

  }



  public function update()
  {

    // get the posted values

    $pname = $this->input->post("txt_parkingspot_pname");
    $sname = $this->input->post("txt_parkingspot_sname");
    $details = $this->input->post("txt_parkingspot_desc");
    $address = $this->input->post("txt_parkingspot_addr");
    $long = $this->input->post("txt_parkingspot_long");
    $lat = $this->input->post("txt_parkingspot_lat");
    $owner = $this->input->post("txt_parkingspot_owner");
    $city = $this->input->post("txt_parkingspot_city");
    $status = $this->input->post("txt_parkingspot_status");
    $id = $this->input->post("txt_id");
    
    $this->form_validation->set_rules("txt_parkingspot_pname", "Parkingspot Public Name", "trim|required|min_length[3]|max_length[300]");

    $this->form_validation->set_rules("txt_parkingspot_sname", "Parkingspot Short Name", "trim|min_length[3]|max_length[100]");

     $this->form_validation->set_rules("txt_parkingspot_addr", "Parkingspot Address", "trim|required|min_length[3]|max_length[200]");

    $this->form_validation->set_rules('txt_parkingspot_long', 'Longitude', 'trim|required|min_length[2]|max_length[60]');

    $this->form_validation->set_rules('txt_parkingspot_lat', 'Latitude', 'trim|required|min_length[2]|max_length[60]');


    if ($this->form_validation->run() == FALSE)
    {

      // validation fails
      $errors=validation_errors();
      $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">
        validation Failed !'.$errors.'</div>');

      $this->load->view('parkingspot/details/'.$id);
    }
    else
    {

      // validation succeeds

      
      $data = array(
          'public_name' => $pname,
          'short_name' => $sname,
           'description' => $details,
            'address' => $address,
             'lat' => $lat,
              'long' => $long,
               'owner_id' => $owner,
                'city_id' => $city,
                  'status' => $status,
        );

        // check if username and password is correct

        $result = $this->Parkingspot_model->update($data,$id);
        if ($result) //active user record is present
        {
          $this->session->set_flashdata('msg', '<div class="alert alert-success text-center"> parkingspots has been updated !</div>');
          redirect('parkingspot/details/'.$id);
        }
        else
        {
          $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">update Failed . Unknown Error ! </div>');
          $this->load->view('parkingspot/details/'.$id);
        }
      
    }
  }


}

