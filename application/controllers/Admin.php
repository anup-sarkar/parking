<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
  class Admin extends CI_Controller

  {
    
    function __construct() 
    {
      parent::__construct();

      if (!isset($_SESSION['admin_email'])) {
         redirect('account');
      }
     
    }

    public  function index()
    {
      $data['current']='dashboard';
      $data["bookings"]=$this->Reservations_model->get_providerwise_transaction();
      $this->load->view('admin/dashboard',$data);
    }

    public function bookings()
    {
        $data['current']='bookings';
           
    
      $data["bookings"]=$this->Reservations_model->get();


      $this->load->view('admin/bookings',$data);
    }

    public  function getCountries()
    {
      $data['current']='country';
       $data['countries'] = $this->Admin_model->get_countries();
      $this->load->view('admin/getcountries', $data);
    }

    public   function addCountries()
    {
      $data['current']='country';
      $this->load->view('admin/addcountries',$data);
    }

    public  function getCities()
    {
      $data['current']='city';
      $data['Cities'] = $this->Admin_model->get_cities();
      $this->load->view('admin/getcities', $data);
    }

    public  function addCities()
    {
      $data['current']='city';
      $data['Countries'] = $this->Admin_model->get_countries();
      $this->load->view('admin/addcities', $data);
    }

     public  function add_events()
    {
      $data['current']='events';
       
      $this->load->view('admin/add_event', $data);
    }

     public  function  events()
    {
      $data['current']='events';
        $data['events'] = $this->Admin_model->get_events();
        
      $this->load->view('admin/events', $data);
    }

      public  function  update_events()
    {
        $status = $this->input->post("status");
        $id = $this->input->post("id");
              // validation succeeds
   

       $data = array(
             'status' => $status,
         
        );

          if($this->Admin_model->update_event_status($data,$id))
          {
            echo "Event Status Changed";
          }
          else
          {
            echo "Error Occured !";
          }
       
    }

    public  function create_country()
    {

      // get the posted values

      $name = $this->input->post("txt_coun_name");
      $code = $this->input->post("txt_coun_code");

      // set validations

      $this->form_validation->set_rules("txt_coun_name", "Country name", "trim|required");
      $this->form_validation->set_rules("txt_coun_code", "Country id", "numeric");
      if ($this->form_validation->run() == FALSE)
      {

        // validation fails
        // $this->load->view('login');

        $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">validation Failed !</div>');
        $this->load->view('Admin/addcountries');
      }
      else
      {

        // validation succeeds

        if ($this->input->post('btn_coun') == "Add Country")
        {
          $data = array(
            'name' => $name,
            'country_code' => $code,
          );

          // check if username and password is correct

          $result = $this->Admin_model->insert_country($data);
          if ($result) //active user record is present
          {
            $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">A New Country has added !</div>');
            redirect('admin/getcountries');
          }
          else
          {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Invalid username and password! </div>');
            $this->load->view('admin/addcountries');
          }
        }
      }
    }

    public  function create_city()
    {

      // get the posted values

      $name = $this->input->post("txt_city_name");
     // $code = $this->input->post("txt_city_code");
      $state = $this->input->post("txt_city_state");
      $country_id = $this->input->post("txt_country_id");
      $timezone = $this->input->post("txt_city_timezone");

      //$long = $this->input->post("txt_city_long");
      //$lat = $this->input->post("txt_city_lat");
      // set validations

      $this->form_validation->set_rules("txt_city_name", "City name", "trim|required");
      $this->form_validation->set_rules("txt_city_state", "State name", "trim|required");
      //$this->form_validation->set_rules("txt_city_code", "City id", "numeric");

      $this->form_validation->set_rules("txt_city_timezone", "Timezone", "required");
      $this->form_validation->set_rules("txt_country_id", "Country", "required");

      //$this->form_validation->set_rules('txt_city_long', 'Longitude', 'trim|required|min_length[2]|max_length[60]');

      //$this->form_validation->set_rules('txt_city_lat', 'Latitude', 'trim|required|min_length[2]|max_length[60]');

      if ($this->form_validation->run() == FALSE)
      {
        $data['current']='city';
        $data['Countries'] = $this->Admin_model->get_countries();
        $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">validation Failed !'.validation_errors().'(</div>');
           $data['current']='city';
        $this->load->view('admin/addcities',$data);
      }
      else
      {

        // validation succeeds

        $data = array(
          'name' => $name,
           
          'state' => $state,
          'country_id' => $country_id,
         
          'timezone' =>$timezone,
        );

        // check if username and password is correct

        $result = $this->Admin_model->insert_city($data);
        if ($result) //active user record is present
        {
          $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">A New City has added !</div>');
          redirect('admin/getCities');
        }
        else
        {
          $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Unknown Error Occured ! </div>');
             $data['current']='city';
         $this->load->view('admin/addcities');
        }
      }
    }

   

    public  function create_event()
    {

      // get the posted values

      $name = $this->input->post("txt_event_name");
      $details = $this->input->post("txt_event_details");
      $start = $this->input->post("txt_event_start");
      $end= $this->input->post("txt_event_end");

       $status= $this->input->post("txt_event_status");
          
          

      // set validations

      $this->form_validation->set_rules("txt_event_name", "Event name", "trim|required");
      $this->form_validation->set_rules("txt_event_details", "Event details", "trim|required");
      $this->form_validation->set_rules("txt_event_start", "Event Start", "required");

        $this->form_validation->set_rules("txt_event_end", "Event End", "required");
        $this->form_validation->set_rules("txt_event_status", "Status", "required");


      if ($this->form_validation->run() == FALSE)
      {

        $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">validation Failed !</div>');
        $this->load->view('Admin/add_event');
      }
      else
      {

        // validation succeeds
  if($status == null)
  {
    $status=0;
  }
  else
  {
    $status=1;
  }
        $data = array(
          'event_name' => $name,
          'event_details' => $details,
          'start_date' => $start,
          'end_date' => $end,
          'status' =>$status,
        );

        // check if username and password is correct

        $result = $this->Admin_model->insert_events($data);
        if ($result) //active user record is present
        {
          $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">A New Event has added !</div>');
          redirect('admin/events');
        }
        else
        {
          $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Unknown Error Occured ! </div>');
         $this->load->view('admin/add_event');
        }
      }
    }



    public  function deleteCountry($id)
    {
      if ($this->Admin_model->delete_country($id))
      {
        if (!isset($_SESSION['username']))
        {
          redirect('account');
        }

        $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Country has been Deleted !</div>');
        redirect('admin/getCountries');
      }
      else
      {
        $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Country Deletion Failed!</div>');
        redirect('admin/getCountries');
      }

      // echo var_dump($data);

    }



    public  function deleteCity($id)
    {
      if ($this->Admin_model->delete_city($id))
      {
        if (!isset($_SESSION['username']))
        {
          redirect('account');
        }

        $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Country has been Deleted !</div>');
        redirect('admin/getCities');
      }
      else
      {
        $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Country Deletion Failed!</div>');
        redirect('admin/getCities');
      }

      // echo var_dump($data);

    }


  public function  options()
  {
       $data['options'] = $this->Admin_model->get_options();
       $this->load->view('admin/options',$data);
  }


  public function add_options()
  {
       $this->load->view('admin/add_options');
  }

  public function create_options()
    {
       
      $name=$this->input->post('txt_opt_name');
      
      $details=$this->input->post('txt_opt_details');

      $img=$this->input->post('image');

   $this->form_validation->set_rules('txt_opt_name', 'Option Name', 'trim|required|min_length[3]|max_length[100] ');
        $this->form_validation->set_rules('txt_opt_details', 'Details', 'trim|min_length[3]|max_length[130]');

      $config['upload_path']          = './assets/img/icons/options';
      $config['allowed_types']        = 'jpg|png';
      $config['max_size']             = 2000;
      $config['file_name']            = $name.'_'.time();


      $this->load->library('upload', $config);

      
      if ($this->form_validation->run() == FALSE)
            {
      $this->session->set_flashdata('msg','<div class="alert alert-danger text-center">Validation Error !</div>');
      $this->load->view('admin/add_options');

            }
      
      if ( ! $this->upload->do_upload('image'))
      {

        $this->session->set_flashdata('err',$this->upload->display_errors());
        $this->load->view('admin/add_options');
      }
      else
      {

   

   if ($this->form_validation->run() == FALSE)
            {
      $this->session->set_flashdata('msg','<div class="alert alert-danger text-center">validation Failed!</div>');
   

            }
            else
            {



        $data = array('upload_data' => $this->upload->data());

        $fileName=$this->upload->data('file_name');
        $fileName=$fileName;
        $filePath='/assets/img/icons/options/'.$fileName;
        $fileSize=$this->upload->data('file_size');
        $filetype=$this->upload->data('file_ext');

        
  $data = array( 
            'option_name' => $name,
            'option_details' => $details,
           
         
            'icon_name' => $filePath );

    if($this->Admin_model->insert_options($data))
    {



          $this->session->set_flashdata('msg','<div class="alert alert-success text-center">New Option Created!</div>');

      

      redirect('admin/options');

      }
      else
      {
    $this->session->set_flashdata('msg','<div class="alert alert-danger text-center">Option Creation Failed !</div>');
     $this->load->view('admin/add_options');
      }
          }
       }
    }






  public function  facilities()
  {
       $data['facilities'] = $this->Admin_model->get_facilities();
       $this->load->view('admin/facilities',$data);
  }


  public function add_facilities()
  {
       $this->load->view('admin/add_facilities');
  }

  public function create_facilities()
    {
       
      $name=$this->input->post('txt_fct_name');
      
      $details=$this->input->post('txt_fct_details');

      $img=$this->input->post('image');

   $this->form_validation->set_rules('txt_fct_name', 'Facility Name', 'trim|required|min_length[3]|max_length[100] ');
        $this->form_validation->set_rules('txt_opt_details', 'Details', 'trim|min_length[3]|max_length[130]');

      $config['upload_path']          = './assets/img/icons/facilities';
      $config['allowed_types']        = 'jpg|png';
      $config['max_size']             = 2000;
      $config['file_name']            = $name.'_'.time();


      $this->load->library('upload', $config);

      
      if ($this->form_validation->run() == FALSE)
            {
      $this->session->set_flashdata('msg','<div class="alert alert-danger text-center">Validation Error !</div>');
      $this->load->view('admin/add_facilities');

            }
      
      if ( ! $this->upload->do_upload('image'))
      {

        $this->session->set_flashdata('err',$this->upload->display_errors());
        $this->load->view('admin/add_facilities');
      }
      else
      {
   
   if ($this->form_validation->run() == FALSE)
            {
      $this->session->set_flashdata('msg','<div class="alert alert-danger text-center">validation Failed!</div>');
   

            }
            else
            {
   
        $data = array('upload_data' => $this->upload->data());

        $fileName=$this->upload->data('file_name');
        $fileName=$fileName;
        $filePath='/assets/img/icons/facilities/'.$fileName;
        $fileSize=$this->upload->data('file_size');
        $filetype=$this->upload->data('file_ext');

        
  $data = array( 
            'con_name' => $name,
            'con_details' => $details,
           
         
            'icon_name' => $filePath );

    if($this->Admin_model->insert_facilities($data))
    {



          $this->session->set_flashdata('msg','<div class="alert alert-success text-center">New Option Created!</div>');

      

      redirect('admin/facilities');

      }
      else
      {
    $this->session->set_flashdata('msg','<div class="alert alert-danger text-center">Option Creation Failed !</div>');
     $this->load->view('admin/add_facilities');
      }
          }
       }
    }




    public  function delete_option($id)
    {
      if ($this->Admin_model->delete_option($id))
      {
       

        $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Options has been Deleted !</div>');
        redirect('admin/options');
      }
      else
      {
        $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Options Deletion Failed!</div>');
        redirect('admin/options');
      }

      // echo var_dump($data);

    }

      public  function delete_facility($id)
    {
      if ($this->Admin_model->delete_facility($id))
      {
        

        $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Facility has been Deleted !</div>');
        redirect('admin/facilities');
      }
      else
      {
        $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Facility Deletion Failed!</div>');
        redirect('admin/facilities');
      }

      // echo var_dump($data);

    }
   
  }

