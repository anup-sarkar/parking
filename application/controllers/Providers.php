<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Providers extends CI_Controller {


 function __construct() 
  {
    parent::__construct();

    if (!isset($_SESSION['admin_email'])) {
       redirect('account');
    }
   
  }
  
  public function index()
  {
    $data['current']='providers';
    $data['Providers'] = $this->Providers_model->get();
    $this->load->view('providers/get',$data);
  }

 

  public function add()
  {
      $data['current']='providers';
      $this->load->view('providers/add',$data);
  }




 public function delete($id)
  {
    if ($this->Providers_model->delete($id))
    {
      $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Provider has been Deleted !</div>');
      redirect('Providers');
    }
    else
    {
      $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Provider Deletion Failed!</div>');
      redirect('Providers');
    }

    // echo var_dump($data);

  }


 public function details($id)
  {
      
      $data['current']='providers';

        $data['provider'] = $this->Providers_model->getById($id);
      $this->load->view('providers/details',$data);
  }




  

  public function account($id)
  {
    $data['current']='providers';
    $data['provider'] = $this->Providers_model->getById($id);
    $this->load->view('providers/details', $data);

    // echo var_dump($data);

  }






  public function create()
  {

    // get the posted values


    $this->form_validation->set_rules("txt_provider_name", "Provider name", "trim|required|min_length[3]|max_length[300]");

     $this->form_validation->set_rules("txt_provider_desig", "Provider Designation", "trim|min_length[3]|max_length[100]");

     $this->form_validation->set_rules("txt_provider_address", "Provider Address", "trim|min_length[3]|max_length[200]");

    $this->form_validation->set_rules('txt_provider_pass', 'Password', 'trim|required|md5|min_length[4]|max_length[40]');
    $this->form_validation->set_rules('txt_provider_email', 'Email ', 'trim|required|valid_email|is_unique[providers.email]');

    $this->form_validation->set_rules('txt_provider_mobile', 'Mobile Number', 'trim|min_length[9]|max_length[14] ');

    $name = $this->input->post("txt_provider_name");
    $desig = $this->input->post("txt_provider_desig");
    $address = $this->input->post("txt_provider_Address");
    $mob = $this->input->post("txt_provider_mobile");
    $email = $this->input->post("txt_provider_email");
    $pass = $this->input->post("txt_provider_pass");
    $type = $this->input->post("txt_provider_type");

    if ($this->form_validation->run() == FALSE)
    {

      // $_POST['txt_provider_name']=$name;
      // $_POST['txt_provider_desig']=$desig;
      // $_POST['txt_provider_Address']=$address;
      // $_POST['txt_provider_mobile']=$mob;
      // $_POST['txt_provider_email']=$email;
      // $_POST['txt_provider_pass']=$pass;
      // $_POST['txt_provider_type']=$type;

      // validation fails
        $data['current']='providers';
      $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">
       '.validation_errors().'</div>');

     

   
      $this->load->view('providers/add',$data);
    }
    else
    {

      // validation succeeds

      
        $data = array(
          'name' => $name,
          'designation' => $desig,
           'address' => $address,
            'mobile' => $mob,
             'email' => $email,
              'pass' => md5($pass),
               'type' => $type,
        );

        // check if username and password is correct

        $result = $this->Providers_model->insert($data);
        if ($result) //active user record is present
        {
          $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">A New Providers has added !</div>');
          redirect('Providers');
        }
        else
        {
          $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">insert Failed . Unknown Error ! </div>');
          $this->load->view('providers/add');
        }
      
    }
  }


 
  public function update()
  {

    // get the posted values

    $name = $this->input->post("txt_name");
    $desig = $this->input->post("txt_desig");
    $address = $this->input->post("txt_address");
    $mob = $this->input->post("txt_mobile");
    $email = $this->input->post("txt_email");
  
    $type = $this->input->post("txt_type");
    $status = $this->input->post("txt_status");
     $pass= $this->input->post("txt_pass");
    $id = $this->input->post("txt_id");

    $this->form_validation->set_rules("txt_name", "Provider name", "trim|required|min_length[3]|max_length[300]");

     $this->form_validation->set_rules("txt_desig", "Provider Designation", "trim|required|min_length[3]|max_length[100]");

     $this->form_validation->set_rules("txt_address", "Provider Address", "trim|min_length[3]|max_length[200]");

    $this->form_validation->set_rules('txt_email', 'Email ID', 'trim|required|valid_email');

    $this->form_validation->set_rules('txt_mobile', 'Mobile Number', 'trim|required|min_length[9]|max_length[14] ');


    $this->form_validation->set_rules('txt_pass', 'Password', 'trim|md5|min_length[4]|max_length[40]');


    if ($this->form_validation->run() == FALSE)
    {

      // validation fails
      $errors=validation_errors();
      $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">'.$errors.'</div>');

      $this->load->view('Providers/details/'.$id);
    }
    else
    {

      // validation succeeds

     if($pass==null)
     {
       $data = array(
         'name' => $name,
          'designation' => $desig,
           'address' => $address,
            'mobile' => $mob,
             'email' => $email,
            
               'type' => $type,
                'status'=> $status,

        );
     } 
     else
     {
       $data = array(
         'name' => $name,
          'designation' => $desig,
           'address' => $address,
            'mobile' => $mob,
             'email' => $email,
            
               'type' => $type,
               'pass'=>md5($pass),
                'status'=> $status,

        );
     }
       

        // check if username and password is correct

        $result = $this->Providers_model->update($data,$id);
        if ($result) //active user record is present
        {
          $this->session->set_flashdata('msg', '<div class="alert alert-success text-center"> Providers has been updated !</div>');
          redirect('Providers/details/'.$id);
        }
        else
        {
          $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">update Failed . Unknown Error ! </div>');
          redirect('Providers/details/'.$id);
          
        }
      
    }
  }





public function hash_password($password){
   return password_hash($password, PASSWORD_BCRYPT);
}

}

