<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Panel extends CI_Controller

{

    


  public function index()
  {
     
   if (!isset($_SESSION['panel_user']))
       {
      redirect('Panel/login');
        }

   $spot_id=$_SESSION['spot_id'];
    
 


   $data['timing'] = $this->Rates_model->get_open_close_array($spot_id);
   $data['flats'] = $this->Rates_model->get_flats_array($spot_id);


 


   $data['timings'] = $this->Rates_model->get($spot_id);


   //var_dump($this->Rates_model->get_open_close($spot_id));
   $data['event_rate'] = $this->Admin_model->get_event_rate();
   $data['flatrate'] = $this->Rates_model->get_flatrate($spot_id);
   $data['hourly_rate'] = $this->Rates_model->get_hourlyrate($spot_id);
   $data['monthlyrate'] = $this->Rates_model->get_monthlyrate($spot_id);
   $data['events'] = $this->Admin_model->get_events();

 $data['counts'] = $this->Reservations_model->get_count_by_id($spot_id);

  



    $this->load->view('panel/dashboard',$data);
  }


 public function bookings()
  {
      $pid=$_SESSION['spot_id'];
    
 $data["bookings"]=$this->Reservations_model->get_by_pid($pid);

    $this->load->view('panel/bookings',$data);
  }  

  public function login()
  {
    $this->load->view('panel/login');
  }

  public function logout()
  {
    $this->session->sess_destroy();
    redirect("panel");
  }

  public function info()
  {
    if (!isset($_SESSION['panel_user']))
       {
      redirect('Panel/login');
        }


    $id = $_SESSION['id'];
    $data['parkingspotinfo'] = $this->Panel_model->get($id);


    $city = $this->Panel_model->getCity($id);
    $data['cityname'] = $city->city;
    $data['timezone'] = $city->timezone;
    $this->load->view('panel/spot_info', $data);
  }

  public function update()
  {
    if (!isset($_SESSION['panel_user']))
       {
      redirect('Panel/login');
        }
    $id = $_SESSION['id'];
    $data['parkingspotinfo'] = $this->Panel_model->get($id);
    $this->load->view('panel/update', $data);
  }

  public function options()
  {
    if (!isset($_SESSION['panel_user']))
       {
      redirect('Panel/login');
        }
   

     $uid = $_SESSION['spot_id'];



    $data['options'] = $this->Admin_model->get_options();

    
    $data['options_map'] = $this->Admin_model->get_options_map($uid);
   
    $this->load->view('panel/set_options', $data);
  }



public function facilities()
  {
    if (!isset($_SESSION['panel_user']))
       {
      redirect('Panel/login');
        }
   

 $uid = $_SESSION['spot_id'];



    $data['facilities'] = $this->Admin_model->get_facilities();

    
    $data['facilities_map'] = $this->Admin_model->get_facilities_map($uid);
   
    $this->load->view('panel/set_facilities', $data);
  }


  public function account()
  {
    if (!isset($_SESSION['panel_user']))
       {
      redirect('Panel/login');
        }
   

  $uid = $_SESSION['id'];


    $data['current']='providers';
    $data['provider'] = $this->Providers_model->getById($uid);

    $this->load->view('panel/account', $data);
  }



 








  public function create()
  {

    // get the posted values


    $pname = $this->input->post("txt_parkingspot_pname");
    $sname = $this->input->post("txt_parkingspot_sname");
    $details = $this->input->post("txt_parkingspot_desc");
    $address = $this->input->post("txt_parkingspot_addr");
    $long = $this->input->post("txt_parkingspot_long");
    $lat = $this->input->post("txt_parkingspot_lat");
    $owner = $this->input->post("txt_parkingspot_owner");
    $city = $this->input->post("txt_parkingspot_city");
    $this->form_validation->set_rules("txt_parkingspot_pname", "Parkingspot Public Name", "trim|required|min_length[3]|max_length[300]");
    $this->form_validation->set_rules("txt_parkingspot_sname", "Parkingspot Short Name", "trim|min_length[3]|max_length[100]");
    $this->form_validation->set_rules("txt_parkingspot_addr", "Parkingspot Address", "trim|required|min_length[3]|max_length[200]");
    $this->form_validation->set_rules('txt_parkingspot_long', 'Longitude', 'trim|required|min_length[2]|max_length[60]');
    $this->form_validation->set_rules('txt_parkingspot_lat', 'Latitude', 'trim|required|min_length[2]|max_length[60]');
    if ($this->form_validation->run() == FALSE)
    {

      // validation fails

      $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">
         ' . validation_errors() . '</div>');
      $this->load->view('parkingspot/add');
    }
    else
    {

      // validation succeeds

      $data = array(
        'public_name' => $pname,
        'short_name' => $sname,
        'description' => $details,
        'address' => $address,
        'lat' => $lat,
        'long' => $long,
        'owner_id' => $owner,
        'city_id' => $city,
      );

      // check if username and password is correct

      $result = $this->ParkingSpot_model->insert($data);
      if ($result) //active user record is present
      {
        $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">A New Parking Spot has added !</div>');
        redirect('parkingspot');
      }
      else
      {
        $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">insert Failed . Unknown Error ! </div>');
        $this->load->view('parkingspot/add');
      }
    }
  }

  public function delete($id)
  {
    if (!isset($_SESSION['panel_user']))
       {
      redirect('Panel/login');
        }
    if ($this->ParkingSpot_model->delete($id))
    {
      $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">parkingspot has been Deleted !</div>');
      redirect('parkingspot');
    }
    else
    {
      $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">parkingspot Deletion Failed!</div>');
      redirect('parkingspot');
    }

    // echo var_dump($data);

  }

  public function details($id)
  {
    if (!isset($_SESSION['panel_user']))
       {
      redirect('Panel/login');
        }

    $data['parkingspot'] = $this->ParkingSpot_model->getById($id);
    $data['Cities'] = $this->Admin_model->get_cities();
    $data['Providers'] = $this->Providers_model->get();
    $this->load->view('parkingspots/details', $data);

    // echo var_dump($data);

  }

    public function images()
  {
    if (!isset($_SESSION['panel_user']))
       {
      redirect('Panel/login');
        }

    $id = $_SESSION['spot_id'];
    

 
    
   
    $data['images'] = $this->Panel_model->get_images($id);
    $this->load->view('panel/images', $data);

    // echo var_dump($data);

  }

    public function delete_images($id)
  {
    if (!isset($_SESSION['panel_user']))
       {
      redirect('Panel/login');
        }


    if ($this->Panel_model->delete_images($id))
    {
      $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Parkingspot image has been Deleted !</div>');
      redirect('panel/images');
    }
    else
    {
      $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Parkingspot image Deletion Failed!</div>');
      redirect('panel/images');
    }

    // echo var_dump($data);

  }


  public function update_data()
  {
    if (!isset($_SESSION['panel_user']))
       {
      redirect('Panel/login');
        }

    // get the posted values

    $id = $_SESSION['id'];
    $default = $this->input->post("txt_parkingspot_default");
    $total = $this->input->post("txt_parkingspot_total");
    $facility = $this->input->post("txt_parkingspot_facility");
    $vheight = $this->input->post("txt_parkingspot_vheight");
    $desc = $this->input->post("txt_parkingspot_details");
    $find = $this->input->post("txt_parkingspot_find");
    $redeem = $this->input->post("txt_parkingspot_redeem");
    $terms = $this->input->post("txt_parkingspot_term");
    $review = $this->input->post("txt_parkingspot_review");
    $status = $this->input->post("txt_parkingspot_status");
    $this->form_validation->set_rules("txt_parkingspot_default", "Default Space", "required");
    $this->form_validation->set_rules("txt_parkingspot_total", "Total Space", "required");
    $this->form_validation->set_rules("txt_parkingspot_facility", "Total facility", "required");
    $this->form_validation->set_rules('txt_parkingspot_vheight', 'Vehicle Heigh', 'trim|required|min_length[2]|max_length[50]');
    $this->form_validation->set_rules('txt_parkingspot_details', 'description', 'trim|required|min_length[2]|max_length[6000]');
    $this->form_validation->set_rules('txt_parkingspot_find', 'How to find', 'trim|required|min_length[2]|max_length[6000]');
    $this->form_validation->set_rules('txt_parkingspot_redeem', 'How to Redeem', 'trim|required|min_length[2]|max_length[6000]');
    $this->form_validation->set_rules('txt_parkingspot_term', 'Terms', 'trim|required|min_length[2]|max_length[6000]');
    $data['parkingspotinfo'] = $this->Panel_model->get($id);
    if ($this->form_validation->run() == FALSE)
    {

      // validation fails

      $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">
        validation Failed ! ' . validation_errors() . '</div>');
      $this->load->view('panel/update', $data);
    }
    else
    {

      // validation succeeds

      $data_new = array(
        'default_space' => $default,
        'total_space' => $total,
        'total_facility_space' => $facility,
        'vehicle_height_restriction' => $vheight,
        'descriptions' => $desc,
        'how_to_find' => $find,
        'how_to_redem' => $redeem,
        'term_and_condition' => $terms,
        'customer_review' => $review,
        'status' => $status,
      );

      // check if username and password is correct

      $result = $this->Panel_model->update($data_new, $id);
      if ($result > 0) //active user record is present
      {
        $this->session->set_flashdata('msg', '<div class="alert alert-success text-center"> Parking Spots Info has been updated !</div>');
        redirect('panel/info');

        // var_dump($data_new);

      }
      else
      {
        $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Update Failed . Unknown Error ! </div>');
        $this->load->view('parkingspot/update', $data);
      }
    }
  }

  public function update_options()
  {
    if (!isset($_SESSION['panel_user']))
       {
      redirect('Panel/login');
        }

    $status = $this->input->post("status");

    // option id

    $oid = $this->input->post("oid");
    $map_id = $this->input->post("map_id");

    // validation succeeds

    $spot_id = $_SESSION['spot_id'];

   
    
      if ($status == 1)
      {
        $data = array(
          'parkingspot_id' => $spot_id,
          'options_id' => $oid,
        );
        if ($this->Admin_model->insert_options_map($data)) echo "Success: Options Activated for your Parking Spot!";
        else echo "Error: Database  Failed!";
      }
      else
      {
        if ($this->Admin_model->delete_options_map($map_id)) echo "Success: Options Deactivated for your Parking Spot!";
        else echo "Error: Database Error Failed!";
      }
    
  }

  public function update_facilities()
  {
    $status = $this->input->post("status");

    // option id

    $fid = $this->input->post("fid");
    $map_id = $this->input->post("map_id");

    // validation succeeds

    $spot_id = $_SESSION['spot_id'];

   
    
      if ($status == 1)
      {
        $data = array(
          'parkingspot_id' => $spot_id,
          'facility_id' => $fid,
        );
        if ($this->Admin_model->insert_facilities_map($data)) echo "Success: Facility Activated for your Parking Spot!";
        else echo "Error: Database  Failed!";
      }
      else
      {
        if ($this->Admin_model->delete_facilities_map($map_id)) echo "Success: Facility Deactivated for your Parking Spot!";
        else echo "Error: Database Error Failed!";
      }
    
  }


public function add_images()
  {
   

     $uid = $_SESSION['id']; 
     $this->load->view('panel/add_images');
  }



  public function upload_images()
    {
      
    $uid = $_SESSION['spot_id']; 
      $name=$this->input->post('txt_img_name');
      
      $details=$this->input->post('txt_img_details');

      $img=$this->input->post('image');

   $this->form_validation->set_rules('txt_img_name', 'Option Name', 'trim|required|min_length[3]|max_length[100] ');
        $this->form_validation->set_rules('txt_img_details', 'Details', 'trim|min_length[3]|max_length[130]');

      $config['upload_path']          = './assets/uploads';
      $config['allowed_types']        = 'jpg|png';
      $config['max_size']             = 2000;
      $config['file_name']            = $name.'_'.time();


      $this->load->library('upload', $config);

      
      if ($this->form_validation->run() == FALSE)
            {
      $this->session->set_flashdata('msg','<div class="alert alert-danger text-center">Validation Error !</div>');
      $this->load->view('panel/add_images');

            }
      
      if ( ! $this->upload->do_upload('image'))
      {

        $this->session->set_flashdata('err',$this->upload->display_errors());
        $this->load->view('panel/add_images');
      }
      else
      {

 
   if ($this->form_validation->run() == FALSE)
            {
      $this->session->set_flashdata('msg','<div class="alert alert-danger text-center">validation Failed!</div>');
   

            }
            else
            {

 
        $data = array('upload_data' => $this->upload->data());

        $fileName=$this->upload->data('file_name');
        $fileName=$fileName;
        $filePath='assets/uploads/'.$fileName;
        $fileSize=$this->upload->data('file_size');
        $filetype=$this->upload->data('file_ext');

        
  $data = array( 
            'img_name' => $name,
            'img_details' => $details,
           
            'parkingspot_id'=>$uid,
            'img_address' => $filePath );

    if($this->Panel_model->insert_images($data))
    {
 
          $this->session->set_flashdata('msg','<div class="alert alert-success text-center">Parking Spot Image Uploaded !</div>');
         
         redirect("Panel/images");
 
      }
      else
      {
    $this->session->set_flashdata('msg','<div class="alert alert-danger text-center">Option Creation Failed !</div>');
     $this->load->view('admin/add_options');
      }
          }
       }
    }



  public  function login_validate()
  {

    // get the posted values

    $email = $this->input->post("panel_email");
    $password = $this->input->post("panel_password");

    // set validations

    $this->form_validation->set_rules("panel_email", "Email", "trim|required|valid_email");
    $this->form_validation->set_rules("panel_password", "Password", "trim|required");
    if ($this->form_validation->run() == FALSE)
    {

      // validation fails
      // $this->load->view('login');

      $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">validation Failed !</div>');
      $this->load->view('panel/login');
    }
    else
    {

      // validation succeeds
      // check if username and password is correct

      $usr_result = $this->Providers_model->user_validate($email, $password);
      if ($usr_result > 0) //active user record is present
      {

        // set the session variables

        $Providers_id = $this->Providers_model->getProviderId($email);

        
        $spot_ids = $this->Providers_model->getSpotId($Providers_id);

        if($spot_ids==null)
        {
           $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">No Parking Spot created for this account ! Please contact Parking Admin. </div>');
        $this->session->set_flashdata('notify', '  <script type="text/javascript">
        $(document).ready(function(){
          demo.initChartist();
            $.notify({
                icon: "ti-gift",
                message: "<b>Failed !</b> - Log in attempt unsuccessful."

            },{
                type: "danger",
                timer: 4000
            });

        });
    </script>');
        redirect('panel/login');
        return;
        }
        $sessiondata = array(
          'panel_user' => $email,
          'type' => 'provider',
          'id' => $Providers_id,
          'spot_id' => $spot_ids
        );
        
        $this->session->set_userdata($sessiondata);
 


        $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">successfully logged in your account!</div>');
        $this->session->set_flashdata('notify', '  <script type="text/javascript">
        $(document).ready(function(){
          demo.initChartist();
            $.notify({
                icon: "ti-gift",
                message: "<b>Successfully</b>  logged in your account !"

            },{
                type: "success",
                timer: 4000
            });

        });
    </script>');
        redirect('panel');
      }
      else
      {
        $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Invalid username and password! </div>');
        $this->session->set_flashdata('notify', '  <script type="text/javascript">
        $(document).ready(function(){
          demo.initChartist();
            $.notify({
                icon: "ti-gift",
                message: "<b>Failed !</b> - log in failed."

            },{
                type: "danger",
                timer: 4000
            });

        });
    </script>');
        $this->load->view('panel/login');
      }
    }
  }


    public function update_account()
  {

    if (!isset($_SESSION['panel_user']))
       {
      redirect('Panel/login');
        }

    // get the posted values

    $name = $this->input->post("txt_name");
    $desig = $this->input->post("txt_desig");
    $address = $this->input->post("txt_address");
    $mob = $this->input->post("txt_mobile");
    $email = $this->input->post("txt_email");
  
    $type = $this->input->post("txt_type");
    $status = $this->input->post("txt_status");
     $pass= $this->input->post("txt_pass");


    $id = $this->input->post("txt_id");

    $this->form_validation->set_rules("txt_name", "Provider name", "trim|required|min_length[3]|max_length[300]");

     $this->form_validation->set_rules("txt_desig", "Provider Designation", "trim|required|min_length[3]|max_length[100]");

     $this->form_validation->set_rules("txt_address", "Provider Address", "trim|min_length[3]|max_length[200]");

    $this->form_validation->set_rules('txt_email', 'Email ID', 'trim|required|valid_email');

    $this->form_validation->set_rules('txt_mobile', 'Mobile Number', 'trim|required|min_length[9]|max_length[14] ');


    $this->form_validation->set_rules('txt_pass', 'Password', 'trim|md5|min_length[4]|max_length[40]');


    if ($this->form_validation->run() == FALSE)
    {

      // validation fails
      $errors=validation_errors();
      $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">'.$errors.'</div>');

      $this->load->view('Providers/details/'.$id);
    }
    else
    {

      // validation succeeds

     if($pass==null)
     {
       $data = array(
         'name' => $name,
          'designation' => $desig,
           'address' => $address,
            'mobile' => $mob,
             'email' => $email,
            
               'type' => $type,
                'status'=> $status,

        );
     } 
     else
     {
       $data = array(
         'name' => $name,
          'designation' => $desig,
           'address' => $address,
            'mobile' => $mob,
             'email' => $email,
            
               'type' => $type,
               'pass'=>md5($pass),
                'status'=> $status,

        );
     }
       

        // check if username and password is correct

        $result = $this->Providers_model->update($data,$id);
        if ($result) //active user record is present
        {
          $this->session->set_flashdata('msg', '<div class="alert alert-success text-center"> Account Info  has been updated !</div>');
          redirect('panel/account/'.$id);
        }
        else
        {
          $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">update Failed . Unknown Error ! </div>');
          redirect('panel/account/'.$id);
          
        }
      
    }
  }







}