<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Reservations extends CI_Controller {
 


 public function payment()
    {
       $this->load->library('paypal_lib');
       $data = $this->input->post();
 

    $date = new DateTime();
       $timestamp= $date->getTimestamp()-1499999000;
  

    $serial='P'.$data["pid"].'U'.$data["uid"]."X".$timestamp;
    $rate=(int)$data["rate"];

       $temp=$rate-1;

         $profit=($temp*15)/100;

         $remit=$temp-$profit;

         $profit=$profit+1;
 
      $values = array('serial' => $serial, 
        'prior_amount' => $rate, 
        'base_rate' => $rate-1, 
        'remit_amount' => $remit, 
        'profit_amount' => $profit, 
        'spot_type' => $data["spot_type"],
         
        'user_name' => $data["f1_first_name"], 
        'user_mobile' => $data["f1_mobile"], 
        'user_email' => $data["f1_email"], 
        'v_type' => $data["type"], 
        'v_make' => $data["make"], 
        'v_model' => $data["model"], 
        'v_year' => (int)$data["year"], 
        'v_color' => $data["color"], 
        'v_state' => $data["state"], 
        'v_plate' => $data["plate"], 
        'pid' => $data["pid"], 
        'spot' => $data["spot"], 
        'uid' => $data["uid"], 
        'duration_day' => (int)$data["day"], 
        'duration_hour' => (int)$data["hour"], 
        'arrive' => $data["arrive"], 
        'depart' => $data["depart"], 
       


    );

      if($this->Reservations_model->insert($values))
      {
         $returnURL = base_url().'index.php/Reservations/success'; //payment success url
        $cancelURL = base_url().'index.php/Reservations/cancel'; //payment cancel url
        $notifyURL = base_url().'index.php/Reservations/ipn'; //ipn url
        
    
        // Add fields to paypal form
        $this->paypal_lib->add_field('return', $returnURL);
        $this->paypal_lib->add_field('cancel_return', $cancelURL);
        $this->paypal_lib->add_field('notify_url', $notifyURL);
        $this->paypal_lib->add_field('item_name', "Parking Spot Reservations");
        $this->paypal_lib->add_field('custom', $serial);
        $this->paypal_lib->add_field('item_number',  $data["pid"]);
        $this->paypal_lib->add_field('amount',  $rate);
        
        // Load paypal form
        $this->paypal_lib->paypal_auto_form();
      }
      else
      {
        echo "Unknown Error Occured";
         $data["status"]="Failed (DB Insert Error) ";
      $this->load->view('search/failed',$data);
      }

 


    //echo "serial: ".$serial;
 
        
    }

function ticket($ticket=null)
{

    
    $t=$this->input->get('ticket');

    if($t!=null)
      $ticket=$t;

    
    $data["ticket"]=$ticket;


    if($ticket !=null)
      $data['tickets']=$this->Reservations_model->get_ticket($ticket);

// /var_dump($this->Reservations_model->get_ticket($ticket));
   $this->load->view('search/ticket',$data);
}



     function success(){
        // Get the transaction data
        $paypalInfo = $this->input->get();
 

      $serial=$paypalInfo["cm"];
      $data["email"]=0;
      $data["serial"]= $serial;

      if($this->Reservations_model->check_status($serial))
      {
             $data["serial"]=$serial;
             $transaction=$this->Reservations_model->get_by_serial($serial);
         
          if($transaction==null)
            echo "No transaction data found !";
          else
          {
            $email=$transaction[0]['user_email'];
           
            $pid=$transaction[0]['pid'];
             $status=$transaction[0]['status'];


            if($status==1)
            {
                if($this->Account_model->send_mail($email,$transaction))
                 {
                  $data["email"]=1;
                    
                 } else
                 {  
                   
                 }
             $this->Reservations_model->update_total_available($pid);
             $this->Reservations_model->update_default_total_available($pid);

             $data["email_id"]=$email; 
             $this->load->view('search/success', $data);
            }elseif($status==0)
            {
              //processing
                $data["err"]=0;
     
                $this->load->view('search/failed',$data);
            }else
            {
                //failed
             $data["err"]=-1;
     
                $this->load->view('search/failed',$data);
            }
            
    

          }
 
        
      }
      else
      {
    
        //not found
         $data["err"]=1;
     
        $this->load->view('search/failed',$data);
      }
        // Pass the transaction data to view
        //
    }
     
     function cancel(){
        // Load payment failed view
        
            $paypalInfo = $this->input->post();

      
    
        foreach ($paypalInfo as $key => $value) {
          # code...
          echo $key;
          echo " : ";
          echo $value;
          echo "<br/>";
        }
 
 
 
      $data["status"]="Cancelled";
      
      $serial=$this->input->post('custom');
 
      $data["serial"]="null";
      $this->load->view('search/failed',$data);
     }


      public function mail($ticket=null){
        // Load payment failed view

       $t=$this->input->get('ticket');

    if($t!=null)
      $ticket=$t;

    
    $data["ticket"]=$ticket;


    if($ticket !=null)
      $data['trans']=$this->Reservations_model->get_ticket($ticket);
      $this->load->view('search/mail',$data);
     }
     
     function ipn(){
        // Paypal return transaction details array
      
          
           $this->load->library('paypal_lib');
           $paypalInfo = $this->input->post();
        
           $amount=$this->input->post('mc_gross');

           $paypal = array('payer_email' => $this->input->post('payer_email'),
                        'payer_id' =>$this->input->post('payer_id'),
                        'payer_status'=>$this->input->post('payer_status'),
                        'first_name' =>$this->input->post('first_name'),
                        'last_name' =>$this->input->post('last_name'),
                        'txn_id'=>$this->input->post('txn_id'),
                        'mc_currency'=>$this->input->post('mc_currency'),
                        'mc_gross'=>$amount,
                        'mc_fee'=>$this->input->post('mc_fee'),
                        'payment_status' =>$this->input->post('payment_status'),
                        'item_name' =>$this->input->post('item_name'),
                        'item_number' =>$this->input->post('item_number'),
                        'serial' =>$this->input->post('custom'),
                        'receiver_id' =>$this->input->post('receiver_id'),
                        'verify_sign' =>$this->input->post('verify_sign'),
                        'payment_date' =>$this->input->post('payment_date'),



         );

 

        $paypalURL = $this->paypal_lib->paypal_url;
        $result     = $this->paypal_lib->curlPost($paypalURL,$paypalInfo);
        $serial = $this->input->post('custom');
      
        
        // Check whether the payment is verified
        if(preg_match("/VERIFIED/i",$result)){
 
         $payment_id= $this->Reservations_model->insert_payment($paypal);
       
 
    //1=success
         $val = array('status' => 1,
                  'payment_id' => $payment_id,
                  'paid_amount' =>   $amount 
                

                   );

          $this->Reservations_model->update($serial,$val);
    
        }
        else
        {
 
      $payment_id= $this->Reservations_model->insert_payment($paypal);
      
      //-1=faild
         $val = array('status' => -1,
                  'payment_id' => $payment_id,
    
                  'paid_amount' =>   $paypalInfo['mc_gross'] ,
                    );

                   $this->Reservations_model->update($serial,$val);

        }
   
        
    }

 



}