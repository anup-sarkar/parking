<?php

/**
 * @property  email
 */
class Client_model extends CI_Model
{
  function __construct()
    {

        parent::__construct();
    }



  
    function search($q,$date )
     {
            $sql = "
              SELECT distinct ps.id as pid,ps.public_name,ps.short_name,ps.description,ps.address,ps.lat,ps.long,
            cities.name as city,  ifnull(flat_rate.rate,0)+ifnull(hourly_rate.rate,0)*24 as total_rate,

            p1.default_space,p1.total_space,p1.total_facility_space,p1.vehicle_height_restriction,p1.timezone,p1.descriptions,p1.how_to_find,p1.how_to_redem,p1.term_and_condition,
    
          op.isAvailable as op_available, op.isAlwaysOpen as op_isAlwaysOpen,op.open_time as op_open_time,op.close_time as op_close_time,op.total_available as op_total_available,op.date as openclose_date,
 
           flat_rate.rate as flt_rate,flat_rate.isAlwaysOpen as flt_isAlwaysOpen,flat_rate.open_time as flt_open_time ,flat_rate.close_time as flt_close_time,
              flat_rate.date as flt_date,flat_rate.status as flt_status,flat_rate.date as flatrate_date,
              
              hourly_rate.status as hourly_status ,hourly_rate.date as hourly_date,  hourly_rate.rate  as hourly_rate,
 
                default_flat_rate.rate as default_flat_rate, default_hourly_rate.rate as default_hourly_rate
                ,default_openclosetime.total_available as total_available_now,
                
                group_concat(so.option_name SEPARATOR ';;') as option_names,  group_concat(so.icon_name SEPARATOR ';;') as option_icons,
                
                 group_concat(mf.con_name SEPARATOR ';;') as facility_names,  group_concat(mf.icon_name SEPARATOR ';;') as facility_icons

            FROM 
              parkingspotinfo p1 
    
            left join 
              parkingspot ps on ps.id=p1.parking_spot_id AND ps.status=1
    
            left join 
              cities on cities.id=ps.city_id

            left join 
             openclosetime op on   op.parkingspot_id=ps.id and op.isAvailable=1   and 
             (op.date = '".$date."') 
 
            left join 
              flat_rate on  flat_rate.parkingspot_id=ps.id     and flat_rate.date=op.date and flat_rate.status=1
                            
             left join 
             hourly_rate on hourly_rate.parkingspot_id=ps.id  and hourly_rate.date=op.date and hourly_rate.status=1

               left join  
             default_flat_rate on default_flat_rate.parkingspot_id=ps.id
             
             LEFT JOIN 
             default_hourly_rate on default_hourly_rate.parkingspot_id=ps.id

             LEFT JOIN
             default_openclosetime on default_openclosetime.parkingspot_id=ps.id
       
             left join
             map_spot_options ms on ms.parkingspot_id=ps.id 
             
             left join 
             spot_options so on so.id=ms.options_id and so.status=1  
            
             
             left join
             map_spot_facilites mfid on mfid.parkingspot_id=ps.id 
             
             left join 
             spot_facilities mf on mf.id=mfid.facility_id and mf.status=1
             
 
         
            where  
              cities.name like '%".$q."%' or ps.address like '%".$q."%'    group by ps.id ";

          $query = $this->db->query($sql);
          return $query->result();
     }

     function search_custom($q,$date,$to,$day_total)
     {
      $sql="
        SELECT distinct ps.id as pid,ps.public_name,ps.short_name,ps.description,ps.address,ps.lat,ps.long,
            cities.name as city, ifnull(sum(flat_rate.rate),0)+ifnull(sum(hourly_rate.rate)*24,0) +  if(count(*) < '".$day_total."' ,(default_flat_rate.rate*('".$day_total."'-count(*))),0) as total_rate, 
             

            p1.default_space,p1.total_space,p1.total_facility_space,p1.vehicle_height_restriction,p1.timezone,p1.descriptions,p1.how_to_find,p1.how_to_redem,p1.term_and_condition,
    
          op.isAvailable as op_available, op.isAlwaysOpen as op_isAlwaysOpen,op.open_time as op_open_time,op.close_time as op_close_time,op.total_available as op_total_available,op.date as openclose_date,
    
        
    
           sum(flat_rate.rate) as flt_rate,flat_rate.isAlwaysOpen as flt_isAlwaysOpen,flat_rate.open_time as flt_open_time ,flat_rate.close_time as flt_close_time,
              flat_rate.date as flt_date,flat_rate.status as flt_status,flat_rate.date as flatrate_date,
              
              hourly_rate.status as hourly_status ,hourly_rate.date as hourly_date, sum(hourly_rate.rate) as hourly_rate,
              
              default_flat_rate.rate as default_flat_rate,
              default_hourly_rate.rate as default_hourly_rate
              ,default_openclosetime.total_available as total_available_now,
                
                group_concat(so.option_name SEPARATOR ';;') as option_names,  group_concat(so.icon_name SEPARATOR ';;') as option_icons,
                
                 group_concat(mf.con_name SEPARATOR ';;') as facility_names,  group_concat(mf.icon_name SEPARATOR ';;') as facility_icons

            FROM 
              parkingspotinfo p1 
    
            left join 
              parkingspot ps on ps.id=p1.parking_spot_id  AND ps.status=1
    
            left join 
              cities on cities.id=ps.city_id

            left join 
             openclosetime op on op.parkingspot_id=ps.id
          

            left join 
              flat_rate on flat_rate.parkingspot_id=ps.id  and flat_rate.date=op.date  and flat_rate.status=1
              
             left join 
              hourly_rate on hourly_rate.parkingspot_id=ps.id and hourly_rate.status=1 and hourly_rate.date=op.date
              LEFT join 
              default_flat_rate on default_flat_rate.parkingspot_id=ps.id
              
              LEFT join 
              default_hourly_rate on default_hourly_rate.parkingspot_id=ps.id
             

              LEFT JOIN
             default_openclosetime on default_openclosetime.parkingspot_id=ps.id

       
             left join
             map_spot_options ms on ms.parkingspot_id=ps.id 
             
             left join 
             spot_options so on so.id=ms.options_id and so.status=1  
            
             
             left join
             map_spot_facilites mfid on mfid.parkingspot_id=ps.id 
             
             left join 
             spot_facilities mf on mf.id=mfid.facility_id and mf.status=1


             where 
              op.isAvailable=1 and op.date>='".$date."'  and op.date<='".$to."'  and cities.name like '%".$q."%' 
              
              group by ps.id
       
      ";

         $query = $this->db->query($sql);
          return $query->result();
    
     }


       function search_by_address($q,$date,$addr)
     {
      $sql="
                  SELECT distinct ps.id as pid,ps.public_name,ps.short_name,ps.description,ps.address,ps.lat,ps.long,
            cities.name as city, sum(flat_rate.rate)+sum(hourly_rate.rate)*24 as total_rate,

            p1.default_space,p1.total_space,p1.total_facility_space,p1.vehicle_height_restriction,p1.timezone,p1.descriptions,p1.how_to_find,p1.how_to_redem,p1.term_and_condition,
    
          openclosetime.isAvailable as op_available, openclosetime.isAlwaysOpen as op_isAlwaysOpen,openclosetime.open_time as op_open_time,openclosetime.close_time as op_close_time,openclosetime.total_available as op_total_available,openclosetime.date as openclose_date,
    
        
    
           sum(flat_rate.rate) as flt_rate,flat_rate.isAlwaysOpen as flt_isAlwaysOpen,flat_rate.open_time as flt_open_time ,flat_rate.close_time as flt_close_time,
              flat_rate.date as flt_date,flat_rate.status as flt_status,flat_rate.date as flatrate_date,
              
              hourly_rate.status as hourly_status ,hourly_rate.date as hourly_date, sum(hourly_rate.rate) as hourly_rate,
    
           default_flat_rate.rate as default_flat_rate, default_hourly_rate.rate as default_hourly_rate
              ,default_openclosetime.total_available as total_available_now,
                
                group_concat(so.option_name SEPARATOR ';;') as option_names,  group_concat(so.icon_name SEPARATOR ';;') as option_icons,
                
                 group_concat(mf.con_name SEPARATOR ';;') as facility_names,  group_concat(mf.icon_name SEPARATOR ';;') as facility_icons

            FROM 
              parkingspotinfo p1 
    
            left join 
              parkingspot ps on ps.id=p1.parking_spot_id  AND ps.status=1
    
            left join 
              cities on cities.id=ps.city_id

            left join 
             openclosetime on openclosetime.parkingspot_id=ps.id
          

            left join 
              flat_rate on flat_rate.parkingspot_id=ps.id  and flat_rate.date='".$date."'  and flat_rate.status=1
              
             left join 
              hourly_rate on hourly_rate.parkingspot_id=ps.id and hourly_rate.status=1 and hourly_rate.date='".$date."' 


               left join  
             default_flat_rate on default_flat_rate.parkingspot_id=ps.id
             
             LEFT JOIN 
             default_hourly_rate on default_hourly_rate.parkingspot_id=ps.id
         
             LEFT JOIN
             default_openclosetime on default_openclosetime.parkingspot_id=ps.id
            
             
             left join
             map_spot_options ms on ms.parkingspot_id=ps.id 
             
             left join 
             spot_options so on so.id=ms.options_id and so.status=1  
            
             
             left join
             map_spot_facilites mfid on mfid.parkingspot_id=ps.id 
             
             left join 
             spot_facilities mf on mf.id=mfid.facility_id and mf.status=1

             where 
              openclosetime.isAvailable=1 and openclosetime.date='".$date."' 
              
                and ps.address like '".$addr."' and cities.name='".$q."' 
                
               
              
              GROUP by ps.id
              
              
              
              
              union 
              
                    SELECT distinct ps.id as pid,ps.public_name,ps.short_name,ps.description,ps.address,ps.lat,ps.long,
            cities.name as city,sum(flat_rate.rate)+sum(hourly_rate.rate)*24 as total_rate,

            p1.default_space,p1.total_space,p1.total_facility_space,p1.vehicle_height_restriction,p1.timezone,p1.descriptions,p1.how_to_find,p1.how_to_redem,p1.term_and_condition,
    
          openclosetime.isAvailable as op_available, openclosetime.isAlwaysOpen as op_isAlwaysOpen,openclosetime.open_time as op_open_time,openclosetime.close_time as op_close_time,openclosetime.total_available as op_total_available,openclosetime.date as openclose_date,
    
        
    
           sum(flat_rate.rate) as flt_rate,flat_rate.isAlwaysOpen as flt_isAlwaysOpen,flat_rate.open_time as flt_open_time ,flat_rate.close_time as flt_close_time,
              flat_rate.date as flt_date,flat_rate.status as flt_status,flat_rate.date as flatrate_date,
              
              hourly_rate.status as hourly_status ,hourly_rate.date as hourly_date, sum(hourly_rate.rate) as hourly_rate,

                default_flat_rate.rate as default_flat_rate, default_hourly_rate.rate as default_hourly_rate
    
        ,default_openclosetime.total_available as total_available_now,
                
                group_concat(so.option_name SEPARATOR ';;') as option_names,  group_concat(so.icon_name SEPARATOR ';;') as option_icons,
                
                 group_concat(mf.con_name SEPARATOR ';;') as facility_names,  group_concat(mf.icon_name SEPARATOR ';;') as facility_icons

              
            FROM 
              parkingspotinfo p1 
    
            left join 
              parkingspot ps on ps.id=p1.parking_spot_id  AND ps.status=1
    
            left join 
              cities on cities.id=ps.city_id

            left join 
             openclosetime on openclosetime.parkingspot_id=ps.id
          

            left join 
              flat_rate on flat_rate.parkingspot_id=ps.id  and flat_rate.date='".$date."' and flat_rate.status=1
              
             left join 
              hourly_rate on hourly_rate.parkingspot_id=ps.id and hourly_rate.status=1 and hourly_rate.date='".$date."'  
             
               left join  
             default_flat_rate on default_flat_rate.parkingspot_id=ps.id
             
             LEFT JOIN 
             default_hourly_rate on default_hourly_rate.parkingspot_id=ps.id
         
             LEFT JOIN
             default_openclosetime on default_openclosetime.parkingspot_id=ps.id
 
             left join
             map_spot_options ms on ms.parkingspot_id=ps.id 
             
             left join 
             spot_options so on so.id=ms.options_id and so.status=1  
            
             
             left join
             map_spot_facilites mfid on mfid.parkingspot_id=ps.id 
             
             left join 
             spot_facilities mf on mf.id=mfid.facility_id and mf.status=1

             where 
              openclosetime.isAvailable=1 and openclosetime.date='".$date."' 

                and ps.address not like '".$addr."' and cities.name='".$q."' 
                
               
              
              GROUP by ps.id
       
      ";

         $query = $this->db->query($sql);
          return $query->result();
     }



        function search_by_address_date($q,$date,$to,$addr,$day_total)
     {
      $sql="SELECT distinct ps.id as pid,ps.public_name,ps.short_name,ps.description,ps.address,ps.lat,ps.long,
            cities.name as city, ifnull(sum(flat_rate.rate),0)+ifnull(sum(hourly_rate.rate)*24,0) +  if(count(*) < '".$day_total."' ,(default_flat_rate.rate*('".$day_total."'-count(*))),0) as total_rate, 
             

            p1.default_space,p1.total_space,p1.total_facility_space,p1.vehicle_height_restriction,p1.timezone,p1.descriptions,p1.how_to_find,p1.how_to_redem,p1.term_and_condition,
    
          op.isAvailable as op_available, op.isAlwaysOpen as op_isAlwaysOpen,op.open_time as op_open_time,op.close_time as op_close_time,op.total_available as op_total_available,op.date as openclose_date,
    
        
    
           sum(flat_rate.rate) as flt_rate,flat_rate.isAlwaysOpen as flt_isAlwaysOpen,flat_rate.open_time as flt_open_time ,flat_rate.close_time as flt_close_time,
              flat_rate.date as flt_date,flat_rate.status as flt_status,flat_rate.date as flatrate_date,
              
              hourly_rate.status as hourly_status ,hourly_rate.date as hourly_date, sum(hourly_rate.rate) as hourly_rate,
              
              default_flat_rate.rate as default_flat_rate,
              default_hourly_rate.rate as default_hourly_rate
    
               
              ,default_openclosetime.total_available as total_available_now,
                
                group_concat(so.option_name SEPARATOR ';;') as option_names,  group_concat(so.icon_name SEPARATOR ';;') as option_icons,
                
                 group_concat(mf.con_name SEPARATOR ';;') as facility_names,  group_concat(mf.icon_name SEPARATOR ';;') as facility_icons

 
              
 
         
              
            FROM 
              parkingspotinfo p1 
    
            left join 
              parkingspot ps on ps.id=p1.parking_spot_id  AND ps.status=1
    
            left join 
              cities on cities.id=ps.city_id

            left join 
             openclosetime op on op.parkingspot_id=ps.id
          

            left join 
              flat_rate on flat_rate.parkingspot_id=ps.id  and flat_rate.date=op.date  and flat_rate.status=1
              
             left join 
              hourly_rate on hourly_rate.parkingspot_id=ps.id and hourly_rate.status=1 and hourly_rate.date=op.date
              LEFT join 
              default_flat_rate on default_flat_rate.parkingspot_id=ps.id
              
              LEFT join 
              default_hourly_rate on default_hourly_rate.parkingspot_id=ps.id
             
              LEFT JOIN
             default_openclosetime on default_openclosetime.parkingspot_id=ps.id

          
             left join
             map_spot_options ms on ms.parkingspot_id=ps.id 
             
             left join 
             spot_options so on so.id=ms.options_id and so.status=1  
            
             
             left join
             map_spot_facilites mfid on mfid.parkingspot_id=ps.id 
             
             left join 
             spot_facilities mf on mf.id=mfid.facility_id and mf.status=1
             
             where 
              op.isAvailable=1 and op.date>='".$date."'  and op.date<='".$to."'  and cities.name='".$q."'  and address='".$addr."'
              
              group by ps.id

              UNION


            SELECT distinct ps.id as pid,ps.public_name,ps.short_name,ps.description,ps.address,ps.lat,ps.long,
            cities.name as city, ifnull(sum(flat_rate.rate),0)+ifnull(sum(hourly_rate.rate)*24,0) +  if(count(*) < 1 ,(default_flat_rate.rate*(1-count(*))),0) as total_rate, 
            

            p1.default_space,p1.total_space,p1.total_facility_space,p1.vehicle_height_restriction,p1.timezone,p1.descriptions,p1.how_to_find,p1.how_to_redem,p1.term_and_condition,
    
          op.isAvailable as op_available, op.isAlwaysOpen as op_isAlwaysOpen,op.open_time as op_open_time,op.close_time as op_close_time,op.total_available as op_total_available,op.date as openclose_date,
    
        
    
           sum(flat_rate.rate) as flt_rate,flat_rate.isAlwaysOpen as flt_isAlwaysOpen,flat_rate.open_time as flt_open_time ,flat_rate.close_time as flt_close_time,
              flat_rate.date as flt_date,flat_rate.status as flt_status,flat_rate.date as flatrate_date,
              
              hourly_rate.status as hourly_status ,hourly_rate.date as hourly_date, sum(hourly_rate.rate) as hourly_rate,
              
              default_flat_rate.rate as default_flat_rate,
              default_hourly_rate.rate as default_hourly_rate
    
               ,default_openclosetime.total_available as total_available_now,
                
                group_concat(so.option_name SEPARATOR ';;') as option_names,  group_concat(so.icon_name SEPARATOR ';;') as option_icons,
                
                 group_concat(mf.con_name SEPARATOR ';;') as facility_names,  group_concat(mf.icon_name SEPARATOR ';;') as facility_icons

              
 
              
 
         
              
            FROM 
              parkingspotinfo p1 
    
            left join 
              parkingspot ps on ps.id=p1.parking_spot_id  AND ps.status=1
    
            left join 
              cities on cities.id=ps.city_id

            left join 
             openclosetime op on op.parkingspot_id=ps.id
          

            left join 
              flat_rate on flat_rate.parkingspot_id=ps.id  and flat_rate.date=op.date  and flat_rate.status=1
              
             left join 
              hourly_rate on hourly_rate.parkingspot_id=ps.id and hourly_rate.status=1 and hourly_rate.date=op.date
              LEFT join 
              default_flat_rate on default_flat_rate.parkingspot_id=ps.id
              
              LEFT join 
              default_hourly_rate on default_hourly_rate.parkingspot_id=ps.id
             LEFT JOIN
             default_openclosetime on default_openclosetime.parkingspot_id=ps.id
      
             left join
             map_spot_options ms on ms.parkingspot_id=ps.id 
             
             left join 
             spot_options so on so.id=ms.options_id and so.status=1  
            
             
             left join
             map_spot_facilites mfid on mfid.parkingspot_id=ps.id 
             
             left join 
             spot_facilities mf on mf.id=mfid.facility_id and mf.status=1
             
             where 
              op.isAvailable=1 and op.date>='".$date."'  and op.date<='".$to."'  and cities.name='".$q."'  and address!='".$addr."'
              






               
      ";

         $query = $this->db->query($sql);
          return $query->result();
     }


function search_partial($q,$date){

  $sql=" SELECT distinct ps.id as pid,ps.public_name,ps.short_name,ps.description,ps.address,ps.lat,ps.long,
            cities.name as city,  ifnull(flat_rate.rate,0)+ifnull(hourly_rate.rate,0)*24 as total_rate,

            p1.default_space,p1.total_space,p1.total_facility_space,p1.vehicle_height_restriction,p1.timezone,p1.descriptions,p1.how_to_find,p1.how_to_redem,p1.term_and_condition,
    
          op.isAvailable as op_available, op.isAlwaysOpen as op_isAlwaysOpen,op.open_time as op_open_time,op.close_time as op_close_time,op.total_available as op_total_available,op.date as openclose_date,
    
        
    
           flat_rate.rate as flt_rate,flat_rate.isAlwaysOpen as flt_isAlwaysOpen,flat_rate.open_time as flt_open_time ,flat_rate.close_time as flt_close_time,
              flat_rate.date as flt_date,flat_rate.status as flt_status,flat_rate.date as flatrate_date,
              
              hourly_rate.status as hourly_status ,hourly_rate.date as hourly_date,  hourly_rate.rate  as hourly_rate,
    
            default_flat_rate.rate as default_flat_rate, default_hourly_rate.rate as default_hourly_rate,
                
                group_concat(so.option_name SEPARATOR ';;') as option_names,  group_concat(so.icon_name SEPARATOR ';;') as option_icons,
                
                 group_concat(mf.con_name SEPARATOR ';;') as facility_names,  group_concat(mf.icon_name SEPARATOR ';;') as facility_icons

              
            FROM 
              parkingspotinfo p1 
    
            left join 
              parkingspot ps on ps.id=p1.parking_spot_id  AND ps.status=1
    
            left join 
              cities on cities.id=ps.city_id

            left join 
             openclosetime op on   op.parkingspot_id=ps.id and op.isAvailable=1   and 
             (op.date = '".$date."') 
              
              
          

            left join 
              flat_rate on     flat_rate.parkingspot_id=ps.id     and flat_rate.date=op.date and flat_rate.status=1
                            
             left join 
             hourly_rate on hourly_rate.parkingspot_id=ps.id  and hourly_rate.date=op.date and hourly_rate.status=1
         

             LEFT join 
              default_flat_rate on default_flat_rate.parkingspot_id=ps.id
              
              LEFT join 
              default_hourly_rate on default_hourly_rate.parkingspot_id=ps.id

              LEFT JOIN
             default_openclosetime on default_openclosetime.parkingspot_id=ps.id
 
             left join
             map_spot_options ms on ms.parkingspot_id=ps.id 
             
             left join 
             spot_options so on so.id=ms.options_id and so.status=1  
            
             
             left join
             map_spot_facilites mfid on mfid.parkingspot_id=ps.id 
             
             left join 
             spot_facilities mf on mf.id=mfid.facility_id and mf.status=1

            where  
              ps.address like '%".$q."%' or cities.name like '%".$q."%'    group by ps.id";



         $query = $this->db->query($sql);
          return $query->result();
         //return $sql;
}


public function get_spot_info($pid)

{
      $sql="
            SELECT   ps.id as pid,ps.public_name,ps.short_name,ps.description,ps.address,ps.lat,ps.long,
            cities.name as city ,

            p1.default_space,p1.total_space,p1.total_facility_space,p1.vehicle_height_restriction,p1.timezone,p1.descriptions,p1.how_to_find,p1.how_to_redem,
            p1.term_and_condition,
                
                group_concat(so.option_name SEPARATOR ';;') as option_names,  group_concat(so.icon_name SEPARATOR ';;') as option_icons,
                
                 group_concat(mf.con_name SEPARATOR ';;') as facility_names,  group_concat(mf.icon_name SEPARATOR ';;') as facility_icons

 
              
            FROM 
              parkingspotinfo p1 
    
            left join 
              parkingspot ps on ps.id=p1.parking_spot_id  AND ps.status=1
              
            left join 
              cities on cities.id=ps.city_id

            
             left join
             map_spot_options ms on ms.parkingspot_id=ps.id 
             
             left join 
             spot_options so on so.id=ms.options_id and so.status=1  
            
             
             left join
             map_spot_facilites mfid on mfid.parkingspot_id=ps.id 
             
             left join 
             spot_facilities mf on mf.id=mfid.facility_id and mf.status=1
              
              
              where  ps.id='".$pid."'";

          $query = $this->db->query($sql);
          return $query->result_array();
}






  function insert_client($data)
    {
    return $this->db->insert('client', $data);
    }


  function insert_vehicle($data)
    {
    return $this->db->insert('vehicle', $data);
    }

    


     public function get_vehicle($id)
    {
        
         $this->db->select('*');
          $this->db->from('vehicle'); /*I assume that film was the table name*/
        
          $this->db->where('client_id', $id);

        $sql= $this->db->get();

        
        $result=$sql->result();

        return $result;
    }


 function user_validate($email, $pwd)
     {
          $sql = "select * from client where email = '" . $email . "' and pass = '" . md5($pwd) . "' and type='registered' and status = '1'";
          $query = $this->db->query($sql);
          $count=$query->num_rows();
     
          return $count;
     }
   




    public function get_client($email,$pwd)
    {
        
          $sql = "select * from client where email = '" . $email . "' and pass = '" . md5($pwd) . "' and status = '1'";
          $query = $this->db->query($sql);
          $result=$query->result_array();
     
           

        return $result;
    }

 



     public function get_client_by_id($id)
    {
        $this->load->database();

        $query = $this->db->get_where('client', array('id' => $id));
        return $query->result_array();
    }

       public function get_vehicle_array($id)
    {
        
         $this->db->select('*');
          $this->db->from('vehicle'); /*I assume that film was the table name*/
        
          $this->db->where('client_id', $id);

        $sql= $this->db->get();

        
        $result=$sql->result_array();

        return $result;
    }


    public function getProviderId($email)
    {
      $this->db->select('id');
      $this->db->from('providers');
      $this->db->where('email',$email);
      $reault_array = $this->db->get()->result_array();
      return $reault_array[0]['id'];
    }


 

     public function update_client($data,$id)
    {
        $this->load->database();
       
        $this->db->where('id', $id);
       
        $this->db->update('client',$data);
        return true;
    }


public function delete($id)
{
    $this->db->where('id', $id);
    $this->db->delete('providers');
    if($this->db->affected_rows() > 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}

    }
           

 
  

