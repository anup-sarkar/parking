<?php
/**
 * @property  email
 */
class admin_model extends CI_Model

  {
  function __construct()
    {
    $this->load->library('email');

    // Call the Model constructor

    parent::__construct();
    }

  // insert into user table

  function user_validate($email, $pwd)
    {
    $sql = "select * from users where email = '" . $email . "' and pass = '" . md5($pwd) . "' and status = '1'";
    $query = $this->db->query($sql);
    return $query->num_rows();
    }

  function get_user_type($usr, $pwd)
    {
    $sql = "select type from users where email = '" . $usr . "' and password = '" . md5($pwd) . "' and status = '1'";
    $query = $this->db->query($sql);
    return $query;
    }

  // send verification email to user's email id

  function insert_Country($data)
    {
    return $this->db->insert('countries', $data);
    }

  function insert_city($data)
    {
    return $this->db->insert('cities', $data);
    }

  public

  function get_countries()
    {
    $this->load->database();
    $sql = $this->db->get("countries");
    $result = $sql->result();
    return $result;
    }

  function insert_events($data)
    {
    return $this->db->insert('events', $data);
    }

  public function get_events()
    {
    $this->load->database();

    // $sql= $this->db->query("Select * from blog");
    // Active Record

    $sql = $this->db->get("events");
    $this->db->where('status', '1');
    $result = $sql->result();
    return $result;
    }

  public  function get_cities()
    {

    // $sql= $this->db->query("Select * from blog");
    // Active Record

    $this->db->select("*,countries.name as country,cities.name as city,cities.id as cid");
    $this->db->from("cities");
    $this->db->join('countries', 'countries.id = cities.country_id ');
    $sql = $this->db->get();
    $result = $sql->result();
    return $result;
    }


    public  function get_address()
    {

    // $sql= $this->db->query("Select * from blog");
    // Active Record

    $this->db->select("parkingspot.address as address,cities.name as city_name ");
    $this->db->from("parkingspot");
    $this->db->join('cities', 'cities.id = parkingspot.city_id ');
    $sql = $this->db->get();
    $result = $sql->result();
    return $result;
    }

  public function update_event_status($data, $id)
    {
    $this->load->database();
    $this->db->where('id', $id);
    $this->db->update('events', $data);
    return true;
    }

  public

  function insert_eventrate($data, $p_id, $e_id)
    {
    $sql = $this->db->get("event_rate");
    $sql = "SELECT * FROM event_rate WHERE parkingspot_id=" . $p_id . " and event_id=" . $e_id . "";
    $query = $this->db->query($sql);

    // cheking if row exists or not

    $result = $query->row();
    if ($result == null)
      {

      // echo "<script type='text/javascript'>console.log('true');</script>";

      return $this->db->insert('event_rate', $data);
      }
      else
      {

      // echo "<script type='text/javascript'>console.log('true');</script>";

      $this->db->where('id', $result->id);
      return $this->db->update('event_rate', $data);
      }
    }

  public

  function get_event_rate()
    {
    $this->load->database();

    // $sql= $this->db->query("Select * from blog");
    // Active Record

    $sql = $this->db->get("event_rate");
    $result = $sql->result();
    return $result;
    }

  public  function getStudentById($id)
    {
    $this->load->database();
    $query = $this->db->get_where('students', array(
      'id' => $id
    ));
    return $query->result();
    }

  public function getStudentBySid($sid)
    {
    $this->load->database();
    $query = $this->db->get_where('students', array(
      'sid' => $sid
    ));
    if ($query->num_rows() > 0)
      {
      return $query->result();
      }
      else
      {
      return false;
      }
    }

  public

  function updateCountry($country, $id)
    {
    $this->load->database();
    $this->db->where('id', $id);
    $this->db->update('countries', $country);
    return true;
    }

  public

  function delete_Country($id)
    {
    $this->db->where('id', $id);
    $this->db->delete('countries');
    if ($this->db->affected_rows() > 0)
      {
      return true;
      }
      else
      {
      return false;
      }
    }

  public

  function delete_city($id)
    {
    $this->db->where('id', $id);
    $this->db->delete('cities');
    if ($this->db->affected_rows() > 0)
      {
      return true;
      }
      else
      {
      return false;
      }
    }

  public

  function deleteDept($id)
    {
    $this->db->where('id', $id);
    $this->db->delete('departments');
    if ($this->db->affected_rows() > 0)
      {
      return true;
      }
      else
      {
      return false;
      }
    }

  public function upload_file_db($file_info)
    {
    $this->load->database();
    $this->db->insert('uploads', $file_info);
    }

  function insert_options($data)
    {
    return $this->db->insert('spot_options', $data);
    }

  public function get_options()
    {
    $this->load->database();
    $sql = $this->db->get("spot_options");
    $result = $sql->result();
    return $result;
    }

  public function delete_option($id)
    {
    $this->db->where('id', $id);
    $this->db->delete('spot_options');
    if ($this->db->affected_rows() > 0)
      {
      return true;
      }
     else
      {
      return false;
      }
    }

  function insert_facilities($data)
    {
    return $this->db->insert('spot_facilities', $data);
    }

  public function get_facilities()
    {
    $this->load->database();
    $sql = $this->db->get("spot_facilities");
    $result = $sql->result();
    return $result;
    }

  public function delete_facility($id)
    {
    $this->db->where('id', $id);
    $this->db->delete('spot_facilities');
    if ($this->db->affected_rows() > 0) return true;  
    else return false;
   
    }

  public function insert_options_map($data)
    {
    return $this->db->insert('map_spot_options', $data);
    }

      public  function get_options_map($id)
    {
    $this->load->database();
    $query = $this->db->get_where('map_spot_options', array(
      'parkingspot_id' => $id
    ));
    return $query->result();
    }


  public function delete_options_map($id)
    {
    $this->db->where('id', $id);
    $this->db->delete('map_spot_options');
    if ($this->db->affected_rows() > 0) return true;  
    else return false;
   
    }



  public function insert_facilities_map($data)
    {
    return $this->db->insert('map_spot_facilites', $data);
    }

      public  function get_facilities_map($id)
    {
    $this->load->database();
    $query = $this->db->get_where('map_spot_facilites', array(
      'parkingspot_id' => $id
    ));
    return $query->result();
    }


  public function delete_facilities_map($id)
    {
    $this->db->where('id', $id);
    $this->db->delete('map_spot_facilites');
    if ($this->db->affected_rows() > 0) return true;  
    else return false;
   
    }


  }


 
