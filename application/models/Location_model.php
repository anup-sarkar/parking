<?php
 
 
class Location_model extends CI_Model
{
  function __construct()
    {

        parent::__construct();
    }



  
    function search($date,$cords,$addr )
     {
            $sql = "
              SELECT distinct ps.id as pid,ps.public_name,ps.short_name,ps.description,ps.address,ps.lat,ps.long,
            cities.name as city,  ifnull(flat_rate.rate,0)+ifnull(hourly_rate.rate,0)*24 as total_rate,

            p1.default_space,p1.total_space,p1.total_facility_space,p1.vehicle_height_restriction,p1.timezone,p1.descriptions,p1.how_to_find,p1.how_to_redem,p1.term_and_condition,
    
          op.isAvailable as op_available, op.isAlwaysOpen as op_isAlwaysOpen,op.open_time as op_open_time,op.close_time as op_close_time,op.total_available as op_total_available,op.date as openclose_date,
 
           flat_rate.rate as flt_rate,flat_rate.isAlwaysOpen as flt_isAlwaysOpen,flat_rate.open_time as flt_open_time ,flat_rate.close_time as flt_close_time,
              flat_rate.date as flt_date,flat_rate.status as flt_status,flat_rate.date as flatrate_date,
              
              hourly_rate.status as hourly_status ,hourly_rate.date as hourly_date,  hourly_rate.rate*4  as hourly_rate,hourly_rate.rate  as hourly_rate_base,
 
                default_flat_rate.rate as default_flat_rate, default_hourly_rate.rate as default_hourly_rate
                ,default_openclosetime.total_available as total_available_now,
                
                group_concat(so.option_name SEPARATOR ';;') as option_names,  group_concat(so.icon_name SEPARATOR ';;') as option_icons,
                
                 group_concat(mf.con_name SEPARATOR ';;') as facility_names,  group_concat(mf.icon_name SEPARATOR ';;') as facility_icons

            FROM 
              parkingspotinfo p1 
    
            left join 
              parkingspot ps on ps.id=p1.parking_spot_id 
    
            left join 
              cities on cities.id=ps.city_id

            left join 
             openclosetime op on   op.parkingspot_id=ps.id and op.isAvailable=1   and 
             (op.date = '".$date."') 
 
            left join 
              flat_rate on  flat_rate.parkingspot_id=ps.id     and flat_rate.date=op.date and flat_rate.status=1
                            
             left join 
             hourly_rate on hourly_rate.parkingspot_id=ps.id  and hourly_rate.date=op.date and hourly_rate.status=1

               left join  
             default_flat_rate on default_flat_rate.parkingspot_id=ps.id
             
             LEFT JOIN 
             default_hourly_rate on default_hourly_rate.parkingspot_id=ps.id

             LEFT JOIN
             default_openclosetime on default_openclosetime.parkingspot_id=ps.id
       
             left join
             map_spot_options ms on ms.parkingspot_id=ps.id 
             
             left join 
             spot_options so on so.id=ms.options_id and so.status=1  
            
             
             left join
             map_spot_facilites mfid on mfid.parkingspot_id=ps.id 
             
             left join 
             spot_facilities mf on mf.id=mfid.facility_id and mf.status=1
             
 
         
            where  
         
              (ps.lat between '".$cords['minlat']."' and '".$cords['maxlat']."') AND   
              (ps.long between '".$cords['minlng']."' and '".$cords['maxlng']."')   

            AND ps.status=1
      
             
             group by ps.id
 

              ORDER BY ps.address desc                               

              
               ";

              

          $query = $this->db->query($sql);
          return $query->result();
     }


   function search_multiple($date,$cords,$day,$hours)
     {

        $total_hour=($day*24)+$hours;


            $sql = "
              SELECT distinct ps.id as pid,ps.public_name,ps.short_name,ps.description,ps.address,ps.lat,ps.long,
            cities.name as city,  ifnull(flat_rate.rate,0)*'".$day."'+ifnull(hourly_rate.rate,0)*'".$total_hour."' as total_rate,

            p1.default_space,p1.total_space,p1.total_facility_space,p1.vehicle_height_restriction,p1.timezone,p1.descriptions,p1.how_to_find,p1.how_to_redem,p1.term_and_condition,
    
          op.isAvailable as op_available, op.isAlwaysOpen as op_isAlwaysOpen,op.open_time as op_open_time,op.close_time as op_close_time,op.total_available as op_total_available,op.date as openclose_date,
 
           flat_rate.rate*'".$day."' as flt_rate,flat_rate.isAlwaysOpen as flt_isAlwaysOpen,flat_rate.open_time as flt_open_time ,flat_rate.close_time as flt_close_time,
              flat_rate.date as flt_date,flat_rate.status as flt_status,flat_rate.date as flatrate_date,
              
              hourly_rate.status as hourly_status ,hourly_rate.date as hourly_date,  hourly_rate.rate*'".$total_hour."'  as hourly_rate, hourly_rate.rate  as hourly_rate_base,
 
                default_flat_rate.rate*'".$day."' as default_flat_rate, default_hourly_rate.rate as default_hourly_rate
                ,default_openclosetime.total_available as total_available_now,
                
                group_concat(so.option_name SEPARATOR ';;') as option_names,  group_concat(so.icon_name SEPARATOR ';;') as option_icons,
                
                 group_concat(mf.con_name SEPARATOR ';;') as facility_names,  group_concat(mf.icon_name SEPARATOR ';;') as facility_icons

            FROM 
              parkingspotinfo p1 
    
            left join 
              parkingspot ps on ps.id=p1.parking_spot_id 
    
            left join 
              cities on cities.id=ps.city_id

            left join 
             openclosetime op on   op.parkingspot_id=ps.id and op.isAvailable=1   and 
             (op.date = '".$date."') 
 
            left join 
              flat_rate on  flat_rate.parkingspot_id=ps.id     and flat_rate.date=op.date and flat_rate.status=1
                            
             left join 
             hourly_rate on hourly_rate.parkingspot_id=ps.id  and hourly_rate.date=op.date and hourly_rate.status=1

               left join  
             default_flat_rate on default_flat_rate.parkingspot_id=ps.id
             
             LEFT JOIN 
             default_hourly_rate on default_hourly_rate.parkingspot_id=ps.id

             LEFT JOIN
             default_openclosetime on default_openclosetime.parkingspot_id=ps.id
       
             left join
             map_spot_options ms on ms.parkingspot_id=ps.id 
             
             left join 
             spot_options so on so.id=ms.options_id and so.status=1  
            
             
             left join
             map_spot_facilites mfid on mfid.parkingspot_id=ps.id 
             
             left join 
             spot_facilities mf on mf.id=mfid.facility_id and mf.status=1
             
 
         
            where  
         
              (ps.lat between '".$cords['minlat']."' and '".$cords['maxlat']."') AND   
              (ps.long between '".$cords['minlng']."' and '".$cords['maxlng']."')   

           
      AND ps.status=1
             
             group by ps.id
 

              ORDER BY ps.address desc                               

              
               ";

              
 
          $query = $this->db->query($sql);
          return $query->result();
     }



    function search_day_multiple($date,$cords,$from,$to,$addr ,$day_total)
     {
            $sql = "
              SELECT distinct ps.id as pid,ps.public_name,ps.short_name,ps.description,ps.address,ps.lat,ps.long,
            cities.name as city, ifnull(sum(flat_rate.rate),0)+ifnull(sum(hourly_rate.rate)*24,0) +  if(count(*) < '".$day_total."' ,(default_flat_rate.rate*('".$day_total."'-count(*))),0) as total_rate, 


            p1.default_space,p1.total_space,p1.total_facility_space,p1.vehicle_height_restriction,p1.timezone,p1.descriptions,p1.how_to_find,p1.how_to_redem,p1.term_and_condition,
    
          op.isAvailable as op_available, op.isAlwaysOpen as op_isAlwaysOpen,op.open_time as op_open_time,op.close_time as op_close_time,op.total_available as op_total_available,op.date as openclose_date,
 
           flat_rate.rate as flt_rate,flat_rate.isAlwaysOpen as flt_isAlwaysOpen,flat_rate.open_time as flt_open_time ,flat_rate.close_time as flt_close_time,
              flat_rate.date as flt_date,flat_rate.status as flt_status,flat_rate.date as flatrate_date,
              
              hourly_rate.status as hourly_status ,hourly_rate.date as hourly_date,  hourly_rate.rate  as hourly_rate,
 
                default_flat_rate.rate as default_flat_rate, default_hourly_rate.rate as default_hourly_rate
                ,default_openclosetime.total_available as total_available_now,
                
                group_concat(so.option_name SEPARATOR ';;') as option_names,  group_concat(so.icon_name SEPARATOR ';;') as option_icons,
                
                 group_concat(mf.con_name SEPARATOR ';;') as facility_names,  group_concat(mf.icon_name SEPARATOR ';;') as facility_icons

            FROM 
              parkingspotinfo p1 
    
            left join 
              parkingspot ps on ps.id=p1.parking_spot_id 
    
            left join 
              cities on cities.id=ps.city_id

            left join 
             openclosetime op on   op.parkingspot_id=ps.id and op.isAvailable=1   and 
             (op.date = '".$date."') 
 
            left join 
              flat_rate on  flat_rate.parkingspot_id=ps.id     and flat_rate.date=op.date and flat_rate.status=1
                            
             left join 
             hourly_rate on hourly_rate.parkingspot_id=ps.id  and hourly_rate.date=op.date and hourly_rate.status=1

               left join  
             default_flat_rate on default_flat_rate.parkingspot_id=ps.id
             
             LEFT JOIN 
             default_hourly_rate on default_hourly_rate.parkingspot_id=ps.id

             LEFT JOIN
             default_openclosetime on default_openclosetime.parkingspot_id=ps.id
       
             left join
             map_spot_options ms on ms.parkingspot_id=ps.id 
             
             left join 
             spot_options so on so.id=ms.options_id and so.status=1  
            
             
             left join
             map_spot_facilites mfid on mfid.parkingspot_id=ps.id 
             
             left join 
             spot_facilities mf on mf.id=mfid.facility_id and mf.status=1
             
 
         
            where  
         
              (ps.lat between '".$cords['minlat']."' and '".$cords['maxlat']."') AND   
              (ps.long between '".$cords['minlng']."' and '".$cords['maxlng']."') AND
               op.isAvailable=1 and op.date>='".$date."'  and op.date<='".$to."'
                
                AND ps.status=1
           
      
             
             group by ps.id
 

              ORDER BY ps.address desc                               

              
               ";

               echo $sql;

          $query = $this->db->query($sql);
          return $query->result();
     }


}