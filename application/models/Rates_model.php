<?php

/**
 * @property  email
 */
class rates_model extends CI_Model
{
	function __construct()
    {

        parent::__construct();
    }
	
	//insert into user table



	  
	//send verification email to user's email id
	

	function insert($data,$pid,$date)
    {
       

          $sql = "select * from openclosetime where parkingspot_id = '" . $pid . "' and date = '" . $date . "'  ";
          $query = $this->db->query($sql);
          
          $rows= $query->result();
    

      if($rows != null)
      {
          $id=$query->row()->id;
           $this->db->where("id = '" . $id . "' and date = '" . $date . "'  ");
       
          return  $this->db->update('openclosetime',$data);
      }
      else
      {
         return $this->db->insert('openclosetime', $data);
      }


		 
	}
    function insert_by_date($data,$pid,$date)
    {
      $query = $this->db->get_where('openclosetime', array('parkingspot_id' => $pid));
      $rows= $query->result();
    

      if($rows != null)
      {
          $id=$query->row()->id;
           $this->db->where('id', $id);
       
          return  $this->db->update('openclosetime',$data);
      }
      else
      {
         return $this->db->insert('openclosetime', $data);
      }


     
  }


  function insert_flatrate($data,$pid,$date)
    {
       $sql = "select * from flat_rate where parkingspot_id = '" . $pid . "' and date = '" . $date . "'  ";
          $query = $this->db->query($sql);
          
          $rows= $query->result();
    

      if($rows != null)
      {
          $id=$query->row()->id;
           $this->db->where("id = '" . $id . "' and date = '" . $date . "'  ");
       
          return  $this->db->update('flat_rate',$data);
      }
      else
      {
         return $this->db->insert('flat_rate', $data);
      }


     
  }


  public function get_default($id)
    {
        
         $this->db->select('*');
          $this->db->from('default_openclosetime'); /*I assume that film was the table name*/
        
          $this->db->where('parkingspot_id', $id);

        $sql= $this->db->get();

        
        $result=$sql->row();

        return $result;
    }

     public function get_flatrate_default($id)
    {
        
         $this->db->select('*');
          $this->db->from('default_flat_rate'); /*I assume that film was the table name*/
        
          $this->db->where('parkingspot_id', $id);

        $sql= $this->db->get();

        
        $result=$sql->row();

        return $result;
    }


  public function get_hour_default($id)
    {
        
         $this->db->select('*');
          $this->db->from('default_hourly_rate'); /*I assume that film was the table name*/
        
          $this->db->where('parkingspot_id', $id);

        $sql= $this->db->get();

        
        $result=$sql->row();

        return $result;

  }  

  function insert_default($data,$pid)
    {
      $query = $this->db->get_where('default_openclosetime', array('parkingspot_id' => $pid));
      $rows= $query->result();
    

      if($rows != null)
      {
          $id=$query->row()->id;
           $this->db->where('id', $id);
       
          return  $this->db->update('default_openclosetime',$data);
      }
      else
      {
         return $this->db->insert('default_openclosetime', $data);
      }


     
  }



  function insert_hour_default($data,$pid)
    {
      $query = $this->db->get_where('default_hourly_rate', array('parkingspot_id' => $pid));
      $rows= $query->result();
    

      if($rows != null)
      {
          $id=$query->row()->id;
           $this->db->where('id', $id);
       
          return  $this->db->update('default_hourly_rate',$data);
      }
      else
      {
         return $this->db->insert('default_hourly_rate', $data);
      }


     
  }

  function insert_flatrate_default($data,$pid)
    {
      $query = $this->db->get_where('default_flat_rate', array('parkingspot_id' => $pid));
      $rows= $query->result();
    
      
      
      if($rows != null)
      {
          $id=$query->row()->id;
           $this->db->where('id', $id);
       
          return  $this->db->update('default_flat_rate',$data);
      }
      else
      {
         return $this->db->insert('default_flat_rate', $data);
      }


     
  }


  function insert_monthlyrate($data,$pid)
    {
      $query = $this->db->get_where('monthly_rate', array('parkingspot_id' => $pid));
      $rows= $query->result();
    

      if($rows != null)
      {
          $id=$query->row()->id;
           $this->db->where('id', $id);
       
          return  $this->db->update('monthly_rate',$data);
      }
      else
      {
         return $this->db->insert('monthly_rate', $data);
      }


     
  }


 

    function insert_hourly_rate($data,$pid,$date)
    {
       $sql = "select * from hourly_rate where parkingspot_id = '" . $pid . "' and date = '" . $date . "'  ";
          $query = $this->db->query($sql);
          
          $rows= $query->result();
    

      if($rows != null)
      {
          $id=$query->row()->id;
           $this->db->where("id = '" . $id . "' and date = '" . $date . "'  ");
       
          return  $this->db->update('hourly_rate',$data);
      }
      else
      {
         return $this->db->insert('hourly_rate', $data);
      }


     
  }


		
  

   



  public function get($id)
    {
        
         $this->db->select('*');
		      $this->db->from('openclosetime'); /*I assume that film was the table name*/
		    
		      $this->db->where('parkingspot_id', $id);

		    $sql= $this->db->get();

        
        $result=$sql->row();

        return $result;
    }


    public function get_open_close_array($id)
    {
        
         $this->db->select('*');
          $this->db->from('openclosetime'); /*I assume that film was the table name*/
        
          $this->db->where('parkingspot_id', $id);

        $result= $this->db->get()->result();

        
        //$result=$sql->row();

        return $result;
    }

  public function get_array($id)
    {
        
         $this->db->select('*');
          $this->db->from('openclosetime'); /*I assume that film was the table name*/
        
          $this->db->where('parkingspot_id', $id);

        $sql= $this->db->get();

        
        $result=$sql->result_array();

        return $result;
    }


  public function get_flats_array($id)
    {
        
         $this->db->select('*');
          $this->db->from('flat_rate'); /*I assume that film was the table name*/
        
          $this->db->where('parkingspot_id', $id);

        $sql= $this->db->get();

        
        $result=$sql->result();

        return $result;
    }

      public function get_timing_by_date($pid,$date)
    {
        
         $this->db->select('*');
        
         $this->db->from('openclosetime'); /*I assume that film was the table name*/
        
           
        $this->db->where("parkingspot_id = '" . $pid . "' and date = '" . $date . "'  ");

         $sql= $this->db->get();

         if($sql==null)
         {
          return false;
         }

        
        $result=$sql->result_array();

        return $result;
    }

  public function get_flat_timing_by_date($pid,$date)
    {
        
         $this->db->select('*');
        
         $this->db->from('flat_rate'); /*I assume that film was the table name*/
        
           
        $this->db->where("parkingspot_id = '" . $pid . "' and date = '" . $date . "'  ");

         $sql= $this->db->get();

         if($sql==null)
         {
          return false;
         }

        
        $result=$sql->result();

        return $result;
    }



     public function get_hourly_timing_by_date($pid,$date)
    {
        
         $this->db->select('*');
        
         $this->db->from('hourly_rate'); /*I assume that film was the table name*/
        
           
        $this->db->where("parkingspot_id = '" . $pid . "' and date = '" . $date . "'  ");

         $sql= $this->db->get();

         if($sql==null)
         {
          return false;
         }

        
        $result=$sql->result();

        return $result;
    }





    public function get_flatrate($id)
    {
        
         $this->db->select('*');
          $this->db->from('flat_rate'); /*I assume that film was the table name*/
        
          $this->db->where('parkingspot_id', $id);                                                     

        $sql= $this->db->get();

        
        $result=$sql->row();

        return $result;
    }

        public function get_flatrate_array($id)
    {
        
         $this->db->select('*');
          $this->db->from('flat_rate'); /*I assume that film was the table name*/
        
          $this->db->where('parkingspot_id', $id);                                                     

        $sql= $this->db->get();

        
        $result=$sql->result_array();

        return $result;
    }

  public function get_monthlyrate()
    {
        
         $this->db->select('*');
          $this->db->from('monthly_rate'); /*I assume that film was the table name*/
        
    

        $sql= $this->db->get();

        
        $result=$sql->row();

        return $result;
    }

     public function get_hourlyrate($id)
    {
        
         $this->db->select('*');
          $this->db->from('hourly_rate'); /*I assume that film was the table name*/
        
          $this->db->where('parkingspot_id', $id);                                                     

        $sql= $this->db->get();

        
        $result=$sql->result();

        return $result;
        return $result;
    }





     public function getById($id)
    {
        $this->load->database();

        $query = $this->db->get_where('providers', array('id' => $id));
        return $query->result();
    }

    public function getProviderId($email)
    {
      $this->db->select('id');
      $this->db->from('providers');
      $this->db->where('email',$email);
      $reault_array = $this->db->get()->result_array();
      return $reault_array[0]['id'];
    }


    public function getSpotId($id)
    {
      $this->db->select('id');
      $this->db->from('ParkingSpot');
      $this->db->where('owner_id',$id);
      $reault_array = $this->db->get()->result_array();
      return $reault_array[0]['id'];
    }




     public function update($data,$id)
    {
        $this->load->database();
       
        $this->db->where('id', $id);
       
        $this->db->update('providers',$data);
        return true;
    }


 public function update_hourly_rate($pid,$status)
    {
        $this->load->database();
       
        $this->db->where('parkingspot_id', $pid);
       
        $data = array('status' => $status );
        $this->db->update('hourly_rate',$data);
        return true;
    }

public function delete($id)
{
    $this->db->where('id', $id);
    $this->db->delete('providers');
    if($this->db->affected_rows() > 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}



  

}