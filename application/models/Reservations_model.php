<?php

/**
 * @property  email
 */
class Reservations_model extends CI_Model
{
	function __construct()
    {

        parent::__construct();
    }
	
	//insert into user table
 

     public function get_by_serial($serial)
    {
      $this->db->select('*');
      $this->db->from('transactions');
      $this->db->where('serial',$serial);
      $reault_array = $this->db->get()->result_array();
      return $reault_array;
    }

     public function get_ticket($serial)
    {
      $this->db->select('*');
      $this->db->from('transactions');
      $this->db->where('serial',$serial);
      $reault_array = $this->db->get()->result_array();
      return $reault_array;
    }





  public function get()
    {        
        $this->db->select('parkingspot.public_name as public_name,parkingspot.address as address,parkingspot.id as pid,transactions.serial as serial,transactions.paid_amount as paid_amount,transactions.remit_amount as remit_amount,transactions.profit_amount as profit_amount,transactions.spot_type as spot_type,transactions.arrive as starts,transactions.depart as ends,transactions.status as  status,transactions.user_name as user_name,transactions.user_email as user_email,transactions.user_mobile as mobile ');
         $this->db->from('transactions');
 
     

        $this->db->join('parkingspot', 'parkingspot.id = transactions.pid');
        //$this->db->where('transactions.status',1);
        $sql= $this->db->get();
 
        $result=$sql->result();

        return $result;

  }  

 
  public function get_by_pid($pid)
    {        
        $this->db->select('parkingspot.public_name as public_name,parkingspot.address as address,parkingspot.id as pid,transactions.serial as serial,transactions.paid_amount as paid_amount,transactions.remit_amount as remit_amount,transactions.profit_amount as profit_amount,transactions.spot_type as spot_type,transactions.arrive as starts,transactions.depart as ends,transactions.status as  status,transactions.user_name as user_name,transactions.user_email as user_email,transactions.user_mobile as mobile ');
         $this->db->from('transactions');
 
     

        $this->db->join('parkingspot', 'parkingspot.id = transactions.pid');

        $this->db->where('parkingspot.id',$pid);
        $this->db->where('transactions.status',1);

        $sql= $this->db->get();
 
        $result=$sql->result();

        return $result;

  } 

    public function get_providerwise_transaction()
    {        
        $this->db->select('parkingspot.public_name as public_name,parkingspot.address as address,parkingspot.id as pid,transactions.serial as serial,sum(transactions.paid_amount) as gross_amount,sum(transactions.remit_amount) as remit_amount,sum(transactions.profit_amount) as profit_amount,count(*) as total');
         $this->db->from('transactions');
 
     

        $this->db->join('parkingspot', 'parkingspot.id = transactions.pid');

   
        $this->db->where('transactions.status',1);
         $this->db->group_by('parkingspot.id'); 
 
        $sql= $this->db->get();
 
        $result=$sql->result();

        return $result;

  }  
 


   
    


  function insert($data)
  {
    return $this->db->insert('transactions', $data);
  }

    function insert_payment($data)
  {
     $this->db->insert('payment', $data);
     return $this->db->insert_id();
  }

    public function update( $sid,$data)
    {
    $this->load->database();
    $this->db->where('serial', $sid);
    $this->db->update('transactions', $data);
    return true;
    }


 public function get_count_by_id($pid)
    {
       
    $this->db->where('pid',$pid);
    $this->db->where('status',1);
    $result = $this->db->get('transactions')->num_rows();
    return $result;
    }

     public function check_status($serial)
    {
       
    $this->db->where('serial',$serial);
   
    $rows = $this->db->get('transactions')->result();
   
    if($rows>0) 
      return true;
    if($rows==0) 
      return false;

    }

 public function check_payment($serial)
    {
       
      $this->db->select('*');
      $this->db->from('payment');
      $this->db->where('serial',$serial);
      $rows = $this->db->get()->result_array();
   
    return $rows;
 

    }


 public function update_payment( $sid,$data)
    {
    $this->load->database();
    $this->db->where('serial', $sid);
    $this->db->update('payment', $data);
    return true;
    }

     public function update_total_available($pid)
    {
        $this->load->database();
       
        $this->db->where('parkingspot_id', $pid);
        $this->db->set('total_available', 'total_available-1', FALSE);
       
        $this->db->update('default_openclosetime');
        return true;
    }



 public function update_default_total_available($pid)
    {
        $this->load->database();
       
        $this->db->where('parkingspot_id', $pid);
        $this->db->set('total_available', 'total_available-1', FALSE);
        $this->db->update('openclosetime');
        return true;
    }













 public function get_gross_sum($pid)
  {
       $this->db->select_sum('paid_amount');
    $this->db->from('transactions');
  $this->db->where('pid',$pid);
    $this->db->where('status',1);
    $query = $this->db->get();
    if($query->row()->paid_amount==null)
    {
      return 0.00;
    }
    return $query->row()->paid_amount;
  }

   public function get_remit_sum($pid)
  {
       $this->db->select_sum('remit_amount');
    $this->db->from('transactions');
  $this->db->where('pid',$pid);
    $this->db->where('status',1);
    $query = $this->db->get();

    if($query->row()->remit_amount==null)
    {
      return 0.000;
    }
    return $query->row()->remit_amount;
  }


 



  public function get_gross_sum_admin()
  {
       $this->db->select_sum('paid_amount');
    $this->db->from('transactions');
 
    
    $query = $this->db->get();

      if($query->row()->paid_amount==null)
    {
      return 0.00;
    }


    return $query->row()->paid_amount;
  }

   public function get_remit_sum_admin()
  {
       $this->db->select_sum('remit_amount');
    $this->db->from('transactions');
 
 
    $query = $this->db->get();

      if($query->row()->remit_amount==null)
    {
      return 0.00;
    }

    return $query->row()->remit_amount;
  }


     public function get_profit_sum_admin()
  {
       $this->db->select_sum('profit_amount');
    $this->db->from('transactions');
  
    $this->db->where('status',1);
    $query = $this->db->get();
    if($query->row()->profit_amount==null)
    {
      return 0.00;
    }
    return $query->row()->profit_amount;
  }



   public function get_count($pid)
    {
       
 
    $this->db->where('status',1);
    $this->db->where('pid',$pid);
    $result = $this->db->get('transactions')->num_rows();
    echo $result;
    }

      public function get_count_admin()
    {
       
 
    $this->db->where('status',1);
    $result = $this->db->get('transactions')->num_rows();
    echo $result;
    }


      public function get_failed_count($pid)
    {
       
 
    $this->db->where('status',0);
    $this->db->where('pid',$pid);
    $result = $this->db->get('transactions')->num_rows();
    echo $result;
    }

      public function get_failed_count_admin()
    {
       
 
    $this->db->where('status',0);
    $result = $this->db->get('transactions')->num_rows();
    echo $result;
    }


    

 





}