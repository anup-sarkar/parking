
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
 
  <footer  style="background-color: black;color: white;">
         <div class="row" style="padding: 50px">
            <div class="col-md-2">
               <img src="<?php echo base_url(); ?>assets/img/logo.png" style="width: 45%;margin-top: 20px">
               <br/>
               <small class="d-block mb-3 text-muted2">&copy; 2017-2018</small>
            </div>
            <div class="col-md-3">
               <h3>Features</h3>
               <ul class="list-unstyled text-small">
                  <li><a class="text-muted2" href="#">Monthly Rent</a></li>
                  <li><a class="text-muted2" href="#">Weekly Rent</a></li>
                  <li><a class="text-muted2" href="#">Event Rent</a></li>
               </ul>
            </div>
            <div class="col-md-3">
               <h3>Work With Us</h3>
               <ul class="list-unstyled text-small">
                  <li><a class="text-muted2" href="<?php echo site_url('admin'); ?>">Admin Login</a></li>
                  <li><a class="text-muted2" href="<?php echo site_url('panel'); ?>">Parking Owners Login</a></li>
                  <li><a class="text-muted2" href="#">Venue</a></li>
                  <li><a class="text-muted2" href="#">Producs</a></li>
                  <li><a class="text-muted2" href="#">App</a></li>
               </ul>
            </div>
            <div class="col-md-2">
               <h3>Follow Us</h3>
               <ul class="list-unstyled text-small">
                  <li><a class="text-muted2" href="#"><i class="fa fa-facebook-official" aria-hidden="true"></i> Facebook</a></li>
                  <li><a class="text-muted2" href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i> Twitter</a></li>
                  <li><a class="text-muted2" href="#"><i class="fa fa-instagram" aria-hidden="true"></i> Instagram</a></li>
                  <li><a class="text-muted2" href="#"><i class="fa fa-google-plus" aria-hidden="true"></i> Google Plus</a></li>
               </ul>
            </div>
            <div class="col-md-2">
               <h3>Support</h3>
               <ul class="list-unstyled text-small">
                  <li><a class="text-muted2" href="#">About </a></li>
                  <li><a class="text-muted2" href="#">Contact Us</a></li>
                  <li><a class="text-muted2" href="#">FAQ</a></li>
                  <li><a class="text-muted2" href="#">Term & Conditions</a></li>
               </ul>
            </div>
         </div>
      </footer>

<style type="text/css">
  
     .text-muted2
   {
      color: #02afdc;
      font-weight: bold;
   }
</style>
 


     <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-3.1.1.min.js"></script>

<!-- <script src="<?php echo base_url(); ?>assets/js/jquery-3.1.1.slim.min.js"  ></script>

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
 --> 
 
<script src="<?php echo base_url(); ?>assets/boot4/bootstrap3-typeahead.min.js"></script>
<link href="<?php echo base_url(); ?>assets/cal/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">

<link href="<?php echo base_url(); ?>assets/css/site.css" rel="stylesheet"  >

<script type="text/javascript" src="<?php echo base_url(); ?>assets/cal/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script src="<?php echo base_url(); ?>assets/boot4/bootstrap.min.js"></script>

<link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
 
   
