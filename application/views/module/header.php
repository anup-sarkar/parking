<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Hello-Hi Admin Panel</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="<?php echo base_url(); ?>assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Paper Dashboard core CSS    -->
    <link href="<?php echo base_url(); ?>assets/css/paper-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="<?php echo base_url(); ?>assets/css/demo.css" rel="stylesheet" />


    <!--  Fonts and icons     -->
  <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">

    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    
    <link href="<?php echo base_url(); ?>assets/css/themify-icons.css" rel="stylesheet">
     <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url(); ?>assets/img/favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url(); ?>assets/img/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url(); ?>assets/img/favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>assets/img/favicon/favicon-16x16.png">
<link rel="manifest" href="<?php echo base_url(); ?>assets/img/favicon/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="<?php echo base_url(); ?>assets/img/favicon/ms-icon-144x144.png">
 


<style type="text/css">
    .sidebar-wrapper
    {
        /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#fcfff4+0,dfe5d7+40,b3bead+100;Wax+3D+%233 */
background: #fcfff4; /* Old browsers */
background: -moz-linear-gradient(top, #fcfff4 0%, #dfe5d7 40%, #b3bead 100%); /* FF3.6-15 */
background: -webkit-linear-gradient(top, #fcfff4 0%,#dfe5d7 40%,#b3bead 100%); /* Chrome10-25,Safari5.1-6 */
background: linear-gradient(to bottom, #fcfff4 0%,#dfe5d7 40%,#b3bead 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fcfff4', endColorstr='#b3bead',GradientType=0 ); /* IE6-9 */
    }
</style>
</head>
<body>

<div class="wrapper">
    <div class="sidebar" data-background-color="white" data-active-color="danger">

    <!--
		Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
		Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
	-->

    	<div class="sidebar-wrapper">
            <div class="logo" style="padding: 0">
                <a href="" class="simple-text">
                    <img src="<?php echo base_url(); ?>assets/img/logo_new.png" style="width: 130px;margin-top: 10px">
                </a>
            </div>

            <ul class="nav" style="margin:0">
               
                <li class="<?php if($isactive=="dashboard") echo "active"; ?>" >
                    <a href="<?php echo site_url('admin'); ?>">
                        <i class="ti-panel"></i>
                        <p>Dashboard</p>
                    </a>
                </li>

                 <li class="<?php if($isactive=="parkingspot") echo "active"; ?>">
                    <a href="<?php echo site_url('parkingspot'); ?>"  
                        >
                        <i class="ti-map"></i>
                        <p>Locations</p>
                    </a>
                </li>
             

                <li class="<?php if($isactive=="providers") echo "active"; ?>" >
                    <a href="<?php echo site_url('providers'); ?>"  >
                        <i class="ti-user"></i>
                        <p>Accounts</p>
                    </a>
                </li>

                 <li  class="<?php if($isactive=="city") echo "active"; ?>">
                    <a href="<?php echo site_url('admin/getCities'); ?>" >
                        <i class="ti-map"></i>
                        <p>Cities</p>
                    </a>
                </li>
          
                 <li class="<?php if($isactive=="country") echo "active"; ?>">
                    <a href="<?php echo site_url('admin/getCountries'); ?>" >
                        <i class="ti-map-alt"></i>
                        <p>Countries</p>
                    </a>
                </li>

                
                <li class="<?php if($isactive=="events") echo "active"; ?>">
                    <a href="<?php echo site_url('admin/events'); ?>" >
                        <i class="ti-crown"></i>
                        <p>Events</p>
                    </a>
                </li>

                 <li class="<?php if($isactive=="options") echo "active"; ?>">
                    <a href="<?php echo site_url('admin/options'); ?>" >
                        <i class="ti-check"></i>
                        <p>Location Amenities</p>
                    </a>
                </li>

                 <li class="<?php if($isactive=="Facilities") echo "active"; ?>">
                    <a href="<?php echo site_url('admin/Facilities'); ?>" >
                        <i class="ti-wheelchair"></i>
                        <p>Spot Facilities</p>
                    </a>
                </li>
                
                <li class="<?php if($isactive=="bookings") echo "active"; ?>">
                    <a href="<?php echo site_url('admin/bookings'); ?>" >
                        <i class="ti-credit-card"></i>
                        <p>Bookings</p>
                    </a>
                </li>
              

                       
				
            </ul>
    	</div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <a class="navbar-brand" href="#">Admin Panel</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                     
                         <li>
                            <a href="">
                                <i class="ti-user"></i>
                                <p><?php echo $_SESSION['admin_email']; ?></p>
                            </a>
                        </li>

                         <li>
                            <a href="<?php echo site_url('account/signup'); ?>" >
                                <i class="ti-pencil"></i>
                                <p>Register</p>
                            </a>
                        </li>

						<li>
                            <a href="<?php echo site_url('account/logout'); ?>">
								<i class="ti-back-left"></i>
								<p>Logout</p>
                            </a>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>