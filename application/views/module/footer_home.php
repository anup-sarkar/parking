


<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
       
        <footer class="footer" style="background-color: #eee">
            <div class="container-fluid">
                <nav class="pull-left">
                    <ul>

                        <li>
                            <a href="">
                                <img src="<?php echo base_url(); ?>assets/img/logo_sm.png" style="padding-bottom: 4px;width:110px" />
                            </a>
                        </li>
                        <li>
                            <a href="" style="color: #68B3C8">
                               About
                            </a>
                        </li>
                        <li>
                            <a href="" style="color: #68B3C8">
                                Sitemap
                            </a>
                        </li>
                    </ul>
                </nav>
                <div class="copyright pull-right">
                    <a href="http://brosoft.biz/site/">
                    &copy; <script>document.write(new Date().getFullYear())</script>  <img src="<?php echo base_url(); ?>assets/img/brosoft.png" style="padding-bottom: 4px;width: 80px;" />&nbsp;&nbsp;|&nbsp;  All Right Reserved  </a>
                </div>
            </div>
        </footer>


    </div>
</div>


</body>

    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-3.1.1.min.js"></script>


     <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

    <!-- Paper Dashboard Core javascript and methods for Demo purpose -->
	   <script src="<?php echo base_url(); ?>assets/js/bootstrap-checkbox-radio.js"></script>

    <!--  Charts Plugin -->
    <script src="<?php echo base_url(); ?>assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="<?php echo base_url(); ?>assets/js/bootstrap-notify.js"></script>

   

    <!-- Paper Dashboard Core javascript and methods for Demo purpose -->
    <script src="<?php echo base_url(); ?>assets/js/paper-dashboard.js"></script>

    <!-- Paper Dashboard DEMO methods, don't include it in your project! -->
    <script src="<?php echo base_url(); ?>assets/js/demo.js"></script>


  <?php echo $this->session->flashdata('notify'); ?>
</html>
