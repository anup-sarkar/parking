<?php
defined('BASEPATH') OR exit('No direct script access allowed');?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="">

    <title>Hello Hi Parking</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>assets/bootstrap.min.css" rel="stylesheet">
 
  <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
   
    
<link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
    <!-- Custom styles for this template -->
  <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url(); ?>assets/img/favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url(); ?>assets/img/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url(); ?>assets/img/favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>assets/img/favicon/favicon-16x16.png">
<link rel="manifest" href="<?php echo base_url(); ?>assets/img/favicon/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="<?php echo base_url(); ?>assets/img/favicon/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
  </head>

  <body id="parent">
 
    <nav class="navbar navbar-expand-lg  " style="background-color: black;-webkit-box-shadow: 0px 4px 11px -3px rgba(0,0,0,0.92);
-moz-box-shadow: 0px 4px 11px -3px rgba(0,0,0,0.92);
box-shadow: 0px 4px 11px -3px rgba(0,0,0,0.92);">
  <a class="navbar-brand"  href="#">  <a class="py-2" href="http://hellohiparking.com">
       <img src="<?php echo base_url(); ?>assets/img/logo_sm2.png" style="height: 34px"></a>

  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a class="nav-link" id="bold" href="#"><i class="fa fa-android" aria-hidden="true"></i>&nbsp; Download App <span class="sr-only">(current)</span></a>
      </li>

      <li class="nav-item">
        <a class="nav-link" id="bold" href="<?php echo base_url(); ?>index.php/reservations/ticket"><i class="fa fa-ticket" aria-hidden="true"></i>&nbsp; Ticket </a>
      </li>


      <li class="nav-item">
        <a class="nav-link" id="bold" href="#"><i class="fa fa-product-hunt" aria-hidden="true"></i>&nbsp; About </a>
      </li>
      
      <li class="nav-item" style="padding-right: 30px;">
        <a class="nav-link" id="bold" href="#"> <i class="fa fa-address-card-o" aria-hidden="true"></i>&nbsp; Contact </a>
      </li>
    </ul>
    <div class="form-inline">
 
 

           <?php  if (isset($_SESSION['client_name'])) { ?>

                 <a class="mr-sm-2" id="bold2" href="<?php echo site_url('home/profile'); ?>"   ><i class="fa fa-user-circle" aria-hidden="true"></i>&nbsp; <?php echo $_SESSION['client_name']; ?></a>


                   <a class="my-2 my-sm-0" id="bold2" href="<?php echo site_url('home/logout'); ?>"   ><i class="fa fa-sign-out" aria-hidden="true"></i> </i>&nbsp; SIGN OUT</a>


                <?php   }else{ ?>

        <a class="mr-sm-2" id="bold3" href="<?php echo site_url('home/login'); ?>"><i class="fa fa-user-circle" aria-hidden="true"></i>&nbsp; SIGN IN</a>


        <a class="my-2 my-sm-0" id="bold2" href="<?php echo site_url('home/signup'); ?>"  ><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp; SIGN UP</a>

                   <?php   }  ?>


    </div>
  </div>
</nav>

<style type="text/css">
  .nav-item:hover
  {
        border-bottom: 2px solid #007bff;
  }
      #bold2
   {
      font-weight:  bold;
     color: #02afdc;
    padding: 10px;
    border:  1px solid;
 
   }

     #bold3
   {
      font-weight:  bold;
     color: #02afdc;
    padding: 10px;
    border:  1px solid;
    /* border-radius: 5px; */
 
   }


</style>

 