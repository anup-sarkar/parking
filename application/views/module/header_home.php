<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
?><!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assets/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Hello Hi Parking! </title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
     <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
     
     <link href="<?php echo base_url(); ?>assets/css/compiled.min.css" rel="stylesheet">

     
  <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">

    <link href='https://fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
    
    <link href='<?php echo base_url(); ?>assets/boot4/Roboto-Light.woff2' rel='stylesheet' type='text/css'>

    <link href="<?php echo base_url(); ?>assets/css/themify-icons.css" rel="stylesheet">


 

 <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url(); ?>assets/img/favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url(); ?>assets/img/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url(); ?>assets/img/favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>assets/img/favicon/favicon-16x16.png">
<link rel="manifest" href="<?php echo base_url(); ?>assets/img/favicon/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="<?php echo base_url(); ?>assets/img/favicon/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">

</head>
<body style="margin: 0" >
 


    <div class="main-panel" style="width:100%">
           <nav class="site-header sticky-top py-1">
      <div class="container d-flex flex-column flex-md-row justify-content-between">
        <a class="py-2" href="http://hellohiparking.com">
       <img src="<?php echo base_url(); ?>assets/img/logo_sm2.png" style="height: 24px">
        </a>
        <a class="py-2 d-none d-md-inline-block" id="bold" href="https://hellohiparking.com"><i class="fa fa-android" aria-hidden="true"></i>&nbsp; Get The App</a>
      
        <a class="py-2 d-none d-md-inline-block" id="bold" href="https://hellohiparking.com"><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp; How It Works ?</a>
        <a class="py-2 d-none d-md-inline-block" id="bold" href="#"><i class="fa fa-address-card-o" aria-hidden="true"></i>&nbsp; Contact</a>
        <a class="py-2 d-none d-md-inline-block" id="bold" href="#"><i class="fa fa-user-circle" aria-hidden="true"></i>&nbsp; Log In</a>
        <a class="py-2 d-none d-md-inline-block" id="bold" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp; Sign Up</a>

         
      </div>
    </nav>