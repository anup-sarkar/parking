<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
       <footer  style="background-color: black;color: white;">
         <div class="row" style="padding: 50px">
            <div class="col-md-2">
               <img src="<?php echo base_url(); ?>assets/img/logo.png" style="width: 45%;margin-top: 20px">
               <br/>
               <small class="d-block mb-3 text-muted2">&copy; 2017-2018</small>
            </div>
            <div class="col-md-3">
               <h3>Features</h3>
               <ul class="list-unstyled text-small">
                  <li><a class="text-muted2" href="#">Monthly Rent</a></li>
                  <li><a class="text-muted2" href="#">Weekly Rent</a></li>
                  <li><a class="text-muted2" href="#">Event Rent</a></li>
               </ul>
            </div>
            <div class="col-md-3">
               <h3>Work With Us</h3>
               <ul class="list-unstyled text-small">
                 
                  <li><a class="text-muted2" href="<?php echo site_url('panel'); ?>">Parking Owners Login</a></li>
                   <li><a class="text-muted2" href="<?php echo site_url('home/provider_listing'); ?>">List Your Parking Spot</a></li>
                  <li><a class="text-muted2" href="#">Venue</a></li>
                  <li><a class="text-muted2" href="#">Producs</a></li>
                  <li><a class="text-muted2" href="#">App</a></li>
               </ul>
            </div>
            <div class="col-md-2">
               <h3>Follow Us</h3>
               <ul class="list-unstyled text-small">
                  <li><a class="text-muted2" href="#">Facebook</a></li>
                  <li><a class="text-muted2" href="#">Twitter</a></li>
                  <li><a class="text-muted2" href="#">Instagram</a></li>
                  <li><a class="text-muted2" href="#">Google Plus</a></li>
               </ul>
            </div>
            <div class="col-md-2">
               <h3>Support</h3>
               <ul class="list-unstyled text-small">
                  <li><a class="text-muted2" href="#">About </a></li>
                  <li><a class="text-muted2" href="#">Contact Us</a></li>
                  <li><a class="text-muted2" href="#">FAQ</a></li>
                  <li><a class="text-muted2" href="#">Term & Conditions</a></li>
               </ul>
            </div>
         </div>
      </footer>

<style type="text/css">
  
     .text-muted2
   {
      color: #02afdc;
      font-weight: bold;
   }
</style>
 
    <link href="<?php echo base_url(); ?>assets/css/card.css" rel="stylesheet">
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-3.1.1.min.js"></script>




    <link href="https://mdbootstrap.com/wp-content/themes/mdbootstrap4/css/compiled.min.css?ver=4.5.0" rel="stylesheet">

    
    <link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">

    <link href="<?php echo base_url(); ?>assets/css/home.css" rel="stylesheet">
 
    <script src="<?php echo base_url(); ?>assets/js/jquery-3.1.1.slim.min.js"  ></script>
 
    <script src="<?php echo base_url(); ?>assets/boot4/bootstrap.min.js"></script>
       <script 
  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDkkzyW9zJvzYJPe-5vrK_Ggko-Ha4NDzM&libraries=places&callback=initMap" async defer></script>

     
   