<?php
   defined('BASEPATH') OR exit('No direct script access allowed');
   $this->load->view("module/header_client");
    date_default_timezone_set("Asia/Dhaka");
     $rate_flag=true;
   ?>



    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
 

<div id="preloader">
  <div id="status">&nbsp;</div>
</div>


<style type="text/css">
    
    body {
  overflow: hidden;
}


/* Preloader */

#preloader {
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #fff;
  /* change if the mask should have another color then white */
  z-index: 99;
  /* makes sure it stays on top */
}

#status {
  width: 200px;
  height: 200px;
  position: absolute;
  left: 50%;
  /* centers the loading animation horizontally one the screen */
  top: 50%;
  /* centers the loading animation vertically one the screen */
  background-image: url(https://cdn.dribbble.com/users/1726478/screenshots/3781908/car-dealer-loader-gif.gif);
  /* path to your loading animation */
  background-repeat: no-repeat;
  background-position: center;
  margin: -100px 0 0 -100px;
  /* is width and height divided by two */
}


</style>
    <div class="row">
        <div class="col-sm-4">
            <div class="card" style="margin-left: 20px;margin-top:  11px;">
                <div class="content">
                    <nav aria-label="breadcrumb" style="font-size: 10px">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Search</a></li>
                            <li class="breadcrumb-item active" aria-current="page">
                                <?php echo $q; ?>
                            </li>
                                     <?php if($addr!=null) { ?>
                               <li class="breadcrumb-item active" aria-current="page">

                                <?php if($addr!=null) echo $addr; ?>
                            </li>

                        <?php } ?>
                        </ol>
                    </nav>
                    <div class="container">

                        <div class="form-inline mr-auto"  >
                            <input class="form-control" id="q" name="q" 
                             type="text" placeholder="Search For Parking" aria-label="Search" style=" border-radius: 0;width: 60%;background-color:  transparent;border:  0;border-bottom: 3px solid #3fa3e2;" value="<?php if($addr==null) echo $q; else echo $addr; ?>">
                             
     
                            <button class="btn blue-gradient btn-rounded btn-sm my-0"  id="find">Search</button>
                        </div>
                        <br/>
                    </div>
                </div>
            </div>
            <hr/>
            <div style="height: 700px;overflow: scroll;padding: 10px">



<?php if($spots==null)
{
    ?>

<div class="card">
    <div class="content">
        <div class="alert alert-danger" role="alert" style="margin: 5px">
<h4><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Oops ! No Spot Found :( </h4>
</div>
        
    </div>
</div>


                <?php }else{
 $i=0;
foreach ($spots as $item) {

  $data=$this->Panel_model->get_images_array($item->pid);
if($data !=null)
{
  $img=$data[0]['img_address'];
}
else
{
  $img="assets/img/p1.jpg";
}
 
?>





                <div class="card" style="margin-left: 10px;padding-bottom: 5px;margin-bottom: 10px">
                    <div class="content">
                        <div class="row">
                            <div class="col-md-4">
                                <img src="<?php echo base_url().$img; ?> " style="margin-left: 5px;margin-top: 5px;width: 120px;height: 105px;">
                                </div>
                            <div class="col-md-8" style="margin-top: 4px;padding-left: 8px">
                                <h6 style="margin-bottom: 0">
                                    <?php echo $item->public_name; ?>
                                </h6>
                                <div class="stats" style="font-size: 15px">
                                  <i class="fa fa-map-marker" aria-hidden="true"></i>
                                  <?php echo $item->address; ?>  
                                </div>
                            

                                <div class="stats">
                                    <table><tr>
                                        
                                                                        <?php 

                                        $names=array_unique(explode(";;",$item->option_names));
                                        $icons=array_unique(explode(";;",$item->option_icons));


                                        $flag=true;

                               



                                    if($flag && $names!=null && $icons!=null)
                                    {


                                       if($names != null){

                                       
                                      foreach ($names as $n => $value) {
                                    
                                                if($n==null)
                                                {
                                                     echo "<td><i class='fa fa-car' aria-hidden='true'></i> </td>";
                                                        echo "<td><i class='fa fa-star' aria-hidden='true'></i> </td>";
                                                }else{
                                        ?>
                                            <td><img src="<?php if($icons !=null) echo base_url().$icons[$n]; ?>" style="width: 22px"   title="<?php if($names!=null) echo $names[$n]; ?>"  />  </td>

                                        <?php 
                                    }
                                    }
                                }
                                    $flag=false;
                                    }

                                    ?>

                    

                                       <?php 

                                   if($item->facility_names !=null)
                                {
                                        $names=array_unique(explode(";;",$item->facility_names));
                                        $icons=array_unique(explode(";;",$item->facility_icons));
                                        $flag2=true;

                                    //print_r (array_unique($names)); 
                                     //print_r (array_unique($icons)); 

                                 
                            
                                    if($flag2)
                                    {

                                        if($names != null)
                                        {


                                        foreach ($names as $m => $value) {
                                    
                                            ?>
                                         
                                            <td><img src="<?php echo base_url().$icons[$m]; ?>" style="width: 22px"   title="<?php echo $value; ?>"  />  </td>


                                <?php 
                                                                         }
                                    } 
                                }

                                    $flag2=false;
                                    }
                                

                                    ?>


                                    
                                        </tr>
                                    </table>

                                </div>
                                <div class="stats">
                                    <a href="" style="font-size: 15px" style="color: cyan" data-toggle="modal" data-target="#exampleModalCenter<?php echo $i; ?>"> <i class="fa fa-info-circle"></i>  Details</a>
                                </div>
                            </div>
                        </div>

                        <input type="hidden" id="rate_val_<?php echo $i;?>" name="rate<?php echo $i;?>" 

                        value="  <?php   
                            if($item->total_rate>0|| $item->total_rate!=0 || $item->total_rate != null)
                            {
                                if($item->flt_status != null || $item->flt_status == 1)
                                {
                                    echo number_format($item->flt_rate, 2, '.', ''); 
                                }
                               elseif($item->hourly_status != null || $item->hourly_status == 1)
                                {
                                    echo number_format($item->hourly_rate, 2, '.', ''); 
                                  
                                }
                                else
                                {
                                     if($item->default_flat_rate!=0)
                                        echo number_format($item->default_flat_rate, 2, '.', '');
                                    else
                                           echo number_format(1, 2, '.', '');
                                }
                            } 
                            else
                            {
                                  if($item->default_flat_rate != 0 )
                                        echo number_format($item->default_flat_rate, 2, '.', '');
                                    else
                                          echo number_format(1, 2, '.', '');
                            }
                                


                            ?> ">

                        <input type="hidden" id="pid_<?php echo $i;?>" name="pid<?php echo $i;?>" value="<?php echo $item->pid; ?>">

                           <input type="hidden" id="flat_<?php echo $i;?>" name="flat<?php echo $i;?>" value="  <?php   
                            if($item->total_rate>0|| $item->total_rate!=0 || $item->total_rate != null)
                            {
                                if($item->flt_status != null || $item->flt_status == 1)
                                {
                                    echo number_format($item->flt_rate, 2, '.', ''); 
                                }
                               elseif($item->hourly_status != null || $item->hourly_status == 1)
                                {
                                    echo number_format($item->hourly_rate, 2, '.', ''); 
                                  
                                }
                                else
                                {
                                     if($item->default_flat_rate!=0)
                                        echo number_format($item->default_flat_rate, 2, '.', '');
                                    else
                                           echo number_format(1, 2, '.', '');
                                }
                            } 
                            else
                            {
                                  if($item->default_flat_rate != 0 )
                                        echo number_format($item->default_flat_rate, 2, '.', '');
                                    else
                                          echo number_format(1, 2, '.', '');
                            }
                                


                            ?>">

                           <input type="hidden" id="hourly_<?php echo $i;?>" name="hourly<?php echo $i;?>" value="<?php echo $item->hourly_rate; ?>">

                            <input type="hidden" id="total_av_<?php echo $i;?>" name="hourly<?php echo $i;?>" value="<?php echo $item->op_total_available; ?>">

                        <div class="footer" style="margin-top: 5px;padding-top: 5px;border-top: 1px solid #80808045;">
                             
                            <button class="btn blue-gradient btn-rounded btn-sm my-0" id="rate<?php echo $i;?>" style="font-size: 16px;width: 96%" >


                                 <input type="hidden" id="type_<?php echo $i;?>" name="type_<?php echo $i;?>" value="<?php 
                                 if($item->hourly_status != null || $item->hourly_status == 1)
                                {
                                    echo "Hourly" ;
                                  
                                }else
                                    echo "Flat";
                                  ?>">

                    <span id="rate<?php echo $i;?>"> 

                                  <span style="font-size: 10px">
 


                      
                           </span>

              <i class="fa fa-usd" aria-hidden="true" style="font-size: 15px" > </i>
                            <?php   
                            if($item->total_rate>0|| $item->total_rate!=0 || $item->total_rate != null)
                            {
                                if($item->flt_status != null || $item->flt_status == 1)
                                {
                                    echo number_format($item->flt_rate, 2, '.', ''); 
                                }
                               elseif($item->hourly_status != null || $item->hourly_status == 1)
                                {
                                    echo number_format($item->hourly_rate, 2, '.', ''); 
                                  
                                }
                                else
                                {
                                     if($item->default_flat_rate!=0)
                                        echo number_format($item->default_flat_rate, 2, '.', '');
                                    else
                                           echo number_format(1, 2, '.', '');
                                }
                            } 
                            else
                            {
                                  if($item->default_flat_rate != 0 )
                                        echo number_format($item->default_flat_rate, 2, '.', '');
                                    else
                                          echo number_format(1, 2, '.', '');
                            }
                                


                            ?> 
                         


                    </span>

                    
                     

                    &nbsp;&nbsp;&nbsp; 
                    |<span style="color: #b9e7f8"><b>&nbsp; &nbsp;&nbsp; RESERVE </b> </span>
                  </button>
                        </div>
                    </div>
                    <!-- Modal -->
                    <div class="modal fade bd-example-modal-lg" id="exampleModalCenter<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle<?php echo $i; ?>" aria-hidden="true" style="z-index: 2000">
                        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <div class="modal-title" style="width: 100%" id="exampleModalLongTitle<?php echo $i; ?>">
                                        <div class="row">

                                            <div class="col-md-3" >
                                                <img  style="height: 140px;width: 160px" src="<?php echo base_url().$img; ?>">


                                            </div>
                                            <div class="col-md-9" style="margin-top: 5px;padding:0">
                                                <h5 style="margin-left: 20px">
                                                    <?php echo $item->public_name; ?>  </h5>
                                         
                                                <div class="stats">
                                                    <h6> <i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;&nbsp;
                                                        <?php echo $item->address; ?> </h6>
                                                </div>

                                                <div class="stats">
                                                    <h6> <i class="ti-info"> </i> <i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;&nbsp;
                                                        <?php   echo date("g:i a", strtotime($item->op_open_time))."-".date("g:i a", strtotime($item->op_close_time)); ?>
                                                    </h6>
                                                </div>


                                                <div class="stats">
                                                    <h6 style="color:#2bbbad"> <span><i class="fa fa-car" aria-hidden="true"></i> &nbsp; Space Available:</span> <b><?php echo $item->total_available_now; ?></b></h6>
                                                </div>

                                                <div class="stats">

                                                  
                                                            <h6><i class="fa fa-money" aria-hidden="true"></i>  &nbsp;
                           
              <i class="fa fa-usd" aria-hidden="true" style="font-size: 15px" > </i>             <?php   
                            if($item->total_rate>0|| $item->total_rate!=0 || $item->total_rate != null)
                            {
                                if($item->flt_status != null || $item->flt_status == 1)
                                {
                                    echo number_format($item->flt_rate, 2, '.', ''); 
                                }
                               elseif($item->hourly_status != null || $item->hourly_status == 1)
                                {
                                    echo number_format($item->hourly_rate, 2, '.', ''); 
                                  
                                }
                                else
                                {
                                     echo number_format($item->default_flat_rate, 2, '.', ''); 
                                }
                            } 
                            else
                            {
                                  echo number_format($item->default_flat_rate, 2, '.', '');
                            }
                                


                            ?> 
                         


                     
<?php 
if($item->hourly_status != null || $item->hourly_status == 1) 
    echo " ( Hourly Rate  @ $".number_format($item->hourly_rate_base, 0, '.', '')."/Per Hour )" ; 

else 
    echo " ( Flat Rate @ Per Day )"; ?>


                      
                         </h6>          </div>
                                            </div>
                                        </div>
                                    </div>
                              
                                  <button class="btn btn-primary" >
                           <b>RESERVE</b>  <span id="rate<?php echo $i;?>"> 

                            </button>
                                </div>
                                <div class="modal-body">
                                    <ul class="nav nav-tabs" id="myTab<?php echo $i; ?>" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="home-tab<?php echo $i; ?>" data-toggle="tab" href="#home<?php echo $i; ?>" role="tab" aria-controls="home" aria-selected="true">Details</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="profile-tab<?php echo $i; ?>" data-toggle="tab" href="#profile<?php echo $i; ?>" role="tab" aria-controls="profile" aria-selected="false">How To Find</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="contact-tab<?php echo $i; ?>" data-toggle="tab" href="#contact<?php echo $i; ?>" role="tab" aria-controls="contact" aria-selected="false">Timings</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="contact2-tab<?php echo $i; ?>" data-toggle="tab" href="#contact2<?php echo $i; ?>" role="tab" aria-controls="contact2" aria-selected="false">How To Redeem</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="image2<?php echo $i; ?>" data-toggle="tab" href="#image<?php echo $i; ?>" role="tab" aria-controls="image<?php echo $i; ?>" aria-selected="false">Images</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="contact4-tab<?php echo $i; ?>" data-toggle="tab" href="#tc<?php echo $i; ?>" role="tab" aria-controls="tc2" aria-selected="false">T & C</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content" id="myTabContent<?php echo $i; ?>">
                                        <div class="tab-pane fade show active" id="home<?php echo $i; ?>" role="tabpanel" aria-labelledby="home-tab<?php echo $i; ?>">
                                            <h5>Details</h5>
                                            <p>
                                                <?php echo $item->descriptions; ?> </p>
                                            <hr/>
                                            <h5>Description</h5>
                                            <p>
                                                <?php echo $item->description; ?> </p>

                                            <hr/>
                                             <h5 style="text-align: center;"><b>Available Options </b> </h5>
                                             

                                                 <table class="table table-hover"> 
                                        
                                                                        <?php 

                                        $names=array_unique(explode(";;",$item->option_names));
                                        $icons=array_unique(explode(";;",$item->option_icons));
                                        $flag=true;

                                   // print_r (array_unique($names)); 

                                    if($flag)
                                    {
                                           foreach ($names as $n => $value) {

                                    //for($index=0;$index<count($names);$index++)

                                    
                                        ?>
                                        <tr>

                                          <?php if($icons[$n]==null){ ?>
                                         <td><i class='fa fa-car' style="font-size: 60px" aria-hidden='true'></i> </td>
                                         <td> </td>
                                       </tr>
                                       <tr>
                                          <td><i class='fa fa-star' style="font-size: 60px" aria-hidden='true'></i> </td> 
                                          <td></td>
                                        </tr>
                                      <?php }else{ ?>

                                        <tr>


                                            <td>
                                              <img src="<?php if($icons !=null) echo base_url().$icons[$n]; ?>" 
                                              style="width: 50px" />  
                                            </td>
                                          <?php } ?>
                                            <td> <?php if($names!=null) echo $names[$n]; ?> </td>


                                     </tr>

                                        <?php 
                                    }
                                    $flag=false;
                                    }

                                    ?>

                    

                                   

                                    
                                    
                                    </table>  

                                    <hr/>

                                    <h5 style="text-align: center;"><b> Available Facilities </b></h5>

                                        <table class="table table-hover"> 
                                        
                                                                     

                    

                                       <?php 

                                   if($item->facility_names !=null)
                                {
                                        $names=array_unique(explode(";;",$item->facility_names));
                                        $icons=array_unique(explode(";;",$item->facility_icons));
                                        $flag2=true;

                                    //print_r (array_unique($names)); 
                                     //print_r (array_unique($icons)); 

                                 
                            
                                    if($flag2)
                                    {


                                        foreach ($names as $h => $value) {
                                    
                                            ?>
                                         <tr>
                                            <td><img src="<?php echo base_url().$icons[$h]; ?>" style="width: 50px"  /> </td>

                                          <td> <?php echo $names[$h]; ?> </td>

                                        </tr>


                                <?php 
                                    }
                                    }
                                    $flag2=false;
                                    }
                                

                                    ?>


                                    
                                        
                                    </table> 
                                        </div>

                                        <div class="tab-pane fade" id="profile<?php echo $i; ?>" role="tabpanel" aria-labelledby="profile-tab<?php echo $i; ?>">
                                            <h3>How To Find</h3>
                                            <p>
                                                <?php echo $item->how_to_find; ?> </p>
                                        </div>

                                        <div class="tab-pane fade" id="contact<?php echo $i; ?>" role="tabpanel" aria-labelledby="contact-tab<?php echo $i; ?>">

                                            <?php if($item->op_isAlwaysOpen == 1)
                                                   {
                                                      echo "<p> Open 24/7 Hours </p>";
                                                   }
                              else
                              {

                              
                            ?>

                                            <table style="margin:0 auto;text-align: right;" class="table table-border">
                                                <tbody>
                                                    <tr>
                                                        <th style="width: 120px"> <b> Open Time </b></th>
                                                        <td>
                                                            <?php     echo date("g:i a", strtotime($item->op_open_time)); ?> </td>


                                                    </tr>
                                                    <tr>
                                                        <th> <b>  Close Time   </b></th>
                                                        <td>
                                                            <?php    echo  date("g:i a", strtotime($item->op_close_time)); ?> </td>
                                                    </tr>

                                                </tbody>
                                            </table>

                                            <?php } ?>
                                        </div>

                                        <div class="tab-pane fade" id="contact2<?php echo $i; ?>" role="tabpanel" aria-labelledby="contact2-tab<?php echo $i; ?>">
                                            <h3>How To Redeem</h3>
                                            <p>
                                                <?php echo $item->how_to_redem; ?> </p>
                                        </div>

                                        <div class="tab-pane fade" id="tc<?php echo $i; ?>" role="tabpanel" aria-labelledby="tc2<?php echo $i; ?>">
                                            <h3>Terms & Condition</h3>
                                            <p>
                                                <?php echo $item->term_and_condition; ?> </p>

                                        </div>

                                        <div class="tab-pane fade" id="image<?php echo $i; ?>" role="tabpanel" aria-labelledby="image2<?php echo $i; ?>">
                                            <input type="text" name="pid<?php echo $i; ?>" id="pid<?php echo $i; ?>" value="<?php echo $item->pid; ?>" style="display: none">

                                            <div name="image_show<?php echo $i; ?>" class="row" id="image_show<?php echo $i; ?>">
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>





<?php 
 
$i++;
}
} ?>


  <?php if(!empty($date) && !empty($to))
                                    {
                                        $t=$to;
                                        $f=$date;
                                    }
                                    else{
                                        $t='+4 hours';
                                    } ?>

            </div>
        </div>

        <div class="col-sm-8">
            <div class="card" style="margin:10px">
                <div class="content">


                   
                    <div class="row" style="margin-left:  10px;padding-top: 20px;">
                        <div class="col-sm-0"><label style=" margin-left: 5px; margin-top:  10px;font-size:  17px;"> From </label></div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <div class="input-group date form_datetime col-md-12" data-date-format="dd MM yyyy - HH:ii p" data-link-field="dtp_input1" style="padding: 0">
                                    <input class="form-control" size="16" type="text" name="date1" id="date1" 
                                    value="" 


                                    readonly>
                                    <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                </div>
                                <input type="hidden" id="dtp_input1" value="" /><br/>
                                <input type="text" id="mirror"     value=""  name="mirror" readonly style="display: none;" />
                            </div>
                        </div>
                        <div class="col-sm-0"><label style="    margin-top:  10px;
                  font-size:  17px;">To </label></div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <div class="input-group date form_datetime2 col-md-12" data-date-format="dd MM yyyy - HH:ii p" data-link-field="dtp_input1" style="padding: 0">
                                    <input class="form-control" size="16" type="text" name="date2" id="date2" 

                 value="" 
                                       readonly>
                                    <span class="input-group-addon"> <i class="fa fa-calendar" aria-hidden="true"></i></span>
                                </div>
                                <input type="hidden" id="dtp_input2" value="" /><br/>
                                <input type="text" id="mirror2" 
                     value="" 
                         name="mirror2" readonly style="display: none;" />
                            </div>
                        </div>
                        <div class="col-md-1" style="padding: 0">
                            <button class="btn blue-gradient btn-rounded btn-sm my-0" type="submit" id="search_date" style="height: 43px;"><i class="fa fa-search" aria-hidden="true" style="font-size: 24px;"></i></button>
                        </div>
                        <div class="col-sm-2"  >
                            <p style="margin-bottom: 0"><i class="fa fa-clock-o" aria-hidden="true"></i><b> Duration:</b> </p>
                            <p style="margin-bottom: 0"> <span id="day"></span> day, <span id="hour"></span> Hrs</p>
                        </div>
                       

                    </div>

                </div>
            </div>
            <div>
                <?php if($spots==null)
{?>

<div style="text-align: center;">
   <img src="<?php echo base_url(); ?>assets/img/not_found.png" />
</div>
<?php } ?>
                <div class="map" style="margin: 10px">
                    <div id="map"></div>




                </div>
            </div>
            <hr/>
        </div>
    </div>

<input type="hidden" name="addr" id="addr" value="<?php echo $date; ?>">
<input type="hidden" name="addr" id="addr" value="<?php echo $addr; ?>">
<input type="hidden" name="city" id="city_val" value="<?php echo $q; ?>">
<input type="hidden" name="lat" id="lat" value="<?php echo $lat;?>">
<input type="hidden" name="long" id="long" value="<?php echo $long;?>">
<input type="hidden" name="place" id="place" value="<?php echo $place;?>">
<input type="hidden" name="address" id="address" >

<input type="hidden" name="addr" id="addr" >
  
                   <div class="pac-card" id="pac-card">
      <div>
         
        <div id="country-selector" class="pac-controls" style="display: none;"  >
          <input type="radio" name="type" id="changecountry-usa">
          <label for="changecountry-usa">USA</label>

          <input type="radio" name="type" id="changecountry-usa-and-uot" checked="checked">
          <label for="changecountry-usa-and-uot">USA and unincorporated organized territories</label>
        </div>
      </div>
       
    </div>



    <link href="https://mdbootstrap.com/wp-content/themes/mdbootstrap4/css/compiled.min.css?ver=4.5.0" rel="stylesheet">

    <?php
  $this->load->view("module/footer_searchpage");

?>


        <script>

          $(window).on('load', function() { // makes sure the whole site is loaded 
  $('#status').fadeOut(); // will first fade out the loading animation 
  $('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website. 
  $('body').delay(350).css({'overflow':'visible'});
})

          
            <?php if($spots!=null)
{?>
        function initMap() {

         $("#map").show();
        var labelIndex = 0;

 
                var markers = [

                    <?php 
            $i=0;
            $center_long="";
            $center_lat="";

            foreach ($spots as $spot) {
              if($i==0)
                {
                  
                    $center_lat=$spot->lat;
                    $center_long=$spot->long;
                 

                
                }
                ?>

                    {
                        coords: {
                            lat: <?php echo $spot->lat; ?>,
                            lng: <?php echo $spot->long; ?>,
                        },

                        content: '<div style="text-align:center"><h5><?php echo $spot->public_name; ?></h5><h6><?php echo $spot->address; ?></h6><hr/><button  class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModalCenter<?php echo $i; ?>"> <i class="fa fa-info-circle" aria-hidden="true"></i> &nbsp;  Details</button> <button onClick="clk(rate<?php echo $i;?>)" class="btn btn-default btn-sm" > <?php if($item->hourly_status != null || $item->hourly_status == 1) echo "Hourly @ "; else echo "Flat @ "; ?>
                       <i class="fa fa-usd" aria-hidden="true" > </i><?php 
                             if($spot->total_rate>0|| $spot->total_rate!=0 || $spot->total_rate != null)
                            {
                                if($spot->flt_status != null || $spot->flt_status == 1)
                                {
                                    echo number_format($spot->flt_rate, 2, '.', ''); 
                                }
                               elseif($spot->hourly_status != null || $spot->hourly_status == 1)
                                {
                                    echo number_format($spot->hourly_rate, 2, '.', ''); 
                                  
                                }
                            else
                                {
                                    if($spot->default_flat_rate!=0  )
                                        echo number_format($spot->default_flat_rate, 2, '.', '');
                                    else
                                          echo number_format(1, 2, '.', '');
                                }
                            } 
                            else
                            {
                                  if($spot->default_flat_rate!=0  )
                                        echo number_format($spot->default_flat_rate, 2, '.', '');
                                    else
                                          echo number_format(1, 2, '.', '');
                            }
                               


                            ?>  &nbsp;&nbsp;|&nbsp;&nbsp; RESERVE  </button></div>',
                        rate: '<?php 
                             
                             if($spot->total_rate>0|| $spot->total_rate!=0 || $spot->total_rate != null)
                            {
                                if($spot->flt_status != null || $spot->flt_status == 1)
                                {
                                    echo number_format($spot->flt_rate, 0, '.', ''); 
                                }
                               elseif($spot->hourly_status != null || $spot->hourly_status == 1)
                                {
                                    echo number_format($spot->hourly_rate, 0, '.', ''); 
                                  
                                }
                                else
                                {
                                    if($spot->default_flat_rate!=0  )
                                        echo number_format($spot->default_flat_rate, 0, '.', '');
                                    else
                                        echo 15;
                                }
                            } 
                            else
                            {
                                  if($spot->default_flat_rate!=0  )
                                        echo number_format($spot->default_flat_rate, 0, '.', '');
                                    else
                                       echo 15;
                            }
                                
                                



                            ?>'
                    },

                    <?php 
                    $i++;
                } 

                    ?>
                ];


                var markerIcon = {
                    url: '<?php echo base_url(); ?>assets/img/map2.svg',
                    scaledSize: new google.maps.Size(65, 75),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(52, 65),
                    labelOrigin: new google.maps.Point(34, 21),
                };


                // Map options
                var options = {
                    zoom: 15,

                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    gestureHandling: 'greedy',
                    center: {
                        lat: <?php echo $center_lat;?>,
                        lng: <?php echo $center_long;?>
                    }

                }

                // New map
                var map = new google.maps.Map(document.getElementById('map'), options);

                // Listen for click on map
                google.maps.event.addListener(map, 'click', function(event) {
                    // Add marker
                    //addMarker({coords:event.latLng});

                });


                // Array of markers


                // Loop through markers
                for (var i = 0; i < markers.length; i++) {
                    // Add marker
                    addMarker(markers[i]);
                }

                // Add Marker Function
                function addMarker(props) {
                    var marker = new google.maps.Marker({
                        position: props.coords,
                        label: props.rate,
                        icon: markerIcon,
                        animation: google.maps.Animation.DROP,

                        map: map,
                        //icon:props.iconImage
                    });

                    // Check for customicon
                    if (props.iconImage) {
                        // Set icon image
                        marker.setIcon(props.iconImage);
                    }

                    // Check content
                    if (props.content) {
                        var infoWindow = new google.maps.InfoWindow({
                            content: props.content
                        });

                        marker.addListener('click', function() {
                            infoWindow.open(map, marker);
                        });
                    }
                }


       
        var card = document.getElementById('pac-card');
        var input = document.getElementById('q');
        var countries = document.getElementById('country-selector');
 


        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

        var autocomplete = new google.maps.places.Autocomplete(input);

        // Set initial restrict to the greater list of countries.
        autocomplete.setComponentRestrictions(
            {'country': ['us', 'pr', 'vi', 'gu', 'mp']});

        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
          map: map,

          anchorPoint: new google.maps.Point(0, -29)
        });

        autocomplete.addListener('place_changed', function() {
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No place available for input: '" + place.name + "'");
            return;
          }

          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
            
          var lat=place.geometry.location.lat();
          var long=place.geometry.location.lng();
             

           $('#lat').val(lat);
           $('#long').val(long); 


          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(16);
              var lat=place.geometry.location.lat();
              var long=place.geometry.location.lng();
            
 

              $('#lat').val(lat);
              $('#long').val(long);   
          }
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);

          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }
 

          console.log(place.name);
          console.log(address);

          $('#place').val(place.name);
          $('#address').val(address);

        });

        // Sets a listener on a given radio button. The radio buttons specify
        // the countries used to restrict the autocomplete search.
        function setupClickListener(id, countries) {
          var radioButton = document.getElementById(id);
          radioButton.addEventListener('click', function() {
            autocomplete.setComponentRestrictions({'country': countries});
          });
        }

        setupClickListener('changecountry-usa', 'us');
        setupClickListener(
            'changecountry-usa-and-uot', ['us', 'pr', 'vi', 'gu', 'mp']);
      


            }

<?php }
        else        // Query Doesnt find any parking spot
      {

    $i=0;
    ?>


       function initMap() {

var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: 38.532675, lng: -103.784548},
            gestureHandling: 'greedy',
              disableDefaultUI: true,
       
          zoom: 3.5
        });
        var card = document.getElementById('pac-card');
        var input = document.getElementById('q');
        var countries = document.getElementById('country-selector');

        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

        var autocomplete = new google.maps.places.Autocomplete(input);

        // Set initial restrict to the greater list of countries.
        autocomplete.setComponentRestrictions(
            {'country': ['us', 'pr', 'vi', 'gu', 'mp']});

        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
          map: map,

          anchorPoint: new google.maps.Point(0, -29)
        });

        autocomplete.addListener('place_changed', function() {
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No place available for input: '" + place.name + "'");
            return;
          }

          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
            
          var lat=place.geometry.location.lat();
          var long=place.geometry.location.lng();
             

        

            var lat=place.geometry.location.lat();
          var long=place.geometry.location.lng();
             

           $('#lat').val(lat);
           $('#long').val(long); 


          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(16);
              var lat=place.geometry.location.lat();
              var long=place.geometry.location.lng();
            
 

              $('#lat').val(lat);
              $('#long').val(long);  // Why 17? Because it looks good.
          }
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);

          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }

     

          console.log(place.name);
          console.log(address);

          $('#place').val(place.name);
          $('#address').val(address);



        });

        // Sets a listener on a given radio button. The radio buttons specify
        // the countries used to restrict the autocomplete search.
        function setupClickListener(id, countries) {
          var radioButton = document.getElementById(id);
          radioButton.addEventListener('click', function() {
            autocomplete.setComponentRestrictions({'country': countries});
          });
        }

        setupClickListener('changecountry-usa', 'us');
        setupClickListener(
            'changecountry-usa-and-uot', ['us', 'pr', 'vi', 'gu', 'mp']);
      

        };

        $("#map").hide();

    <?php
} ?>

if( localStorage.date_full === undefined && localStorage.date_full2 === undefined)
{
 


     console.log("false");

      $("#date1").val('<?php  echo date(" d M Y - h:i a "); ?>');
      $("#date2").val('<?php echo date(" d M Y - h:i a ",strtotime('+4 hours')); ?>');
}
else
{    console.log( "#####"+ localStorage.date_full);
     console.log( "#####"+ localStorage.date_full2);
     
     $("#date1").val(localStorage.date_full);
     $("#date2").val(localStorage.date_full2);
}



if( localStorage.sd1=== undefined && localStorage.sd2=== undefined)
{
    
   $("#mirror").val('<?php echo date(" Y-m-d H:i:s ");?>');
    $("#mirror2").val('<?php echo date(" Y-m-d H:i:s ",strtotime('+4 hours'));?>');

}
else
{
 console.log( "#####"+localStorage.sd1);
     console.log( "#####"+localStorage.sd2);
    
     $("#mirror").val(localStorage.sd1);
     $("#mirror2").val(localStorage.sd2);

}


            var now = new Date();
            var day = now.getMonth() + 1;
            var today = now.getFullYear() + '-' + day + "-" + now.getDate();
            // console.log(today);


            var date1 = $("#mirror").val();
            var date2 = $("#mirror2").val();


            var flag1 = $("#date1").val();
            var flag2 = $("#date2").val();


            var temp1 = date1;
            var temp2 = date2;


  
            var d = new Date(date1.replace(/-/g, "/"));
            var d2 = new Date(date2.replace(/-/g, "/"));
           

            var seconds = Math.floor((d2 - d) / 1000);
     

            var minutes = Math.floor(seconds / 60);
            var hours = Math.ceil(minutes / 60);
            var days = Math.floor(hours / 24);



            if (days > 0) {
                hours = Math.ceil(hours / 24);
            }

            //hours = hours - (days * 24);
            //minutes = minutes-(days*24*60)-(hours*60);

           console.log(days);
           console.log(hours);

            $("#day").text(days);
            $("#hour").text(hours);
 

            $('.form_datetime').datetimepicker({
                //language:  'fr',
                weekStart: 1,
                todayBtn: 1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                forceParse: 0,
                showMeridian: 1,
                startDate: today,
                format: "dd MM yyyy - HH:ii p",
                linkField: "mirror",
                linkFormat: "yyyy-mm-dd hh:ii:ss"
            }).on('changeDate', function(ev) {



                date1 = $("#mirror").val();
                date2 = $("#mirror2").val();


                d = new Date(date1.replace(/-/g, "/"));
                d2 = new Date(date2.replace(/-/g, "/"));

                if (d > d2) {
                    alert("Error: Date From Can Not Be Greater Than Date End ! ");
                    $("#mirror").val(temp1);
                    $("#date1").val(flag1);


                } else {


                    date1 = $("#mirror").val();
                    date2 = $("#mirror2").val();

                    console.log("date1 : "+date1);
                    console.log("date2 : "+date2);

                    flag1 = $("#date1").val();
                    flag2 = $("#date2").val();

                    seconds = Math.floor((d2 - (d)) / 1000);
                    minutes = Math.floor(seconds / 60);
                    hours = Math.ceil(minutes / 60);
                    days = Math.floor(hours / 24);


                    if (days > 0) {
                        hours = Math.ceil(hours / 24);
                    }

                    $("#day").text(days);
                    $("#hour").text(hours);


                }

            });




            $('.form_datetime2').datetimepicker({
                //language:  'fr',
                weekStart: 1,
                todayBtn: 1,
                autoclose: 1,
                todayHighlight: 1,
                startDate: today,
                startView: 2,
                forceParse: 0,
                showMeridian: 1,
                format: "dd MM yyyy - HH:ii p",
                linkField: "mirror2",
                linkFormat: "yyyy-mm-dd hh:ii:ss"
            }).on('changeDate', function(ev) {


                date1 = $("#mirror").val();
                date2 = $("#mirror2").val();


     

                d = new Date(date1.replace(/-/g, "/"));
                d2 = new Date(date2.replace(/-/g, "/"));

                if (d > d2) {
                    alert("Error: Date From Can Not Be Greater Than Date End ! ");
                    $("#mirror2").val(temp2);
                    $("#date2").val(flag2);


                } else {


                    date1 = $("#mirror").val();
                    date2 = $("#mirror2").val();

                    console.log("date1 : "+date1);
                    console.log("date2 : "+date2);


                    flag1 = $("#date1").val();
                    flag2 = $("#date2").val();
                    seconds = Math.floor((d2 - (d)) / 1000);
                    minutes = Math.floor(seconds / 60);
                    hours = Math.ceil(minutes / 60);
                    days = Math.floor(hours / 24);


                    if (days > 0) {
                        hours = Math.ceil(hours / 24);
                    }

                    $("#day").text(days);
                    $("#hour").text(hours);

                   // $('#search_date').click();



                }

            });

            $('#search_date').click(function (){

                    var from=new Date(date1);
                    var to =new Date(date2);

                    var m1=from.getMonth() + 1 ; 
                    var m2=to.getMonth() + 1 ; 

                    from= from.getFullYear() + '-' +  m1 + "-" + from.getDate();
                    to = to.getFullYear() + '-' +  m2 + "-" + to.getDate();

                    console.log(from);
                    console.log(to);

                    var city=$('#q').val();
                    var days=$("#day").text();

                    days=parseInt(days)+1;

                    var hour=  $("#hour").text();


                 if( hour > 0)
                 {
                    
                    var lat=$('#lat').val();
                    var long=$('#long').val(); 

                    var place=  $('#place').val();
                    var addr=  $('#address').val();
                    

                        var redirectUrl="<?php echo base_url(); ?>index.php/search/locations"
                        var form = $('<form action="' + redirectUrl + '" method="post">' +
                            '<input type="hidden" name="place" value="'+ place +'" />' +
                            '<input type="hidden" name="addr" value="'+ addr +'" />' +
                            '<input type="hidden" name="lat" value="'+ lat +'" />' +

                          
                            '<input type="hidden" name="long" value="'+ long +'" />' +
                            '<input type="hidden" name="from" value="'+ from +'" />' +
                            '<input type="hidden" name="to" value="'+ to +'" />' +
                                  '<input type="hidden" name="days" value="'+ days +'" />' +
                                     '<input type="hidden" name="hours" value="'+ hour +'" />' +
                             
                           '</form>');
                            $('body').append(form);
                            $(form).submit();
                 }
          
                    
                   
             

 

                localStorage.sd1= date1;
                localStorage.sd2= date2;

                localStorage.date_full= $("#date1").val();
                localStorage.date_full2= $("#date2").val();
/*
                console.log(localStorage.sd1);
                  console.log(localStorage.sd2);
                    console.log(localStorage.date_full);
                      console.log(localStorage.date_full2);*/
               //window.location = url;
            });



function clk(btn)
{
   $(btn).click();
}
          
$('#search').click(function() {
  var current = $input.typeahead("getActive");

  

 window.localStorage.clear();

  if (current) {


  var city= current['name'];
  var id= current['id'];

  console.log(city);
  console.log(id);
    // Some item from your model is active!
    if (current.name == $input.val()) {
      // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
      //alert("Exact match");
console.log(current.id);
     if(current['id']=="City")
     {
      var url="<?php echo base_url();?>"+"index.php/search?q="+$input.val()+"&stype=city";
       window.location = url;
     }
     else
     {
      var addr=city;
      var url2="<?php echo base_url();?>"+"index.php/search?q="+current['id']+"&addr="+addr+"&stype=address"

      console.log(url2);
       window.location = url2;
     }
     


    } else {
      // This means it is only a partial match, you can either add a new item
      // or take the active if you don't want new items
       // alert("Partial match");

       window.location ="<?php echo base_url();?>"+"index.php/search?q="+$input.val()+"&stype=partial";
    }
  } else {
    // Nothing is active so it is a new value (or maybe empty value)
      //alert("No match");
             window.location = "<?php echo base_url();?>"+"index.php/search?q="+$input.val()+"&stype=partial";
  }
});

 
            $(document).ready(function() {

                <?php 
                $x=0;
                if($spots != null){
                foreach ($spots as $data) {
                    # code...
                ?>

                  $('#rate<?php echo $x; ?>').click(function()
                    {
                        var rate<?php echo $x; ?>=$("#rate_val_<?php echo $x; ?>").val();
                        var pid_<?php echo $x; ?>=$("#pid_<?php echo $x; ?>").val();

                        var flat_<?php echo $x; ?>=$("#flat_<?php echo $x; ?>").val();
                        var hourly_<?php echo $x; ?>=$("#hourly_<?php echo $x; ?>").val();

                        var total_av_<?php echo $x; ?>=$("#total_av_<?php echo $x; ?>").val();


                        var date  = $("#date1").val();
                        var to  =$("#date2").val();

                        var m_date1  = $("#mirror").val();
                        var m_date2  =$("#mirror2").val();

                        var day=   $("#day").text();
                        var hour=  $("#hour").text();
                        var type=  $("#type_<?php echo $x; ?>").val();
/*
 
*/

                        var redirectUrl="<?php echo base_url(); ?>index.php/search/book";
                        var form = $('<form action="' + redirectUrl + '" method="post">' +
                            '<input type="hidden" name="pid" value="'+ pid_<?php echo $x; ?> +'" />' +
                            '<input type="hidden" name="rate" value="'+ rate<?php echo $x; ?> +'" />' +
                            '<input type="hidden" name="flat_rate" value="'+ flat_<?php echo $x; ?> +'" />' +
                            '<input type="hidden" name="hourly_rate" value="'+ hourly_<?php echo $x; ?> +'" />' +
                            '<input type="hidden" name="from" value="'+ date +'" />' +
                            '<input type="hidden" name="to" value="'+ to +'" />' +
 
                            '<input type="hidden" name="day" value="'+ day +'" />' +
                            '<input type="hidden" name="hour" value="'+ hour +'" />' +
                              '<input type="hidden" name="type" value="'+ type +'" />' +

                            '<input type="hidden" name="m_date1" value="'+ m_date1 +'" />' +
                            '<input type="hidden" name="m_date2" value="'+ m_date2 +'" />' +
                              '<input type="hidden" name="total_available" value="'+total_av_<?php echo $x; ?> +'" />' +
                            '</form>');
                            $('body').append(form);
                            $(form).submit();



                    });

                  <?php $x++;} 
              }
                  ?>

    <?php 
           
            for($k=0;$k<$i;$k++)
            {?>

              

                $('#image2<?php echo $k; ?>').click(function() {
 
                    $.getJSON('<?php echo base_url(); ?>index.php/search/images/' + $("#pid<?php echo $k; ?>").val(), function(data) {

                        for (var x = 0; x < data.length; ++x) {
                            $('#image_show<?php echo $k; ?>').append('<div class="col-md-4"><img style="width:250px" src="<?php echo base_url();?>' + data[x]["img_address"] + '" /><hr/><h5 style="text-align:center">' + data[x]["img_name"] + '</h5> <hr/> </div>');

                        }

                    });

                });

                <?php } ?>

            });



 



$('#find').click(function() {


 if($('#q').val()!='')
       {
        
       var lat=$('#lat').val();
       var long=$('#long').val(); 

        var place=  $('#place').val();
         var addr=  $('#address').val();
         var q2=$('#q').val();
 

                        var redirectUrl="<?php echo base_url(); ?>index.php/search/locations"
                        var form = $('<form action="' + redirectUrl + '" method="post">' +
                            '<input type="hidden" name="place" value="'+ place +'" />' +
                            '<input type="hidden" name="addr" value="'+ addr +'" />' +
                            '<input type="hidden" name="lat" value="'+ lat +'" />' +
                            '<input type="hidden" name="q2" value="'+ q2 +'" />' +
                            '<input type="hidden" name="long" value="'+ long +'" />' +
                             
                           '</form>');
                            $('body').append(form);
                            $(form).submit();
}
else
{
  alert("Your Search Query Cannot Be Empty !");
}

});


$('#q').keypress(function (e) {
 var key = e.which;
 if(key == 13)  // the enter key code
  {
     $('#find').click();

      
    
  }


  
});  


        </script>

        
<script 
  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDkkzyW9zJvzYJPe-5vrK_Ggko-Ha4NDzM&libraries=places&callback=initMap" async defer></script>
