<?php
   defined('BASEPATH') OR exit('No direct script access allowed');
   
    date_default_timezone_set("Asia/Dhaka");
   ?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Hello Hi Parking </title>
      <!-- CSS -->
      <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/wizard/bootstrap/css/bootstrap.min.css">
      <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/wizard/css/form-elements.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/wizard/css/style.css">
      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
      <!-- Favicon and touch icons -->
      <link rel="shortcut icon" href="assets/ico/favicon.png">
      <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
      <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
   </head>
   <body>
      <!-- Top menu -->
      <nav class="navbar navbar-inverse" role="navigation" style="background-color: black">
         <div class="container">
            <div class="navbar-header">
               <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-navbar-1">
               <span class="sr-only">Toggle navigation</span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               </button>
               <a class="navbar-brand" href="http://hellohiparking.com">Hello Hi Parking</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="top-navbar-1">
               <ul class="nav navbar-nav navbar-right">
                  <li id="main_menu">
                     <span><a href="" style="color: #02afdc"><i class="fa fa-android" aria-hidden="true" ></i>&nbsp; Get The App</a></span>
                  </li>
                  <li id="main_menu">
                     <span><a href="<?php echo base_url(); ?>/index.php/reservations/ticket" style="color: #02afdc">  <i class="fa fa-ticket" aria-hidden="true"></i>&nbsp; Ticket</a></span>
                  </li>
                  <li id="main_menu">
                     <span style="color: #02afdc"><i class="fa fa-product-hunt" aria-hidden="true"></i>&nbsp; About
                  </li>
                  <?php  if (isset($_SESSION['client_name'])) { ?>
                  <li id="main_menu">
                     <span  > <a style="color: #02afdc"  href="<?php echo site_url('home/profile'); ?>"   ><i class="fa fa-user-circle" aria-hidden="true"></i>&nbsp; <?php echo $_SESSION['client_name']; ?></a> </span>
                  </li>
                  <li id="main_menu">
                     <span> <a style="color: #02afdc"  href="<?php echo site_url('home/logout'); ?>"   ><i class="fa fa-sign-out" aria-hidden="true"></i> </i>&nbsp; Sign Out</a></span>
                  </li>
                  <?php   }else{ ?>
                  <li id="main_menu">
                     <span>  <a  style="color: #02afdc"  href="<?php echo site_url('home/login'); ?>"><i class="fa fa-user" aria-hidden="true"></i></i>&nbsp; Log In</a>
                     </span>
                  </li>
                  <li id="main_menu">
                     <span>   <a  style="color: #02afdc"  href="<?php echo site_url('home/signup'); ?>"  ><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp; Sign Up</a>
                     </span>
                  </li>
                  <?php   }  ?>
               </ul>
            </div>
         </div>
      </nav>


<br/>
  
<?php if($ticket==null)
{ ?>
    <div class="search-text" style="margin-top: 100px;margin-bottom: 200px"> 
       <div class="container">
         <div class="row text-center">
         <h2 style="color:white"> Get Your Reservation Information Here ! </h2>
           <div class="form" method="post" action="<?php echo base_url(); ?>/index.php/Reservation/ticket">
                <form id="search-form" class="form-search form-horizontal">
                    <input type="text" class="input-search" name="ticket" placeholder="Enter Ticket No.">
                    <button type="submit" class="btn-search"><i class="fa fa-search" aria-hidden="true"></i> Search</button>
                </form>
            </div>
        
          </div>  
          <br>       
       </div>     
   </div>
    

 <?php }else{ ?>


<?php if($tickets==null){ ?>

   <div class="search-text" style="margin-top: 100px;margin-bottom: 200px"> 
       <div class="container">
         <div class="row text-center">
         <h2 style="color:white"><b>Opps! </b> Your Given Ticket Number is Invalid ! Please Try Again. </h2>
         <br>
           <div class="form" method="post" action="<?php echo base_url(); ?>/index.php/Reservation/ticket">
                <form id="search-form" class="form-search form-horizontal">
                    <input type="text" class="input-search" name="ticket" placeholder="Enter Ticket No.">
                    <button type="submit" class="btn-search"><i class="fa fa-search" aria-hidden="true"></i> Search</button>
                </form>
            </div>
        
          </div>  
          <br>       
       </div>     
   </div>

<?php }else{ ?>
<div class="container">
   <div class="row">
 
        
        
       <div class="col-md-12 ">

<div class="panel panel-default">
  <div class="panel-heading">  <h2 >Reservation Info</h2></div>
   <div class="panel-body">
       
    <div class="box box-info">

        
            <div class="box-body">
                     <div class="col-sm-6">
                        <?php $msg="Name: ".$tickets[0]["user_name"]."%0AEmail: ".$tickets[0]["user_email"]."%0ATicket: ".$tickets[0]["serial"]; ?>
                     <div  align="center"> <img alt="qr" src="https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=<?php echo $msg; ?>" id="profile-image1" class="img-responsive"> 
                
                <input id="profile-image-upload" class="hidden" type="file">
<div style="color:#999;" >QR Code contains ticket info.</div>
                <!--Upload Image Js And Css-->
           
              
   
                
                
                     
                     
                     </div>
              
              <br>
    
              <!-- /input-group -->
            </div>
            <div class="col-sm-6" style="text-align: left">
            <h4 style="color:#00b1b1;font-size: 28px"><?php echo $tickets[0]["user_name"];?> </h4></span>
              <span><i class="fa fa-envelope" aria-hidden="true"></i> <?php echo $tickets[0]["user_email"];?></span>     
              <span><p><i class="fa fa-phone-square" aria-hidden="true"></i> <?php echo $tickets[0]["user_mobile"];?></p></span> 
             

              <span><div class="btn-group btn-group-justified" style="width: 70%">
  <a href="<?php echo base_url();?>" class="btn btn-primary btn-lg"><i class="fa fa-search" aria-hidden="true"></i> Book Another</a>
  
   <?php   if($tickets[0]["status"]==1)
  { ?>
  <a href="#" class="btn btn-danger btn-lg"><i class="fa fa-remove" aria-hidden="true"></i> Cancel/Refund</a>
 <?php }else{
   ?>
     <a href="#" class="btn btn-success btn-lg"><i class="fa fa-refresh" aria-hidden="true"></i> Retry</a>
  <?php } ?>
</div></span>      
            </div>
            <div class="clearfix"></div>
            <hr style="margin:5px 0 5px 0;">
    
    <div class="col-sm-5 col-xs-6 tital " >Ticket No: </div><div class="col-sm-7 col-xs-6 "><?php echo $tickets[0]["serial"];?></div>
     <div class="clearfix"></div>
<div class="bot-border"></div>
              
<div class="col-sm-5 col-xs-6 tital " >Spot Name: </div><div class="col-sm-7 col-xs-6 "><?php echo $tickets[0]["spot"];?> </div>
     <div class="clearfix"></div>
<div class="bot-border"></div>

 

<div class="col-sm-5 col-xs-6 tital " >Amount Paid:</div><div class="col-sm-7"> $<?php echo $tickets[0]["paid_amount"];?></div>
  <div class="clearfix"></div>
<div class="bot-border"></div>

<div class="col-sm-5 col-xs-6 tital " >Arrival:</div><div class="col-sm-7"><?php echo $tickets[0]["arrive"];?></div>

  <div class="clearfix"></div>
<div class="bot-border"></div>

<div class="col-sm-5 col-xs-6 tital " >Depart:</div><div class="col-sm-7"><?php echo $tickets[0]["depart"];?></div>

  <div class="clearfix"></div>
<div class="bot-border"></div>

<div class="col-sm-5 col-xs-6 tital " >Spot Type:</div><div class="col-sm-7"><?php echo $tickets[0]["spot_type"];?></div>

 <div class="clearfix"></div>
<div class="bot-border"></div>

<div class="col-sm-5 col-xs-6 tital " >Duration:</div><div class="col-sm-7"><?php echo $tickets[0]["duration_day"]." Day, ".$tickets[0]["duration_hour"]." Hrs";?></div>

 <div class="clearfix"></div>
<div class="bot-border"></div>

  <?php 
  $status="";
  if($tickets[0]["status"]==1)
  {
   $status="<p class='text-success'><b>Completed</b></p>";
  } else
  {
   $status="<p class='text-danger'><b>Pending / Cancelled</b></p>";
  }?>

<div class="col-sm-5 col-xs-6 tital " >Patment Status:</div><div class="col-sm-7"><?php echo $status;?></div>
 
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
 
            
    </div> 

       <div class="panel-group">
  <div class="panel panel-success">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" href="#collapse1"> <i class="fa fa-car" aria-hidden="true"></i> Vehicle Info &nbsp; <i class="fa fa-chevron-down" aria-hidden="true"></i> </a>
      </h4>
    </div>
    <div id="collapse1" class="panel-collapse collapse">
      <ul class="list-group">
        <li class="list-group-item">Type : <b><?php echo $tickets[0]["v_type"];?></b></li>
        <li class="list-group-item">Make : <b><?php echo $tickets[0]["v_make"];?></b></li>
        <li class="list-group-item">Model : <b><?php echo $tickets[0]["v_model"];?></b></li>

           <li class="list-group-item">Year : <b><?php echo $tickets[0]["v_year"];?></b></li>
        <li class="list-group-item">Color : <b><?php echo $tickets[0]["v_color"];?></b></li>
        <li class="list-group-item">State : <b><?php echo $tickets[0]["v_state"];?></b></li>
      </ul>
      <div class="panel-footer">Happy Parking :)</div>
    </div>
  </div>
</div>
    </div>

 <?php } ?>
</div>  

    <script>
              $(function() {
    $('#profile-image1').on('click', function() {
        $('#profile-image-upload').click();
    });
});       
              </script> 
       
       
       
       
       
       
       
       
       
   </div>
</div>




         






 <?php } ?>





      <script src="<?php echo base_url(); ?>assets/wizard/js/jquery-1.11.1.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
      
      <script src="<?php echo base_url(); ?>assets/wizard/bootstrap/js/bootstrap.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/wizard/js/jquery.backstretch.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/wizard/js/retina-1.1.0.min.js"></script>
 
      <!--[if lt IE 10]>
      <script src="assets/js/placeholder.js"></script>
      <![endif]-->
      <style type="text/css">

                    input.hidden {
    position: absolute;
    left: -9999px;
}

#profile-image1 {
    cursor: pointer;
  
     width: 150px;
    height: 150px;
    }
   .tital{ font-size:16px; font-weight:500;}
    .bot-border{ border-bottom:1px #f8f8f8 solid;  margin:5px 0  5px 0} 


         .navbar-brand {
         background-image: url(<?php echo base_url(); ?>assets/wizard/img/logo@2x.png) !important; background-repeat: no-repeat !important; background-size: 162px 36px !important;
         }
         .navbar-brand {
         width: 162px;
         background: url(<?php echo base_url(); ?>assets/wizard/img/logo.png) left center no-repeat;
         text-indent: -99999px;
         }
         #main_menu
         {
         margin-right: 25px;
            font-size: 17px;
            font-weight: bold;
            padding-right: 10px;
         }
         span
         {
            color: black;
         }
         .text-muted
         {
         color: #03A9F4;
         }

         h3
         {
         color:white;
         }
         p
         {
         color: black;
         }
               .error
         {
         color: orangered;
         }

         .search-text{
   margin-top:50px;
   background-color:#7dabdb;
   padding-top:60px;
   padding-bottom:60px;
}
   
.search-text .input-search{
   height:55px;
   width:400px;
   padding-left:20px;
    color:#333;
    background-color: rgba(255, 255, 255, 0.16);
    border-radius: 0;
} 

.search-text .btn-search{
   background-color:#7dabdb;
   border:1px solid #FFF;
   color:#FFF;
   padding: 11px 22px;
}

.search-text  .btn-search:hover{
   background-color:#FFF;
   color:#7dabdb;
}
      </style>
      <footer  style="background-color: black;color: black;">
         <div class="row">
            <div class="col-md-2">
               <img src="<?php echo base_url(); ?>assets/img/logo.png" style="width: 45%;margin-top: 20px">
               <br/>
               <small class="d-block mb-3 text-muted">&copy; 2017-2018</small>
            </div>
            <div class="col-md-3">
               <h3>Features</h3>
               <ul class="list-unstyled text-small">
                  <li><a class="text-muted" href="#">Monthly Rent</a></li>
                  <li><a class="text-muted" href="#">Weekly Rent</a></li>
                  <li><a class="text-muted" href="#">Event Rent</a></li>
               </ul>
            </div>
            <div class="col-md-3">
               <h3>Work With Us</h3>
               <ul class="list-unstyled text-small">
                  <li><a class="text-muted" href="<?php echo site_url('admin'); ?>">Admin Login</a></li>
                  <li><a class="text-muted" href="<?php echo site_url('panel'); ?>">Parking Owners Login</a></li>
                  <li><a class="text-muted" href="#">Venue</a></li>
                  <li><a class="text-muted" href="#">Producs</a></li>
                  <li><a class="text-muted" href="#">App</a></li>
               </ul>
            </div>
            <div class="col-md-2">
               <h3>Follow Us</h3>
               <ul class="list-unstyled text-small">
                  <li><a class="text-muted" href="#">Facebook</a></li>
                  <li><a class="text-muted" href="#">Twitter</a></li>
                  <li><a class="text-muted" href="#">Instagram</a></li>
                  <li><a class="text-muted" href="#">Google Plus</a></li>
               </ul>
            </div>
            <div class="col-md-2">
               <h3>Support</h3>
               <ul class="list-unstyled text-small">
                  <li><a class="text-muted" href="#">About </a></li>
                  <li><a class="text-muted" href="#">Contact Us</a></li>
                  <li><a class="text-muted" href="#">FAQ</a></li>
                  <li><a class="text-muted" href="#">Term & Conditions</a></li>
               </ul>
            </div>
         </div>
      </footer>
       
        </body>
</html>