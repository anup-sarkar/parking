<?php
   defined('BASEPATH') OR exit('No direct script access allowed');
   
    date_default_timezone_set("Asia/Dhaka");
   ?>

<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
   <!--  Fonts and icons     -->
  <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
<!--
Responsive Email Template by @keenthemes
A component of Metronic Theme - #1 Selling Bootstrap 3 Admin Theme in Themeforest: http://j.mp/metronictheme
Licensed under MIT
-->

<div id="mailsub" class="notification" align="center">

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="min-width: 320px;"><tr><td align="center" bgcolor="#eff3f8">


<!--[if gte mso 10]>
<table width="680" border="0" cellspacing="0" cellpadding="0">
<tr><td>
<![endif]-->

<table border="0" cellspacing="0" cellpadding="0" class="table_width_100" width="100%" style="max-width: 680px; min-width: 300px;">
    <tr><td>
	<!-- padding --><div style="height: 80px; line-height: 80px; font-size: 10px;"> </div>
	</td></tr>
	<!--header -->
	<tr><td align="center" bgcolor="#ffffff">
		<!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;"> </div>
		<table width="90%" border="0" cellspacing="0" cellpadding="0">
			<tr><td align="left"><!-- 

				Item --><div class="mob_center_bl" style="float: left; display: inline-block; width: 115px;">
					<table class="mob_center" width="115" border="0" cellspacing="0" cellpadding="0" align="left" style="border-collapse: collapse;">
						<tr><td align="left" valign="middle">
							<!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"> </div>
							<table width="115" border="0" cellspacing="0" cellpadding="0" >
								<tr><td align="left" valign="top" class="mob_center">
									<a href="#" target="_blank" style="color: #428bca; font-family: Arial, Helvetica, sans-serif; font-size: 13px;">
									<font face="Arial, Helvetica, sans-seri; font-size: 13px;" size="3" color="#428bca">
									<img src="<?php echo base_url();?>assets/img/logo_new.png" width="115"   alt="Hello Hi Parking" border="0" style="display: block;" /></font></a>
								</td></tr>
							</table>						
						</td></tr>
					</table></div><!-- Item END--><!--[if gte mso 10]>
					</td>
					<td align="right">
				<![endif]--><!-- 

				Item --><div class="mob_center_bl" style="float: right; display: inline-block; width: 88px;">
					<table width="88" border="0" cellspacing="0" cellpadding="0" align="right" style="border-collapse: collapse;">
						<tr><td align="right" valign="middle">
							<!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"> </div>
							<table width="100%" border="0" cellspacing="0" cellpadding="0" >
								<tr><td align="right">
									<!--social -->
									<div class="mob_center_bl" style="width: 120px;">
									<table border="0" cellspacing="0" cellpadding="0">
										<tr><td width="30" align="center" style="line-height: 19px;padding-right: 6px">
											<a href="#" target="_blank" style="color: #428bca; font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
											<font face="Arial, Helvetica, sans-serif" size="5" color="#428bca">
											<i class="fa fa-facebook" aria-hidden="true"></i>	</font></a>
										</td><td width="39" align="center" style="line-height: 19px;padding-right: 6px">
											<a href="#" target="_blank" style="color: #428bca; font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
											<font face="Arial, Helvetica, sans-serif" size="5" color="#428bca">
										<i class="fa fa-twitter" aria-hidden="true"></i> </font></a>
										</td><td width="29" align="right" style="line-height: 19px;;padding-right: 6px">
											<a href="#" target="_blank" style="color: #428bca; font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
											<font face="Arial, Helvetica, sans-serif" size="5" color="#428bca">
											<i class="fa fa-google-plus" aria-hidden="true"></i></font></a>
										</td>


											<td width="29" align="right" style="line-height: 19px;;padding-right: 6px">
											<a href="#" target="_blank" style="color: #428bca; font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
											<font face="Arial, Helvetica, sans-serif" size="5" color="#428bca">
											<i class="fa fa-android" aria-hidden="true"></i></font></a>
										</td>


										<td width="29" align="right" style="line-height: 19px;;padding-right: 6px">
											<a href="#" target="_blank" style="color: #428bca; font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
											<font face="Arial, Helvetica, sans-serif" size="5" color="#428bca">
											<i class="fa fa-apple" aria-hidden="true"></i></font></a>
										</td>
									</tr>
									</table>
									</div>
									<!--social END-->
								</td></tr>
							</table>
						</td></tr>
					</table></div><!-- Item END--></td>
			</tr>
		</table>
		<!-- padding --><div style="height: 50px; line-height: 50px; font-size: 10px;"> </div>
	</td></tr>
	<!--header END-->

	<!--content 1 -->
	<tr><td align="center" bgcolor="#fbfcfd">
		<table width="90%" border="0" cellspacing="0" cellpadding="0">
			<tr><td align="center">
				<!-- padding --><div style="height: 60px; line-height: 60px; font-size: 10px;"> </div>
				<div style="line-height: 44px;">
					<font face="Arial, Helvetica, sans-serif" size="5"   style="font-size: 34px;">
					<span style="font-family: Arial, Helvetica, sans-serif; font-size: 34px; color: #596167;">
				 Your reservations is confirmed !
					</span></font>
				</div>
				<!-- padding --><div style="height: 40px; line-height: 40px; font-size: 10px;"> </div>
			</td></tr>
			<tr><td align="center">
				<div style="line-height: 24px;">
					<font face="Arial, Helvetica, sans-serif" size="4" color="#02b1df" style="font-size: 15px;">
					<span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">
						To access your spot & pass, Please follow this instruction. 
					</span></font>
				</div>
				<!-- padding --><div style="height: 40px; line-height: 40px; font-size: 10px;"> </div>
			</td></tr>
			<tr>
				<td>  <?php $msg="Name: ".$trans[0]["user_name"]."%0AEmail: ".$trans[0]["user_email"]."%0ATicket: ".$trans[0]["serial"]; ?>
                     <div  align="center" style="text-align: center;"> <img alt="qr" src="https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=<?php echo $msg; ?>" >

                      </td>
			</tr>
			<tr><td> <br></td></tr>
			<tr>

				<td align="center">
				<div style="line-height: 24px;">
					<a     href="<?php echo base_url()."index.php/reservations/ticket/".$trans[0]['serial'];?>"  target="_blank" class="btn btn-primary btn-lg" style="font-family:Arial,Helvetica,sans-serif;font-size:35px;background-color: #4d90fe;padding:  14px;color:  white;text-decoration:  none;"><i class="fa fa-car" aria-hidden="true"></i> Get Your Pass
						</a>
				</div>
				<!-- padding --><div style="height: 60px; line-height: 60px; font-size: 10px;"> </div>
			</td></tr>
		</table>		
	</td> 





<tr>
<td >

	
<div  >
                     <div >
                        <div class="well col-md-12" style="margin:0">
                           <div class="row" style="margin-left: 7px">
                              <div style="text-align: center;">
                                 <img src="<?php echo base_url();?>assets/img/logo_sm2.png" style="height: 24px" href="https://hellohiparking.com">
                                 <hr>
                              </div>
                              <div class="col-xs-6 col-sm-6 col-md-6">
                                 <address style="text-align: left">
                                    <p style="font-size: 25px"><i class="fa fa-user-circle-o" aria-hidden="true"></i> <span id="uname">Name: <?php echo $trans[0]['user_name'];?></span></p>
                                    <p><i class="fa fa-envelope" aria-hidden="true"></i>  <span id="email">Email: <?php echo $trans[0]['user_email'];?></span> </p>
                                    <p><i class="fa fa-phone-square" aria-hidden="true"></i> <span id="mobile">Mobile: <?php echo $trans[0]['user_mobile'];?></span> 
                                    </p>
                                 </address>
                              </div>
                              <div class="col-xs-6 col-sm-6 col-md-6 text-right">
                                 <p>
                                    <em><b>Date:  <?php echo date(" d M Y - h:i a ",strtotime($trans[0]['date_transaction'])); ?></b> </em>
                                 </p>
                                 <p style="margin-bottom: 0">
                                    <em>Arrival #:     <?php echo date(" d M Y - h:i a ",strtotime($trans[0]['arrive'])); ?> </em>
                                 </p>
                                 <p style="margin-bottom: 0">
                                    <em>Depart #:  <?php echo date(" d M Y - h:i a ",strtotime($trans[0]['depart'])); ?>  </em>
                                 </p>

                                 <br/>
                                    <p style="margin-bottom: 0">
                                    <em><b>Pass #:  <?php echo  $trans[0]['serial']; ?> &nbsp;&nbsp;&nbsp; &nbsp;</b></em>
                                 </p>
                              </div>
                           </div>


                           <div class="row" style="text-align: center;">
                              <div class="text-center" >
                                 <h1>Receipt</h1>
                              </div>
                              
                              <t <tr="">
                                       </t><table class="table table-hover" style="width: 100%">
                                 <thead>
                                    <tr>
                                       <th>Parking Spot</th>
                                       <th class="text-center">Price</th>
                                       <th class="text-center" colspan="2">Duration</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td class="col-md-9" style=" "><em><b><?php echo  $trans[0]['spot']; ?></b></em></td>
                                       <td class="col-md-1 text-center"><?php echo  $trans[0]['paid_amount']; ?></td>
                                       <td class="col-md-1 text-center" colspan="2"> <span id="day"><?php echo  $trans[0]['duration_day']; ?>  </span> day, <span id="hour"><?php echo  $trans[0]['duration_hour']; ?>  </span> Hrs<p></p> </td>
                                    </tr>
                                    <tr><td>   </td>
                                       <td>   </td>
                                       <td class="text-right">
                                          <p>
                                             <strong>Subtotal: </strong>
                                          </p>
                                          <p>
                                             <strong>Convenient Fee: </strong>
                                          </p>
                                       </td>
                                       <td class="text-center">
                                          <p>
                                             <strong>$<?php echo  $trans[0]['paid_amount']-1; ?></strong>
                                          </p>
                                          <p>
                                             <strong>$1.00</strong>
                                          </p>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td>   </td>
                                       <td>   </td>
                                       <td class="text-right">
                                          <h4><strong>Total: </strong></h4>
                                       </td>
                                       <td class="text-center text-danger">
                                          <h4><strong>$<?php echo  $trans[0]['paid_amount']; ?></strong></h4>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                         
                              <br>
                       
                             

   
                              
                           </div>
                        </div>
                     </div>
             
               </div>
</td>

</tr>




















	<!--content 2 -->
	<tr><td align="center" bgcolor="#ffffff" style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #eff2f4;">
		<table width="94%" border="0" cellspacing="0" cellpadding="0">
			<tr><td align="center">
				<!-- padding --><div style="height: 40px; line-height: 40px; font-size: 10px;"> </div>

				<div class="mob_100" style="float: left; display: inline-block; width: 33%;">
					<table class="mob_100" width="100%" border="0" cellspacing="0" cellpadding="0" align="left" style="border-collapse: collapse;">
						<tr><td align="center" style="line-height: 14px; padding: 0 27px;">
							<!-- padding --><div style="height: 40px; line-height: 40px; font-size: 10px;"> </div>
							<img src="<?php echo base_url();?>assets/img/fast.png" style="width: 90px;padding-bottom: 10px">
							<br/>
							<div style="line-height: 14px;">

								<font face="Arial, Helvetica, sans-serif" size="3" color="#4db3a4" style="font-size: 14px;">
								<strong style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #4db3a4;">
									<a href="#1" target="_blank" style="color: #4db3a4; text-decoration: none;">Super Fast Parking</a>
								</strong></font>
							</div>
							<!-- padding --><div style="height: 18px; line-height: 18px; font-size: 10px;"> </div>
							<div style="line-height: 21px;">
								<font face="Arial, Helvetica, sans-serif" size="3" color="#98a7b9" style="font-size: 14px;">
								<span style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #98a7b9;">Book your spot & Park your car  in seconds and do your things.
								</span></font>
							</div>
						</td></tr>
					</table>
				</div>


				<div class="mob_100" style="float: left; display: inline-block; width: 33%;">
					<table class="mob_100" width="100%" border="0" cellspacing="0" cellpadding="0" align="left" style="border-collapse: collapse;">
						<tr><td align="center" style="line-height: 14px; padding: 0 27px;">
							<!-- padding --><div style="height: 40px; line-height: 40px; font-size: 10px;"> </div>
							<img src="<?php echo base_url();?>assets/img/best.png" style="width: 90px;padding-bottom: 20px">
							<br/>
							<div style="line-height: 14px;">
								<font face="Arial, Helvetica, sans-serif" size="3" color="#4db3a4" style="font-size: 14px;">
								<strong style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #4db3a4;">
									<a href="#2" target="_blank" style="color: #4db3a4; text-decoration: none;">Best Deal</a>
								</strong></font>
							</div>
							<!-- padding --><div style="height: 18px; line-height: 18px; font-size: 10px;"> </div>
							<div style="line-height: 21px;">
								<font face="Arial, Helvetica, sans-serif" size="3" color="#98a7b9" style="font-size: 14px;">
								<span style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #98a7b9;">
									Find   best deal with affordable rate all over the country.
								</span></font>
							</div>
						</td></tr>
					</table>
				</div>
				<div class="mob_100" style="float: left; display: inline-block; width: 33%;">
					<table class="mob_100" width="100%" border="0" cellspacing="0" cellpadding="0" align="left" style="border-collapse: collapse;">
						<tr><td align="center" style="line-height: 14px; padding: 0 27px;">
							<!-- padding --><div style="height: 40px; line-height: 40px; font-size: 10px;"> </div>
							<img src="<?php echo base_url();?>assets/img/hassle.png" style="width: 90px;padding-bottom: 30px">
							<br/>
							<div style="line-height: 14px;">
								<font face="Arial, Helvetica, sans-serif" size="3" color="#4db3a4" style="font-size: 14px;">
								<strong style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #4db3a4;">
									<a href="#3" target="_blank" style="color: #4db3a4; text-decoration: none;">Hassle-Free Booking</a>
								</strong></font>
							</div>
							<!-- padding --><div style="height: 18px; line-height: 18px; font-size: 10px;"> </div>
							<div style="line-height: 21px;">
								<font face="Arial, Helvetica, sans-serif" size="3" color="#98a7b9" style="font-size: 14px;">
								<span style="font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #98a7b9;">
									No need to search for parking here and there.Hello Hi Parking is there for you.
								</span></font>
							</div>
						</td></tr>
					</table>
				</div>								
			</td></tr>
			<tr><td><!-- padding --><div style="height: 80px; line-height: 80px; font-size: 10px;"> </div></td></tr>
		</table>		
	</td></tr>
	<!--content 2 END-->

	<!--links -->
	<tr><td align="center" bgcolor="#ffffff" style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #eff2f4;">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr><td align="center">
				<!-- padding --><div style="height: 32px; line-height: 32px; font-size: 10px;"> </div>
        <table width="80%" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td align="center" valign="middle" style="font-size: 12px; line-height: 22px;">
            	<font face="Tahoma, Arial, Helvetica, sans-serif" size="2" color="#282f37" style="font-size: 12px;">
								<span style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #5b9bd1;">
		              <a href="#" target="_blank" style="color: #5b9bd1; text-decoration: none;">ABOUT US</a>
		                   
		              <a href="#" target="_blank" style="color: #5b9bd1; text-decoration: none;">APP</a>
		                  
		              <a href="#" target="_blank" style="color: #5b9bd1; text-decoration: none;">FIND PARKING</a>
		                  
		               
		                  
		              <a href="#" target="_blank" style="color: #5b9bd1; text-decoration: none;">FAQ</a>
              </span></font>
            </td>
          </tr>                                        
        </table>
			</td></tr>
			<tr><td><!-- padding --><div style="height: 32px; line-height: 32px; font-size: 10px;"> </div></td></tr>
		</table>		
	</td></tr>
	<!--links END-->

	<!--content 3 -->
	<tr><td align="center" bgcolor="#ffffff" style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #eff2f4;">
		<table width="94%" border="0" cellspacing="0" cellpadding="0">
			<tr><td align="center">
				<!-- padding --><div style="height: 80px; line-height: 80px; font-size: 10px;"> </div>

				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr><td align="center">
						<font face="Arial, Helvetica, sans-serif" size="3" color="#57697e" style="font-size: 26px;">
						<span style="font-family: Arial, Helvetica, sans-serif; font-size: 26px; color: #57697e;">
							Featured Cities
						</span></font>				
					</td></tr>			
				</table>				

				<div class="mob_100" style="float: left; display: inline-block; width: 33%;">
					<table class="mob_100" width="100%" border="0" cellspacing="0" cellpadding="0" align="left" style="border-collapse: collapse;">
						<tr><td align="center" style="line-height: 14px; padding: 0 10px;">
							<!-- padding --><div style="height: 40px; line-height: 40px; font-size: 10px;"> </div>
							<div style="line-height: 14px;">							
								<a href="#" target="_blank" style="color: #428bca; font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
									<font face="Arial, Helvetica, sans-serif" size="2" color="#428bca">
									<img src="<?php echo base_url(); ?>assets/img/city/atlanta.jpg" width="185" alt="Atlanta" border="0" style="display: block; width: 100%; height: auto;" /></font></a>
									<h4>Atlanta</h4>
							</div>
						</td></tr>
					</table>
				</div>
				<div class="mob_100" style="float: left; display: inline-block; width: 33%;">
					<table class="mob_100" width="100%" border="0" cellspacing="0" cellpadding="0" align="left" style="border-collapse: collapse;">
						<tr><td align="center" style="line-height: 14px; padding: 0 10px;">
							<!-- padding --><div style="height: 40px; line-height: 40px; font-size: 10px;"> </div>
							<div style="line-height: 14px;">							
								<a href="#" target="_blank" style="color: #428bca; font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
									<font face="Arial, Helvetica, sans-serif" size="2" color="#428bca">
									<img src="<?php echo base_url(); ?>assets/img/city/vegas.jpg"  width="185" alt="Vegas" border="0" style="display: block; width: 100%; height: auto;" /></font></a>

									<h4>Vegas </h4>
							</div>
						</td></tr>
					</table>
				</div>
				<div class="mob_100" style="float: left; display: inline-block; width: 33%;">
					<table class="mob_100" width="100%" border="0" cellspacing="0" cellpadding="0" align="left" style="border-collapse: collapse;">
						<tr><td align="center" style="line-height: 14px; padding: 0 10px;">
							<!-- padding --><div style="height: 40px; line-height: 40px; font-size: 10px;"> </div>
							<div style="line-height: 14px;">							
								<a href="#" target="_blank" style="color: #428bca; font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
									<font face="Arial, Helvetica, sans-serif" size="2" color="#428bca">
									<img src="<?php echo base_url(); ?>assets/img/city/york.jpg"  width="185" alt="New York" border="0" style="display: block; width: 100%; height: auto;" /></font></a>
									<h4>New York</h4>
							</div>
						</td></tr>
					</table>
				</div>								
			</td></tr>
			<tr><td><!-- padding --><div style="height: 80px; line-height: 80px; font-size: 10px;"> </div></td></tr>
		</table>		
	</td></tr>
	<!--content 3 END-->

	<!--brands -->
	<tr><td align="center" bgcolor="#ffffff" style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #eff2f4;">
		<table width="94%" border="0" cellspacing="0" cellpadding="0">
			<tr><td align="center">

				<div class="mob_100" style="float: left; display: inline-block; width: 25%;">
					<table class="mob_100" width="100%" border="0" cellspacing="0" cellpadding="0" align="left" style="border-collapse: collapse;">
						<tr><td align="center" style="line-height: 14px; padding: 0 24px;">
							<!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;"> </div>
							<div style="line-height: 14px;">							
								<a href="#" target="_blank" style="color: #428bca; font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
									<font face="Arial, Helvetica, sans-serif" size="2" color="#428bca">
									<img src="<?php echo base_url(); ?>assets/img/city/chicago.jpg" width="125" alt="Chicago" border="0" class="mob_width_50" style="display: block; width: 100%; height: auto;" /></font></a>
									<h4>Chicago</h4>
							</div>
						</td></tr>
					</table>
				</div>

				<div class="mob_100" style="float: left; display: inline-block; width: 25%;">
					<table class="mob_100" width="100%" border="0" cellspacing="0" cellpadding="0" align="left" style="border-collapse: collapse;">
						<tr><td align="center" style="line-height: 14px; padding: 0 24px;">
							<!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;"> </div>
							<div style="line-height: 14px;">							
								<a href="#" target="_blank" style="color: #428bca; font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
									<font face="Arial, Helvetica, sans-serif" size="2" color="#428bca">
									<img  src="<?php echo base_url(); ?>assets/img/city/atlanta.jpg" width="107" alt="Atlanta" border="0" class="mob_width_50" style="display: block; width: 100%; height: auto;" /></font></a>
									<h4>Atlanta</h4>
							</div>
						</td></tr>
					</table>
				</div>

				<div class="mob_100" style="float: left; display: inline-block; width: 25%;">
					<table class="mob_100" width="100%" border="0" cellspacing="0" cellpadding="0" align="left" style="border-collapse: collapse;">
						<tr><td align="center" style="line-height: 14px; padding: 0 24px;">
							<!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;"> </div>
							<div style="line-height: 14px;">							
								<a href="#" target="_blank" style="color: #428bca; font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
									<font face="Arial, Helvetica, sans-serif" size="2" color="#428bca">
									<img src="<?php echo base_url(); ?>assets/img/city/florida.jpg" width="103" alt="Fllorida" border="0" class="mob_width_50" style="display: block; width: 100%; height: auto;" /></font></a>
									<h4>Florida</h4>
							</div>
						</td></tr>
					</table>
				</div>

				<div class="mob_100" style="float: left; display: inline-block; width: 25%;">
					<table class="mob_100" width="100%" border="0" cellspacing="0" cellpadding="0" align="left" style="border-collapse: collapse;">
						<tr><td align="center" style="line-height: 14px; padding: 0 24px;">
							<!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;"> </div>
							<div style="line-height: 14px;">							
								<a href="#" target="_blank" style="color: #428bca; font-family: Arial, Helvetica, sans-serif; font-size: 12px;">
									<font face="Arial, Helvetica, sans-serif" size="2" color="#428bca">
									<img src="<?php echo base_url(); ?>assets/img/city/atlanta.jpg" width="116" alt="California" border="0" class="mob_width_50" style="display: block; width: 100%; height: auto;" /></font></a>
									<h4>California</h4>
							</div>
						</td></tr>
					</table>
				</div>

			</td></tr>
			<tr><td><!-- padding --><div style="height: 28px; line-height: 28px; font-size: 10px;"> </div></td></tr>
		</table>		
	</td></tr>
	<!--brands END-->

	<!--footer -->
	<tr><td class="iage_footer" align="center" bgcolor="#ffffff">
		<!-- padding --><div style="height: 80px; line-height: 80px; font-size: 10px;"> </div>	
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr><td align="center">
				<font face="Arial, Helvetica, sans-serif" size="3" color="#96a5b5" style="font-size: 13px;">
				<span style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #96a5b5;">
				 <a href="http://brosoft.biz/site/">
                    &copy; <b>2018</b> &nbsp;  <img src="<?php echo base_url();?>assets/img/logo_sm2.png" style="font-family: Arial, Helvetica, sans-serif;font-size: 23px;width: 10%;" href="https://hellohiparking.com">&nbsp;&nbsp;|&nbsp;  All Right Reserved  </a>
				</span></font>				
			</td></tr>			
		</table>
		
		<!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;"> </div>	
	</td></tr>
	<!--footer END-->
	<tr><td>
	<!-- padding --><div style="height: 80px; line-height: 80px; font-size: 10px;"> </div>
	</td></tr>
</table>
<!--[if gte mso 10]>
</td></tr>
</table>
<![endif]-->
 
</td></tr>
</table>
			
</div> 

<br>
<br>
<center>
<table>
	<tr> 
		<td> <strong>Powered by &nbsp;</strong>  </td>
		<td>  <a href="" target="_blank">  <img src="<?php echo base_url(); ?>assets/img/brosoft.png" style="padding-bottom: 4px;width: 80px;" />  </a>  </td>

	</tr>
	</table>
	</center>
 
<br>
<br>


<style type="text/css">
	
/***
User Profile Sidebar by @keenthemes
A component of Metronic Theme - #1 Selling Bootstrap 3 Admin Theme in Themeforest: http://j.mp/metronictheme
Licensed under MIT
***/

body {
    padding: 0;
    margin: 0;

}

html { -webkit-text-size-adjust:none; -ms-text-size-adjust: none;}
@media only screen and (max-device-width: 680px), only screen and (max-width: 680px) { 
    *[class="table_width_100"] {
		width: 96% !important;
	}
	*[class="border-right_mob"] {
		border-right: 1px solid #dddddd;
	}
	*[class="mob_100"] {
		width: 100% !important;
	}
	*[class="mob_center"] {
		text-align: center !important;
	}
	*[class="mob_center_bl"] {
		float: none !important;
		display: block !important;
		margin: 0px auto;
	}	
	.iage_footer a {
		text-decoration: none;
		color: #929ca8;
	}
	img.mob_display_none {
		width: 0px !important;
		height: 0px !important;
		display: none !important;
	}
	img.mob_width_50 {
		width: 40% !important;
		height: auto !important;
	}
}
.table_width_100 {
	width: 680px;
}


</style>