<?php
   defined('BASEPATH') OR exit('No direct script access allowed');
   
    date_default_timezone_set("Asia/Dhaka");
   ?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Hello Hi Parking </title>
      <!-- CSS -->
      <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/wizard/bootstrap/css/bootstrap.min.css">
      <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/wizard/css/form-elements.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/wizard/css/style.css">
       <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url(); ?>assets/img/favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url(); ?>assets/img/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url(); ?>assets/img/favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>assets/img/favicon/favicon-16x16.png">
<link rel="manifest" href="<?php echo base_url(); ?>assets/img/favicon/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="<?php echo base_url(); ?>assets/img/favicon/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
   </head>
   <body>
      <!-- Top menu -->
      <nav class="navbar navbar-inverse" role="navigation" style="background-color: black">
         <div class="container">
            <div class="navbar-header">
               <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-navbar-1">
               <span class="sr-only">Toggle navigation</span>
               <span class="icon-bar"></span>log
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               </button>
               <a class="navbar-brand" href="http://hellohiparking.com">Hello Hi Parking</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="top-navbar-1">
               <ul class="nav navbar-nav navbar-right">
                  <li id="main_menu">
                     <span><a id="cyan" href=""><i class="fa fa-android" aria-hidden="true"></i>&nbsp; Get The App</a></span>
                  </li>
                  <li id="main_menu">
                     <span><a id="cyan" href="<?php echo base_url(); ?>/index.php/reservations/ticket">  <i class="fa fa-ticket" aria-hidden="true"></i>&nbsp; Ticket</a></span>
                  </li>
                  <li id="main_menu">
                     <span><a href="" id="cyan" ><i  class="fa fa-product-hunt" aria-hidden="true"></i>&nbsp; About  </a></span>
                  </li>
                  <?php  if (isset($_SESSION['client_name'])) { ?>
                  <li id="main_menu">
                     <span> <a id="cyan" href="<?php echo site_url('home/profile'); ?>"   ><i class="fa fa-user-circle" aria-hidden="true"></i>&nbsp; <?php echo $_SESSION['client_name']; ?></a> </span>
                  </li>
                  <li id="main_menu">
                     <span> <a id="cyan" href="<?php echo site_url('home/logout'); ?>"   ><i class="fa fa-sign-out" aria-hidden="true"></i> </i>&nbsp; Sign Out</a></span>
                  </li>
                  <?php   }else{ ?>
                  <li id="main_menu">
                     <span>  <a id="cyan"  href="<?php echo site_url('home/login'); ?>"><i class="fa fa-user" aria-hidden="true"></i></i>&nbsp; Log In</a>
                     </span>
                  </li>
                  <li id="main_menu">
                     <span>   <a  id="cyan"  href="<?php echo site_url('home/signup'); ?>"  ><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp; Sign Up</a>
                     </span>
                  </li>
                  <?php   }  ?>
               </ul>
            </div>
         </div>
      </nav>

      <style type="text/css">
         #cyan
         {
             color: #02afdc;
             font-weight: bold;
         }
      </style>
      <!-- Top content -->
      <div class="top-content">
      <div class="container">
         <div class="row">
            <div class="col-sm-8 col-sm-offset-2 text">
               <h1>Parking Spot  <strong>RESERVATION</strong> Wizard</h1>
               <div class="description">
                  <h3 style=" color: #337ab7;"><strong>
                     Fill your info & <b>Book</b> your spot
                  </h3>
                  </strong>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-md-5" style="background-color: white;color: black;margin-top: 40px">
               <h2 style="margin:0;color:black">Spot Info </h2>
               <hr/>
               <div class="row">
                  <div class="col-md-4" style="">
                     <?php 
                        if($images!=null)
                        {
                        
                        
                        foreach ($images as $img) {
                        # code...
                        ?>
                     <img src="<?php echo base_url().$img->img_address;?>" style=" width: 150px">
                     <?php break;} }else{ ?>
                     <img src="<?php echo base_url();?>/assets/img/p1.jpg" style=" width: 150px">
                     <?php } ?>
                  </div>
                  <div class="col-md-8" style=" text-align: left">
                     <h4 style="margin:0">
                        <?php echo $spot[0]["public_name"]; ?>                             
                     </h4>
                     <div class="stats">
                        <?php echo $spot[0]["address"]; ?>                                
                     </div>
                     <button class="btn btn-primary" data-toggle="modal" data-target="#myModal" style="z-index: 10000">View Details</button>
                  </div>
               </div>
               <br/>
               <div class="panel panel-default">
                  <div class="panel-heading">Reservation Summary</div>
                  <div class="panel-body">
                     <div class="row">
                        <div class="col-md-6" style="text-align: left;">
                           <span><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> Arrive</span>
                        </div>
                        <div class="col-md-6"  style="text-align: right;">
                           <?php echo $from; ?>   
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-6"  style="text-align: left;">
                           <span><i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i> Depart</span>
                        </div>
                        <div class="col-md-6" style="text-align: right;">
                           <?php echo $to; ?>   
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-6"  style="text-align: left;">
                           <span><i class="fa fa-clock-o" aria-hidden="true"></i>  Duration:  </span>
                        </div>
                        <div class="col-md-6" style="text-align: right;">
                           <?php  if($type=="Flat") { ?>
                           <p  > <span id="day"><?php  echo $day+1; ?>  </span> day </p>

                                 <?php }else{ ?>
                          <p  > <span id="day"><?php  echo $day; ?>  </span> day, <span id="hour"><?php echo $hour; ?>  </span> Hrs</p>

                       <?php } ?>
                        </div>

               
                     </div>
                     <div class="row">
                        <div class="col-md-6"  style="text-align: left;">
                           <span><i class="fa fa-money" aria-hidden="true"></i> Base Price (<?php  
                              if($type=="Flat") 
                                 echo "Flat Rate";
                              else
                                 echo "Hourly Rate";
                                   ?>) </span>
                        </div>
                        <div class="col-md-6" style="text-align: right;">
                           <i class="fa fa-usd" aria-hidden="true"></i>   <?php echo $rate; ?>   
                        </div>
                     </div>

                             <div class="row">
                        <div class="col-md-6"  style="text-align: left;">
                           <span><i class="fa fa-money" aria-hidden="true"></i> Convenient Fee</span>
                        </div>
                        <div class="col-md-6" style="text-align: right;">
                           <i class="fa fa-usd" aria-hidden="true"></i>   1.00 
                        </div>
                     </div>
                     <hr/>
                     <div class="row">
                        <div class="col-md-6"  style="text-align: left;">
                           <h2>Total </h2>
                        </div>
                        <div class="col-md-6" style="text-align: right;">
                           <h2>  <i class="fa fa-usd" aria-hidden="true"></i><?php echo $rate+1; ?> </h2>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-7 form-box">
               <?php $attributes = array("class" => "f1", "id" => "f1", "name" => "f1");
                  echo form_open("Reservations/payment", $attributes);?>
               <h3 style="color:black">Account Info</h3>
               <p>Fill in the form to get instant booking</p>
               <div class="f1-steps">
                  <div class="f1-progress">
                     <div class="f1-progress-line" data-now-value="16.66" data-number-of-steps="3" style="width: 16.66%;"></div>
                  </div>
                  <div class="f1-step active">
                     <div class="f1-step-icon"><i class="fa fa-user"></i></div>
                     <p>Account</p>
                  </div>
                  <div class="f1-step">
                     <div class="f1-step-icon"><i class="fa fa-car" aria-hidden="true"></i></div>
                     <p>Vehicle Info</p>
                  </div>
                  <div class="f1-step">
                     <div class="f1-step-icon"><i class="fa fa-cc-paypal" aria-hidden="true"></i></div>
                     <p>Payment</p>
                  </div>
               </div>
               <fieldset>
                  <h4>Tell us who you are:</h4>
                  <div class="form-group">
                     <label   for="f1_first_name">Name <span style="color: red">*</span> </label>
                     <input type="text" name="f1_first_name" placeholder="First name..." class="f1-first_name form-control" id="f1_first_name">
                  </div>
                  <div class="form-group">
                     <label   for="f1_email">Email <span style="color: red">*</span></label>
                     <input type="text" name="f1_email" placeholder="Email..." class="f1_email form-control" id="f1_email"  required="required">
                  </div>
                  <div class="form-group">
                     <label   for="f1-mobile">Mobile No. <span style="color: red">*</span> </label>
                     <input class="f1_mobile form-control"  id="f1_mobile" name="f1_mobile"   type="text" placeholder="Ex. XXX-XXXXXXX" value="">
                  </div>
                  <div class="f1-buttons">
                     <button type="button" class="btn btn-next" id="b1">Next <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> </button>
                  </div>
               </fieldset>
               <fieldset id="vehicle">
                  <h4>Your Vehicle Info</h4>
                  <div class="row">
                     <div class="col-md-6">
                        <p style="text-align: left;font-weight: bolder;">Type <span style="color: red">*</span></p>
                        <input type="text" placeholder="Ex. Car/Truck" id="type" name="type" class="form-control" value="">
                       
                     </div>
                     <div class="col-md-6">
                        <p style="text-align: left;font-weight: bolder;">Make <span style="color: red">*</span></p>
                        <input type="text" placeholder="Ex. Audi/BMW" id="make" name="make" class="form-control" value="">
                               
                     </div>
                  </div>
                  <br/>
                  <div class="row">
                     <div class="col-md-6">
                        <p style="text-align: left;font-weight: bolder;">Model <span style="color: red">*</span></p>
                        <input type="text" placeholder="Vehicle Model" id="model" name="model" class="form-control" value="">
                     
                     </div>
                     <div class="col-md-6">
                        <p style="text-align: left;font-weight: bolder;">Year</p>
                        <select    id="year" name="year" class="form-control"   style="height: 43px" >
                        <?php for($i=2018;$i>1980;$i--)
                           {
                            echo "<option value='".$i."'>".$i." </option>";
                           }
                           
                           ?>
                        </select>
                        
                     </div>
                  </div>
                  <br/>
                  <div class="row">
                     <div class="col-md-6">
                        <p style="text-align: left;font-weight: bolder;">Color</p>
                        <input type="text" placeholder="Vehicle Color" id="color" name="color" class="form-control" value="">
                     </div>
                     <div class="col-md-6">
                        <p style="text-align: left;font-weight: bolder;">Licence State</p>
                        <input type="text" placeholder="State" id="state" name="state" class="form-control" value="">
                     </div>
                  </div>
                  <br/>
                  <div class="row">
                     <div class="col-md-6">
                        <p style="text-align: left;font-weight: bolder;">Licence Plate Number</p>
                        <input type="text" placeholder="Plate" id="plate" name="plate" class="form-control" value="">
                     </div>
                     <div class="col-md-6">
                        <p style="text-align: left;font-weight: bolder;">Vehicle Name</p>
                        <input type="text" placeholder="Vehicle Name" id="name" name="name" class="form-control" value="">
                     </div>
                  </div>
                  <br/>
                  </hr>
                  <div class="f1-buttons">
                     <button type="button" class="btn btn-previous"><i class="fa fa-chevron-circle-left" aria-hidden="true"></i> Previous</button>
                     <button type="button" class="btn btn-next" id="btn_ignore">Add Vehicle Later</button>
                     <button type="button" class="btn btn-next" id="btn2">Next <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>  </button>
                  </div>
               </fieldset>
               <fieldset   style="text-align: center;">
                  <!------ Include the above in your HEAD tag ---------->
                  <div class="container">
                     <div class="row">
                        <div class="well col-md-6" style="margin:0">
                           <div class="row">
                              <div style="text-align: center;">
                                 <img src="<?php echo base_url(); ?>assets/img/logo_sm2.png" style="height: 24px" href="https://hellohiparking.com">
                                 <hr/>
                              </div>
                              <div class="col-xs-6 col-sm-6 col-md-6">
                                 <address style="text-align: left">
                                    <p style="font-size: 25px"><i class="fa fa-user-circle-o" aria-hidden="true"></i> <span id="uname"> User Name  </span></p>
                                    <p><i class="fa fa-envelope" aria-hidden="true"></i>  <span id="email"> </span> </p>
                                    <p><i class="fa fa-phone-square" aria-hidden="true"></i> <span id="mobile">
                                       </span> 
                                    </p>
                                 </address>
                              </div>
                              <div class="col-xs-6 col-sm-6 col-md-6 text-right">
                                 <p>
                                    <em><?php  echo date(" d M Y - h:i a "); ?></em>
                                 </p>
                                 <p style="margin-bottom: 0">
                                    <em>Arrival #:  <?php echo $from; ?>  </em>
                                 </p>
                                 <p style="margin-bottom: 0">
                                    <em>Depart #:  <?php echo $to; ?>  </em>
                                 </p>
                              </div>
                           </div>
                           <div class="row">
                              <div class="text-center">
                                 <h1>Receipt</h1>
                              </div>
                              </span>
                              <table class="table table-hover">
                                 <thead>
                                    <tr>
                                       <th>Parking Spot</th>
                                       <th class="text-center">Price</th>
                                       <th class="text-center" colspan="2">Duration</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td class="col-md-9" style="text-align: left;"><em><?php echo $spot[0]["public_name"]; ?>   </em></h4></td>
                                       <td class="col-md-1 text-center"><?php echo $rate ;?></td>
                                       <td class="col-md-1 text-center" colspan="2"> <span id="day"><?php echo $day; ?>  </span> day, <span id="hour"><?php echo $hour; ?>  </span> Hrs</p> </td>
                                    </tr>
                                    <t
                                    <tr>
                                       <td>   </td>
                                       <td>   </td>
                                       <td class="text-right">
                                          <p>
                                             <strong>Subtotal: </strong>
                                          </p>
                                          <p>
                                             <strong> Convenient Fee: </strong>
                                          </p>
                                       </td>
                                       <td class="text-center">
                                          <p>
                                             <strong><?php echo $rate ;?></strong>
                                          </p>
                                          <p>
                                             <strong>$1.00</strong>
                                          </p>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td>   </td>
                                       <td>   </td>
                                       <td class="text-right">
                                          <h4><strong>Total: </strong></h4>
                                       </td>
                                       <td class="text-center text-danger">
                                          <h4><strong><?php echo number_format($rate+1, 2, '.', ''); ?></strong></h4>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                              <div style="text-align: center;">
                                 <img src="<?php echo base_url(); ?>assets/img/pay.png" style="width: 140px">
                                 <h3 style="color:#4788c7"><b>Pay Securely With Paypal Now </b> </h3>
                              </div>
                              </br/>
                              <input type="hidden" value="<?php echo $rate+1; ?>" name="rate">
                              <input type="hidden" value="<?php echo $m_date1; ?>" name="arrive">
                              <input type="hidden" value="<?php echo $m_date2; ?>" name="depart">
                              <input type="hidden" value="<?php echo $day; ?>" name="day">
                              <input type="hidden" value="<?php echo $hour; ?>" name="hour">
                              <input type="hidden" name="spot_type" value="<?php echo $type ?>">
                              <input type="hidden" value="<?php echo $pid; ?>" name="pid">
                              <input type="hidden" name="spot" id="spot" value="  <?php echo $spot[0]["public_name"]; ?>  ">
                              <input type="hidden" value="<?php if(isset($_SESSION['cid'])) echo $_SESSION['cid']; else echo 0; ?>" name="uid">
                             

                                   <input type="image" class="btn btn-default" id="btn_paypa" src="<?php echo base_url(); ?>assets/img/paypal2.png"
                                    style="background-color: #ffc439;background-position: center;">
                              </td>
                           </div>
                        </div>
                     </div>
                     <button type="button" class="btn btn-previous" style="float: left;margin-top: 10px"><i class="fa fa-chevron-circle-left" aria-hidden="true"></i> Previous</button>
               </fieldset>
          
               </div>

                    <?php echo form_close(); ?>
            </div>
         </div>
      </div>
      <!-- Javascript -->
      <script src="<?php echo base_url(); ?>assets/wizard/js/jquery-1.11.1.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
      
      <script src="<?php echo base_url(); ?>assets/wizard/bootstrap/js/bootstrap.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/wizard/js/jquery.backstretch.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/wizard/js/retina-1.1.0.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/wizard/js/scripts.js"></script>
      <!--[if lt IE 10]>
      <script src="assets/js/placeholder.js"></script>
      <![endif]-->
      <style type="text/css">
         .navbar-brand {
         background-image: url(<?php echo base_url(); ?>assets/wizard/img/logo@2x.png) !important; background-repeat: no-repeat !important; background-size: 162px 36px !important;
         }
         .navbar-brand {
         width: 162px;
         background: url(<?php echo base_url(); ?>assets/wizard/img/logo.png) left center no-repeat;
         text-indent: -99999px;
         }
         #main_menu
         {
         margin-right: 25px
         }
         .text-muted
         {
         color: #03A9F4;
         }
         h3
         {
         color:white;
         }
         p
         {
         color: black;
         }
         /*  bhoechie tab */
         div.bhoechie-tab-container{
         z-index: 10;
         background-color: #ffffff;
         padding: 0 !important;
         border-radius: 4px;
         -moz-border-radius: 4px;
         border:1px solid #ddd;
         margin-top: 20px;
         margin-left: 50px;
         -webkit-box-shadow: 0 6px 12px rgba(0,0,0,.175);
         box-shadow: 0 6px 12px rgba(0,0,0,.175);
         -moz-box-shadow: 0 6px 12px rgba(0,0,0,.175);
         background-clip: padding-box;
         opacity: 0.97;
         filter: alpha(opacity=97);
         }
         div.bhoechie-tab-menu{
         padding-right: 0;
         padding-left: 0;
         padding-bottom: 0;
         }
         div.bhoechie-tab-menu div.list-group{
         margin-bottom: 0;
         }
         div.bhoechie-tab-menu div.list-group>a{
         margin-bottom: 0;
         }
         div.bhoechie-tab-menu div.list-group>a .glyphicon,
         div.bhoechie-tab-menu div.list-group>a .fa {
         color: #5A55A3;
         }
         div.bhoechie-tab-menu div.list-group>a:first-child{
         border-top-right-radius: 0;
         -moz-border-top-right-radius: 0;
         }
         div.bhoechie-tab-menu div.list-group>a:last-child{
         border-bottom-right-radius: 0;
         -moz-border-bottom-right-radius: 0;
         }
         div.bhoechie-tab-menu div.list-group>a.active,
         div.bhoechie-tab-menu div.list-group>a.active .glyphicon,
         div.bhoechie-tab-menu div.list-group>a.active .fa{
         background-color: #5A55A3;
         background-image: #5A55A3;
         color: #ffffff;
         }
         div.bhoechie-tab-menu div.list-group>a.active:after{
         content: '';
         position: absolute;
         left: 100%;
         top: 50%;
         margin-top: -13px;
         border-left: 0;
         border-bottom: 13px solid transparent;
         border-top: 13px solid transparent;
         border-left: 10px solid #5A55A3;
         }
         div.bhoechie-tab-content{
         background-color: #ffffff;
         /* border: 1px solid #eeeeee; */
         padding-left: 20px;
         padding-top: 10px;
         }
         div.bhoechie-tab div.bhoechie-tab-content:not(.active){
         display: none;
         }
         .error
         {
         color: orangered;
         }
      </style>
      <footer  style="background-color: #bdd5e938;color: white;">
         <div class="row">
            <div class="col-md-2">
               <img src="<?php echo base_url(); ?>assets/img/logo.png" style="width: 45%;margin-top: 20px">
               <br/>
               <small class="d-block mb-3 text-muted">&copy; 2017-2018</small>
            </div>
            <div class="col-md-3">
               <h3>Features</h3>
               <ul class="list-unstyled text-small">
                  <li><a class="text-muted" href="#">Monthly Rent</a></li>
                  <li><a class="text-muted" href="#">Weekly Rent</a></li>
                  <li><a class="text-muted" href="#">Event Rent</a></li>
               </ul>
            </div>
            <div class="col-md-3">
               <h3>Work With Us</h3>
               <ul class="list-unstyled text-small">
                  <li><a class="text-muted" href="<?php echo site_url('admin'); ?>">Admin Login</a></li>
                  <li><a class="text-muted" href="<?php echo site_url('panel'); ?>">Parking Owners Login</a></li>
                  <li><a class="text-muted" href="#">Venue</a></li>
                  <li><a class="text-muted" href="#">Producs</a></li>
                  <li><a class="text-muted" href="#">App</a></li>
               </ul>
            </div>
            <div class="col-md-2">
               <h3>Follow Us</h3>
               <ul class="list-unstyled text-small">
          <li><a class="text-muted2" href="#"><i class="fa fa-facebook-official" aria-hidden="true"></i> Facebook</a></li>
                  <li><a class="text-muted2" href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i> Twitter</a></li>
                  <li><a class="text-muted2" href="#"><i class="fa fa-instagram" aria-hidden="true"></i> Instagram</a></li>
                  <li><a class="text-muted2" href="#"><i class="fa fa-google-plus" aria-hidden="true"></i> Google Plus</a></li>
               </ul>
            </div>
            <div class="col-md-2">
               <h3>Support</h3>
               <ul class="list-unstyled text-small">
                  <li><a class="text-muted" href="#">About </a></li>
                  <li><a class="text-muted" href="#">Contact Us</a></li>
                  <li><a class="text-muted" href="#">FAQ</a></li>
                  <li><a class="text-muted" href="#">Term & Conditions</a></li>
               </ul>
            </div>
         </div>
      </footer>
      <div class="container">
         <!-- Modal -->
         <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog modal-lg" style="color:black;color: black;">
               <!-- Modal content-->
               <div class="modal-content">
                  <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal">&times;</button>
                     <div class="row" style="text-align: left">
                        <div class="col-md-2">
                           <?php 
                              if($images!=null)
                              {
                              
                              
                              foreach ($images as $img) {
                              # code...
                              ?>
                           <img src="<?php echo base_url().$img->img_address;?>" style=" width: 150px">
                           <?php break;} }else{ ?>
                           <img src="<?php echo base_url();?>/assets/img/p1.jpg" style=" width: 150px">
                           <?php } ?>
                        </div>
                        <div class="col-md-8" style="margin-top: 10px">
                           <h3 style="padding: 0;margin: 0;color: black">
                              <?php echo $spot[0]["public_name"]; ?>   
                           </h3>
                           <div class="stats">
                              <h4> <i class="ti-info"> </i>
                                 <?php echo $spot[0]["address"]; ?>                                                 
                              </h4>
                           </div>
                           <div class="stats">
                              <h4 style="color:#2bbbad"> <span>Seats Available : </span> <b>        <?php echo $total_available; ?>   </b></h4>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="modal-body">
                     <div >
                        <div class="row">
                           <div class="col-md-12 bhoechie-tab-container" style="margin: 0">
                              <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 bhoechie-tab-menu">
                                 <div class="list-group">
                                    <a href="#" class="list-group-item active text-center">
                                       <h4 class="glyphicon glyphicon-map-marker"></h4>
                                       <br/>Maps
                                    </a>
                                    <a href="#" class="list-group-item text-center">
                                       <h4 class="glyphicon glyphicon-info-sign"></h4>
                                       <br/>Details
                                    </a>
                                    <a href="#" class="list-group-item text-center">
                                       <h4 class="glyphicon glyphicon-road"></h4>
                                       <br/>How To Find
                                    </a>
                                    <a href="#" class="list-group-item text-center">
                                       <h4 class="glyphicon glyphicon-picture"></h4>
                                       <br/>Images
                                    </a>
                                    <a href="#" class="list-group-item text-center">
                                       <h4 class="glyphicon glyphicon-exclamation-sign"></h4>
                                       <br/>Terms & Conditions
                                    </a>
                                 </div>
                              </div>
                              <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab">
                                 <!-- flight section -->
                                 <div class="bhoechie-tab-content active">
                                    <div id="map" style="width:100%;height:500px"></div>
                                 </div>
                                 <!-- train section -->
                                 <div class="bhoechie-tab-content">
                                    <center>
                                       <h2>Description</h2>
                                       <p>
                                          <?php echo $spot[0]["description"]; ?>    
                                       </p>
                                       <hr>
                                       <h2>
                                       Facilities</h2
                                       <p>
                                          no details 
                                       </p>
                                    </center>
                                 </div>
                                 <!-- hotel search -->
                                 <div class="bhoechie-tab-content">
                                    <center>
                                       <h3>How To Find</h3>
                                       <p>
                                          <?php echo $spot[0]["how_to_find"]; ?> 
                                       </p>
                                    </center>
                                 </div>
                                 <div class="bhoechie-tab-content">
                                    <div name="image_show1" class="row" id="image_show1">
                                       <?php foreach ($images as $img) {
                                          # code...
                                          ?>
                                       <div class="col-md-4">
                                          <img style="width:250px" 
                                             src="<?php echo base_url().$img->img_address;?>">
                                          <hr>
                                          <h5 style="text-align:center"><?php echo $img->img_name; ?></h5>
                                          <hr>
                                       </div>
                                       <?php } ?>
                                    </div>
                                 </div>
                                 <div class="bhoechie-tab-content">
                                    <center>
                                       <h2>Terms &amp; Condition</h2>
                                       <p>
                                          <?php echo $spot[0]["term_and_condition"]; ?> 
                                       </p>
                                    </center>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="modal-footer">
                     <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <script async defer
         src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDkkzyW9zJvzYJPe-5vrK_Ggko-Ha4NDzM&callback=initMap"></script>
      <script type="text/javascript">
         $(document).ready(function() {
          $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
              e.preventDefault();
              $(this).siblings('a.active').removeClass("active");
              $(this).addClass("active");
              var index = $(this).index();
              $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
              $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
          });
         });
         
         
         function initMap() {
         var myCenter = new google.maps.LatLng(<?php echo $spot[0]["lat"]; ?>,<?php echo $spot[0]["long"]; ?>);
         var mapCanvas = document.getElementById("map");
         var mapOptions = {center: myCenter, zoom: 12};
         var map = new google.maps.Map(mapCanvas, mapOptions);
         var marker = new google.maps.Marker({position:myCenter});
         marker.setMap(map);
         }

 
         
         $("#btn_paypal").click(function()
          {
                var uname=$("#f1_first_name").val();
                var email=$("#f1_email").val();
                var mob=$("#f1_mobile").val();
         
         
                var type=($("#type").val() || 'null') ;
                var make=($("#make").val() || 'null') ;
                 var  model =($("#model").val() || 'null') ;
                  var   year=($("#year").val() || 'null');
                   var   color=($("#color").val() || 'null');
                    var   state=($("#state").val() || 'null');
                     var   plate=($("#plate").val() || 'null');
                      var   name=($("#name").val() || 'null');
                       var   spot=($("#spot").val() || 'null');

                       var type=$("#type").val();
         
         
          
         
                  
                  console.log(uname+" === "+email+" === "+mob+" === "+type+" === "+make+" === "+model+" === "+year+" === "+color+" === "+state+" === "+plate+" === "+name);
         
          


                   var redirectUrl="<?php echo base_url();?>index.php/Reservations/payment";


                        var form = $('<form action="' + redirectUrl + '" method="post" />' +
                            '<input type="hidden" value="<?php echo $rate; ?>" name="rate" />'+
                             '<input type="hidden" value="<?php echo $m_date1; ?>" name="arrive" />'+
                              '<input type="hidden" value="<?php echo $m_date2; ?>" name="depart" />'+
                              '<input type="hidden" value="<?php echo $day; ?>" name="day" />'+
                              '<input type="hidden" value="<?php echo $hour; ?>" name="hour" />'+
                              '<input type="hidden" value="<?php echo $pid; ?>" name="pid" />'+
                                '<input type="hidden" value="<?php echo $type; ?>" name="spot_type" />'+

   
                              '<input type="hidden" value="<?php if(isset($_SESSION['cid'])) echo $_SESSION['cid']; else echo 0; ?>" name="uid" />'+
                               '<input type="hidden" name="type" value="'+ type +'" />' +
                            '<input type="hidden" name="make" value="'+ make +'" />' +
                            '<input type="hidden" name="model" value="'+ model +'" />' +
                            '<input type="hidden" name="year" value="'+ year +'" />' +
                             '<input type="hidden" name="color" value="'+ color +'" />' +
                            '<input type="hidden" name="state" value="'+ state +'" />' +
                            '<input type="hidden" name="plate" value="'+ plate +'" />' +
                            '<input type="hidden" name="name" value="'+ name +'" />' +
                                              '<input type="hidden" name="spot" value="'+ spot +'" />' +
                             
                            '</form>');

                    
                            $('body').append(form);
                           // $(form).submit();



               });



         
         
         
         
        
         
         
         $('#f1_mobile').bind('keyup paste', function () {
                  this.value = this.value.replace(/[^0-9]/g, '');
         
              }); 
         
         
         validate_user();
         
         
         
         
         $("#b1").click(function(){
         
         
         
         
         var next_step=false;
         if ($('form').valid()) {
         
           validate_user();
          next_step=true; 
          console.log("yes");  // do something
         }
         else
         {
            console.log("no");
         }
         
            var parent_fieldset = $(this).parents('fieldset');
         
              
            // navigation steps / progress steps
            var current_active_step = $(this).parents('.f1').find('.f1-step.active');
            var progress_line = $(this).parents('.f1').find('.f1-progress-line');
         
            
            // fields validation
            parent_fieldset.find('input[type="text"], input[type="email"], .f1 input[type="number"]').each(function() {
           
         
             $i=1;
         
              if($i<=3)
              {
         
         
               if( $(this).val() == "" ) {
                  $(this).addClass('input-error');
                 
               }
               else {
                  $(this).removeClass('input-error');
               }
            }
            $i++;
         
            });
            // fields validation
            
            if( next_step ) {
               console.log("true");
               parent_fieldset.fadeOut(400, function() {
                  // change icons
                  current_active_step.removeClass('active').addClass('activated').next().addClass('active');
                  // progress bar
                  bar_progress(progress_line, 'right');
                  // show next step
                  $(this).next().fadeIn();
                  // scroll window to beginning of the form
                  scroll_to_class( $('.f1'), 20 );
         
                  var uname=$("#f1_first_name").val();
                  var email=$("#f1_email").val();
                   var mob=$("#f1_mobile").val();
         
                  $("#uname").text(uname);
                  $("#mobile").text(mob);
                  $("#email").text(email);
         
               });
            }
            else
            {
               console.log("false");
            }
            });
         
         
         
         
         
         
         $("#btn2").click(function(){
         
 
         
         
         var next_step=false;
         if ($('form').valid()) {
          next_step=true; 
          console.log("yes");  // do something
         }
         else
         {
            next_step=false;
            console.log("no");
         }
         
            var parent_fieldset = $(this).parents('fieldset');
         
              
            // navigation steps / progress steps
            var current_active_step = $(this).parents('.f1').find('.f1-step.active');
            var progress_line = $(this).parents('.f1').find('.f1-progress-line');
         
            
            // fields validation
            parent_fieldset.find('input[type="text"], input[type="email"], .f1 input[type="number"]').each(function() {
              
              $i=1;
         
              if($i<=3)
              {
         
         
               if( $(this).val() == "" ) {
                  $(this).addClass('input-error');
                 
               }
               else {
                  $(this).removeClass('input-error');
               }
            }
            $i++;
            });
            // fields validation
            
            if( next_step ) {
               console.log("true");
               parent_fieldset.fadeOut(400, function() {
                  // change icons
                  current_active_step.removeClass('active').addClass('activated').next().addClass('active');
                  // progress bar
                  bar_progress(progress_line, 'right');
                  // show next step
                  $(this).next().fadeIn();
                  // scroll window to beginning of the form
                  scroll_to_class( $('.f1'), 20 );
               });
            }
            else
            {
               console.log("false");
            }
            });
         
         
         
         
         
         
         $("#btn_ignore").click(function(){
         
         
         
         
         var next_step=false;
         
            var parent_fieldset = $(this).parents('fieldset');
         
              
            // navigation steps / progress steps
            var current_active_step = $(this).parents('.f1').find('.f1-step.active');
            var progress_line = $(this).parents('.f1').find('.f1-progress-line');
         
            
            // f 
            // fields validation
            
         
               parent_fieldset.fadeOut(400, function() {
                  // change icons
                  current_active_step.removeClass('active').addClass('activated').next().addClass('active');
                  // progress bar
                  bar_progress(progress_line, 'right');
                  // show next step
                  $(this).next().fadeIn();
                  // scroll window to beginning of the form
                  scroll_to_class( $('.f1'), 20 );
               });
            });
             
         
         
         function validate_user()
         {
         
                 $( "#f1" ).validate({
              rules: {
                  f1_first_name: {
                      required: false,
                      minlength: 4
         
                  },
                  f1_mobile: {
                      required: false,
                      minlength: 10,
                      maxlength:10,
                       //or look at the additional-methods.js to see available phone validations
                  },
                  f1_email: {
                      required: true,
                      email: true
                  } 
         
              },
              messages: {
                  f1_first_name: {
                      required: "<i class='fa fa-exclamation-triangle' aria-hidden='true'></i>&nbsp; Please enter your name"                         
                  },
                  f1_mobile: {
                      required: "<i class='fa fa-exclamation-triangle' aria-hidden='true'></i>&nbsp; Mobile Number is required",
                      minlength:  "<i class='fa fa-exclamation-triangle' aria-hidden='true'></i>&nbsp; Mobile Number must have 10 digits at least.",
                      maxlength:"<i class='fa fa-exclamation-triangle' aria-hidden='true'></i>&nbsp; Maximum 10 digits are allowed."              
                  },
                  f1_email: {
                      required: "<i class='fa fa-exclamation-triangle' aria-hidden='true'></i>&nbsp; Please enter your email address."                         
                  } 
              },
          });
         
         }
         
         
         
         
         function validate_vehicle()
         {
         
                 $("#f1").validate({
              rules: {
                  type: {
                      required: true,
                      minlength: 2
         
                  },
                  make: {
                      required: true,
                      minlength: 2
         
                  },
                  model: {
                      required: true,
                      minlength: 2
         
                  }
              },
              messages: {
                  type: {
                      required: "<i class='fa fa-exclamation-triangle' aria-hidden='true'></i>&nbsp; Vehicle Type are required . Please enter your Vehicle type.",
         
                  },
                  make: {
                      required: "<i class='fa fa-exclamation-triangle' aria-hidden='true'></i>&nbsp;Vehicle make are required . Please enter your Vehicle make."                         
                  },
                  model: {
                      required: "<i class='fa fa-exclamation-triangle' aria-hidden='true'></i>&nbsp;Vehicle model are required . Please enter your Vehicle model."                         
                  } 
              },
          });
         
         }
      </script>
   </body>
</html>