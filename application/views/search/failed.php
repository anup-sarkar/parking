<?php
   defined('BASEPATH') OR exit('No direct script access allowed');
   
    date_default_timezone_set("Asia/Dhaka");
   ?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Hello Hi Parking </title>
      <!-- CSS -->
      <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/wizard/bootstrap/css/bootstrap.min.css">
      <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/wizard/css/form-elements.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/wizard/css/style.css">
      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
      <!-- Favicon and touch icons -->
      <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url(); ?>assets/img/favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url(); ?>assets/img/favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url(); ?>assets/img/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url(); ?>assets/img/favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>assets/img/favicon/favicon-16x16.png">
<link rel="manifest" href="<?php echo base_url(); ?>assets/img/favicon/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="<?php echo base_url(); ?>assets/img/favicon/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
   </head>
   <body>
      <!-- Top menu -->
    <nav class="navbar navbar-inverse" role="navigation" style="background-color: black">
         <div class="container">
            <div class="navbar-header">
               <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-navbar-1">
               <span class="sr-only">Toggle navigation</span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               </button>
               <a class="navbar-brand" href="http://hellohiparking.com">Hello Hi Parking</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="top-navbar-1">
               <ul class="nav navbar-nav navbar-right">
                  <li id="main_menu">
                     <span><a href="" style="color: #02afdc"><i class="fa fa-android" aria-hidden="true" ></i>&nbsp; Get The App</a></span>
                  </li>
                  <li id="main_menu">
                     <span><a href="<?php echo base_url(); ?>/index.php/reservations/ticket" style="color: #02afdc">  <i class="fa fa-ticket" aria-hidden="true"></i>&nbsp; Ticket</a></span>
                  </li>
                  <li id="main_menu">
                     <span style="color: #02afdc"><i class="fa fa-product-hunt" aria-hidden="true"></i>&nbsp; About
                  </li>
                  <?php  if (isset($_SESSION['client_name'])) { ?>
                  <li id="main_menu">
                     <span  > <a style="color: #02afdc"  href="<?php echo site_url('home/profile'); ?>"   ><i class="fa fa-user-circle" aria-hidden="true"></i>&nbsp; <?php echo $_SESSION['client_name']; ?></a> </span>
                  </li>
                  <li id="main_menu">
                     <span> <a style="color: #02afdc"  href="<?php echo site_url('home/logout'); ?>"   ><i class="fa fa-sign-out" aria-hidden="true"></i> </i>&nbsp; Sign Out</a></span>
                  </li>
                  <?php   }else{ ?>
                  <li id="main_menu">
                     <span>  <a  style="color: #02afdc"  href="<?php echo site_url('home/login'); ?>"><i class="fa fa-user" aria-hidden="true"></i></i>&nbsp; Log In</a>
                     </span>
                  </li>
                  <li id="main_menu">
                     <span>   <a  style="color: #02afdc"  href="<?php echo site_url('home/signup'); ?>"  ><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp; Sign Up</a>
                     </span>
                  </li>
                  <?php   }  ?>
               </ul>
            </div>
         </div>
      </nav>

<br/><br/>
<br/>
<br/>
      <div class="container">
         <div class="row" style="text-align: center;color: green">

               <img src="<?php echo base_url(); ?>assets/img/failed.png" style="width: 300px">
               <h2 style="color:red">Oops! <b>Your Transaction was <?php echo $status; ?>. </b></h2>


                
         </div>


         <br/>
         <br/>

        

      </div>

<br/><br/><br/>














      <script src="<?php echo base_url(); ?>assets/wizard/js/jquery-1.11.1.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
      
      <script src="<?php echo base_url(); ?>assets/wizard/bootstrap/js/bootstrap.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/wizard/js/jquery.backstretch.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/wizard/js/retina-1.1.0.min.js"></script>
 
      <!--[if lt IE 10]>
      <script src="assets/js/placeholder.js"></script>
      <![endif]-->
      <style type="text/css">
         .navbar-brand {
         background-image: url(<?php echo base_url(); ?>assets/wizard/img/logo@2x.png) !important; background-repeat: no-repeat !important; background-size: 162px 36px !important;
         }
         .navbar-brand {
         width: 162px;
         background: url(<?php echo base_url(); ?>assets/wizard/img/logo.png) left center no-repeat;
         text-indent: -99999px;
         }
         #main_menu
         {
         margin-right: 25px;
         color: black;
         }
         span
         {
            color: black;
         }
         .text-muted
         {
         color: #03A9F4;
         }

         h3
         {
         color:white;
         }
         p
         {
         color: black;
         }
         /*  bhoechie tab */
         div.bhoechie-tab-container{
         z-index: 10;
         background-color: #ffffff;
         padding: 0 !important;
         border-radius: 4px;
         -moz-border-radius: 4px;
         border:1px solid #ddd;
         margin-top: 20px;
         margin-left: 50px;
         -webkit-box-shadow: 0 6px 12px rgba(0,0,0,.175);
         box-shadow: 0 6px 12px rgba(0,0,0,.175);
         -moz-box-shadow: 0 6px 12px rgba(0,0,0,.175);
         background-clip: padding-box;
         opacity: 0.97;
         filter: alpha(opacity=97);
         }
         div.bhoechie-tab-menu{
         padding-right: 0;
         padding-left: 0;
         padding-bottom: 0;
         }
         div.bhoechie-tab-menu div.list-group{
         margin-bottom: 0;
         }
         div.bhoechie-tab-menu div.list-group>a{
         margin-bottom: 0;
         }
         div.bhoechie-tab-menu div.list-group>a .glyphicon,
         div.bhoechie-tab-menu div.list-group>a .fa {
         color: #5A55A3;
         }
         div.bhoechie-tab-menu div.list-group>a:first-child{
         border-top-right-radius: 0;
         -moz-border-top-right-radius: 0;
         }
         div.bhoechie-tab-menu div.list-group>a:last-child{
         border-bottom-right-radius: 0;
         -moz-border-bottom-right-radius: 0;
         }
         div.bhoechie-tab-menu div.list-group>a.active,
         div.bhoechie-tab-menu div.list-group>a.active .glyphicon,
         div.bhoechie-tab-menu div.list-group>a.active .fa{
         background-color: #5A55A3;
         background-image: #5A55A3;
         color: #ffffff;
         }
         div.bhoechie-tab-menu div.list-group>a.active:after{
         content: '';
         position: absolute;
         left: 100%;
         top: 50%;
         margin-top: -13px;
         border-left: 0;
         border-bottom: 13px solid transparent;
         border-top: 13px solid transparent;
         border-left: 10px solid #5A55A3;
         }
         div.bhoechie-tab-content{
         background-color: #ffffff;
         /* border: 1px solid #eeeeee; */
         padding-left: 20px;
         padding-top: 10px;
         }
         div.bhoechie-tab div.bhoechie-tab-content:not(.active){
         display: none;
         }
         .error
         {
         color: orangered;
         }

         #bold{

    color: #02afdc;
    font-weight:  bold;

}

#cyan{

    color: #02afdc;
    

}

    #bold2
   {
      font-weight:  bold;
     color: #02afdc;
    padding: 10px;
    border:  1px solid;
 
   }

     #bold3
   {
      font-weight:  bold;
     color: #02afdc;
    padding: 10px;
    border:  1px solid;
    /* border-radius: 5px; */
 
   }

      </style>
       <footer  style="background-color: black;color: black;">
         <div class="row">
            <div class="col-md-2">
               <img src="<?php echo base_url(); ?>assets/img/logo.png" style="width: 45%;margin-top: 20px">
               <br/>
               <small class="d-block mb-3 text-muted">&copy; 2017-2018</small>
            </div>
            <div class="col-md-3">
               <h3>Features</h3>
               <ul class="list-unstyled text-small">
                  <li><a class="text-muted" href="#">Monthly Rent</a></li>
                  <li><a class="text-muted" href="#">Weekly Rent</a></li>
                  <li><a class="text-muted" href="#">Event Rent</a></li>
               </ul>
            </div>
            <div class="col-md-3">
               <h3>Work With Us</h3>
               <ul class="list-unstyled text-small">
                  <li><a class="text-muted" href="<?php echo site_url('admin'); ?>">Admin Login</a></li>
                  <li><a class="text-muted" href="<?php echo site_url('panel'); ?>">Parking Owners Login</a></li>
                  <li><a class="text-muted" href="#">Venue</a></li>
                  <li><a class="text-muted" href="#">Producs</a></li>
                  <li><a class="text-muted" href="#">App</a></li>
               </ul>
            </div>
            <div class="col-md-2">
               <h3>Follow Us</h3>
               <ul class="list-unstyled text-small">
                  <li><a class="text-muted" href="#">Facebook</a></li>
                  <li><a class="text-muted" href="#">Twitter</a></li>
                  <li><a class="text-muted" href="#">Instagram</a></li>
                  <li><a class="text-muted" href="#">Google Plus</a></li>
               </ul>
            </div>
            <div class="col-md-2">
               <h3>Support</h3>
               <ul class="list-unstyled text-small">
                  <li><a class="text-muted" href="#">About </a></li>
                  <li><a class="text-muted" href="#">Contact Us</a></li>
                  <li><a class="text-muted" href="#">FAQ</a></li>
                  <li><a class="text-muted" href="#">Term & Conditions</a></li>
               </ul>
            </div>
         </div>
      </footer>
        </body>
</html>