<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$data['isactive']='images';
$this->load->view("module/header_panel",$data);
?>



      <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div >
                            <div class="header" >

                                    <div class="card" style="height: 65px">
                                      <div class="content">

                                <h4 class="title">Parking Spot Images  <a class="btn btn-primary" href="<?php echo base_url().'index.php/panel/add_images' ?>" style="float: right"> Add New </a> </h4>
                               
                                      </div>
                                    </div>

                              
                            </div>
                            <div class="content">

<span><?php echo $this->session->flashdata('msg'); ?></span>


 <div class="row" style='margin-top: 20px'>

          <?php 
       $i=1;
 
           foreach ($images as $item2)
            {
                  $id=$item2->id; 
                  $pid=$item2->parkingspot_id;
                  $name=$item2->img_name; 
                  $details=$item2->img_details;
                  $icon=$item2->img_address;
                  $icon=base_url().$icon;
                  $url=base_url();

                  echo "<div class='col-md-4' ><div class='card' style='text-align:center'><div class='content'>";

                  echo " <img src='".$icon."' style='width:200px' /> ";

                  echo "<h5> ".$name."</h5>";


                       echo "<a class='btn btn-danger' href='".$url."index.php/panel/delete_images/".$id."'><i class='fa fa-remove'></i> </a>"; 
                  echo "</div></div></div>";


                  if($i==3)
                  {
                    echo "</div> <div class='row' style='margin-top: 20px'>";
                    $i=0;
             
                  }
                  $i++;
            }
          

            ?>
      
 </div>


                                
                            </div>
                        </div>
                    </div>


                 


                </div>
            </div>
        </div>


<?php

$this->load->view("module/footer");
?>
