<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$data['isactive']="location";
$this->load->view("module/header_panel",$data);
?>

<?php 



foreach ($provider as $item)
            {
  # code...


?>

   <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-5 col-md-5">
                        <div class="card card-user" style="height: 479px">
                            <div class="image">
                                <img src="<?php echo base_url(); ?>assets/img/background.jpg" alt="..."/>
                            </div>
                            <div class="content">
                                <div class="author">
                                  <img class="avatar border-white" src="<?php echo base_url(); ?>assets/img/person2.png" alt="..."/>
                                  <h2 class="title"> <?php echo $item->name; ?><br />
                                     <a href="#"><small><?php echo $item->designation; ?></small></a>
                                  </h2>
                                </div>
                                <p class="description text-center">
                                    <table style="margin:0 auto">
                                        <tr>
                                            <th style="width: 120px"> <b> Address: </b></th>
                                            <td ><?php echo $item->address; ?> </td>
                                        </tr>
                                         <tr>
                                            <th>   <b> Account Type: </b>   </th>
                                            <td > <?php echo $item->type; ?> </td>
                                        </tr>
                                    </table>
                                  
                                        
                                   
                                </p>
                            </div>
                            <hr>
                            <div class="text-center">
                                <div class="row">
                                    <div class="col-md-5 col-md-offset-1">
                                        <h5><?php echo $item->mobile; ?><br /><small>Mobile</small></h5>
                                    </div>
                                    <div class="col-md-6">
                                        <h5><?php echo $item->email; ?><br /><small>Email</small></h5>
                                    </div>
                                   
                                </div>
                            </div>
                        </div>
                     
                    </div>

                    
                    <div class="col-lg-7 col-md-7">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Edit Provider</h4>
                            </div>
                            <div class="content">
                                <?php $attributes = array("class" => "", "id" => "providerUpdateform", "name" => "providerUpdateform");
                                   echo form_open("panel/update_account", $attributes);?>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Provider  Name</label>
                                                <input type="text" id="txt_name" name="txt_name" class="form-control border-input"  placeholder="Company"  value="<?php echo $item->name; ?>">
                                              <span class="text-danger"><?php echo form_error('txt_name'); ?></span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                             <div class="form-group">
                                                <label>Designation</label>
                                                <input type="text" placeholder="designation" id="txt_desig" name="txt_desig" class="form-control border-input"  value="<?php echo $item->designation; ?>">

                                                  <span class="text-danger"><?php echo form_error('txt_desig'); ?></span>
                                            </div>
                                        </div>
                                       
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                           

                                            <div class="form-group">
                                                <label>Address</label>
                                                <input type="text" id="txt_address" name="txt_address" class="form-control border-input" placeholder="Address" value="<?php echo $item->address; ?>">
                                                    <span class="text-danger"><?php echo form_error('txt_address'); ?></span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input type="email" placeholder="Email" id="txt_email" name="txt_email" class="form-control border-input"  value="<?php echo $item->email; ?>">

                                                  <span class="text-danger"><?php echo form_error('txt_email'); ?></span>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Mobile</label>
                                                <input type="number" id="txt_mobile" name="txt_mobile"  class="form-control border-input"  value="<?php echo $item->mobile; ?>">
                                            </div>
                                        </div>
                                          <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Account Type</label>
                                              <select  id="txt_type" name="txt_type" class="form-control" style="border: 1px solid lightgrey;">
                    
                   
                                                  <option value="<?php echo $item->type; ?>" ><?php echo $item->type; ?> </option>
                        
                                          </select>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                       
                                         <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Status</label>

                                                <?php 
                                                $isactive="lightgrey";
                                                if($item->status==0) 
                                                      $isactive="red"; ?>
                                                 <select  id="txt_status" name="txt_status" class="form-control"  style="border: 1px solid <?php echo $isactive; ?> ">
                    
                                                
                                                  <option value="1" <?php if($item->status==1) echo "selected"; ?>> Active </option>
                                                  <option value="0"  <?php if($item->status==0) echo "selected"; ?>> Inactive </option>
                        
                                          </select>
                                            </div>
                                        </div>


                                        <div class="col-md-6">
                                              <div class="form-group">
                                                <label>Password</label>
                                                <input type="Password" id="txt_pass" name="txt_pass"  class="form-control border-input"  value="">
                                            </div>
                                        </div>    

                                         <div style="display: none">
                                            <div class="form-group">
                                                <label>Id</label>
                                                <input type="number"  id="txt_id" name="txt_id" class="form-control border-input"  value="<?php echo $item->id; ?>">
                                            </div>
                                        </div>

                                        
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-info btn-fill btn-wd">Update Provider</button>
                                    </div>
                                    <div class="clearfix"></div>
                                <?php echo form_close(); ?>
                              
                            </div>
                        </div>
                        <?php echo $this->session->flashdata('msg'); ?>
                    </div>


                </div>
            </div>
        </div>


<?php }
                    ?>

  <?php echo $this->session->flashdata('notify'); ?>


<?php

$this->load->view("module/footer");
?>
