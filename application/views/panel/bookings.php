
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$data['isactive']="bookings";
$this->load->view("module/header_panel",$data);
?>


  <div class="content" style="padding:0;padding-top: 20px">
            <div class="">
                <div class="row" style="margin: 0;padding: 0">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                            
                               
                             <div class="row">
                              <div class="col-md-5">
                                <h2 style="padding:0;margin:0">Reservations </h2>
                              </div>
                                <div class="col-md-5" style="font-size: 18px;padding-top:10px">
                                   <div style="float: right;"><i class="fa fa-circle text-success"></i> Completed   &nbsp;&nbsp;
                                   <i class="fa fa-circle text-danger"></i> Cancelled / Pending 
                                 </div>
                              </div>
                                <div class="col-md-2">
                                    <a onclick="tableToExcel('tbl', 'Reservations Summary')"  class="btn btn-primary" >     
                                      <i class="fa fa-download" aria-hidden="true"></i> Export 
                                         </a>
                              </div>
                             </div>           
                              
                            </div>
                            <div class="content table-responsive table-full-width">





                                <table class="table table-bordered table-hover" id="tbl" style="font-size: 10px">
                                    <thead>
                                        <tr style="background-color: #f5f5f5 "><th>SL</th>
                                          <th>Spot Name </th>
                                      <th>Spot ID</th>
                                      <th>Trans. ID</th>
                                      <th>Base Price</th>
                                      <th>Remit</th>
                                      <th>Revenue</th>
                                        <th>Type</th>

                                       <th style="width: 100px">Starts</th>
                                       <th style="width: 100px">Ends</th>
                                         <th>Status</th>
 
                                        <th>Client Email</th>
                                   


                                       
                                
                                    </tr></thead>
                                    <tbody>

<?php

$i=1;
            foreach ($bookings as $item)
            {
                  $url=base_url();
                  $id=$item->pid; 
                   
                   $serial=$item->serial; 
                   $Arrive=$item->starts;
                    $Depart=$item->ends;

                    $flag="";
                    if($item->status==0)
                    {
                  

                       $flag=" <i class='fa fa-circle text-danger'  style='font-size:30px' ></i> ";
                    }
                    else
                    {
                         $flag="<i class='fa fa-circle text-success' style='font-size:30px'></i>";
                    }



                                       echo "<tr>";
                                       echo "<td>".$i."</td>"; 
                                      echo  "<td>".$item->public_name."</td>";
                                      echo  "<td>".$item->pid."</td>";

                                     echo  "<td style='color:#7ac29a'><b>".$serial."</b></td>";
                                         echo  "<td style='font-size:12px' ><b>$".$item->paid_amount."</b></td>";
                                              echo  "<td style='font-size:12px'><b>$".$item->remit_amount."</b></td>";
                                                   echo  "<td style='font-size:12px'><b>$".$item->profit_amount."</b></td>";
                                                        echo  "<td>".$item->spot_type."</td>";
                                  
                                        echo "<td>".date(" d M Y - h:i a ",strtotime($Arrive))."</td>";
                             
                                       echo  "<td>".date(" d M Y - h:i a ",strtotime($Depart))."</td>";
                                        
                                          echo  "<td>".$flag."</td>";
                                        echo  "<td>".$item->user_email."</td>";
                                    
                                      
                                    
                                       echo    "</tr>";
                                       $i++; 

            }
                                        ?>

                                        <tr> <?php echo $this->session->flashdata('msg'); ?>  </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>


                 


                </div>
            </div>
        </div>
<script type="text/javascript">
  var tableToExcel = (function() {
  var uri = 'data:application/vnd.ms-excel;base64,'
    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
    , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
    , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
  return function(table, name) {
    if (!table.nodeType) table = document.getElementById(table)
    var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
    window.location.href = uri + base64(format(template, ctx))
  }
})()


</script>

 


<?php

$this->load->view("module/footer");
?>
