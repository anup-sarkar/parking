
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$data['isactive']='location';
$this->load->view("module/header_panel",$data);
?>

<?php 



foreach ($parkingspotinfo as $item)
            {

?>

   <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-5 col-md-5">
                        <div class="card card-user" style="height: 579px">
                            
                            <div class="content">
                                <div class="author">
                                  <img class="avatar " style="margin-top: 20px" src="<?php echo base_url(); ?>assets/img/p5.png" alt="..."/>
                                  <h4 class="title"> <?php echo $item->public_name; ?><br />
                                     <a href="#"><small><?php echo $item->short_name; ?></small></a>
                                  </h4>
                                </div>
                                <p class="description text-center">


                            <div class="text-center">
                                <div class="row" style="background-color: #b8bcb261">
                                    <div class="col-md-5 col-md-offset-1">
                                        <h5><?php echo $item->owner; ?><br /><small>Owner</small></h5>
                                    </div>
                                    <div class="col-md-6">
                                        <h5><?php echo $cityname; ?><br /><small>City</small></h5>
                                    </div>
                           
                                   
                                </div>
                            </div>
                               <hr>
                                    <table style="margin:0 auto;text-align: right;" class="table table-border">
                                        <tr>
                                            <th style="width: 120px"> <b> Address: </b></th>
                                            <td ><?php echo $item->address; ?> </td>
                                        </tr>
                                         <tr>
                                            <th> <b> <b> Details </b>  </b></th>
                                            <td  "> <?php echo $item->description; ?> </td>
                                        </tr>

                                        <tr>
                                            <th style="width: 120px"> <b> Longitude </b></th>
                                            <td ><?php echo $item->long; ?> </td>
                                        </tr>
                                         <tr>
                                            <th> <b> <b> Latitude </b>  </b></th>
                                            <td  > <?php echo $item->lat; ?> </td>
                                        </tr>
                                        <tr>
                                            <th> <b> <b> Owner Email </b>  </b></th>
                                            <td  > <?php echo $item->email; ?> </td>
                                        </tr>
                                        <tr>
                                            <th> <b> <b> Owner Mobile </b>  </b></th>
                                            <td  > <?php echo $item->mobile; ?> </td>
                                        </tr>

                                        <tr>
                                            <td colspan="2"> <a href="<?php echo base_url().'index.php/panel/update'; ?> " class="btn btn-primary" style="width: 100%">Update Info</a>   </td>
                                        </tr>

                                      
                                    </table>
                                  
                                        
                                   
                                </p>
                            </div>
                            
                        </div>
                     
                    </div>

                    
                    <div class="col-lg-7 col-md-7">
                        <div class="card" style="height: 578px">
                           
                           <table style="margin:0 auto;text-align: right;" class="table table-border">
                                        <tr>
                                            <th style="width: 120px"> <b> Default Space </b></th>
                                            <td ><?php echo $item->default_space; ?> </td>
                                        </tr>
                                         <tr>
                                            <th> <b> Total Space   </b></th>
                                            <td  "> <?php echo $item->total_space; ?> </td>
                                        </tr>

                                        <tr>
                                            <th style="width: 120px"> <b> Total Facility Space </b></th>
                                            <td ><?php echo $item->total_facility_space; ?> </td>
                                        </tr>
                                         <tr>
                                            <th> <b>   Vehicle Height Restriction    </b></th>
                                            <td  > <?php echo $item->vehicle_height_restriction; ?> </td>
                                        </tr>
                                        <tr>
                                            <th> <b>   Customer Review    </b></th>
                                            <td  > <?php if($item->customer_review==0)
                                            {
                                                echo "Turned Off";
                                            }
                                            else
                                             {
                                                echo "On";    # code...
                                             } ?> </td>
                                        </tr>
                                          <tr>
                                            <th> <b>  Timezone</b>   </th>
                                            <td  >       <?php echo $timezone; ?> </td>
                                        </tr>

                                  
                                        

                                      
                                    </table>
                            <div class="content">
                                <h3> Description </h3>
                                <hr>

                           
                                       <?php echo $item->details; ?>

                                 

                              
                              
                            </div>
                        </div>
                        <?php echo $this->session->flashdata('msg'); ?>
                    </div>

                    <div class="col-lg-12 col-md-12">
                       
                            <div class="content">
                                <div class="col-lg-4 col-md-4">
                                     <h3> How To Find </h3>
                                <hr>

 
                                        
                                        <blockquote>
                                
                                        <?php echo $item->how_to_find; ?>
                                       
                             
                                    </blockquote>
                            

                                </div>

                                 <div class="col-lg-4 col-md-4">
                                     <h3> How To Reedem </h3>
                                <hr>

                             
                                        
                                        <blockquote>
                                
                                       <?php echo $item->how_to_redem; ?>
                                       
                             
                                    </blockquote>
                            

                                </div>

                                 <div class="col-lg-4 col-md-4">
                                     <h3> Terms & Condition</h3>
                                <hr>
                                     <blockquote>
                                
                                        <?php echo $item->terms; ?>
                                       
                             
                                    </blockquote>
                                </div>

                            </div>
                        </div>
                        
              


                </div>
            </div>
        </div>


<?php }
                    ?>

  <?php echo $this->session->flashdata('notify'); ?>


<?php

$this->load->view("module/footer");
?>
