<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$data['isactive']="dashboard";
$this->load->view("module/header_panel",$data);

$pid=$_SESSION['spot_id'];
?>

<script src='<?php echo base_url(); ?>assets/full_cal/jquery.min.js'></script>
<link rel='stylesheet' href='<?php echo base_url(); ?>assets/full_cal/fullcalendar.min.css' />
<link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>

<script src='<?php echo base_url(); ?>assets/full_cal/moment.min.js'></script>

<script src='<?php echo base_url(); ?>assets/full_cal/jquery-ui.min.js'></script>
<script src='<?php echo base_url(); ?>assets/full_cal/fullcalendar.min.js'></script>
 

 <link href="<?php echo base_url(); ?>assets/css/switch.css" rel="stylesheet">


        <div class="content">
            <div class="container-fluid">
                


                 <div class="row">
                    <div class="col-lg-3 col-sm-6">
                        <div class="card">
                            <div class="content">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="icon-big icon-warning text-center">
                                            <i class="ti-check"></i>
                                        </div>
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="numbers">
                                            <p>Total Reservations</p>
                                                  <?php echo $this->Reservations_model->get_count($pid); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <hr />
                                    <div class="stats">
                                        <i class="ti-reload"></i> LifeTime
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card">
                            <div class="content">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="icon-big icon-success text-center">
                                            <i class="ti-money"></i>
                                        </div>
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="numbers">
                                            <p>Total Remit Amount</p>
                                                 $<?php echo $this->Reservations_model->get_remit_sum($pid); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <hr />
                                    <div class="stats">LifeTime
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card">
                            <div class="content">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="icon-big icon-danger text-center">
                                            <i class="ti-credit-card"></i>
                                        </div>
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="numbers">
                                            <p>Total Gross Sale Amount</p>
                                             $<?php echo $this->Reservations_model->get_gross_sum($_SESSION['spot_id']); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <hr />
                                    <div class="stats">
                                        <i class="ti-timer"></i> LifeTime
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card">
                            <div class="content">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="icon-big icon-info text-center">
                                            <i class="ti-money"></i>
                                        </div>
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="numbers">
                                            <p>Reservations This Month</p>
                                              $0.0
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <hr />
                                    <div class="stats">
                                        <i class="ti-reload"></i> This Month
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">

                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Dashboard</h4>
                             
                            </div>
                            <div class="content">
                                <div  >
                                     <div id='calendar'></div>
                                </div>
                              
                            </div>
                        </div>
                    </div>
                </div>
               
            </div>
        </div>


<script>
$('#myModal').on('shown.bs.modal', function () {
  $('#myInput').focus()
})
</script>
 
 

<div class="container">
 
  <!-- Trigger the modal with a button -->
  <button type="button" class="btn btn-info btn-lg" data-toggle="modal" id="rates" data-target="#myModal" style="display: none">Open Small Modal</button>

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header" style="text-align: center;padding: 0;margin: 0">

          <button type="button" class="btn btn-danger btn-sm" style="float: right;"  id="close" data-dismiss="modal">&times;</button>
          <h3 class="modal-title" style="padding: 0;margin: 0">Rates & Timings</h3>
          <h5 style="padding: 0;margin: 0"><i class="fa fa-calendar" aria-hidden="true"></i>&nbsp;
            <b> 
              <span id="date_rate" style="color:#006ffc;font-size: 22px"></span>
            </b> 
        </h5>
        </div>
        <div class="modal-body" style="padding: 0;margin: 0">
          





 




  <div class="bs-example" style=" ">
      <ul class="nav nav-tabs" id="myTab">
         <li id="options" class="active" style="text-align: center;width: 33%"><a href="#sectionA" id="aaa"><i class="fa fa-calendar-check-o" aria-hidden="true"></i>Availablity</a></li>
         <li id="options1"><a href="#sectionB"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Flat Rate </a></li>
         <li id="options2"><a href="#sectionE"><i class="fa fa-clock-o" aria-hidden="true"></i> Hourly Rate </a></li>
     
  
      </ul>
      <div class="tab-content">
         <div id="sectionA" class="tab-pane fade in active">
            <div style="">
              
                
               <div class="card card-plain">
                  <div class="content">
                     <br/>

                     <div class="form-group">
                         <div class="btn-group" id="status" data-toggle="buttons" style="width: 100%">
                           <label class="btn btn-default btn-on-1 btn-lg" style="width: 50%" id="av_yes">
                              <input type="radio" value="1" name="available" id="available1" >Available
                            </label>

                             <label class="btn btn-default btn-off-1 btn-lg" style="width: 50%" id="av_no">
                                 <input type="radio" value="0" id="available2" name="available"   >Not Available
                             </label>
                        </div>
                     </div>

                     <div id="show">
                     <div class="form-control" style="z-index:100;height: 45px">
                        <div class="col-md-8" style="z-index:100"><label style="
                           color:  black;
                           font-size: 20px;
                           ">Is 24 Hours Open? </label></div>
                        <div class="col-md-4" style="z-index:100">
                           <label class="switch" style="float: right;">
                           <input type="checkbox"   id="txt_openclose_isalwaysopen" name="txt_openclose_isalwaysopen">
                           <span class="slider round"></span>
                           </label>
                        </div>
                     </div>
                     <div class="form-group" id="o_t">
                        <br/>
                        <label style="font-size: 20px">Open Time</label>
                        <input type="time"    id="txt_openclose_open" name="txt_openclose_open" class="form-control" value="">
                        <span class="text-danger"><?php echo form_error('txt_openclose_open'); ?></span>
                     </div>
                     <div class="form-group" id="c_t">
                        <label style="font-size: 20px">Close Time</label>
                        <input type="time"    id="txt_openclose_close" name="txt_openclose_close" class="form-control" value="">
                        <span class="text-danger"><?php echo form_error('txt_openclose_close'); ?></span>
                     </div>
                     <div class="form-group">
                        <label style="font-size: 20px">Total Available Space</label>
                        <input type="number" placeholder="Total" id="txt_openclose_total" name="txt_openclose_total"  class="form-control" value="">
                        <span class="text-danger"><?php echo form_error('txt_openclose_total'); ?></span>
                     </div>
 
                  </div>
                </div>
                  <div class="footer text-center">
                     <button class="btn btn-primary" id="btn_openclose" name="btn_openclose"  style=" width: 96%;">
                        <i class="ti-save"></i>&nbsp; SET
                      </button>
                  </div>
               </div>
              
               <h5 id="error_open_close" style="color:red"></h5>
               <?php echo $this->session->flashdata('msg6'); ?>
            </div>
         </div>
         <div id="sectionB" class="tab-pane fade">
            <div >
            
               <div class="card card-plain" style="
                  ">
                  <div class="content">
                     <br/>

                           <div class="form-group">
                         <div class="btn-group" id="status" data-toggle="buttons" style="width: 100%">
                           <label   id="f1" style="width: 50%">
                              <input type="radio" value="1" name="flat_active" id="flat_active1"  >Active
                            </label>

                             <label   id="f2" style="width: 50%">
                                 <input type="radio" value="0" id="flat_active2" name="flat_active" >Inactive
                             </label>
                        </div>
                     </div>


                     <div id="show_flat">
                     <div class="form-control" style="z-index:100;height: 45px">
                        <div class="col-md-8" style="z-index:100"><label style="
                           color:  black;
                           font-size: 20px;
                           ">Is 24 Hours Open? </label></div>
                        <div class="col-md-4" style="z-index:100">
                           <label class="switch" style="float: right;">
                           <input type="checkbox"   id="txt_flatrate_isalwaysopen" name="txt_flatrate_isalwaysopen">
                           <span class="slider round"></span>
                           </label>
                        </div>
                     </div>
                     <div class="form-group" id="flat_ot">
                        <br/>
                        <label style="font-size: 20px">Open Time</label>
                        <input type="time"    id="txt_flatrate_open" name="txt_flatrate_open" class="form-control" value="<?php echo $flatrate->open_time; ?>">
                        <span class="text-danger"><?php echo form_error('txt_flatrate_open'); ?></span>
                     </div>
                     <div class="form-group" id="flat_ct">
                        <label style="font-size: 20px">Close Time</label>
                        <input type="time"    id="txt_flatrate_close" name="txt_flatrate_close" class="form-control"  >
                        <span class="text-danger"><?php echo form_error('txt_flatrate_close'); ?></span>
                     </div>
                         
                     <br/>
                     <div class="form-group">
                        <label style="font-size: 20px">Rate($)</label>
                        <input type="number"   id="txt_flatrate_rate" name="txt_flatrate_rate"  class="form-control"  >
                        <span class="text-danger"><?php echo form_error('txt_flatrate_rate'); ?></span>
                     </div>
                       </div>

                     <input type="date" name="date" id="date" value="" style="display: none">
                  </div>


                 <div class="footer text-center">
                     <button class="btn btn-primary" id="btn_flat" name="btn_flat"  style=" width: 96%;">
                        <i class="ti-save"></i>&nbsp; SET
                      </button>
                  </div>
               </div>
 

                 <h5 id="error_flat" style="color:red"></h5>
               <?php echo $this->session->flashdata('msg2'); ?>
            </div>
         </div>
         <div id="sectionE" class="tab-pane fade">
            <div >
             
               <div class="card card-plain" style="
                  ">
                  <div class="content">
                     <br/>
                     <div class="form-control" style="z-index:100;height: 45px">
                        <div class="col-md-8" style="z-index:100"><label style="
                           color:  black;
                           font-size: 20px;
                           ">Status </label></div>
                        <div class="col-md-4" style="z-index:100">
                           <label class="switch" style="float: right;">
                           <input type="checkbox"   id="txt_hourly_status" name="txt_hourly_status">
                           <span class="slider round"></span>
                           </label>
                        </div>
                     </div>
                     <div class="form-group" id="h_rate">
                        <br/>
                        <label style="font-size: 20px">Rate($)</label>
                        <input type="number"  
                           id="txt_hourly_rate" name="txt_hourly_rate"  class="form-control" 
                           value="">
                        <span class="text-danger"><?php echo form_error('txt_hourly_rate'); ?></span>
                     </div>
                  </div>
                  <div class="footer text-center">
                        <button class="btn btn-primary" id="btn_hour" name="btn_hour"  style=" width: 96%;">
                        <i class="ti-save"></i>&nbsp; SET
                      </button>
                  </div>
               </div>
               <?php echo form_close(); ?>
               <?php echo $this->session->flashdata('msg4'); ?>
            </div>
         </div>
         
 
      </div>
   </div>

















<div id="notify">
  <input type="number" name="" id="flag"   style="display: none">
    <input type="number" name="" id="isAvailable"  value=""  style="display: none">
     <input type="number" name="" id="isAlwaysAvailable"  value=""  style="display: none">

      <input type="number" name="" id="flat_status"  value=""  style="display: none">
            <input type="number" name="" id="hourly_av"  value=""  style="display: none">





</div>

 




        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal" id="close2">&times; Close</button>
        </div>
      </div>
    </div>
  </div>
</div>


<input type="number" id="isFlatAvailable" style="display: none" value="1" name="">

<style type="text/css">
  .modal-backdrop {
  z-index: -1;
}
</style>

       <script>
            $(document).ready(function() {

           

                $('#calendar').fullCalendar({
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'month,agendaWeek,agendaDay'
                    },
                   
                    navLinks: true, // can click day/week names to navigate views
                    selectable: true,
                    selectHelper: true,
                    select: function(start, end) {
                        // var title = prompt('Event Title:');
                        var eventData;
                        if (title) {
                            eventData = {
                                title: title,
                                start: start,
                                end: end
                            };
                            $('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
                        }
                        $('#calendar').fullCalendar('unselect');
                    },
                    editable: true,
                    eventLimit: true, // allow "more" link when too many events
                    events: [
                      
                    <?php foreach ($timing as $item) {
                      # code...
                    

                    ?>
                        {
                            title: 'Available : <?php if($item->isAvailable) echo " Yes"; else echo " No"; ?>',
                            start: '<?php echo date('Y-m-d', strtotime($item->date)); ?>'
                        },



                        <?php if($item->isAvailable==1){ ?>
                        {
                            title: 'Total Seats : <?php  echo $item->total_available;  ?>',
                            start: '<?php echo date('Y-m-d', strtotime($item->date)); ?>'
                        },

                      <?php } ?>

                      <?php } ?>



                        <?php foreach ($flats as $item) {
                      # code...
                    

                    ?>
                       

                        <?php if($item->status==1){ ?>
                        {
                            title: 'Flat Rate : $<?php echo $item->rate;   ?>',
                            start: '<?php echo date('Y-m-d', strtotime($item->date)); ?>'
                        },

                      <?php } ?>

                      <?php } ?>



                        <?php foreach ($hourly_rate as $item) {
 
                           ?>
                       

                        <?php if($item->status==1){ ?>
                        {
                            title: 'Hourly Rate : $<?php echo $item->rate;   ?>',
                            start: '<?php echo date('Y-m-d', strtotime($item->date)); ?>'
                        },

                      <?php } ?>

                      <?php } ?>



                    ],
                    dayClick: function(date, jsEvent, view) {

                        $("#rates").click();
                        $("#date_rate").text(date.format());
                        $("#date").val(date.format());
 
                   
  
                        get_openclose();
                        get_flat_rate();
                        get_hourly_timings();
                        //window.open("/Reports/Details/" + calEvent.id);
                        // alert('Clicked on: ' + date.format());
                      

                    }
                });
      

                var flag = 0;
                 var flag_rate = 0;

                 $("#close,#close2").click(function()
                  {
                       $('#available1').attr('checked',false);

                       $('#available2').attr('checked',false);

                       $("#options1,#options2,#options3,#options4").removeClass("active");
                       $("#options").addClass('active');
                       $("#aaa").click();

                         // alert("closed");
 
                  });


                $("#myTab a").click(function(e) {
                    e.preventDefault();
                    $(this).tab('show');
                });





/////////////////////////////////////////////Event Status/////////////////////////////////////////////////// 
/////////////////////////////////////////////////////////////////////////////////////////////////////////






                $('#txt_openclose_isalwaysopen').click(function() {
                    if ($(this).is(':checked')) {
                            
                         $("#o_t").hide();
                         $("#c_t").hide();

                        flag = 1;

                    } else {
                        

                           $("#o_t").show();
                              $("#c_t").show();
                        flag = 0;

                    }
                });


                $('#txt_monthly_status').click(function() {
                    if ($(this).is(':checked')) {
                        $("#txt_monthly_rate").prop("disabled", false);

                    } else {
                        $("#txt_monthly_rate").prop("disabled", true);
                    }
                });





/////////////////////////////////////////////Event Status/////////////////////////////////////////////////// 
/////////////////////////////////////////////////////////////////////////////////////////////////////////
                <?php 
                  $j=1;
                  foreach ($events as $item) { ?>



                $("#txt_event_status<?php echo $j; ?>").click(function() {


                    if ($(this).is(':checked')) {

                        var request = $.ajax({
                            url: "<?php echo base_url().'index.php/rates/update_eventrate';?>",
                            type: "POST",
                            data: {
                                id: $("#txt_event_id<?php echo $j; ?>").val(),
                                status: 1,
                                rate: $("#rate<?php echo $j; ?>").val(),
                                start: $("#start<?php echo $j; ?>").val(),
                                end: $("#end<?php echo $j; ?>").val(),
                                dataType: "html"
                            }
                        });
                    } else {

                        var request = $.ajax({
                            url: "<?php echo base_url().'index.php/rates/update_eventrate';?>",
                            type: "POST",
                            data: {
                                id: $("#txt_event_id<?php echo $j; ?>").val(),
                                status: 0,
                                rate: $("#rate<?php echo $j; ?>").val(),
                                start: $("#start<?php echo $j; ?>").val(),
                                end: $("#end<?php echo $j; ?>").val(),
                                dataType: "html"
                            }

                        });
                    }

                    request.done(function(msg) {

                        var m = msg.substr(2, 5);


                        if (m == 'Error') {
                            alert(msg);
                            $("#txt_event_status<?php echo $j; ?>").prop('checked', false);
                        } else {

                            alert("Success: Events Rates Updated!");
                        }

                    });

                    request.fail(function(jqXHR, textStatus) {
                        alert("Request failed: " + textStatus);


                    });


                });

                <?php $j++; }?>


//////////////////////////////////////////////////////////////////////////////////////////////////////// 
/////////////////////////////////////////////////////////////////////////////////////////////////////////


                $("#txt_hourly_status").click(function() {


                    if ($(this).is(':checked')) {
                        $("#txt_hourly_rate").prop('disabled', false);


                        $("#hourly_av").val("1");

                        $("#h_rate").show();
                               $("#options1").addClass('disabled');


                    } else {

                        $("#txt_hourly_rate").prop('disabled', true);
                            $("#hourly_av").val("0");

                            $("#h_rate").hide();
                                   $("#options1").removeClass('disabled');

                    }



                });




/////////////////////////////////////////////Available Switch/////////////////////////////////////////////// 
/////////////////////////////////////////////////////////////////////////////////////////////////////////
 var isAvailable=1;
 

$("#av_yes").click(function()
{
    $("#show").show();
    $("#options1,#options2,#options3,#options4").removeClass('disabled');
});

$("#av_no").click(function()
{
     $("#show").hide();
     $("#options1,#options2,#options3,#options4").addClass('disabled');
});



               
                $('input:radio[name=available]').change(function() {

                    var val = $('input:radio[name=available]:checked').val();



                    if ($("#available1").is(":checked")) {

                        $("#show").show();
                        isAvailable = 1;
                        $("#options1,#options2,#options3,#options4").removeClass('disabled');

                        $('#available1').attr('checked',true);

                          $('#available2').attr('checked',false);
                   


                    } else {
                        $("#show").hide();
                        isAvailable = 0;
                        $("#options1,#options2,#options3,#options4").addClass('disabled');
                                $('#available1').attr('checked',false);

                          $('#available2').attr('checked',true);
 
                    }


                });





/////////////////////////////////////////////Availablity//////////////////////////////////////////////////  
/////////////////////////////////////////////////////////////////////////////////////////////////////////

                $("#btn_openclose").click(function() {

                    console.log("flag:" + flag);
                    var request = $.ajax({
                        url: "<?php echo base_url().'index.php/rates/create_open_close';?>",
                        type: "POST",
                        data: {
                            isAvailable: isAvailable,
                            txt_openclose_open: $("#txt_openclose_open").val(),

                            txt_openclose_close: $("#txt_openclose_close").val(),
                            txt_openclose_total: $("#txt_openclose_total").val(),
                            txt_openclose_isalwaysopen: flag,
                            date: $("#date").val(),
                            dataType: "html",

                        },
 
                    });

                    request.done(function(msg) {
 
                        var data = $.parseJSON(msg);
 
                        console.log(data["status"]);
                        console.log(data["msg"]);
             
 
                        if (data["status"] == 1) {
                            $("#txt_openclose_open").val();
                            $("#txt_openclose_close").val();
                            $("#txt_openclose_total").val();


                            $("#notify").prepend(data["msg"]);
                            $("#error_open_close").text(" ");

                        } else {
                            $("#error_open_close").text(" ");
                            $("#error_open_close").prepend(data["errors"]);
                            $("#notify").prepend(data["msg"]);

                        }
 
                    });

                    request.fail(function(jqXHR, textStatus) {
                        alert("Request failed: " + textStatus);

                    });
  
                });


/////////////////////////////////////////////flat///////////////////////////////////////////////////// 
/////////////////////////////////////////////////////////////////////////////////////////////////////////

   
    


        
                $('#txt_flatrate_isalwaysopen').click(function() {
                    if ($(this).is(':checked')) {
                        $("#flat_ot,#flat_ct").hide();
                     

                  

                        $("#isAlwaysAvailable").val("1");
                          console.log("av_check : " + $("#isAlwaysAvailable").val());

                    } else {
                        $("#flat_ot,#flat_ct").show();
                             $("#isAlwaysAvailable").val("0");

                             console.log("av_unchek : " + $("#isAlwaysAvailable").val());

               
                    }
                });


                $('input:radio[name=flat_active]').change(function() {

                 



                    if ($("#flat_active1").is(":checked")) {

                        $("#show_flat").show();
                        
                         $("#flat_status").val("1");

                                 $("#options2").addClass('disabled');
                     
                         

                     
                         $('#f1,#f2').removeClass();
                             $('#f1').addClass('btn btn-default btn-on-1 btn-lg active focus');
                             $('#f2').addClass('btn btn-default btn-off-1 btn-lg');
                  
                  


                    } else {
                        $("#show_flat").hide();

                         $("#flat_status").val("0");

                             $('#f1,#f2').removeClass();

                             $('#f2').addClass('btn btn-default btn-off-1 btn-lg active focus');
                             $('#f1').addClass('btn btn-default btn-on-1 btn-lg');
                              
                              $('#flat_active1').attr('checked',false);

                              $('#flat_active2').attr('checked',true);
                              $("#options2").removeClass('disabled');
                 
              
                         
                    }


                });





                $("#btn_flat").click(function() {

                 
                         var status= $("#flat_status").val();
                         var av=$("#isAlwaysAvailable").val();     

                         console.log("st : "+status);
                         console.log("av : "+av);

                    var request = $.ajax({
                        url: "<?php echo base_url().'index.php/rates/create_flat_rate';?>",
                        type: "POST",
                        data: {
                            isAvailable: av,
                            txt_flatrate_open: $("#txt_flatrate_open").val(),

                            txt_flatrate_close: $("#txt_flatrate_close").val(),
                            txt_flatrate_rate: $("#txt_flatrate_rate").val(),
                            txt_flatrate_isalwaysopen: flag_rate,
                            date: $("#date").val(),
                            status: status,
                            dataType: "html",

                        },
 
                    });

                    request.done(function(msg) {
 
                        var data = $.parseJSON(msg);
 
                        console.log(data["status"]);
                        console.log(data["msg"]);
                  
 
                        if (data["status"] == 1) {
                            $("#txt_openclose_open").val();
                            $("#txt_openclose_close").val();
                            $("#txt_openclose_total").val();


                            $("#notify").prepend(data["msg"]);
                            $("#error_flat").text(" ");

                        } else {
                            $("#error_flat").text(" ");
                            $("#error_flat").prepend(data["errors"]);
                            $("#notify").prepend(data["msg"]);

                        }
 
                    });

                    request.fail(function(jqXHR, textStatus) {
                        alert("Request failed: " + textStatus);

                    });
  
                });



                $("#btn_hour").click(function() {

                 
                      
                   
                    var request = $.ajax({
                        url: "<?php echo base_url().'index.php/rates/create_hourly_rate';?>",
                        type: "POST",
                        data: {
                        txt_hourly_rate: $("#txt_hourly_rate").val(),
                        
                        date: $("#date").val(),
                        txt_hourly_status: $("#hourly_av").val(),
                        dataType: "html",

                        },
 
                    });

                    request.done(function(msg) {
 
                        var data = $.parseJSON(msg);
 
                        console.log(data["status"]);
                        console.log(data["msg"]);
                  
 
                        if (data["status"] == 1) {
                        


                            $("#notify").prepend(data["msg"]);
                            $("#error_flat").text(" ");

                        } else {
                            $("#error_flat").text(" ");
                        
                            $("#notify").prepend(data["msg"]);

                        }
 
                    });

                    request.fail(function(jqXHR, textStatus) {
                        alert("Request failed: " + textStatus);

                    });
  
                });




$("#f1").click(function()
{
          $("#show_flat").show();
                   $("#options2").addClass('disabled');




});

$("#f2").click(function()
{
         $("#show_flat").hide();
                   $("#options2").removeClass('disabled');
});

/////////////////////////////////////////////////////////////////////////////////////////////



function get_openclose()
{

   
   var request = $.ajax({
                        url: "<?php echo base_url().'index.php/rates/get_timings';?>",
                        type: "POST",
                        data: {
                          
                            date: $("#date").val(),
                           
                            dataType: "html",

                        },
 
                    });

                    request.done(function(msg) {
 
                        var data = $.parseJSON(msg);
 
                        console.log(data);
                        console.log(data["msg"]);

                       
 
                        if (data["status"] == 1) {
                         
                        $("#options1,#options2,#options3,#options4").removeClass('disabled');


                            console.log("true");
                            if(data[0]["isAlwaysOpen"]==1)
                            {
                              $("#txt_openclose_isalwaysopen").prop('checked', true);
                                 $("#o_t").hide();
                                 $("#c_t").hide();
                            }
                            else
                            {
                               $("#txt_openclose_isalwaysopen").prop('checked', false);
                               $("#o_t").show();
                               $("#c_t").show();
                            }
 
                            if(data[0]["isAvailable"]==1)
                            {

                             $('#av_no,#av_yes').removeClass('btn btn-default btn-on-1 btn-lg');
                             $('#av_no,#av_yes').removeClass('btn btn-default btn-on-1 btn-lg active focus');

                             $('#av_yes').addClass('btn btn-default btn-on-1 btn-lg  active focus');
                             $('#av_no').addClass('btn btn-default btn-on-1 btn-lg');
                             
                             $('#available1').attr('checked',true);

                             $('#available2').attr('checked',false);
                             $("#c_t").show();

                            }
                            else
                            {
                              $("#options1,#options2,#options3,#options4").addClass('disabled');
                              $('#available2').attr('checked',true);

                              $('#available1').attr('checked',false);
                            

                              $('#av_no,#av_yes').removeClass('btn btn-default btn-on-1 btn-lg');
                              $('#av_no,#av_yes').removeClass('btn btn-default btn-on-1 btn-lg active focus');

                              $('#av_no').addClass('btn btn-default btn-on-1 btn-lg  active focus');
                              $('#av_yes').addClass('btn btn-default btn-on-1 btn-lg');
                              $("#options1,#options2,#options3,#options4").addClass('disabled');
                              $("#show").hide();
                            }


                              $("#txt_openclose_open").val(data[0]["open_time"]);

                              $("#txt_openclose_close").val(data[0]["close_time"]);
                              $("#txt_openclose_total").val(data[0]["total_available"]);
                         
                               //$("#date").val(data[0]["date"]),  
                              
                             

                        } else {
                           console.log("false");
                        
                          $('#av_no,#av_yes').removeClass('btn btn-default btn-on-1 btn-lg');
                          $('#av_no,#av_yes').removeClass('btn btn-default btn-on-1 btn-lg active focus');


                          $('#av_yes').addClass('btn btn-default btn-on-1 btn-lg  active focus');
                          $('#av_no').addClass('btn btn-default btn-on-1 btn-lg');

                          $("#txt_openclose_isalwaysopen").prop('checked', true);


                          $("#txt_openclose_open").val("00:00:00");

                          $("#txt_openclose_close").val("00:00:00");
                          $("#txt_openclose_total").val(25);

                          $('#available1').attr('checked',true);
                          $('#available2').attr('checked',false);

                          $("#o_t").hide();
                          $("#c_t").hide();
                          $("#show").show();
  

                        }

 
                    });

                    request.fail(function(jqXHR, textStatus) {
                        alert("Request failed: " + textStatus);

                    });
  
}


function get_flat_rate()
{
  
   var request = $.ajax({
                        url: "<?php echo base_url().'index.php/rates/get_flat_timings';?>",
                        type: "POST",
                        data: {
                          
                            date: $("#date").val(),
                           
                            dataType: "html",

                        },
 
                    });

                    request.done(function(msg) {
 
                        var data = $.parseJSON(msg);
 
                        console.log(data);
                        console.log(data["msg"]);


                          if(data["status"]==1)
                          {

                            console.log("rate flt "+data[0]["rate"]);
                             $("#txt_flatrate_open").val(data[0]["open_time"]);

                             $("#txt_flatrate_close").val(data[0]["close_time"]);
                             $("#txt_flatrate_rate").val(data[0]["rate"]);

                            if(data[0]["status"]==1)
                            {
                              console.log("yo");

             
                               $('#f1,#f2').removeClass();
                             $('#f1').addClass('btn btn-default btn-on-1 btn-lg active focus');
                             $('#f2').addClass('btn btn-default btn-off-1 btn-lg');

                             $('#flat_active1').attr('checked',true);

                             $('#flat_active2').attr('checked',false);

                            
                              $("#show_flat").show();
                           

                                $("#flat_status").val("1");

                               

                              $("#options2").addClass('disabled');

                               
                            }
                            else
                            {
                              console.log("nyo");
                              
                             $('#f1,#f2').removeClass();
                             

                             $('#f2').addClass('btn btn-default btn-off-1 btn-lg active focus');
                             $('#f1').addClass('btn btn-default btn-on-1 btn-lg');
                              
                              $('#flat_active1').attr('checked',false);

                              $('#flat_active2').attr('checked',true);

                               $("#show_flat").hide();

                                $("#flat_status").val("0");
                                    
                               
                              
                               $("#options2").removeClass('disabled');
                            }



                            if(data[0]["isAlwaysOpen"]==1)
                            {
                                 $("#txt_flatrate_isalwaysopen").prop('checked', true);
                                 $("#flat_ot,#flat_ct").hide();

                           
                                  $("#isAlwaysAvailable").val("1")
                                 
                            }
                            else
                            {
                               $("#txt_flatrate_isalwaysopen").prop('checked', false);
                               $("#flat_ot,#flat_ct").show();

                         
                               $("#isAlwaysAvailable").val("0");
                                   
                            }



                          }
                          else
                          {
                            console.log("no data");

                             $("#txt_flatrate_rate").val("0");
                             $("#txt_flatrate_close").val("00:00:00");
                             $("#txt_flatrate_open").val("00:00:00");

                              $('#f1,#f2').removeClass();

                             $('#f2').addClass('btn btn-default btn-off-1 btn-lg active focus');
                             $('#f1').addClass('btn btn-default btn-on-1 btn-lg');

                             $("#isAlwaysAvailable").val("0");
                            $("#show_flat").hide();

                             $("#flat_status").val("1");

                          }

 
                   });

                    request.fail(function(jqXHR, textStatus) {
                        alert("Request failed: " + textStatus);

                    });
  
}



function get_hourly_timings()
{
  
   var request = $.ajax({
                        url: "<?php echo base_url().'index.php/rates/get_hourly_timings';?>",
                        type: "POST",
                        data: {
                          
                            date: $("#date").val(),
                           
                            dataType: "html",

                        },
 
                    });

                    request.done(function(msg) {
 
                        var data = $.parseJSON(msg);
 
                        console.log(data);
                        console.log(data["msg"]);


                          if(data["status"]==1)
                          {

                        
                             $("#txt_hourly_rate").val(data[0]["rate"]);

                      

                            if(data[0]["status"]==1)
                            {
                             

                            
                              $("#txt_hourly_status").prop('checked', true);
                              $("#hourly_av").val("1");

                              $("#h_rate").show();
                           
 
                              $("#options1").addClass('disabled');

                               
                            }
                            else
                            {
                       
                              $("#txt_hourly_status").prop('checked', false);

                             $("#hourly_av").val("0");

                              $("#h_rate").hide();
                           
 
                              $("#options1").removeClass('disabled');
                     
                            }
 
                          }
                          else
                          {
                            $("#txt_hourly_status").prop('checked', false);

                            $("#hourly_av").val("0");

                            
                            $("#h_rate").hide();
                            $("#txt_hourly_rate").val("0");
                           
 
                            
                          }

 
                   });

                    request.fail(function(jqXHR, textStatus) {
                        alert("Request failed: " + textStatus);

                    });
  
}

            });
        </script>
      

 

<style>
.fc-title {  font-size: 10px;}
.btn-default.btn-on.active{background-color: #5BB75B;color: white;}
.btn-default.btn-off.active{background-color: #DA4F49;color: white;}

.btn-default.btn-on-1.active{background-color: #006FFC;color: white;}
.btn-default.btn-off-1.active{background-color: #DA4F49;color: white;}

.btn-default.btn-on-2.active{background-color: #00D590;color: white;}
.btn-default.btn-off-2.active{background-color: #A7A7A7;color: white;}

.btn-default.btn-on-3.active{color: #5BB75B;font-weight:bolder;}
.btn-default.btn-off-3.active{color: #DA4F49;font-weight:bolder;}

.btn-default.btn-on-4.active{background-color: #006FFC;color: #5BB75B;}
.btn-default.btn-off-4.active{background-color: #DA4F49;color: #DA4F49;}

.bs-example{ margin: 20px;}
.disabled{    pointer-events: none;}
.icons {display: none;}
</style>

 

 

        <footer class="footer" style="background-color: #eee">
            <div class="container-fluid">
                <nav class="pull-left">
                    <ul>

                        <li>
                            <a href="">
                                <img src="<?php echo base_url(); ?>assets/img/logo_sm.png" style="padding-bottom: 4px;width:110px" />
                            </a>
                        </li>
                        <li>
                            <a href="" style="color: #68B3C8">
                               About
                            </a>
                        </li>
                        <li>
                            <a href="" style="color: #68B3C8">
                                Sitemap
                            </a>
                        </li>
                    </ul>
                </nav>
                <div class="copyright pull-right">
                    <a href="http://brosoft.biz/site/">
                    &copy; <script>document.write(new Date().getFullYear())</script>  <img src="<?php echo base_url(); ?>assets/img/brosoft.png" style="padding-bottom: 4px;width: 80px;" />&nbsp;&nbsp;|&nbsp;  All Right Reserved  </a>
                </div>
            </div>
        </footer>


    </div>
</div>


</body>

 
     <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

    <!-- Paper Dashboard Core javascript and methods for Demo purpose -->
     <script src="<?php echo base_url(); ?>assets/js/bootstrap-checkbox-radio.js"></script>

    <!--  Charts Plugin -->
    <script src="<?php echo base_url(); ?>assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="<?php echo base_url(); ?>assets/js/bootstrap-notify.js"></script>

   

    <!-- Paper Dashboard Core javascript and methods for Demo purpose -->
    <script src="<?php echo base_url(); ?>assets/js/paper-dashboard.js"></script>

    <!-- Paper Dashboard DEMO methods, don't include it in your project! -->
    <script src="<?php echo base_url(); ?>assets/js/demo.js"></script>


  <?php echo $this->session->flashdata('notify'); ?>
</html>
