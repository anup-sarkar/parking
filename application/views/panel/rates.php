<?php
   defined('BASEPATH') OR exit('No direct script access allowed');
   $data['isactive']='rates';
   $this->load->view("module/header_panel",$data);
   ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/paper-dashboard.js"></script>
 <link href="<?php echo base_url(); ?>assets/css/switch.css" rel="stylesheet">

</head>
<body>
   <br/>
   <br/>
   <div class="bs-example" style="width: 95%;margin:0 auto">
      <ul class="nav nav-tabs" id="myTab">
         <li class="active"><a href="#sectionA"><i class="fa fa-calendar-check-o" aria-hidden="true"></i> Open & Close Timing</a></li>
         <li><a href="#sectionB"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Flat Rate </a></li>
         <li><a href="#sectionE"><i class="fa fa-clock-o" aria-hidden="true"></i> Hourly Rate </a></li>
         <li><a href="#sectionC"><i class="fa fa-calendar" aria-hidden="true"></i> Monthly Rate</a></li>
         <li><a href="#sectionD"><i class="fa fa-trophy" aria-hidden="true"></i> Event Rate </a></li>
      </ul>
      <div class="tab-content">
         <div id="sectionA" class="tab-pane fade in active">
            <div style="width: 70%">
               <?php $attributes = array("class" => "rates", "id" => "opencloseform", "name" => "opencloseform");
                  echo form_open("rates/create_open_close_default", $attributes);?>
               <div class="card card-plain" style="
                  ">
                  <div class="content">
                     <br/>
                     <div class="form-control" style="z-index:100;height: 45px">
                        <div class="col-md-8" style="z-index:100"><label style="
                           color:  black;
                           font-size: 20px;
                           ">Is 24 Hours Open? </label></div>
                        <div class="col-md-4" style="z-index:100">
                           <label class="switch" style="float: right;">
                           <input type="checkbox" <?php if($timings->isAlwaysOpen ==1) echo "checked"; ?> id="txt_openclose_isalwaysopen" name="txt_openclose_isalwaysopen">
                           <span class="slider round"></span>
                           </label>
                        </div>
                     </div>
                     <div class="form-group">
                        <br/>
                        <label style="font-size: 20px">Open Time</label>
                        <input type="time"  <?php if($timings->isAlwaysOpen ==1) echo "disabled"; ?> id="txt_openclose_open" name="txt_openclose_open" class="form-control" value="<?php echo $timings->open_time; ?>">
                        <span class="text-danger"><?php echo form_error('txt_openclose_open'); ?></span>
                     </div>
                     <div class="form-group">
                        <label style="font-size: 20px">Close Time</label>
                        <input type="time"  <?php if($timings->isAlwaysOpen ==1) echo "disabled"; ?>  id="txt_openclose_close" name="txt_openclose_close" class="form-control" value="<?php echo $timings->close_time; ?>">
                        <span class="text-danger"><?php echo form_error('txt_openclose_close'); ?></span>
                     </div>
                     <div class="form-group">
                        <label style="font-size: 20px">Total Available Space</label>
                        <input type="number" placeholder="Total" id="txt_openclose_total" name="txt_openclose_total"  class="form-control" value="<?php echo $timings->total_available; ?>">
                        <span class="text-danger"><?php echo form_error('txt_openclose_total'); ?></span>
                     </div>
                  </div>
                  <div class="footer text-center">
                     <input type="submit" class="btn btn-fill btn-neutral btn-wd" id="btn_openclose" name="btn_openclose" value="SET" style="
                        color: #337ab7;
                        width: 95%;
                        background: #eee;
                        border-color: #2196f340;
                        ">
                  </div>
               </div>
               <?php echo form_close(); ?>
               <?php echo $this->session->flashdata('msg'); ?>
            </div>
         </div>
         <div id="sectionB" class="tab-pane fade">
            <div style="width: 70%">
               <?php $attributes = array("class" => "rates", "id" => "flatRateform", "name" => "flatRateform");
                  echo form_open("rates/create_flat_rate_default", $attributes);?>
               <div class="card card-plain" style="
                  ">
                  <div class="content">
                     <br/>
                     <div class="form-control" style="z-index:100;height: 45px">
                        <div class="col-md-8" style="z-index:100"><label style="
                           color:  black;
                           font-size: 20px;
                           ">Is 24 Hours Open? </label></div>
                        <div class="col-md-4" style="z-index:100">
                           <label class="switch" style="float: right;">
                           <input type="checkbox" <?php if($flatrate->isAlwaysOpen ==1) echo "checked"; ?> id="txt_flatrate_isalwaysopen" name="txt_flatrate_isalwaysopen">
                           <span class="slider round"></span>
                           </label>
                        </div>
                     </div>
                     <div class="form-group">
                        <br/>
                        <label style="font-size: 20px">Open Time</label>
                        <input type="time" <?php if($flatrate->isAlwaysOpen ==1) echo "disabled"; ?>  id="txt_flatrate_open" name="txt_flatrate_open" class="form-control" value="<?php echo $flatrate->open_time; ?>">
                        <span class="text-danger"><?php echo form_error('txt_flatrate_open'); ?></span>
                     </div>
                     <div class="form-group">
                        <label style="font-size: 20px">Close Time</label>
                        <input type="time" <?php if($flatrate->isAlwaysOpen ==1) echo "disabled"; ?>  id="txt_flatrate_close" name="txt_flatrate_close" class="form-control" value="<?php echo $flatrate->close_time; ?>">
                        <span class="text-danger"><?php echo form_error('txt_flatrate_close'); ?></span>
                     </div>
                     <div class="form-group">
                        <label style="font-size: 20px">Rate($)</label>
                        <input type="number"   id="txt_flatrate_rate" name="txt_flatrate_rate"  class="form-control" value="<?php echo $flatrate->rate; ?>">
                        <span class="text-danger"><?php echo form_error('txt_flatrate_rate'); ?></span>
                     </div>
                  </div>
                  <div class="footer text-center">
                     <input type="submit" class="btn btn-fill btn-neutral btn-wd" id="btn_flatrate" name="btn_flatrate" value="SET" style="
                        color: #337ab7;
                        width: 95%;
                        background: #eee;
                        border-color: #2196f340;
                        ">
                  </div>
               </div>
               <?php echo form_close(); ?>
               <?php echo $this->session->flashdata('msg2'); ?>
            </div>
         </div>
         <div id="sectionE" class="tab-pane fade">
            <div style="width: 70%">
               <?php $attributes = array("class" => "rates", "id" => "flatRateform", "name" => "flatRateform");
                  echo form_open("rates/create_hourly_rate_dafault", $attributes);?>
               <div class="card card-plain" style="
                  ">
                  <div class="content">
                     <br/>
                     <div class="form-control" style="z-index:100;height: 45px">
                        <div class="col-md-8" style="z-index:100"><label style="
                           color:  black;
                           font-size: 20px;
                           ">Status </label></div>
                        <div class="col-md-4" style="z-index:100">
                           <label class="switch" style="float: right;">
                           <input type="checkbox" <?php if($hourly_rate !=null ) if($hourly_rate->status ==1) echo "checked";   ?> id="txt_hourly_status" name="txt_hourly_status">
                           <span class="slider round"></span>
                           </label>
                        </div>
                     </div>
                     <div class="form-group">
                        <br/>
                        <label style="font-size: 20px">Rate($)</label>
                        <input type="number" <?php 
                           if($hourly_rate !=null ) {
                               if($hourly_rate->status ==0) 
                                   echo "disabled"; } else{echo "disabled"; }?> 
                           id="txt_hourly_rate" name="txt_hourly_rate"  class="form-control" 
                           value="<?php if($hourly_rate !=null ) echo $hourly_rate->rate;
                              else echo  "0"; ?>">
                        <span class="text-danger"><?php echo form_error('txt_hourly_rate'); ?></span>
                     </div>
                  </div>
                  <div class="footer text-center">
                     <input type="submit" class="btn btn-fill btn-neutral btn-wd" id="btn_hourlyrate" name="btn_hourlyrate" value="SET" 
                        style="
                        color: #337ab7;
                        width: 95%;
                        background: #eee;
                        border-color: #2196f340;
                        ">
                  </div>
               </div>
               <?php echo form_close(); ?>
               <?php echo $this->session->flashdata('msg4'); ?>
            </div>
         </div>
         <div id="sectionC" class="tab-pane fade">
            <div style="width: 70%">
               <?php $attributes = array("class" => "rates", "id" => "flatRateform", "name" => "flatRateform");
                  echo form_open("rates/create_monthly_rate", $attributes);?>
               <div class="card card-plain" style="
                  ">
                  <div class="content">
                     <br/>
                     <div class="form-control" style="z-index:100;height: 45px">
                        <div class="col-md-8" style="z-index:100"><label style="
                           color:  black;
                           font-size: 20px;
                           ">Status </label></div>
                        <div class="col-md-4" style="z-index:100">
                           <label class="switch" style="float: right;">
                           <input type="checkbox" <?php if($monthlyrate !=null ) if($monthlyrate->status ==1) echo "checked";   ?> id="txt_monthly_status" name="txt_monthly_status">
                           <span class="slider round"></span>
                           </label>
                        </div>
                     </div>
                     <div class="form-group">
                        <br/>
                        <label style="font-size: 20px">Rate($)</label>
                        <input type="number" <?php 
                           if($monthlyrate !=null ) {
                               if($monthlyrate->status ==0) 
                                   echo "disabled"; 
                                
                           }
                           else
                           {
                                 echo "disabled"; 
                           }
                               
                               ?> 
                           id="txt_monthly_rate" name="txt_monthly_rate"  class="form-control" 
                           value="<?php if($monthlyrate !=null ) echo $monthlyrate->rate;
                              else echo  "0"; ?>">
                        <span class="text-danger"><?php echo form_error('txt_monthly_rate'); ?></span>
                     </div>
                  </div>
                  <div class="footer text-center">
                     <input type="submit" class="btn btn-fill btn-neutral btn-wd" id="btn_flatrate" name="btn_flatrate" value="SET" style="
                        color: #337ab7;
                        width: 95%;
                        background: #eee;
                        border-color: #2196f340;
                        ">
                  </div>
               </div>
               <?php echo form_close(); ?>
               <?php echo $this->session->flashdata('msg3'); ?>
            </div>
         </div>
         <div id="sectionD" class="tab-pane fade">
            <div class="content">
               <div class="container-fluid">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="card" style="background-color: transparent;">
                           <div class="header">
                           </div>
                           <div class="content table-responsive table-full-width">
                              <table class="table table-hover" style="font-size: 12px">
                                 <thead style="color: cornflowerblue">
                                    <tr>
                                       <th style="width: 100px"></th>
                                       <th style="width: 120px"><b>Events Name</b></th>
                                       <th style="width: 110px"><b>Start Date</b></th>
                                       <th style="width: 110px"><b>End Date</b></th>
                                       <th><b>Open Time</b></th>
                                       <th><b>Close Time</b></th>
                                       <th><b>Rate</b></th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <?php
                                       $i=1;
                                                   foreach ($events as $item)
                                                   {
                                                         $url=base_url();
                                                         $id=$item->id; 
                                                         $name=$item->event_name; 
                                                         $details=$item->event_details;
                                                         $sdate=date_create($item->start_date);
                                                         $edate=date_create($item->end_date);
                                       
                                                         $rate=0;
                                                         $start="";
                                                         $end="";
                                                         $status="";
                                       
                                                         foreach ($event_rate as $er ) 
                                                         {
                                                           # code...
                                                           if($id==$er->event_id)
                                                           {
                                                             $rate=$er->rate;
                                                             if($er->status==1)
                                                             {
                                                               $status="checked";
                                                               $start=$er->open_time;
                                                               $end=$er->close_time;
                                                             }
                                                           
                                                           }
                                                   
                                                         }
                                       
                                                                           
                                                    if($item->status==1)
                                                                               {
                                       
                                        echo "<tr>";
                                                                           
                                        echo "<td><label class='switch'  >
                                        <input type='checkbox'  ".$status." id='txt_event_status".$i."' name='txt_event_status".$i."' value='".$i."'>
                                        <span class='slider round'></span>
                                        </label></td>";
                                        
                                        echo "<td>".$name."</td>";   
                                                                          
                                        echo "<td>".date_format($sdate,"d-m-Y ")."</td>";   
                                        echo  "<td>".date_format($edate,"d-m-Y ")."</td>";
                                        echo  "<td><input type='time' id='start".$i."'  name='start".$i."'  class='form-control'  value='".$start."' > </td>";
                                        echo  "<td><input type='time' id='end".$i."' name='end".$i."' class='form-control'   value='".$end."'   > </td>";
                                       
                                        echo  "<td><input type='number' id='rate".$i."' name='rate".$i."' class='form-control' value='".$rate."'  > </td>";
                                       
                                    
                                        echo "<td style='display:none'> <input type='text' id='txt_event_id".$i."'  name='txt_event_id".$i."' value='".$id."' > </td>";
                                       
                                 
                                                                       }
                                                                              
                                                                              $i++; 
                                       
                                                   }
                                                                               ?>
                                    <tr> <?php echo $this->session->flashdata('msg'); ?>  </tr>
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
 



     
         </div>
      </div>
   </div>
   <br/>
   <br/>



       <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
            <script type="text/javascript">
               $(document).ready(function(){
             
               $("#myTab a").click(function(e){
                  e.preventDefault();
                  $(this).tab('show');
               });
               
               
               $('#txt_flatrate_isalwaysopen').click(function()
               {
                   if ($(this).is(':checked')){
                   $("#txt_flatrate_open").prop("disabled", true);
                   $("#txt_flatrate_close").prop("disabled", true);
               }
               else{
                  $("#txt_flatrate_open").prop("disabled", false);
                  $("#txt_flatrate_close").prop("disabled", false);
               }
               });
               
               
               
               
               $('#txt_openclose_isalwaysopen').click(function()
               {
                   if ($(this).is(':checked')){
                   $("#txt_openclose_open").prop("disabled", true);
                   $("#txt_openclose_close").prop("disabled", true);
               }
               else{
                   $("#txt_openclose_open").prop("disabled", false);
                   $("#txt_openclose_close").prop("disabled", false);
               }
               });
               
               
               $('#txt_monthly_status').click(function()
               {
               if ($(this).is(':checked')){
                   $("#txt_monthly_rate").prop("disabled", false);
               
               } 
               else
               {
                   $("#txt_monthly_rate").prop("disabled", true);
               }
               });
               
               
               
               
               
               <?php 
                  $j=1;
                  foreach ($events as $item) { ?>
               
               
               
               
               $("#txt_event_status<?php echo $j; ?>").click(function()
               {
               
               
                  if ($(this).is(':checked')){
               
                 var request = $.ajax({
               url: "<?php echo base_url().'index.php/rates/update_eventrate';?>",
               type: "POST",
               data: {id : $("#txt_event_id<?php echo $j; ?>").val(),
               status : 1,
               rate : $("#rate<?php echo $j; ?>").val(),
               start : $("#start<?php echo $j; ?>").val(),
               end : $("#end<?php echo $j; ?>").val(),
               dataType: "html"
               }
               });
                  }
                  else
                  {
               
                    var request = $.ajax({
               url: "<?php echo base_url().'index.php/rates/update_eventrate';?>",
               type: "POST",
               data: {id : $("#txt_event_id<?php echo $j; ?>").val(),
               status : 0,
               rate : $("#rate<?php echo $j; ?>").val(),
               start : $("#start<?php echo $j; ?>").val(),
               end : $("#end<?php echo $j; ?>").val(),
               dataType: "html"
               }
               
               });
                  }
      
                   request.done(function(msg) {
               
                   var m=msg.substr(2,5);
                    
               
               if(m=='Error')
               {
                   alert(msg);
               $("#txt_event_status<?php echo $j; ?>").prop('checked', false);
               }
               else
               {
               
                   alert("Success: Events Rates Updated!");
               }
               
               });
               
                     request.fail(function(jqXHR, textStatus) {
                     alert( "Request failed: " + textStatus );
               
               
                   });
  
                  
                });
               
               <?php $j++; }?>
        
               
               $("#txt_hourly_status").click(function()
               {
               
               
                  if ($(this).is(':checked')){
                  $("#txt_hourly_rate").prop('disabled', false);
                      
                
                  }
                  else
                  {
                
                    $("#txt_hourly_rate").prop('disabled', true);
                         
                  }
               
   
                  
                });
               
                 
               });
               
            </script>

<style type="text/css">
   .bs-example{
   margin: 20px;
   }
   #opencloseform,#flatRateform
   {
      background-color: transparent;
   }
</style>
   <?php
      $this->load->view("module/footer");
      ?>