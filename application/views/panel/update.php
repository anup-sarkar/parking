
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$data['isactive']="location";
$this->load->view("module/header_panel",$data);
?>

<?php 



foreach ($parkingspotinfo as $item)
            {
  # code...
                



?>

   <div class="content">
            <div class="container-fluid">
                <div class="row">
                   

                    
                    <div class="col-lg-12 col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Edit Parking Spot Info</h4>
                            </div>
                            <div class="content">
                                <?php $attributes = array("class" => "", "id" => "parkingSpotInfoUpdateform", "name" => "parkingspotInfoUpdateform");
                                   echo form_open("Panel/update_data", $attributes);?>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Default Space</label>
                                                <input type="number" id="txt_parkingspot_default" name="txt_parkingspot_default" class="form-control border-input"    value="<?php echo $item->default_space; ?>">
                                              <span class="text-danger"><?php echo form_error('txt_parkingspot_default'); ?></span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                             <div class="form-group">
                                                <label>Total Space</label>
                                                <input type="number"  id="txt_parkingspot_total" name="txt_parkingspot_total" class="form-control border-input"  value="<?php echo $item->total_space; ?>">

                                                  <span class="text-danger"><?php echo form_error('txt_parkingspot_total'); ?></span>
                                            </div>
                                        </div>
                                       
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                           

                                            <div class="form-group">
                                                <label>Total Facility Space</label>
                                                <input type="number" id="txt_parkingspot_facility" name="txt_parkingspot_facility" class="form-control border-input"  value="<?php echo $item->total_facility_space; ?>">
                                                    <span class="text-danger"><?php echo form_error('txt_parkingspot_facility'); ?></span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Vehicle Height Restriction</label>
                                                <input type="text"   id="txt_parkingspot_vheight" name="txt_parkingspot_vheight" class="form-control border-input"  value="<?php echo $item->vehicle_height_restriction; ?>">

                                                  <span class="text-danger"><?php echo form_error('txt_parkingspot_vheight'); ?></span>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Description</label>
                                                <input type="text" style="height:100px" id="txt_parkingspot_details" name="txt_parkingspot_details"  class="form-control border-input"  value="   <?php echo $item->descriptions; ?>" >
                                                 
                                                </textarea>
                                                    <span class="text-danger"><?php echo form_error('txt_parkingspot_details'); ?></span>
                                            </div>
                                        </div>
                                         <div class="col-md-6">
                                             <div class="form-group">
                                                <label>How To Find</label>
                                                <input type="text" style="height:100px" id="txt_parkingspot_find" name="txt_parkingspot_find"  class="form-control border-input" value="<?php echo $item->how_to_find; ?>"  >
                                                    
                                                </textarea>
                                                    <span class="text-danger"><?php echo form_error('txt_parkingspot_find'); ?></span>
                                            </div>
                                        </div>
                                          
                                    </div>

                                    <div class="row">
                                        
                                      <div class="col-md-6">
                                            <div class="form-group">
                                                <label>How To Redeem</label>
                                                <input type="text" style="height:100px" id="txt_parkingspot_redeem" name="txt_parkingspot_redeem"  class="form-control border-input"   value="<?php echo $item->how_to_redem; ?>">
                             
                                                    <span class="text-danger"><?php echo form_error('txt_parkingspot_redeem'); ?></span>
                                            </div>
                                        </div>
                                         <div class="col-md-6">
                                             <div class="form-group">
                                                <label>Terms & Condition</label>
                                                <input type="text" style="height:100px"  id="txt_parkingspot_terms" name="txt_parkingspot_term"  class="form-control border-input"   value="<?php echo $item->terms; ?>" >
                                                    
                                                
                                                    <span class="text-danger"><?php echo form_error('txt_parkingspot_term'); ?></span>
                                            </div>
                                        </div>
                                          
                                    </div>


                                    <div class="row">
                                       
                                         <div class="col-md-6      ">
                                            <div class="form-group">
                                                <label>Status</label>

                                                <?php 
                                                $isactive="lightgrey";
                                                if($item->customer_review!=1) 
                                                      $isactive="red"; ?>
                                                 <select  id="txt_parkingspot_review" name="txt_parkingspot_status" class="form-control"  style="border: 1px solid <?php echo $isactive; ?> ">
                    
                                              
                                               
                                                    

                                                  <?php if ($item->customer_review==1) 
                                                  {
                                                    echo '<option value="1"  selected> Enabled </option>  <option value="0"  > Disabled </option>';
                                                  }else
                                                  {
                                                  
                                                    echo '<option value="0" selected > Disabled </option>  <option value="1"  > Enabled</option>';
                                                  }
                                            
                                           ?>
                        
                                          </select>
                                            </div>
                                        </div>

                                          <div class="col-md-6      ">
                                            <div class="form-group">
                                                <label>Status</label>

                                                <?php 
                                                $isactive2="lightgrey";
                                                if($item->status!=1) 
                                                      $isactive="red"; ?>
                                                 <select  id="txt_parkingspot_status" name="txt_parkingspot_status" class="form-control"  style="border: 1px solid <?php echo $isactive; ?> ">
                    
                                              
                                               
                                                    

                                                  <?php if ($item->status==1) 
                                                  {
                                                    echo '<option value="1"  selected> Enabled </option>  <option value="0"  > Disabled </option>';
                                                  }else
                                                  {
                                                  
                                                    echo '<option value="0" selected > Disabled </option>  <option value="1"  > Enabled</option>';
                                                  }
                                            
                                           ?>
                        
                                          </select>
                                            </div>
                                        </div>

                                         <div style="display: none">
                                            <div class="form-group">
                                                <label>Id</label>
                                                <input type="number"  id="txt_id" name="txt_id" class="form-control border-input"  value="<?php echo $item->pid; ?>">
                                            </div>
                                        </div>

                                        
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-info btn-fill btn-wd">Update Parking Spot Info</button>
                                    </div>
                                    <div class="clearfix"></div>
                                <?php echo form_close(); ?>
                              
                            </div>
                        </div>
                        <?php echo $this->session->flashdata('msg'); ?>
                    </div>


                </div>
            </div>
        </div>


<?php }
                    ?>

  <?php echo $this->session->flashdata('notify'); ?>


<?php

$this->load->view("module/footer");
?>
