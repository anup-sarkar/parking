<?php
   defined('BASEPATH') OR exit('No direct script access allowed');
   $this->load->view("module/header_client");
    date_default_timezone_set("Asia/Dhaka");
   ?>






    <div class="position-relative overflow-hidden p-3 p-md-5 m-md-3 text-center bg-light">
      <div class="col-md-6 p-lg-5 mx-auto my-5">
        <h1 class="display-5 font-weight-normal" id="cyan" style="text-shadow: 1px 2px 2px #000000;" >SEARCH FOR PARKING</h1>
        <p class="lead font-weight-normal">Find Our Best Deal with Affordable Rate !</p>

      <div class="container">
         <div class="form-inline mr-auto" >
    <input    id="pac-input" name="pac-input" type="text" placeholder="Location"
     aria-label="Search" style=" border-radius: 0;width: 60%;background-color:  transparent;border:  0;border-bottom: 3px solid #3fa3e2;">
     
    <button class="btn blue-gradient btn-rounded btn-sm my-0"   id="search">Search</button>


</div>



      </div>
    

      </div>
      <div class="product-device box-shadow d-none d-md-block" style="
    background-color: #e5e5e5;
    padding: 10px;
">
                 
                   <div class="pac-card" id="pac-card">
      <div>
         
        <div id="country-selector" class="pac-controls" style="display: none">
          <input type="radio" name="type" id="changecountry-usa">
          <label for="changecountry-usa">USA</label>

          <input type="radio" name="type" id="changecountry-usa-and-uot" checked="checked">
          <label for="changecountry-usa-and-uot">USA and unincorporated organized territories</label>
        </div>
      </div>
       
    </div>
    <div id="map" style="height: 100%;border-radius: 15px "></div>
    <div id="infowindow-content">
      <img src="" width="16" height="16" id="place-icon">
      <span id="place-name"  class="title"></span><br>
      <span id="place-address"></span>
    </div>

      </div>
      <div class="product-device product-device-2 box-shadow d-none d-md-block">
         <img src="<?php echo base_url(); ?>assets/img/logo_trans2.png" style="margin-top: 150px" >
      </div>


    </div>

    <div class="d-md-flex flex-md-equal w-100 my-md-3 pl-md-3">
      <div class="bg-dark mr-md-3 pt-3 px-3 pt-md-5 px-md-5 text-center text-white overflow-hidden">
        <div class="my-3 py-3">
          <h2 class="display-5">Find</h2>
          <p class="lead">Search and compare all available parking options and prices at thousands of parking lots</p>
        </div>
        <div class="bg-light box-shadow mx-auto" style="width: 80%; height: 300px; border-radius: 21px 21px 0 0;">
          
          <img src="<?php echo base_url(); ?>assets/img/find.png" style="margin-top: 50px;width: 200px" >
        </div>
      </div>
      <div class="bg-light mr-md-3 pt-3 px-3 pt-md-5 px-md-5 text-center overflow-hidden">
        <div class="my-3 p-3">
          <h2 class="display-5" id="cyan">Book</h2>
          <p class="lead">Pre-purchase the perfect spot and have a guaranteed space waiting for you when you need it.</p>
        </div>
        <div class="bg-dark box-shadow mx-auto" style="width: 80%; height: 300px; border-radius: 21px 21px 0 0;">
          
             <img src="<?php echo base_url(); ?>assets/img/booking.png" style="width: 200px;margin-top: 50px" >
          </div>
      </div>
    </div>

    <div class="d-md-flex flex-md-equal w-100 my-md-3 pl-md-3">
      <div class="bg-light mr-md-3 pt-3 px-3 pt-md-5 px-md-5 text-center overflow-hidden">
        <div class="my-3 p-3">
          <h2 class="display-5">Best Deal</h2>
          <p class="lead">Find the best deal with affordable rate all over the country</p>
        </div>
        <div class="bg-dark box-shadow mx-auto" style="width: 80%; height: 300px; border-radius: 21px 21px 0 0;">
            <img src="<?php echo base_url(); ?>assets/img/deal.png" style="width: 200px;margin-top: 50px" >

        </div>
      </div>
      <div class="bg-primary mr-md-3 pt-3 px-3 pt-md-5 px-md-5 text-center text-white overflow-hidden">
        <div class="my-3 py-3">
          <h2 class="display-5">Your Favorite City</h2>
          <p class="lead">Find our service in your every favorite cities all over the country.</p>
        </div>
        <div class="bg-light box-shadow mx-auto" style="width: 80%; height: 300px; border-radius: 21px 21px 0 0;">
                      <img src="<?php echo base_url(); ?>assets/img/city.png" style="width: 200px;margin-top: 50px" >
        </div>
      </div>
    </div>

  
 
 <h1 style="text-align: center;"> Find Parking In Your Faviorite Cities </h1>
  <div class="row" style="margin-left: 2px;margin-right: 2px">
    <div class="col-md-3">
     <div class="card" style="width: 18rem;">
          <img class="card-img-top" src="<?php echo base_url(); ?>assets/img/city/city.jpg" width="330" alt="Card image cap">
          <div class="card-body">
                <h3 class="card-text" style="text-align: center;">BOSTON</h3>
          </div>
    </div>
    </div>
    <div class="col-md-3">
      <div class="card" style="width: 18rem;">
           <img class="card-img-top" src="<?php echo base_url(); ?>assets/img/city/vegas.jpg" width="330" alt="Card image cap">
          <div class="card-body">
                <h3 class="card-text" style="text-align: center;">LAS VEGAS</h3>
          </div>
    </div>
    </div>
    <div class="col-md-3">
     <div class="card" style="width: 18rem;">
          <img class="card-img-top" src="<?php echo base_url(); ?>assets/img/city/york.jpg" width="330" alt="Card image cap">
          <div class="card-body">
                <h3 class="card-text" style="text-align: center;">NEW YORK</h3>
          </div>
    </div>
    </div>

     <div class="col-md-3">
     <div class="card" style="width: 18rem;">
          <img class="card-img-top" src="<?php echo base_url(); ?>assets/img/city/chicago.jpg" width="330" alt="Card image cap">
          <div class="card-body">
                <h3 class="card-text" style="text-align: center;">CHICAGO</h3>
          </div>
    </div>
    </div>
  </div>

<br/>

    <div class="row" style="margin-left: 2px;margin-right: 2px">
    <div class="col-md-3">
     <div class="card" style="width: 18rem;">
          <img class="card-img-top" src="<?php echo base_url(); ?>assets/img/city/atlanta.jpg" width="330" alt="Card image cap">
          <div class="card-body">
                <h3 class="card-text" style="text-align: center;">ATLANTA</h3>
          </div>
    </div>
    </div>
    <div class="col-md-3">
      <div class="card" style="width: 18rem;">
           <img class="card-img-top" src="<?php echo base_url(); ?>assets/img/city/florida.jpg" width="330" alt="Card image cap">
          <div class="card-body">
                <h3 class="card-text" style="text-align: center;">FLORIDA</h3>
          </div>
    </div>
    </div>
    <div class="col-md-3">
     <div class="card" style="width: 18rem;">
          <img class="card-img-top" src="<?php echo base_url(); ?>assets/img/city/san.jpg" width="330" alt="Card image cap">
          <div class="card-body">
                <h3 class="card-text" style="text-align: center;">SAN FRANCISCO</h3>
          </div>
    </div>
    </div>

     <div class="col-md-3">
     <div class="card" style="width: 18rem;">
          <img class="card-img-top" src="<?php echo base_url(); ?>assets/img/city/california.jpg" width="330" alt="Card image cap">
          <div class="card-body">
                <h3 class="card-text" style="text-align: center;">CALIFORNIA</h3>
          </div>
    </div>
    </div>
  </div>
 


<input type="hidden" id="lat">
<input type="hidden" id="long">
<input type="hidden" id="place">
<input type="hidden" id="address">



<?php
   $this->load->view("module/footer_client");
 ?>







 

 


<script>



      function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: 38.532675, lng: -103.784548},
            gestureHandling: 'greedy',
              disableDefaultUI: true,
       
          zoom: 3.5
        });
        var card = document.getElementById('pac-card');
        var input = document.getElementById('pac-input');
        var countries = document.getElementById('country-selector');

        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

        var autocomplete = new google.maps.places.Autocomplete(input);

        // Set initial restrict to the greater list of countries.
        autocomplete.setComponentRestrictions(
            {'country': ['us', 'pr', 'vi', 'gu', 'mp']});

        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
          map: map,

          anchorPoint: new google.maps.Point(0, -29)
        });

        autocomplete.addListener('place_changed', function() {
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No place available for input: '" + place.name + "'");
            return;
          }

          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
            
          var lat=place.geometry.location.lat();
          var long=place.geometry.location.lng();
             

           $('#lat').val(lat);
           $('#long').val(long); 


          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(16);
              var lat=place.geometry.location.lat();
              var long=place.geometry.location.lng();
            
 

              $('#lat').val(lat);
              $('#long').val(long);  // Why 17? Because it looks good.
          }
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);

          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }

          infowindowContent.children['place-icon'].src = place.icon;
          infowindowContent.children['place-name'].textContent = place.name;

          infowindowContent.children['place-address'].textContent = address;
          infowindow.open(map, marker);

          console.log(place.name);
          console.log(address);

          $('#place').val(place.name);
          $('#address').val(address);

        });

        // Sets a listener on a given radio button. The radio buttons specify
        // the countries used to restrict the autocomplete search.
        function setupClickListener(id, countries) {
          var radioButton = document.getElementById(id);
          radioButton.addEventListener('click', function() {
            autocomplete.setComponentRestrictions({'country': countries});
          });
        }

        setupClickListener('changecountry-usa', 'us');
        setupClickListener(
            'changecountry-usa-and-uot', ['us', 'pr', 'vi', 'gu', 'mp']);
      }


     
 



$('#search').click(function() {


 if($('#pac-input').val()!='')
       {
        
       var lat=$('#lat').val();
       var long=$('#long').val(); 

        var place=  $('#place').val();
         var addr=  $('#address').val();
         var q2=$('#pac-input').val();
 

                        var redirectUrl="<?php echo base_url(); ?>index.php/search/locations"
                        var form = $('<form action="' + redirectUrl + '" method="post">' +
                            '<input type="hidden" name="place" value="'+ place +'" />' +
                            '<input type="hidden" name="addr" value="'+ addr +'" />' +
                            '<input type="hidden" name="lat" value="'+ lat +'" />' +
                            '<input type="hidden" name="q2" value="'+ q2 +'" />' +
                            '<input type="hidden" name="long" value="'+ long +'" />' +
                             
                           '</form>');
                            $('body').append(form);
                            $(form).submit();
}
else
{
  alert("Search Box Cannot Be Empty !");
}

});


$('#pac-input').keypress(function (e) {
 var key = e.which;
 if(key == 13)  // the enter key code
  {
     $('#search').click();

      
    
  }


  
});  

              localStorage.clear();

</script>

 



  </body>
</html>


