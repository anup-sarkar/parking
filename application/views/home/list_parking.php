<?php
   defined('BASEPATH') OR exit('No direct script access allowed');
   $this->load->view("module/header_client");
   ?>

   <div class="jumbotron">
        <h1><b>PROVIDERS FACILITES & BENEFITS ! </b></h1>
        <p class="lead text-success">Control, flexibility, support and cutting-edge technology at your fingertips</p>

        <div class="row">
            <div class="col-md-6">
              <p><i class="fa fa-chevron-circle-right" style="font-size:16px" aria-hidden="true"></i>&nbsp; Directly access and manage inventory for all your locations from any web-enabled device through our easy-to-use seller console. </p>
            </div>

            <div class="col-md-6">
               <p><i class="fa fa-chevron-circle-right" style="font-size:16px" aria-hidden="true"></i>&nbsp; Adjust your pricing, black out dates and inventory at your discretion and in real time. Hello-Hi Parking accommodates any rate table, black-out dates or exceptions. </p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
               <p><i class="fa fa-chevron-circle-right" style="font-size:16px" aria-hidden="true"></i>&nbsp;  Offer drivers frictionless validation while maintaining seamless back-office reporting and revenue control.</p>
            </div>

            <div class="col-md-6">
               <p><i class="fa fa-chevron-circle-right" style="font-size:16px" aria-hidden="true"></i>&nbsp; Our dedicated seller support reps and superior customer experience team are ready to meet the needs of your business and your drivers. </p>
            </div>
        </div>

        <p><a class="btn btn-lg btn-success" id="bt" href="#card" role="button">Get Started Today</a></p>
      </div>

      <!-- Example row of columns -->
      <div class="row" style="padding: 30px">
        <div class="col-lg-4">
          <h1><i class="fa fa-tachometer" aria-hidden="true" style="font-size: 120px;color: #01afdc;"></i></h1>
          <h2>Provider Admin Panel</h2>
          <p>An all in one admin panel for the parking owners to run business smoothly </p>
           
        </div>
        <div class="col-lg-4">
          <h1><i class="fa fa-desktop" style="font-size: 120px;color: #01afdc;"  aria-hidden="true"></i></h1>
          <h2>Real Time Data</h2>
          <p>Review current and future reservations for all your facilities in real time.</p>
           
       </div>
        <div class="col-lg-4">
          <h1><i class="fa fa-android" style="font-size: 120px;color: #01afdc;"  aria-hidden="true"></i></h1>
          <h2>Mobile App</h2>
          <p>Optimize your inventory with a single robust tool, available for iOS and Android.</p>
          
        </div>
      </div>



    <hr>
<div class="container">
  <div style="text-align: center;">

  <div>
    
    <div class="card card-container" id="card">
        <!-- <img class="profile-img-card" src="//lh3.googleusercontent.com/-6V8xOA6M7BA/AAAAAAAAAAI/AAAAAAAAAAA/rzlHcD0KYwo/photo.jpg?sz=120" alt="" /> -->
        <img id="profile-img" class="profile-img-card" src="<?php echo base_url(); ?>assets/img/hassle.png" />
        <h3 id="profile-name" class="profile-name-card text-success">Register Now To List Your Spot</h3>
        <br/>

 <?php $attributes = array("class" => "", "id" => "providerform", "name" => "providerform");
               echo form_open("home/listing", $attributes);?>

              <?php echo $this->session->flashdata('msg'); ?>
                  
                  <div class="form-group">
                     <input type="text" placeholder="Account Name" id="provider_name" name="provider_name" class="form-control" value="<?php echo set_value('provider_name'); ?>">
                     <span class="text-danger"><?php echo form_error('provider_name'); ?></span>
                  </div>

                   <div class="form-group">
                     <input type="text" placeholder="Designation" id="txt_provider_desig" name="txt_provider_desig" class="form-control" value="<?php echo set_value('txt_provider_desig'); ?>">
                     <span class="text-danger"><?php echo form_error('txt_provider_desig'); ?></span>
                  </div>

                   <div class="form-group">
                     <input type="text" placeholder=" Address" id="txt_provider_Address" name="txt_provider_Address" class="form-control" value="<?php echo set_value('txt_provider_Address'); ?>">
                     <span class="text-danger"><?php echo form_error('txt_provider_Address'); ?></span>
                  </div>
                 
                  <div class="form-group">
                     <input type="number" placeholder="Mobile No." id="txt_provider_mobile" name="txt_provider_mobile" class="form-control" value="<?php echo set_value('txt_provider_mobile'); ?>">

                         <span class="text-danger"><?php echo form_error('txt_provider_mobile'); ?></span>
                  </div>

                  <div class="form-group">
                     <input type="email" placeholder="Enter email" id="txt_provider_email" name="txt_provider_email"  class="form-control" value="<?php echo set_value('txt_provider_email'); ?>">

                        <span class="text-danger"><?php echo form_error('txt_provider_email'); ?></span>
                  </div>


                <div class="form-group">
                    <input type="password" placeholder="Password"   id="txt_provider_pass" name="txt_provider_pass" class="form-control" value="<?php echo set_value('txt_provider_pass'); ?>">

                      <span class="text-danger"><?php echo form_error('txt_provider_pass'); ?></span>
                </div>
 


            <button class="btn btn-primary btn-block btn-signin" type="submit"><i class="fa fa-user-circle-o" aria-hidden="true"></i> Sign Up</button>
           
             <?php echo form_close(); ?>
                             
                             <br/>
                                   
    </div><!-- /card-container -->
</div><!-- /container -->












</div>
</div>
<style type="text/css">
  #bt
  {
    padding-top: 10px;
  }
</style>




<br/>
<hr/>

  

<?php
 
   $this->load->view("module/footer_client");
   ?>


    <script>
      Holder.addTheme('thumb', {
        bg: '#55595c',
        fg: '#eceeef',
        text: 'Thumbnail'
      });
    </script>
 

    <script>
var $input = $(".typeahead");
$input.typeahead({
  source: [

  <?php 
     foreach ($Cities as $item)
            {

 echo   "{id: '".$item->cid."', name: '".$item->city."'},";
 


  } ?>
  ],
  autoSelect: true
});


$('#search').click(function() {
  var current = $input.typeahead("getActive");
  if (current) {
    // Some item from your model is active!
    if (current.name == $input.val()) {
      // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
      //alert("Exact match");

       
    } else {
      // This means it is only a partial match, you can either add a new item
      // or take the active if you don't want new items
       // alert("Partial match");
    }
  } else {
    // Nothing is active so it is a new value (or maybe empty value)
      //alert("No match");
  }
});
</script>
  </body>
</html>


