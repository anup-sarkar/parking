<?php
   defined('BASEPATH') OR exit('No direct script access allowed');
       $this->load->view("module/header_client");
   
                       $fname=$client[0]['fname'];
                       $lname=$client[0]['lname'];
                       $email=$client[0]['email'];
                     
                       $mobile=$client[0]['mobile'];
   ?>
<?php  if (isset($_SESSION['client_name'])) { ?>
<br/>
<br/>
<div class="container">
  <h1 style="text-align: center;" >ACCOUNT INFO</h1>
              <?php echo $this->session->flashdata('msg'); ?>  
                          <?php echo $this->session->flashdata('msg3'); ?>  
   <ul class="nav nav-tabs" id="myTab" role="tablist">
      <li class="nav-item">
         <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Profile</a>
      </li>
      <li class="nav-item">
         <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Vehicles</a>
      </li>
      <li class="nav-item">
         <a class="nav-link" id="contact-tab1" data-toggle="tab" href="#contact1" role="tab" aria-controls="contact1" aria-selected="false">Bookings</a>
      </li>
   </ul>
   <div class="tab-content" id="myTabContent">
      <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
         <div style="text-align: center;">
            <img id="profile-img" class="profile-img-card" src="<?php echo base_url(); ?>assets/img/user.png" />
            <h3><?php echo $fname.' '.$lname; ?></h3>
            <hr/>
          <?php $attributes = array("class" => "", "id" => "updateForm", "name" => "updateForm");
               echo form_open("home/update_user", $attributes);?>
            <div class="row">
               <div class="col-md-6">
                  <p style="text-align: left;font-weight: bolder;">First Name</p>
                  <input type="text" placeholder="First Name" id="fname" name="fname" class="form-control" value="<?php echo $fname; ?>">
                  <span class="text-danger"><?php echo form_error('fname'); ?></span>
               </div>
               <div class="col-md-6">
                  <p style="text-align: left;font-weight: bolder;">Last Name</p>
                  <input type="text" placeholder="Last Name" id="lname" name="lname" class="form-control" value="<?php echo $lname; ?>">
                  <span class="text-danger"><?php echo form_error('lname'); ?></span>     
               </div>
            </div>
            <br/>
            <div class="row">
               <div class="col-md-12">
                  <p style="text-align: left;font-weight: bolder;">Email</p>
                  <input type="email" placeholder="Enter email" id="email" name="email"  class="form-control" value="<?php echo $email; ?>">
                  <span class="text-danger"><?php echo form_error('email'); ?></span>
               </div>
            </div>
            <br/>
            <div class="row">
               <div class="col-md-6">
                  <p style="text-align: left;font-weight: bolder;">Mobile</p>
                  <input type="number" placeholder="Mobile No." id="mobile" name="mobile" class="form-control" value="<?php echo $mobile; ?>">
                  <span class="text-danger"><?php echo form_error('mobile'); ?></span>
               </div>
               <div class="col-md-6">
                  <p style="text-align: left;font-weight: bolder;">Password</p>
                  <input type="password" placeholder="Password"  id="pass" name="pass"class="form-control"  >
                  <span class="text-danger"><?php echo form_error('pass'); ?></span>     
               </div>
            </div>
            <br/>
            <br/>
            <div >
               <button type="submit" class="btn btn-primary" style="width: 100%;padding: 0;">Save</button>
            </div>
            <?php echo form_close(); ?>
            <br/>
            <?php echo $this->session->flashdata('msg'); ?>    
         </div>
      </div>
      <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
         <br/>
         <img id="profile-img" class="profile-img-card" src="<?php echo base_url(); ?>assets/img/vehicle.png" />
         <hr/>
         <div class="panel-group" id="accordion">
            <div class="panel panel-default">
               <div class="panel-heading">
                  <h4 class="panel-title" id="wells">
                     <a data-toggle="collapse" data-parent="#accordion" class="well well-sm" href="#collapse1" aria-expanded="true">Your Vehicles <i class="fa fa-chevron-down" aria-hidden="true"></i></a>
                  </h4>
               </div>
               <div id="collapse1" class="panel-collapse collapse in">
                  <div class="panel-body"> 

          <h3>Your Vehicles</h3>
          <table class="table table-striped table-hover ">
  <thead>
    <tr>
      <th>SL</th>
      <th>Type</th>
      <th>Make</th>
      <th>Model</th>

      <th>Year</th>
      <th>Color</th>
      <th>Licence State</th>
      <th>Licence Plate</th>
      <th>Name</th>
    </tr>
  </thead>
  <tbody>
    <?php 
    $i=1;
    foreach ($vehicles as $item) {
      # code...
    ?>

    <tr>
      <td><?php echo $i; ?></td>
      <td><?php echo $item->type;?></td>
      <td><?php echo $item->make;?></td>
      <td><?php echo $item->model;?></td>
      <td><?php echo $item->year;?></td>
      <td><?php echo $item->color;?></td>
      <td><?php echo $item->licence_state;?></td>
      <td><?php echo $item->licence_plate;?></td>
      <td><?php echo $item->name;?></td>
      
    </tr>
     <?php $i++;} ?>
  </tbody>
</table> 



                  </div>
               </div>
            </div>
            <div class="panel panel-default">
               <div class="panel-heading">
                  <h4 class="panel-title" id="wells">
                     <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">Add New Vehicles
                     <i class="fa fa-chevron-down" aria-hidden="true"></i></a>
                  </h4>
               </div>
               <div id="collapse2" class="panel-collapse collapse">
                  <div class="panel-body">
                     <div style="text-align: center ">
                        <br/>

    <?php $attributes = array("class" => "", "id" => "updateForm", "name" => "updateForm");
               echo form_open("home/add_vehicle", $attributes);?>
                        <div class="row">
                           <div class="col-md-6">
                              <p style="text-align: left;font-weight: bolder;">Type</p>
                   
                  <input type="text" placeholder="Ex. Car/Truck" id="type" name="type" class="form-control" value="<?php echo set_value('type'); ?>">
                  <span class="text-danger"><?php echo form_error('type'); ?></span>

                           </div>
                           <div class="col-md-6">
                              <p style="text-align: left;font-weight: bolder;">Make</p>
                  <input type="text" placeholder="Ex. Audi/BMW" id="make" name="make" class="form-control" value="<?php echo set_value('make'); ?>">
                  <span class="text-danger"><?php echo form_error('make'); ?></span>           
                           </div>
                        </div>
                        <br/>
                        <div class="row">
                           <div class="col-md-6">
                              <p style="text-align: left;font-weight: bolder;">Model</p>
                    <input type="text" placeholder="Vehicle Model" id="model" name="model" class="form-control" value="<?php echo set_value('model'); ?>">
                  <span class="text-danger"><?php echo form_error('model'); ?></span>  
                           </div>
                           <div class="col-md-6">
                              <p style="text-align: left;font-weight: bolder;">Year</p>
                             <select    id="year" name="year" class="form-control"   >
                               
                               <?php for($i=2018;$i>1980;$i--)
                               {
                                echo "<option value='".$i."'>".$i." </option>";
                               }

                               ?>
                             </select>
                  <span class="text-danger"><?php echo form_error('year'); ?></span>  
                           </div>
                        </div>
                        <br/>
                        <div class="row">
                           <div class="col-md-6">
                              <p style="text-align: left;font-weight: bolder;">Color</p>
                               <input type="text" placeholder="Vehicle Color" id="color" name="color" class="form-control" value="<?php echo set_value('color'); ?>">
                  <span class="text-danger"><?php echo form_error('color'); ?></span>  
                           </div>
                           <div class="col-md-6">
                              <p style="text-align: left;font-weight: bolder;">Licence State</p>
                               <input type="text" placeholder="State" id="state" name="state" class="form-control" value="<?php echo set_value('state'); ?>">
                  <span class="text-danger"><?php echo form_error('state'); ?></span>          
                           </div>
                        </div>
                        <br/>
                        <div class="row">
                           <div class="col-md-6">
                              <p style="text-align: left;font-weight: bolder;">Licence Plate Number</p>
                             <input type="text" placeholder="Plate" id="plate" name="plate" class="form-control" value="<?php echo set_value('plate'); ?>">
                  <span class="text-danger"><?php echo form_error('plate'); ?></span>    
                           </div>
                           <div class="col-md-6">
                              <p style="text-align: left;font-weight: bolder;">Vehicle Name</p>
                               <input type="text" placeholder="Vehicle Name" id="name" name="name" class="form-control" value="<?php echo set_value('name'); ?>">
                  <span class="text-danger"><?php echo form_error('name'); ?></span>           
                           </div>
                        </div>
                        <br/>
                        <br/>
                        <div >
                           <button type="submit" class="btn btn-primary" style="width: 100%;padding: 0;">Add</button>
                        </div>

                            <?php echo form_close(); ?>
            <br/>
            <?php echo $this->session->flashdata('msg3'); ?>  
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="tab-pane fade" id="contact1" role="tabpanel" aria-labelledby="contact-tab1">
         <h3>No Bookings Yet ! </h3>
      </div>
   </div>
</div>
<?php } ?>
<br/>
<hr/>
<?php
   $this->load->view("module/footer_client");
   ?>