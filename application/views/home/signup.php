<?php
   defined('BASEPATH') OR exit('No direct script access allowed');
   $this->load->view("module/header_client");
   ?>


    
<div class="container">
    <div class="card card-container">
        <!-- <img class="profile-img-card" src="//lh3.googleusercontent.com/-6V8xOA6M7BA/AAAAAAAAAAI/AAAAAAAAAAA/rzlHcD0KYwo/photo.jpg?sz=120" alt="" /> -->
        <img id="profile-img" class="profile-img-card" src="<?php echo base_url(); ?>assets/img/signup.png" />
        <h3 id="profile-name" class="profile-name-card">Sign Up Now</h3>
        <br/>

  <?php $attributes = array("class" => "", "id" => "registerForm", "name" => "registerForm");
          echo form_open("home/register", $attributes);?>

            <div class="form-group">
                                                    <input type="text" placeholder="First Name" id="fname" name="fname" class="form-control" value="<?php echo set_value('fname'); ?>">

                                                    <span class="text-danger"><?php echo form_error('fname'); ?></span>
                                                </div>


                                                <div class="form-group">
                                                    <input type="text" placeholder="Last Name" id="lname" name="lname" class="form-control" value="<?php echo set_value('lname'); ?>">

                                                      <span class="text-danger"><?php echo form_error('lname'); ?></span>
                                                </div>


                                                <div class="form-group">
                                                    <input type="number" placeholder="Mobile No." id="mobile" name="mobile" class="form-control" value="<?php echo set_value('mobile'); ?>">

                                                      <span class="text-danger"><?php echo form_error('mobile'); ?></span>
                                                </div>

                                                <div class="form-group">
                                                    <input type="email" placeholder="Enter email" id="email" name="email"  class="form-control" value="<?php echo set_value('email'); ?>">

                                                      <span class="text-danger"><?php echo form_error('email'); ?></span>
                                                </div>


                                                <div class="form-group">
                                                    <input type="password" placeholder="Password"  id="pass" name="pass"class="form-control" value="<?php echo set_value('pass'); ?>">

                                                      <span class="text-danger"><?php echo form_error('pass'); ?></span>
                                                </div>
                                            


            <button class="btn btn-primary btn-block btn-signin" type="submit"><i class="fa fa-user-circle-o" aria-hidden="true"></i> Sign Up</button>
           
             <?php echo form_close(); ?>
                             
                             <br/>
                             <?php echo $this->session->flashdata('msg'); ?>               
    </div><!-- /card-container -->
</div><!-- /container -->



















<br/>
<hr/>

  <style type="text/css">
  #parent
  {
      background-color: #33b5e5;
  }
</style>

<?php
 
   $this->load->view("module/footer_client");
   ?>


    <script>
      Holder.addTheme('thumb', {
        bg: '#55595c',
        fg: '#eceeef',
        text: 'Thumbnail'
      });
    </script>
 

    <script>
var $input = $(".typeahead");
$input.typeahead({
  source: [

  <?php 
     foreach ($Cities as $item)
            {

 echo   "{id: '".$item->cid."', name: '".$item->city."'},";
 


  } ?>
  ],
  autoSelect: true
});


$('#search').click(function() {
  var current = $input.typeahead("getActive");
  if (current) {
    // Some item from your model is active!
    if (current.name == $input.val()) {
      // This means the exact match is found. Use toLowerCase() if you want case insensitive match.
      //alert("Exact match");

       
    } else {
      // This means it is only a partial match, you can either add a new item
      // or take the active if you don't want new items
       // alert("Partial match");
    }
  } else {
    // Nothing is active so it is a new value (or maybe empty value)
      //alert("No match");
  }
});
</script>
  </body>
</html>


