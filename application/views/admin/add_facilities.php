
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$data['isactive']="facilities";
$this->load->view("module/header",$data);
?>



        <div class="content">
            <div class="container-fluid">
                <div class="row">
                  
                  <div class="col-md-12 mr-auto">
                                   
                                         <?php $attributes = array("class" => "", "id" => "departmentform", "name" => "departmentform");
                                                      echo form_open_multipart("Admin/create_facilities", $attributes);?>
                                        <div class="card card-plain" style=" border:  1px solid;padding-bottom: 40px;margin-left: 200px;margin-right: 200px;background: #f5f5f599; border-radius: 10px;">
                                            <div class="content">
                                                 <h2 class="card-title">Add Facilities</h2>
                                         
                                        <hr />
                                                <?php echo $this->session->flashdata('msg'); ?>
                                            <div class="form-group">
                                                    <input type="text" placeholder="facilities Name" id="txt_fct_name" name="txt_fct_name" class="form-control" value="<?php echo set_value('txt_fct_name'); ?>">

                                                      <span class="text-danger"><?php echo form_error('txt_fct_name'); ?></span>
                                                </div>

                                                 <div class="form-group">
                                                    <input type="text" placeholder="facilities Details" id="txt_fct_details" name="txt_fct_details" class="form-control" value="<?php echo set_value('txt_fct_details'); ?>">

                                                      <span class="text-danger"><?php echo form_error('txt_fct_details'); ?></span>
                                                </div>
                                                    <div class="form-group">
                                                      <label>Upload Icon</label>
                                                    <input type="file" id="image" name="image"  class="form-control"  size="20">

                                                      <span class="text-danger"><?php echo form_error('image'); ?></span>
                                                </div>
                                              

                                               <?php echo $this->session->flashdata('err'); ?>
                                               
                                            </div>
                                            <div class="footer text-center">
                                                <input type="submit" class="btn btn-fill btn-neutral btn-wd" id="btn_coun" name="btn_coun" value="Add Facility" />

                                             
                                            </div>

                                        </div>

                                      <?php echo form_close(); ?>
                                  
                                </div>
                            </div>

                </div>
            </div>
        </div>



  <?php echo $this->session->flashdata('notify'); ?>


<?php

$this->load->view("module/footer");
?>
