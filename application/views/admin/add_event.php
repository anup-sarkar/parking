
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$data['isactive']="country";
$this->load->view("module/header",$data);
?>
 














        <div class="content">
            <div class="container-fluid">
                <div class="row">
                  
                  <div class="col-md-12 mr-auto">
                                   
                  <?php $attributes = array("class" => "", "id" => "form", "name" => "form");
                      echo form_open("Admin/create_event", $attributes);?>
                                        <div class="card card-plain" style=" border:  1px solid;padding-bottom: 40px;margin-left: 200px;margin-right: 200px;background: #f5f5f599; border-radius: 10px;">
                                            <div class="content">
                                                 <h2 class="card-title">Add EVENT</h2>
                                     
                                        <hr />
                                            
                    <div class="form-control" style="z-index:100;height: 45px">
                        <div class="col-md-8" style="z-index:100"><label style="
                           color:  black;
                           font-size: 20px;
                           ">Status </label></div>
                        <div class="col-md-4" style="z-index:100">
                           <label class="switch" style="float: right;">
                           <input type="checkbox"  checked  id="txt_event_status" name="txt_event_status">
                           <span class="slider round"></span>
                           </label>
                        </div>
                     </div>
                       <div class="form-group">
                      <br/>

                        <label style="font-size: 20px">Event Name</label>
                        <input type="text"    id="txt_event_name" name="txt_event_name" class="form-control" value="<?php echo set_value('txt_event_name'); ?>" >
                        <span class="text-danger"><?php echo form_error('txt_event_name'); ?></span>
                     </div>

                       <div class="form-group">
                      <br/>

                        <label style="font-size: 20px">Event Details</label>
                        <input type="text"    id="txt_event_details" name="txt_event_details" class="form-control" value="<?php echo set_value('txt_event_details'); ?>" >
                        <span class="text-danger"><?php echo form_error('txt_event_details'); ?></span>
                     </div>
                     <div class="form-group">
                      <br/>

                        <label style="font-size: 20px">Start Date</label>
                        <input type="date"    id="txt_event_start" name="txt_event_start" class="form-control" value="<?php echo set_value('txt_event_start'); ?>" >
                        <span class="text-danger"><?php echo form_error('txt_event_start'); ?></span>
                     </div>

                     <div class="form-group">
                        <label style="font-size: 20px">End Date</label>
                        <input type="date"     id="txt_event_end" name="txt_event_end" class="form-control" value="<?php echo set_value('txt_event_end'); ?>" >
                        <span class="text-danger"><?php echo form_error('txt_event_end'); ?></span>
                     </div>
                  
                    
                  </div>
                  <div class="footer text-center">
                  
                     <input type="submit" class="btn btn-fill btn-neutral btn-wd" id="btn_event" name="btn_event" value="Add EVENT" style="
                        color: orange;
                        width: 95%;
                        background: #eee;
                        border-color: black;
                        ">
                  </div>
                                        </div>
                                      <?php echo form_close(); ?>
                                    <?php echo $this->session->flashdata('msg'); ?>
                                </div>
                            </div>

                </div>
            </div>
        </div>



  <?php echo $this->session->flashdata('notify'); ?>

  <script type="text/javascript">
   $(document).ready(function(){

    


 



  $('#txt_event_isalwaysopen').click(function()
  {
      if ($(this).is(':checked')){
         $("#txt_event_open").prop("disabled", true);
   $("#txt_event_close").prop("disabled", true);
      }
      else{
         $("#txt_event_open").prop("disabled", false);
   $("#txt_event_close").prop("disabled", false);
      }
    });

 



     
   });

  </script> 


 <style>
      .switch {
      position: relative;
      display: inline-block;
      width: 60px;
      height: 34px;
      }
      .switch input {display:none;}
      .slider {
      position: absolute;
      cursor: pointer;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      background-color: #ccc;
      -webkit-transition: .4s;
      transition: .4s;
      }
      .slider:before {
      position: absolute;
      content: "";
      height: 26px;
      width: 26px;
      left: 4px;
      bottom: 4px;
      background-color: white;
      -webkit-transition: .4s;
      transition: .4s;
      }
      input:checked + .slider {
      background-color: #2196F3;
      }
      input:focus + .slider {
      box-shadow: 0 0 1px #2196F3;
      }
      input:checked + .slider:before {
      -webkit-transform: translateX(26px);
      -ms-transform: translateX(26px);
      transform: translateX(26px);
      }
      /* Rounded sliders */
      .slider.round {
      border-radius: 34px;
      }
      .slider.round:before {
      border-radius: 50%;
      }
   </style>
<?php

$this->load->view("module/footer");
?>
