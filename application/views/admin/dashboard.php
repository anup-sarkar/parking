
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$data['isactive']=$current;
$data['email']=$_SESSION['admin_email'];
$this->load->view("module/header",$data);

?>
 
 
<link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>

 

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-3 col-sm-6">
                        <div class="card">
                            <div class="content">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="icon-big icon-warning text-center">
                                            <i class="ti-check"></i>
                                        </div>
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="numbers">
                                            <p>Reservations</p>
                                                  <?php echo $this->Reservations_model->get_count_admin(); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <hr />
                                    <div class="stats">
                                        <i class="ti-reload"></i> LifeTime
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card">
                            <div class="content">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="icon-big icon-success text-center">
                                            <i class="ti-money"></i>
                                        </div>
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="numbers">
                                            <p>Total Remit</p>
                                                 $<?php echo $this->Reservations_model->get_remit_sum_admin(); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <hr />
                                    <div class="stats">LifeTime
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card">
                            <div class="content">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="icon-big icon-danger text-center">
                                            <i class="ti-credit-card"></i>
                                        </div>
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="numbers">
                                            <p>Gross Sale</p>
                                             $<?php echo $this->Reservations_model->get_gross_sum_admin(); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <hr />
                                    <div class="stats">
                                        <i class="ti-timer"></i> LifeTime
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="card">
                            <div class="content">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="icon-big icon-info text-center">
                                            <i class="ti-money"></i>
                                        </div>
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="numbers">
                                            <p>Revenue</p>
                                              $<?php echo $this->Reservations_model->get_profit_sum_admin(); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <hr />
                                    <div class="stats">
                                        <i class="ti-reload"></i> LifeTime
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                               
                                <div class="row">
                                    <div class="col-md-10">
                                         <h3 class="title">Reservation Statistics</h3>
                                         <p class="category">Group By Account</p>
                                    </div>
                                    <div class="col-md-2">
                                        <a onclick="tableToExcel('tbl', 'Reservations Summary')"  class="btn btn-primary" >     
                                             <i class="fa fa-download" aria-hidden="true"></i> Export 
                                         </a>
                                    </div>
                                </div>
                            </div>
                            <div class="content">
                                <div   style="height: auto;">
                                    
                                      <table class="table table-bordered table-hover" id="tbl" style="font-size: 12px">
                                    <thead>
                                        <tr><th>SL</th>
                                          <th>Spot Name </th>
                                      <th style="max-width:230px;">Spot Address</th>
                                       <th>Spot ID</th>
                                     
                                      <th><i class="fa fa-circle text-info"></i>Gross Sale</th>
                                      <th>  <i class="fa fa-circle text-danger"></i>Remit</th>
                                      <th><i class="fa fa-circle text-warning"> </i>Revenue</th>
                                       
                                        <th>Total Trans.</th>
                                   


                                       
                                
                                    </tr></thead>
                                    <tbody>

<?php

$i=1;
            foreach ($bookings as $item)
            {
                  $url=base_url();
                  




                                       echo "<tr>";
                                       echo "<td>".$i."</td>"; 
                                      echo  "<td>".$item->public_name."</td>";
                                      echo  "<td>".$item->address."</td>";
                                       echo  "<td>".$item->pid."</td>";

                                      echo  "<td>$".  number_format($item->gross_amount, 2, '.', '') ."</td>";
                                         echo  "<td>$".  number_format($item->remit_amount, 2, '.', '')."</td>";
                                              echo  "<td>$".$item->profit_amount."</td>";
                                                       echo  "<td>".$item->total."</td>";
                               
                                    
                                       echo    "</tr>";
                                       $i++; 

            }
                                        ?>

                                       
                                    </tbody>
                                </table>




                                </div>

                          
                            </div>
                        </div>
                    </div>
           
                </div>



            </div>
        </div>




<style>
    .fc-title {
        font-size: 10px;
    }

 

</style>

<script type="text/javascript">
  var tableToExcel = (function() {
  var uri = 'data:application/vnd.ms-excel;base64,'
    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
    , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
    , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
  return function(table, name) {
    if (!table.nodeType) table = document.getElementById(table)
    var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
    window.location.href = uri + base64(format(template, ctx))
  }
})()


</script>

<?php

$this->load->view("module/footer_home");
?>
