
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$data['isactive']='options';
$this->load->view("module/header",$data);
?>



      <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">All Options  <a class="btn btn-primary" href="<?php echo base_url().'index.php/admin/add_options' ?>" style="float: right"> Add New </a> </h4>
                               
                              
                            </div>
                            <div class="content table-responsive table-full-width">





                                <table class="table table-hover">
                                    <thead>
                                        <tr><th>SL</th>
                                      <th>Icon</th>
                                      <th>Name</th>
                                      <th>Details</th>
                                      <th></th>
                                    </tr></thead>
                                    <tbody>

<?php

$i=1;
            foreach ($options as $item)
            {
                  $url=base_url();
                  $id=$item->id; 
                  $name=$item->option_name; 
                  $details=$item->option_details;
                  $icon=$item->icon_name;
                  $icon=base_url().$icon;



                                       echo "<tr>";
                                       echo "<td>".$i."</td>";   
                                        echo "<td><img src='".$icon."' style='width:50px' /></td>";
                                       echo "<td>".$name."</td>";   
                                       echo  "<td>".$details."</td>";
                                         echo  "<td>
                                      <a class='btn btn-danger' href='".$url."index.php/admin/delete_option/".$id."'><i class='fa fa-times' aria-hidden='true'></i>  </a>
                                      
                                      </td>";
                                       echo    "</tr>";
                                       $i++; 

            }
                                        ?>

                                        <tr> <?php echo $this->session->flashdata('msg'); ?>  </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>


                 


                </div>
            </div>
        </div>



 


<?php

$this->load->view("module/footer");
?>
