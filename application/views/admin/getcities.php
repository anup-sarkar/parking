
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$data['isactive']=$current;
$this->load->view("module/header",$data);
?>



      <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">All Cities  <a class="btn btn-primary" href="<?php echo base_url().'index.php/admin/addCities' ?>" style="float: right"> Add New City </a> </h4>
                               
                              
                            </div>
                            <div class="content table-responsive table-full-width">





                                <table class="table table-hover">
                                    <thead>
                                        <tr><th>SL</th>
                                      <th>Country </th>
                                      <th>City</th>
                                      <th>State</th>
                                        
                                       <th>TimeZone</th>
                                       
                                      <th></th>
                                    </tr></thead>
                                    <tbody>

<?php

$i=1;
            foreach ($Cities as $item)
            {
                  $url=base_url();
                  $id=$item->id; 
                  $name=$item->city; 
                   $state=$item->state; 
                   $code=$item->postal_code;
                    $country=$item->country;



                                       echo "<tr>";
                                       echo "<td>".$i."</td>"; 
                                        echo "<td>".$country."</td>";   
                                       echo "<td>".$name."</td>";   
                                        echo "<td>".$state."</td>";
                             
                                       echo  "<td>".$item->timezone."</td>";
                                        
                                         echo  "<td>
                                      <a class='btn btn-danger' href='".$url."index.php/admin/deletecity/".$id."'><i class='fa fa-times' aria-hidden='true'></i>  </a>
                                      
                                      </td>";
                                       echo    "</tr>";
                                       $i++; 

            }
                                        ?>

                                        <tr> <?php echo $this->session->flashdata('msg'); ?>  </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>


                 


                </div>
            </div>
        </div>



 


<?php

$this->load->view("module/footer");
?>
