
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$data['isactive']="country";
$this->load->view("module/header",$data);
?>



        <div class="content">
            <div class="container-fluid">
                <div class="row">
                  
                  <div class="col-md-12 mr-auto">
                                   
                                         <?php $attributes = array("class" => "", "id" => "departmentform", "name" => "departmentform");
                                                      echo form_open("Admin/create_country", $attributes);?>
                                        <div class="card card-plain" style=" border:  1px solid;padding-bottom: 40px;margin-left: 200px;margin-right: 200px;background: #f5f5f599; border-radius: 10px;">
                                            <div class="content">
                                                 <h2 class="card-title">Add Country</h2>
                                        <h4 class="card-subtitle">Provide Country Name & Code</h4>
                                        <hr />
                                            
                                            <div class="form-group">
                                                    <input type="text" placeholder="Country Name" id="txt_coun_name" name="txt_coun_name" class="form-control" value="<?php echo set_value('txt_coun_name'); ?>">

                                                      <span class="text-danger"><?php echo form_error('txt_coun_name'); ?></span>
                                                </div>

                                                <div class="form-group">
                                                    <input type="number" placeholder="Country Code" id="txt_coun_code" name="txt_coun_code"  class="form-control" value="<?php echo set_value('txt_coun_code'); ?>">

                                                      <span class="text-danger"><?php echo form_error('txt_coun_code'); ?></span>
                                                </div>


                                               
                                               
                                            </div>
                                            <div class="footer text-center">
                                                <input type="submit" class="btn btn-fill btn-neutral btn-wd" id="btn_coun" name="btn_coun" value="Add Country" />

                                             
                                            </div>
                                        </div>
                                      <?php echo form_close(); ?>
                                    <?php echo $this->session->flashdata('msg'); ?>
                                </div>
                            </div>

                </div>
            </div>
        </div>



  <?php echo $this->session->flashdata('notify'); ?>


<?php

$this->load->view("module/footer");
?>
