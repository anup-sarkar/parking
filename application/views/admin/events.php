
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$data['isactive']=$current;
$this->load->view("module/header",$data);
?>



      <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">All Events   <a class="btn btn-primary" href="<?php echo base_url().'index.php/admin/add_events' ?>" style="float: right"> Add New Event </a> </h4>
                               
                              
                            </div>
                            <div class="content table-responsive table-full-width">





                                <table class="table table-hover">
                                    <thead>
                                        <tr><th>SL</th>
                                      <th>Events Name</th>
                                      <th>Events Details</th>
                                      <th>Start Date</th>
                                      <th>End Date</th>
                                      <th>Status</th>
             
                                    </tr></thead>
                                    <tbody>

<?php

$i=1;
            foreach ($events as $item)
            {
                  $url=base_url();
                  $id=$item->id; 
                  $name=$item->event_name; 
                  $details=$item->event_details;
                   $sdate=date_create($item->start_date);
                     $edate=date_create($item->end_date);


                                    
                                     
                                          echo "<tr>";
                                        
                                       echo "<td>".$i."</td>";   
                                       echo "<td>".$name."</td>";   
                                       echo  "<td>".$details."</td>";
                                         echo "<td>".date_format($sdate,"d M Y ")."</td>";   
                                       echo  "<td>".date_format($edate,"d M Y ")."</td>";

                                        if($item->status==1)
                                        {
                                          echo "<td><label class='switch'  >
                                        <input type='checkbox' checked   id='txt_event_status".$i."' name='txt_event_status".$i."' value='".$id."'>
                                        <span class='slider round'></span>
                                        </label></td>";
                                        }
                                        else
                                        {
                                           echo "<td><label class='switch'  >
                                        <input type='checkbox'   id='txt_event_status".$i."' name='txt_event_status".$i."' value='".$id."'>
                                        <span class='slider round'></span>
                                        </label></td>";
                                        }
                                      

                                      echo "<td style='display:none'> <input type='text' id='txt_event_id".$i."'  name='txt_event_id".$i."' value='".$id."' > </td>";

                                        
                                
                                       
                                       $i++; 

            }
                                        ?>

                                        <tr> <?php echo $this->session->flashdata('msg'); ?>  </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>


                 


                </div>
            </div>
        </div>
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script type="text/javascript">
   $(document).ready(function(){
 
  <?php 
   $j=1;
   foreach ($events as $item) { ?>

  $('#txt_event_status<?php echo $j; ?>').click(function()
  {
      if ($(this).is(':checked')){


  
     var request = $.ajax({
  url: "<?php echo base_url().'index.php/admin/update_events';?>",
  type: "POST",
  data: {id : $("#txt_event_id<?php echo $j; ?>").val(),status : 1},
  dataType: "html"
});
      }
      else
      {
        var request = $.ajax({
  url: "<?php echo base_url().'index.php/admin/update_events';?>",
  type: "POST",
  data: {id : $("#txt_event_id<?php echo $j; ?>").val(),status : 0},
  dataType: "html"
});
      }




request.done(function(msg) {
  alert(msg);
});

request.fail(function(jqXHR, textStatus) {
  alert( "Request failed: " + textStatus );
});


      
    });

<?php $j++; }?>
     
   });

  </script> 

<style>
      .switch {
      position: relative;
      display: inline-block;
      width: 60px;
      height: 34px;
      }
      .switch input {display:none;}
      .slider {
      position: absolute;
      cursor: pointer;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      background-color: #ccc;
      -webkit-transition: .4s;
      transition: .4s;
      }
      .slider:before {
      position: absolute;
      content: "";
      height: 26px;
      width: 26px;
      left: 4px;
      bottom: 4px;
      background-color: white;
      -webkit-transition: .4s;
      transition: .4s;
      }
      input:checked + .slider {
      background-color: #2196F3;
      }
      input:focus + .slider {
      box-shadow: 0 0 1px #2196F3;
      }
      input:checked + .slider:before {
      -webkit-transform: translateX(26px);
      -ms-transform: translateX(26px);
      transform: translateX(26px);
      }
      /* Rounded sliders */
      .slider.round {
      border-radius: 34px;
      }
      .slider.round:before {
      border-radius: 50%;
      }
   </style>
 


<?php

$this->load->view("module/footer");
?>
