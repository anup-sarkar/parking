<?php
   defined('BASEPATH') OR exit('No direct script access allowed');
$data['isactive']=$current;
$this->load->view("module/header",$data);
   ?>
<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-12 mr-auto">
            <?php $attributes = array("class" => "", "id" => "providerform", "name" => "providerform");
               echo form_open("providers/create", $attributes);?>
            <div class="card card-plain" style="
               border:  1px solid;
               padding-bottom: 40px;
               margin-left: 200px;
               margin-right: 200px;
               background: #f5f5f599;
               border-radius: 10px;
               ">
               <div class="content">
                  <h2 class="card-title">Add Provider</h2>
                  <h4 class="card-subtitle">Provide provider Info</h4>
                  <hr />
                   <?php echo $this->session->flashdata('msg'); ?>
                  
                  <div class="form-group">
                     <input type="text" placeholder="Account Name" id="txt_provider_name" name="txt_provider_name" class="form-control" value="<?php echo set_value('txt_provider_name'); ?>">
                     <span class="text-danger"><?php echo form_error('txt_provider_name'); ?></span>
                  </div>

                   <div class="form-group">
                     <input type="text" placeholder="Designation" id="txt_provider_desig" name="txt_provider_desig" class="form-control" value="<?php echo set_value('txt_provider_desig'); ?>">
                     <span class="text-danger"><?php echo form_error('txt_provider_desig'); ?></span>
                  </div>

                   <div class="form-group">
                     <input type="text" placeholder=" Address" id="txt_provider_Address" name="txt_provider_Address" class="form-control" value="<?php echo set_value('txt_provider_Address'); ?>">
                     <span class="text-danger"><?php echo form_error('txt_provider_Address'); ?></span>
                  </div>
                 
                  <div class="form-group">
                     <input type="number" placeholder="Mobile No." id="txt_provider_mobile" name="txt_provider_mobile" class="form-control" value="<?php echo set_value('txt_provider_mobile'); ?>">

                         <span class="text-danger"><?php echo form_error('txt_provider_mobile'); ?></span>
                  </div>

                  <div class="form-group">
                     <input type="email" placeholder="Enter email" id="txt_provider_email" name="txt_provider_email"  class="form-control" value="<?php echo set_value('txt_provider_email'); ?>">

                        <span class="text-danger"><?php echo form_error('txt_provider_email'); ?></span>
                  </div>


                <div class="form-group">
                    <input type="password"    id="txt_provider_pass" name="txt_provider_pass" class="form-control" value="<?php echo set_value('txt_provider_pass'); ?>">

                      <span class="text-danger"><?php echo form_error('txt_provider_pass'); ?></span>
                </div>

                  <div class="form-group">
                   <label style="text-align: center;"><span>Account Type</span></label>
                     <select  id="txt_provider_type" name="txt_provider_type" class="form-control" >
                    
                   
                        <option value="Provider" > Provider </option>
                        
                     </select>
                  </div>
               </div>
               <div class="footer text-center">
                  <input type="submit" class="btn btn-fill btn-neutral btn-wd" id="btn_provider" name="btn_provider" value="Add Acount" />
               </div>
            </div>
            <?php echo form_close(); ?>
           
         </div>
      </div>
   </div>
</div>
</div>
<?php echo $this->session->flashdata('notify'); ?>
<?php
   $this->load->view("module/footer");
   ?>