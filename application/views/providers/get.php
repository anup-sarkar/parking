
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$data['isactive']=$current;
$this->load->view("module/header",$data);
?>



      <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h2 class="title">All Providers  <a class="btn btn-primary" href="<?php echo base_url().'index.php/Providers/add' ?>" style="float: right"> Add New Provider </a> </h2>
                               
                              
                            </div>
                            <div class="content table-responsive table-full-width">





                                <table class="table table-hover">
                                    <thead>
                                        <tr><th>SL</th>
                                      <th>Name</th>
                                      <th>Designation</th>
                                      <th>Address</th>
                                       <th>Mobile</th>
                                      <th>Email</th>
                                  
                                      <th>Type</th>
                                       <th>Status</th>
                                      <th></th>
                                      <th></th>
                                    </tr></thead>
                                    <tbody>

<?php

$i=1;
            foreach ($Providers as $item)
            {
                  $url=base_url();
                  $id=$item->id; 
                  $name=$item->name; 
                  $text="";
                  if($item->status==1)
                  {
                    $text='<p style="color:green" >Active </p>';
                  }
                  else
                  {
                    $text='<p style="color:red" >Inactive </p>';
                  }



                                       echo "<tr>";
                                       echo "<td>".$i."</td>"; 
                                      
                                       echo "<td>".$name."</td>";   
                                        echo "<td>".$item->designation."</td>";
                                         echo "<td>".$item->address."</td>";
                                          echo "<td>".$item->mobile."</td>";
                                           echo "<td>".$item->email."</td>";
                                            echo "<td>".$item->type."</td>";
                                             echo "<td>".$text."</td>";
                                  

                                  
                                

                                           echo  "<td>
                                         <a class='btn btn-success'href='".$url."index.php/Providers/details/".$id."' ><i class='fa fa-pencil-square-o' aria-hidden='true'></i> </a>
                                     
                                      
                                      </td>
                                      <td> <a class='btn btn-danger' href='".$url."index.php/Providers/delete/".$id."' ><i class='fa fa-times' aria-hidden='true'></i>   </a>  </td>
                                      ";
                                       echo    "</tr>";
                                       $i++; 

            }
                                        ?>

                                        <tr> <?php echo $this->session->flashdata('msg'); ?>  </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>


                 


                </div>
            </div>
        </div>



 


<?php

$this->load->view("module/footer");
?>
