
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$data['isactive']=$current;
$this->load->view("module/header",$data);
?>

<?php 



foreach ($parkingspot as $item)
            {
  # code...
                $status=$item->status2;
                $pro_id=$item->pro_id;


?>

   <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-5 col-md-5">
                        <div class="card card-user" style="height: 579px">
                            
                            <div class="content">
                                <div class="author">
                                  <img class="avatar " style="margin-top: 20px" src="<?php echo base_url(); ?>assets/img/p5.png" alt="..."/>
                                  <h4 class="title"> <?php echo $item->public_name; ?><br />
                                     <a href="#"><small><?php echo $item->short_name; ?></small></a>
                                  </h4>
                                </div>
                                <p class="description text-center">


                            <div class="text-center">
                                <div class="row" style="background-color: #b8bcb261">
                                    <div class="col-md-5 col-md-offset-1">
                                        <h5><?php echo $item->owner; ?><br /><small>Owner</small></h5>
                                    </div>
                                    <div class="col-md-6">
                                        <h5><?php echo $item->city; ?><br /><small>City</small></h5>
                                    </div>
                                   
                                </div>
                            </div>
                               <hr>
                                    <table style="margin:0 auto;text-align: right;" class="table table-border">
                                        <tr>
                                            <th style="width: 120px"> <b> Address: </b></th>
                                            <td ><?php echo $item->address; ?> </td>
                                        </tr>
                                         <tr>
                                            <th> <b> <b> Details </b>  </b></th>
                                            <td  "> <?php echo $item->description; ?> </td>
                                        </tr>

                                     
                                        <tr>
                                            <th> <b> <b> Owner Email </b>  </b></th>
                                            <td  > <?php echo $item->email; ?> </td>
                                        </tr>
                                        <tr>
                                            <th> <b> <b> Owner Mobile </b>  </b></th>
                                            <td  > <?php echo $item->mobile; ?> </td>
                                        </tr>

                                        <tr>
                                            <th> <b> <b> Status </b>  </b></th>
                                            <td  >  <?php if ($status==1) 
                                            echo "Active";
                                            else
                                            echo "Inactive"; {
                                                # code...
                                            }; ?></td>
                                        </tr>
                                    </table>
                                  
                                        
                                   
                                </p>
                            </div>
                            
                        </div>
                     
                    </div>

                    
                    <div class="col-lg-7 col-md-7">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Edit Parking Spot</h4>
                            </div>
                            <div class="content">
                                <?php $attributes = array("class" => "", "id" => "parkingSpotUpdateform", "name" => "parkingspotUpdateform");
                                   echo form_open("parkingspot/update", $attributes);?>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Public  Name</label>
                                                <input type="text" id="txt_parkingspot_pname" name="txt_parkingspot_pname" class="form-control border-input"    value="<?php echo $item->public_name; ?>">
                                              <span class="text-danger"><?php echo form_error('txt_parkingspot_pname'); ?></span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                             <div class="form-group">
                                                <label>Short Name</label>
                                                <input type="text"  id="txt_parkingspot_sname" name="txt_parkingspot_sname" class="form-control border-input"  value="<?php echo $item->short_name; ?>">

                                                  <span class="text-danger"><?php echo form_error('txt_parkingspot_sname'); ?></span>
                                            </div>
                                        </div>
                                       
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                           

                                            <div class="form-group">
                                                <label>Address</label>
                                                <input type="text" id="txt_parkingspot_addr" name="txt_parkingspot_addr" class="form-control border-input"  value="<?php echo $item->address; ?>">
                                                    <span class="text-danger"><?php echo form_error('txt_parkingspot_addr'); ?></span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Details</label>
                                                <input type="text"   id="txt_parkingspot_desc" name="txt_parkingspot_desc" class="form-control border-input"  value="<?php echo $item->description; ?>">

                                                  <span class="text-danger"><?php echo form_error('txt_parkingspot_desc'); ?></span>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Latitude</label>
                                                <input type="number" id="txt_parkingspot_lat" name="txt_parkingspot_lat"  class="form-control border-input"  value="<?php echo $item->lat; ?>">
                                                    <span class="text-danger"><?php echo form_error('txt_parkingspot_lat'); ?></span>
                                            </div>
                                        </div>
                                         <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Longitude</label>
                                                <input type="number" id="txt_parkingspot_long" name="txt_parkingspot_long"  class="form-control border-input"  value="<?php echo $item->long; ?>">
                                                    <span class="text-danger"><?php echo form_error('txt_parkingspot_long'); ?></span>
                                            </div>
                                        </div>
                                          
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                              <div class="form-group">
                                                     <label style="text-align: center;"><span>Cities</span></label>
                                                    <select  id="txt_parkingspot_city" style="border: 1px solid lightgrey" name="txt_parkingspot_city" class="form-control" >
                                                            <?php foreach($Cities as $city )
                                                           {
                                                            if($city->cid == $item->cid ) 
                                                                echo "<option value='".$city->cid."'  selected >".$city->city."</option>";
                                                            else
                                                                echo "<option value='".$city->cid."' >".$city->city."</option>";
                                                            

                                                             } ?>
                        
                                                    </select>
                                                </div>

                                        </div>
                                        <div class="col-md-6">
                                              
                                                  <div class="form-group">
                                                           <label style="text-align: center;"><span>Provider</span></label>
                                                           <select  id="txt_parkingspot_owner" name="txt_parkingspot_owner" class="form-control" style="border: 1px solid lightgrey">
                                                <?php foreach($Providers as $Provider )
                                                   { 
                                                        if($Provider->id == $pro_id )
                                                            echo "<option value='".$Provider->id."'  selected >".$Provider->id.'- '.$Provider->name.'( '.$Provider->designation." )</option>";
                                                        else
                                                            echo "<option value='".$Provider->id."'   >".$Provider->id.'- '.$Provider->name.'( '.$Provider->designation." )</option>";
                                                    } ?>
                                            
                                                     </select>
                                                 </div>
                                          </div>
                                    </div>


                                    <div class="row">
                                       
                                         <div class="col-md-12      ">
                                            <div class="form-group">
                                                <label>Status</label>

                                                <?php 
                                                $isactive="lightgrey";
                                                if($status!=1) 
                                                      $isactive="red"; ?>
                                                 <select  id="txt_parkingspot_status" name="txt_parkingspot_status" class="form-control"  style="border: 1px solid <?php echo $isactive; ?> ">
                    
                                              
                                               
                                                    

                                                  <?php if ($status==1) 
                                                  {
                                                    echo '<option value="1"  > Active </option>  <option value="0"  > Inactive </option>';
                                                  }else
                                                  {
                                                    echo "yes";
                                                    echo '<option value="0" selected > Inactive </option>  <option value="1"  > Active </option>';
                                                  }
                                            
                                           ?>
                        
                                          </select>
                                            </div>
                                        </div>

                                         <div style="display: none">
                                            <div class="form-group">
                                                <label>Id</label>
                                                <input type="number"  id="txt_id" name="txt_id" class="form-control border-input"  value="<?php echo $item->pid; ?>">
                                            </div>
                                        </div>

                                        
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-info btn-fill btn-wd">Update Parking Spot</button>
                                    </div>
                                    <div class="clearfix"></div>
                                <?php echo form_close(); ?>
                              
                            </div>
                        </div>
                        <?php echo $this->session->flashdata('msg'); ?>
                    </div>


                </div>
            </div>
        </div>


<?php }
                    ?>

  <?php echo $this->session->flashdata('notify'); ?>


<?php

$this->load->view("module/footer");
?>
