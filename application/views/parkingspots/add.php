<?php
   defined('BASEPATH') OR exit('No direct script access allowed');
  $data['isactive']=$current;
$this->load->view("module/header",$data);
   ?>
<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-12 mr-auto">
            <?php $attributes = array("class" => "", "id" => "parkingspotform", "name" => "parkingspotform");
               echo form_open("parkingspot/create", $attributes);?>
            <div class="card card-plain" style="
               border:  1px solid;
               padding-bottom: 40px;
               margin-left: 100px;
               margin-right: 100px;
               background: #f5f5f599;
               border-radius: 10px;
               ">
               <div class="content">
                  <h2 class="card-title">Add Location</h2>
                  <h4 class="card-subtitle">Provide Location Info</h4>
                  <hr />
                      <?php echo $this->session->flashdata('msg'); ?>
                  <div class="form-group">
                     <input type="text" placeholder="Public Name" id="txt_parkingspot_pname" name="txt_parkingspot_pname" class="form-control" value="<?php echo set_value('txt_parkingspot_pname'); ?>">
                     <span class="text-danger"><?php echo form_error('txt_parkingspot_pname'); ?></span>
                  </div>

                   <div class="form-group">
                     <input type="text" placeholder="Short Name" id="txt_parkingspot_sname" name="txt_parkingspot_sname" class="form-control" value="<?php echo set_value('txt_parkingspot_sname'); ?>">
                     <span class="text-danger"><?php echo form_error('txt_parkingspot_sname'); ?></span>
                  </div>

                  <div class="form-group">
                     <input type="text" placeholder="Details(If any)" id="txt_parkingspot_desc" name="txt_parkingspot_desc" class="form-control" value="<?php echo set_value('txt_parkingspot_desc'); ?>">
                     <span class="text-danger"><?php echo form_error('txt_parkingspot_desc'); ?></span>
                  </div>

                   <div class="form-group">
                     <input type="text" placeholder="Address" id="txt_parkingspot_addr" name="txt_parkingspot_addr" class="form-control" value="<?php echo set_value('txt_parkingspot_addr'); ?>">
                     <span class="text-danger"><?php echo form_error('txt_parkingspot_addr'); ?></span>
                  </div>

                  <div class="form-group">
               
                   <div class="pac-card" id="pac-card">
      <div>
        <div id="title">
          Set Your Location
        </div>
        <div id="country-selector" class="pac-controls" style="display: none">
          <input type="radio" name="type" id="changecountry-usa">
          <label for="changecountry-usa">USA</label>

          <input type="radio" name="type" id="changecountry-usa-and-uot" checked="checked">
          <label for="changecountry-usa-and-uot">USA and unincorporated organized territories</label>
        </div>
      </div>
      <div id="pac-container">
        <input id="pac-input" type="text"
            placeholder="Enter a location">
      </div>
    </div>
    <div id="map" style="height: 400px"></div>
    <div id="infowindow-content">
      <img src="" width="16" height="16" id="place-icon">
      <span id="place-name"  class="title"></span><br>
      <span id="place-address"></span>
    </div>

                  </div>
                  <div class="row">
                    <div class="col-md-6">
                  <div class="form-group">
                     <label style="text-align: center;"><span>Lat</span></label>
                     <input type="number" placeholder="Latitude" step="any"  id="txt_parkingspot_lat" name="txt_parkingspot_lat" class="form-control" value="<?php echo set_value('txt_parkingspot_lat'); ?>"  >
                     <span class="text-danger"><?php echo form_error('txt_parkingspot_lat'); ?></span>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                     <label style="text-align: center;"><span>Long</span></label>
                     <input type="number" step="any" placeholder="Longitude" id="txt_parkingspot_long" name="txt_parkingspot_long" class="form-control" value="<?php echo set_value('txt_parkingspot_long'); ?>"  >
                     <span class="text-danger"><?php echo form_error('txt_parkingspot_long'); ?></span>
                  </div>
                </div>
              </div>
              
                 
                   <div class="form-group">
                   <label style="text-align: center;"><span>City</span></label>
                     <select  id="txt_parkingspot_city" name="txt_parkingspot_city" class="form-control" >
                     <?php foreach($Cities as $item )
                            {
                                echo "<option value='".$item->cid."' >".$item->city."</option>";
                             } ?>
                        
                     </select>
                  </div>

                  <div class="form-group">
                     <label style="text-align: center;"><span>Account(Spot Authority)</span></label>

                     <select  id="txt_parkingspot_owner" name="txt_parkingspot_owner" class="form-control" >
                     <?php foreach($Providers as $item )
                            {
                                echo "<option value='".$item->id."' ><b>".$item->name.'</b> ( '.$item->email." )</option>";
                             } ?>
                        
                     </select>
                       <?php if($Providers==null){echo "<h5 class='text-danger'>No Account available ! Consider create some Account first.</h5>";}?>
                  </div>
        
               </div>
               <div class="footer text-center">
                  <input type="submit" class="btn btn-fill btn-neutral btn-wd" id="btn_parkingspot" name="btn_parkingspot" value="Add Location" />
               </div>
            </div>
            <?php echo form_close(); ?>
        
         </div>
      </div>
   </div>
</div>
</div>

 <style>
     * element that contains the map. */
      #map {
        height: 350px;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
        overflow:hidden;
      }
      #description {
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
      }

      #infowindow-content .title {
        font-weight: bold;
      }

      #infowindow-content {
        display: none;
      }

      #map #infowindow-content {
        display: inline;
      }

      .pac-card {
        margin: 10px 10px 0 0;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        background-color: #fff;
        font-family: Roboto;
      }

      #pac-container {
        padding-bottom: 12px;
        margin-right: 12px;
      }

      .pac-controls {
        display: inline-block;
        padding: 5px 11px;
      }

      .pac-controls label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }

      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 400px;
        margin-top: 10px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }

      #title {
        color: #fff;
        background-color: #4d90fe;
        font-size: 25px;
        font-weight: 500;
        padding: 6px 12px;
      }
    </style>


<script>
      // This example adds a search box to a map, using the Google Place Autocomplete
      // feature. People can enter geographical searches. The search box will return a
      // pick list containing a mix of places and predicted search terms.

      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

 
      function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: 50.064192, lng: -130.605469},
            gestureHandling: 'greedy',
       
          zoom: 3
        });
        var card = document.getElementById('pac-card');
        var input = document.getElementById('pac-input');
        var countries = document.getElementById('country-selector');

        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

        var autocomplete = new google.maps.places.Autocomplete(input);

        // Set initial restrict to the greater list of countries.
        autocomplete.setComponentRestrictions(
            {'country': ['us', 'pr', 'vi', 'gu', 'mp']});

        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
          map: map,

          anchorPoint: new google.maps.Point(0, -29)
        });

        autocomplete.addListener('place_changed', function() {
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
          }

          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
            
          var lat=place.geometry.location.lat();
              var long=place.geometry.location.lng();
             console.log("lat: " + lat);
              console.log("long: " + long);

              $('#txt_parkingspot_lat').val(lat);
              $('#txt_parkingspot_long').val(long);
          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(16);
              var lat=place.geometry.location.lat();
              var long=place.geometry.location.lng();
             console.log("lat: " + lat);
              console.log("long: " + long);

              $('#txt_parkingspot_lat').val(lat);
              $('#txt_parkingspot_long').val(long);  // Why 17? Because it looks good.
          }
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);

          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }

          infowindowContent.children['place-icon'].src = place.icon;
          infowindowContent.children['place-name'].textContent = place.name;
          infowindowContent.children['place-address'].textContent = address;
          infowindow.open(map, marker);
        });

        // Sets a listener on a given radio button. The radio buttons specify
        // the countries used to restrict the autocomplete search.
        function setupClickListener(id, countries) {
          var radioButton = document.getElementById(id);
          radioButton.addEventListener('click', function() {
            autocomplete.setComponentRestrictions({'country': countries});
          });
        }

        setupClickListener('changecountry-usa', 'us');
        setupClickListener(
            'changecountry-usa-and-uot', ['us', 'pr', 'vi', 'gu', 'mp']);
      }
    </script>
 
      <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDkkzyW9zJvzYJPe-5vrK_Ggko-Ha4NDzM&libraries=places&callback=initMap"
        async defer></script>
<?php echo $this->session->flashdata('notify'); ?>
<?php
   $this->load->view("module/footer");
   ?>